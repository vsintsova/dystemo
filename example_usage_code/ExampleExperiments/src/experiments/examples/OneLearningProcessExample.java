/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experiments.examples;

import experimentsavers.ExperimentsResultsSaver;
import learners.given.ClassifierLearnerFactory.LearnerName;
import learners.given.ClassifierLearnerForWeka.WekaCategoryClassifierLearnerParams;
import learners.given.ClassifierLearnerWeightedBalancedVoting;
import learning.experiments.OneLearningProcessHelper;
import learning.parameters.AllLearningParameters;
import learning.parameters.BinaryLearnerParameters.BinaryLearnerName;
import learning.parameters.BinaryLearnerParameters.ProbabilisticOutputTreatment;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.DefaultAllLearningParameters;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureSelectionParameters;
import learning.parameters.IndependentLearnerParameters;
import learning.parameters.IndependentLearnerParameters.CloseCategoryCalculation;
import learning.parameters.IndependentLearnerParameters.CloseCategoryUseType;
import learning.parameters.MultiLabelRefineParams;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams;
import learning.parameters.FeatureExtractionParameters.FeatureType;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import learning.parameters.FeatureSelectionParameters.SelectionType;
import learning.parameters.RebalancingParameters;
import learning.parameters.RebalancingParameters.BalanceWeightType;
import linguistic.TermDetectionParameters;
import linguistic.NegationDetector.NegationTreatmentParams;
import linguistic.NegationDetector.NegationTreatmentType;

public class OneLearningProcessExample {

	public static void runExample() throws Exception
	{
		ExperimentsResultsSaver.currentTestName = "OneLearningProcessExample";
		AllLearningParameters fullLearningParams = getExampleParameters(
				"ExampleTrainingData", 
				LearnerName.CategoryWekaMNB);
		String testDataName = "ExampleTestData";
		OneLearningProcessHelper.runOverParametersWithGivenTrainTest(fullLearningParams, testDataName);
	}
	
	/**
	 * Here, the learning is starting from a given list of features from the saved file (with the name 'example-feature-list' added in the GivenFeaturesIndex)
	 * @throws Exception
	 */
	public static void runExample2() throws Exception
	{
		ExperimentsResultsSaver.currentTestName = "OneLearningProcessExample";
		AllLearningParameters fullLearningParams = getExampleParameters(
				"ExampleTrainingData", 
				LearnerName.CategoryWekaMNB);
		
		fullLearningParams.featureExtractionParams.featureTypes = new FeatureType[1];
		fullLearningParams.featureExtractionParams.featureTypes[0] = FeatureType.GivenList;
		fullLearningParams.featureExtractionParams.givenFeatureListsNames = "example-feature-list"; // The name of the given ngrams list, should be present in GivenFeaturesIndex. To add the file with the feature list, run GivenFeaturesIndex.addNewFeatureList(String featureListName, String fileWithFeatureEnumeration)
		
		String testDataName = "ExampleTestData";
		OneLearningProcessHelper.runOverParametersWithGivenTrainTest(fullLearningParams, testDataName);
	}
	
	public static AllLearningParameters  getExampleParameters(
			String trainName, LearnerName learnerName) throws Exception
	{
		// set up all the parameter for learning run
		AllLearningParameters fullLearningParams = new AllLearningParameters();
		
		fullLearningParams.featureExtractionParams = new FeatureExtractionParameters();
		
		fullLearningParams.featureExtractionParams.useCache = true; // if true, will need to set up currentSetup parameters as well
		
		// if to extract ngrams
		fullLearningParams.featureExtractionParams.featureTypes = new FeatureType[1];
		fullLearningParams.featureExtractionParams.featureTypes[0] = FeatureType.Ngrams;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams = new DataNgramExtractionParams();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.maxN = 2;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.minTermOccurThreshold = 10;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.removeStopWords = true; // tried with false too - it looks like precision is higher
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection = new TermDetectionParameters();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.ignoreHashtags = true;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams = new NegationTreatmentParams();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams.lookBehindLength = 2;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams.negTreatmentType = NegationTreatmentType.ToReplace;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.applySevereNonOverlap = false;
		
		// if to use the given list
		/*
		fullLearningParams.featureExtractionParams.featureTypes = new FeatureType[1];
		fullLearningParams.featureExtractionParams.featureTypes[0] = FeatureType.GivenList;
		fullLearningParams.featureExtractionParams.givenFeatureListsNames = "some name, e.g. 'emojis'"; // The name of the given ngrams list, should be present in GivenFeaturesIndex. To add the file with the feature list, run GivenFeaturesIndex.addNewFeatureList(String featureListName, String fileWithFeatureEnumeration)
		*/
		
		fullLearningParams.featureSelectionParams = new FeatureSelectionParameters();
		fullLearningParams.featureSelectionParams.selectionTypesToApply = new SelectionType[1];
		fullLearningParams.featureSelectionParams.selectionTypesToApply[0] = SelectionType.OnPolarity;
		fullLearningParams.featureSelectionParams.polaritySelectionParams = new BinarySelectionParams();
		fullLearningParams.featureSelectionParams.polaritySelectionParams.selectionAlg = FeatureSelectionAlgOption.PMI;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.scoreThreshold = 0.1;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.minOccurNum = 5;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.applySmoothing = true;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.includeNeutral = false;
		
		
		fullLearningParams.learnerName = learnerName;
		switch (learnerName) {
			case WeightedBalancedVoting: {
				RebalancingParameters rebalancingParams = new RebalancingParameters(BalanceWeightType.LogPrior);
				
				fullLearningParams.learnerParams = new ClassifierLearnerWeightedBalancedVoting.WeightedBalancedVotingLearnerParams(rebalancingParams, false, false);
				fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null;
				fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams =  new MultiLabelRefineParams(0.9, 30);
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
				fullLearningParams.learnerParams.specificLearnerParams = null;
				
				break;
			}
			case CategoryWekaMNB: case CategoryWekaLogReg: {
				fullLearningParams.learnerParams = new WekaCategoryClassifierLearnerParams(fullLearningParams.learnerName);
				fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null; 
				fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams = new MultiLabelRefineParams(1.0, 30); 
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
				fullLearningParams.learnerParams.specificLearnerParams = null;
				fullLearningParams.learnerParams.rebalancingParameters = null;
				
				break;
			}
			
			case IndependentPMI: {
				IndependentLearnerParameters curParams = new IndependentLearnerParameters();
				curParams.closeCategoryUseType = CloseCategoryUseType.IncludeOut;
				curParams.closeCategoryCalculation =  CloseCategoryCalculation.Own;
				curParams.binaryLearnerParameters.binaryLearnerName = BinaryLearnerName.PMISimple;//PMI
				curParams.binaryLearnerParameters.binaryLexiconParams = "-S1"; // "-T0.1 - this should be an equivalent of feature selection option with PMI with the same threshold
				curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.No;
				fullLearningParams.learnerParams = curParams;
				fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null;
				fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams =  new MultiLabelRefineParams(0.9, 30);
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
				fullLearningParams.learnerParams.specificLearnerParams = null;
				fullLearningParams.learnerParams.rebalancingParameters = null;
				break;
			}
			
			case IndependentWekaLogReg: {
				IndependentLearnerParameters curParams = new IndependentLearnerParameters(fullLearningParams.learnerName);
			    curParams.closeCategoryUseType = CloseCategoryUseType.IncludeOut;
				curParams.closeCategoryCalculation =  CloseCategoryCalculation.Own;
				curParams.binaryLearnerParameters.binaryLexiconParams = ""; 
				curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.No;
				fullLearningParams.learnerParams = curParams;
				fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null;
				fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams =  new MultiLabelRefineParams(0.9, 30);
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
				fullLearningParams.learnerParams.outputClassifierApplicationParameters.probabilisticBinaryOutputTreatmentParams = new ProbabilisticOutputTreatment(true, 0.7);
				fullLearningParams.learnerParams.specificLearnerParams = null;
				fullLearningParams.learnerParams.rebalancingParameters = null;
				break;
			}
			
			case IndependentWekaMNB: {
				 IndependentLearnerParameters curParams = new IndependentLearnerParameters(fullLearningParams.learnerName);
				    curParams.closeCategoryUseType = CloseCategoryUseType.IncludeOut;
					curParams.closeCategoryCalculation =  CloseCategoryCalculation.Own;
					curParams.binaryLearnerParameters.binaryLexiconParams = ""; 
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.PMI;
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.applySmoothing = true;
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.minOccurNum = 5;
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.scoreThreshold = 0.1;
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.includeNeutral = false;
					fullLearningParams.learnerParams = curParams;
					fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null;
					fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
					fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams =  new MultiLabelRefineParams(1.0, 30);
					fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true);
					fullLearningParams.learnerParams.outputClassifierApplicationParameters.probabilisticBinaryOutputTreatmentParams = new ProbabilisticOutputTreatment(true, 0.5);
					fullLearningParams.learnerParams.specificLearnerParams = null;
					fullLearningParams.learnerParams.rebalancingParameters = null;
				break;
			}
			
			default:
				fullLearningParams.learnerParams = DefaultAllLearningParameters.getDefaultClassifierLearnerParamsForClassifier(fullLearningParams.learnerName);
				if (fullLearningParams.learnerParams == null)
					throw new Exception("The default parameters for learner " + learnerName + " are not set!");
				break;
		}
		
		// set data for training
		fullLearningParams.trainDataName = trainName;
		
		return fullLearningParams;
	
	}
}

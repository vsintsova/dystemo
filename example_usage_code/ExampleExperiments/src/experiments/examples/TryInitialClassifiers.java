/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experiments.examples;

import java.util.Collections;
import java.util.List;

import learning.parameters.ClassifierApplicationParameters;
import linguistic.TermDetectionParameters;
import linguistic.NegationDetector.NegationTreatmentParams;
import linguistic.NegationDetector.NegationTreatmentType;
import classification.definitions.WeightedClassifier;
import classifiers.initialization.UsedClassifierFactory;
import data.categories.CategoryUtil;
import data.categories.ICategoriesData;
import functionality.UtilCollections;

public class TryInitialClassifiers {
	
	public static void TryPMIHash() throws Exception {
		TryInitialClassifier("PMI-Hash");
	}
	
	public static void TryOlympLex() throws Exception {
		TryInitialClassifier("OlympLex-Prep");
	}
	
	public static void TryGALCWithPatterns() throws Exception {
		TryInitialClassifier("GALC");
	}

	public static void TryGALCInstantiated() throws Exception {
		TryInitialClassifier("GALC-R-Inst");
	}
	
	/**
	 * 
	 * @param classifierName Should be given in UsedClassifierFactory
	 * @throws Exception
	 */
	public static void TryInitialClassifier(String classifierName) throws Exception {
		
		WeightedClassifier classifier = UsedClassifierFactory.getClassifierByName(classifierName, 
				new ClassifierApplicationParameters(new TermDetectionParameters(new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2), true)));
		
		String[] texts = {
				"nothing happened", // 1
				"yeeaahh ! we are online", // 2
				"so cool <emot51>" , // 3
				"not happy today", // 4
				"we are going", // 5
				"not this again", // 6
				"very #happy ", // 7
				"#olympics : now a silver for the rowing men 's lightweight fours", // 8
				"watching olympic bmx #bmx #olympics", // 9
				"i love you", // 10
				"i hate you" // 11
		};
		
		ICategoriesData categoriesData = classifier.getCategoriesData();
		
		
		for (int i = 0; i < texts.length ; ++i) {
			String text = texts[i];
			double[] weights = classifier.findCategoryWeights(text);
			List<Integer> dominantCategories = classifier.getDominantCategories(weights);
			Collections.sort(dominantCategories);
			String classifierOutput = UtilCollections.join(dominantCategories, ",") + 
					" (" + UtilCollections.join(CategoryUtil.getCategoriesNamesFromIds(dominantCategories, categoriesData), ",") + ")";
			
			System.out.println(classifierName + " classified text '" + text + "' as " + classifierOutput );
		}
		
	}
}

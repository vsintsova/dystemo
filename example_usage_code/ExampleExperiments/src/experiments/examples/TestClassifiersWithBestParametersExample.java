/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experiments.examples;

import learners.given.ClassifierLearnerFactory.LearnerName;
import learning.experiments.TestClassifiersWithBestParametersHelper;
import experiments.helpers.BestResultsParamsLoader.SearchSettings;
import experiments.helpers.FindAllParametersVariations;

public class TestClassifiersWithBestParametersExample {
	
	public static void runExample_saveBestParametersIntoFile() throws Exception
	{
		LearnerName[] evaluatedLearners = new LearnerName[]{
				LearnerName.WeightedBalancedVoting,
				LearnerName.CategoryWekaMNB,
				LearnerName.CategoryWekaLogReg,
				LearnerName.IndependentWekaMNB,
				LearnerName.IndependentWekaLogReg,
				LearnerName.IndependentPMI
				
		};
		
		String[] evaluatedInitialClassifiers = new String[]{"GALC-R-Inst", "OlympLex-Prep", "PMI-Hash"};
		
		String[] usedTestNames = new String[]{"ExampleMultipleSSLearningExperiment"}; // the names of the used experiments
		
		String fileOutputForParams = "output/bestparams/bestParams_SSLExampleOnDev_opt=mF1.txt";
		
		SearchSettings[] criteriaForBestParamsSearch = new SearchSettings[]{SearchSettings.F1Micro};
		String queryConditionOnParams = " TestFileName = \"ExampleDevLabeledDomainData\" and " +
				" sslFullParams like \"%DT=ExampleCompositeU+NData;%\" ";
		
		TestClassifiersWithBestParametersHelper.saveBestParametersIntoFile(
				evaluatedLearners, evaluatedInitialClassifiers,
				usedTestNames, fileOutputForParams, 
				criteriaForBestParamsSearch,
				queryConditionOnParams);
	}
	
	public static void runExample_testClassifiersWithBestParametersOnBiggerSizeFromFile(boolean saveResultantClassifiers) throws Exception {
		// the file with best parameters should already be generated, e.g. by calling saveBestParametersIntoFile
		String bestParametersFilename = "output/bestparams/example-best-params.txt";
		String curExperimentName = "TestBestOnLargerDataExample";
		
		String[] testDataNames = new String[]{
				"ExampleTestData",
				"ExampleTestData2"};
		
		String[] differentUnlabeledData = new String[]{"ExampleCompositeU+NData_Large"}; // the list of unlabeled/composite data that will be used for learning ssl-classifiers. 
		int[] diffFeatureOccMins = new int[] {5}; // minimum numbers of feature (n-gram) occurrences to be included in the feature set.
		
		TestClassifiersWithBestParametersHelper.testClassifiersWithBestParametersOnBiggerSizeFromFile(
				bestParametersFilename, curExperimentName, testDataNames, 
				differentUnlabeledData, diffFeatureOccMins, 0, saveResultantClassifiers);
		
	}
	
	public static void runExample_getAllParametersInExperiments() throws Exception
	{
		String testFileName = "ExampleTestData";
		String[] usedTestNames = new String[]{"ExampleMultipleSSLearningExperiment"}; // the names of the used experiments
		
		FindAllParametersVariations.printAllResultsWithParametersVariations(
				usedTestNames, testFileName, "output/bestparams/all-dev-parameters-example-experiment.txt");
	}
}

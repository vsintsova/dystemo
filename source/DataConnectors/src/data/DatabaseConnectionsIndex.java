/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "DatabaseConnectionsIndex" )
public class DatabaseConnectionsIndex {
	
	@XmlRootElement( name = "DatabaseConnectionParameters" )
	public static class DatabaseConnectionParameters {
		String connectionName;
		String serverName;
		String username; 
		String password;
		String databaseName;
		
		@XmlElement
		public void setConnectionName(String connectionName) {
			this.connectionName = connectionName;
		}
		
		public String getConnectionName() {
			return connectionName;
		}
		
		@XmlElement
		public void setServerName(String serverName) {
			this.serverName = serverName;
		}
		
		public String getServerName() {
			return serverName;
		}
		
		@XmlElement
		public void setUsername(String username) {
			this.username = username;
		}
		
		public String getUsername() {
			return username;
		}
		
		@XmlElement
		public void setPassword(String password) {
			this.password = password;
		}
		
		public String getPassword() {
			return password;
		}
		
		@XmlElement
		public void setDatabaseName(String databaseName) {
			this.databaseName = databaseName;
		}
		
		public String getDatabaseName() {
			return databaseName;
		}

	}
	
	@XmlElement(name = "DatabaseConnectionParameters")
	public void setConnectionsList( List<DatabaseConnectionParameters> connectionsList )
	{
		this.connectionsList = connectionsList;
	}
	
	public List<DatabaseConnectionParameters> getConnectionsList()
	{
		return connectionsList;
	}
	
	private void updateConnectionsMap() {
		this.connectionsMap = new HashMap<String, DatabaseConnectionParameters>();
		for (DatabaseConnectionParameters connParam : connectionsList) {
			this.connectionsMap.put(connParam.connectionName, connParam);
		}
	}

	private List<DatabaseConnectionParameters> connectionsList;

	/** Map of connection names to the corresponding database parameters for connection. **/
	private Map<String, DatabaseConnectionParameters> connectionsMap;
	
	/**
	 * Initialize an empty index
	 */
	public DatabaseConnectionsIndex() {
		this.connectionsList = new ArrayList<DatabaseConnectionParameters>();
		this.connectionsMap = new HashMap<String, DatabaseConnectionParameters>();
	}
	
	public DatabaseConnectionsIndex(List<DatabaseConnectionParameters> connectionsList) {
		this.connectionsList = connectionsList;
		updateConnectionsMap();
	}
	
	private String getConnectionLine(DatabaseConnectionParameters connParams) {
		return "jdbc:mysql://" + connParams.serverName + "/" + connParams.databaseName + "?" +
				"user="+ connParams.username +"&password=" + connParams.password + 
				"&useUnicode=true&characterEncoding=UTF-8";
	}
	
	/**
	 * Set up the mysql connection to the provided database using the stored parameters.
	 * @param connectionName
	 * @return
	 * @throws Exception
	 */
	public Connection getSpecificConnection(String connectionName) throws Exception
	{
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		DatabaseConnectionParameters connParams = connectionsMap.get(connectionName);
		if (connParams == null) {
			throw new Exception("The connection '" + connectionName +"' was not found in the index.");
		}
	    Connection conn = DriverManager.getConnection(
	    		getConnectionLine(connParams));
	    return conn;
	}
	
	public void addConnectionParameters(DatabaseConnectionParameters connParams) {
		connectionsMap.put(connParams.connectionName, connParams);
		connectionsList.add(connParams);
	}
	
	/**
	 * 
	 * @param filePath Should ideally have .xml extension
	 */
	public void saveConnectionParametersToFile(String filePath) {
		JAXB.marshal(this, new File(filePath));
	}
	
	/**
	 * Load available connections from the XML file.
	 * An example XML file can be found in DataConnectors/examples/database-connections-index-example.xml
	 * @param connectionsIndexFile
	 */
	static public DatabaseConnectionsIndex loadFromXMLFile(String connectionsIndexFile) {
		DatabaseConnectionsIndex res = JAXB.unmarshal(new File(connectionsIndexFile), 
				DatabaseConnectionsIndex.class);
		res.updateConnectionsMap();
		return res;
	}
	
	/**
	 * Save the given databaseConnectionsIndex into the XML file
	 * @param connectionsIndex
	 * @param filePath
	 */
	static public void saveConnectionParametersToFile(DatabaseConnectionsIndex connectionsIndex, String filePath) {
		connectionsIndex.saveConnectionParametersToFile(filePath);
	}
	
	private static DatabaseConnectionsIndex usedDatabaseConnectionsIndex = null;
	
	static public void setDefaultDatabaseConnectionsIndex(DatabaseConnectionsIndex connectionsIndex) {
		usedDatabaseConnectionsIndex = connectionsIndex;
	}
	
	/**
	 * Set up the mysql connection to the provided database using the stored parameters.
	 * @param connectionName
	 * @return
	 * @throws Exception
	 */
	public static Connection getConnection(String connectionName) throws Exception
	{
		if (usedDatabaseConnectionsIndex != null)
			return usedDatabaseConnectionsIndex.getSpecificConnection(connectionName);
		else
			throw new Exception("The default database connection index is not set!");
	}
}

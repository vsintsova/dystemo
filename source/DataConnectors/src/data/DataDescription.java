/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "DataDescription" )
public class DataDescription {
	
	public enum DataSourceType {Database, TextFile, Merged};
	
	/**
	 * The type of the data:
	 * Labeled: to each tweet corresponds the set of categorical labels (LabeledData representation)
	 * TextOnly: each tweet has only text information (TweetData representation)
	 * Composite: contains several types of the tweets, e.g. unlabeled (text-only) + pseudo-neutral (CompositeData representation). For the data of Composite type, the source type has to be Merged
	 */
	public enum DataType {Labeled, TextOnly, Composite};
	
	public String dataName;
	public DataSourceType sourceType;
	public DataType dataType;
	
	// If sourceType is Database: parameters
	@XmlElement(name = "DatabaseDataDetails")
	public DatabaseDataDetails databaseDataDetails;
	
	// If sourceType is TextFile: parameters
	public String textFileName;
	
	// If sourceType is Merged: parameters
	@XmlElement(name = "MergedDataDetails")
	public MergedDataDetails mergedDataDetails;
	
	@XmlRootElement( name = "DatabaseDataDetails" )
	public static class DatabaseDataDetails {
		public String connectionName;
		public String tableName;
		public String condition;
		
		public String idColumnName;
		public String textColumnName;
		public String categoryColumnName; // for labeled data type
		public Integer sizeLimit; // if specific size of dataset is to be loaded. If not set or 0 - all available data in the table will be loaded.
		public String sizeColumnName; // the column where the dataset size attribution is specified (usually "tSize", assigned randomly to the entries to be used.)
	}
	
	@XmlRootElement( name = "MergedDataDetails" )
	public static class MergedDataDetails {
		
		@XmlRootElement( name = "CompositeDataDetails" )
		public static class CompositeDataDetails {
			@XmlAttribute
			String name;
			
			/**
			 * Can be 'unlabeled' or 'pseudo-neutral'
			 */
			@XmlAttribute
			String compositeType;
		}
		
		@XmlElement(name = "DataToMerge")
		List<CompositeDataDetails> dataToMerge;
	}
	
}

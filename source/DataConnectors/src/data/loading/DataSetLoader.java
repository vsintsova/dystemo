/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.loading;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import data.DataDescription.DatabaseDataDetails;
import data.DatabaseWrapper;
import data.LabeledData;
import data.TweetData;
import data.documents.Tweet;

public class DataSetLoader {
	
	/**
	 * Reads LabeledData format from the text file.
	 * 
	 * @param textFile
	 * @return
	 * @throws IOException
	 */
	public static LabeledData getLabeledDataFromTextFile(String textFile) throws Exception
	{
		Map<Integer, List<Integer>> emotionLabels = new HashMap<Integer, List<Integer>>();
		Map<Integer, String> textData = new LinkedHashMap<Integer, String> ();
		DataFilesFormatReader.readLabeledDataFromFile(textFile, emotionLabels, textData);

		return new LabeledData(emotionLabels, textData);
	}
	
	/**
	 * Reads TweetData format from the text file.
	 * 
	 * @param textFile
	 * @return
	 * @throws IOException
	 */
	public static TweetData getTweetDataFromTextFile(String textFile) throws Exception
	{
		List<Tweet> tweets = new ArrayList<Tweet>();
		DataFilesFormatReader.readTextDataFromFile(textFile, tweets);

		return new TweetData(tweets);
	}
	
	private static String tmpSavedConnectionName = null;
	
	public static LabeledData getLabeledDataFromDatabase(DatabaseDataDetails databaseDetails) throws Exception {
		if (!databaseDetails.connectionName.equals(DatabaseWrapper.getCurrentConnectionName())) {
			tmpSavedConnectionName = DatabaseWrapper.getCurrentConnectionName();
			DatabaseWrapper.setCurrentConnection(databaseDetails.connectionName);
		}
		
		Map<Integer, List<Integer>> emotionLabels = new HashMap<Integer, List<Integer>>();
		Map<Integer, String> textData = new HashMap<Integer, String> ();
		
		if (databaseDetails.sizeLimit != null && databaseDetails.sizeLimit > 0)
			DatabaseWrapper.readTweetCategoryDataFromTableWithSize(databaseDetails.tableName, databaseDetails.idColumnName, 
					databaseDetails.textColumnName, databaseDetails.categoryColumnName, databaseDetails.condition, 
					databaseDetails.sizeLimit, databaseDetails.sizeColumnName, emotionLabels, textData);
		else
			DatabaseWrapper.readTweetCategoryData(databaseDetails.tableName, databaseDetails.idColumnName, 
					databaseDetails.textColumnName, databaseDetails.categoryColumnName, databaseDetails.condition, 
					emotionLabels, textData);
		
		if (tmpSavedConnectionName != null) // return to the previous default connection
		{
			DatabaseWrapper.setCurrentConnection(tmpSavedConnectionName);
			tmpSavedConnectionName = null;
		}
		
		return new LabeledData(emotionLabels, textData);
	}
	
	public static TweetData getTweetDataFromDatabase(DatabaseDataDetails databaseDetails) throws Exception {
		if (!databaseDetails.connectionName.equals(DatabaseWrapper.getCurrentConnectionName())) {
			tmpSavedConnectionName = DatabaseWrapper.getCurrentConnectionName();
			DatabaseWrapper.setCurrentConnection(databaseDetails.connectionName);
		}
		
		List<Tweet> tweets;
		
		if (databaseDetails.sizeLimit != null && databaseDetails.sizeLimit > 0)
			tweets = DatabaseWrapper.getTweetsDataFromTableWithSize(databaseDetails.tableName, databaseDetails.idColumnName, 
					databaseDetails.textColumnName, databaseDetails.condition, 
					databaseDetails.sizeLimit, databaseDetails.sizeColumnName);
		else
			tweets = DatabaseWrapper.getTweetsFromDatabaseWithCondition(
					databaseDetails.tableName, databaseDetails.condition,
					databaseDetails.idColumnName, databaseDetails.textColumnName);
		
		if (tmpSavedConnectionName != null) // return to the previous default connection
		{
			DatabaseWrapper.setCurrentConnection(tmpSavedConnectionName);
			tmpSavedConnectionName = null;
		}
		
		return new TweetData(tweets);
	}
	
}

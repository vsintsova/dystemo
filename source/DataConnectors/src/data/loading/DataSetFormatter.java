/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.loading;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import data.DataRepositoryIndex;
import data.LabeledData;
import data.TweetData;
import data.categories.CategoryUtil;
import data.categories.CategoryProcessor.CategoryDataType;
import data.documents.Tweet;

public class DataSetFormatter {

	public static List<Tweet> getTweetsFromStrings(Map<Integer,String> tweetData)
	{
		List<Tweet> resTweets = new ArrayList<Tweet>();
		for (Map.Entry<Integer, String> tweetEntry : tweetData.entrySet())
		{
			Tweet tweet = new Tweet();
			tweet.tweetId = tweetEntry.getKey();
			tweet.text = tweetEntry.getValue();
			resTweets.add(tweet);
		}
		return resTweets;
	}
	
	public static Set<Integer> getTweetIds(Collection<? extends Tweet> tweets)
	{
		Set<Integer> tweetIds = new HashSet<Integer>();
		for (Tweet tw : tweets)
			tweetIds.add(tw.tweetId);
		return tweetIds;
	}
	
	public static Map<Integer,String> getStringFromTweets(List<? extends Tweet> tweetData)
	{
		Map<Integer,String> resTextData = new HashMap<Integer, String>();
		for (Tweet tweet : tweetData)
		{
			resTextData.put(tweet.tweetId, tweet.text);
		}
		return resTextData;
	}
	
	public static Map<Integer, double[]> extractEmotionWeightsFromLabeledDataForTweetsSubset(
			LabeledData allLabeledData, Set<Integer> tweetsToInclude, CategoryDataType catDataType) {
		Map<Integer, double[]> selectedTweetsEmotions = new HashMap<Integer, double[]>();
		for (Integer tweet : tweetsToInclude) {
			if (allLabeledData.emotionLabels.containsKey(tweet)) {
				selectedTweetsEmotions.put(tweet, 
						CategoryUtil.transformCategoryListToWeights(allLabeledData.emotionLabels.get(tweet), catDataType));
			}
		}
		return selectedTweetsEmotions;
	}
	
	public static Map<Integer, double[]> extractEmotionWeightsFromLabeledDataForAllTweets(
			LabeledData allLabeledData, CategoryDataType catDataType) {
		Map<Integer, double[]> tweetsEmotions = new HashMap<Integer, double[]>();
		for (Integer tweet : allLabeledData.emotionLabels.keySet()) {
			tweetsEmotions.put(tweet, 
						CategoryUtil.transformCategoryListToWeights(allLabeledData.emotionLabels.get(tweet), catDataType));
		}
		return tweetsEmotions;
	}
	
	
	public static LabeledData transformTweetDataIntoLabeledByAssigningCategory(TweetData tweetData, int categoryToAssign) {
		List<? extends Tweet> tweets = tweetData.tweets;
		
		Map<Integer, List<Integer>> emotionLabels = new HashMap<Integer, List<Integer>>();
		Map<Integer, String> textData = new HashMap<Integer,String>();
	
		List<Integer> fixedAssignment = new ArrayList<Integer>();
		fixedAssignment.add(categoryToAssign);
		for ( Tweet tweet : tweets)
		{
			textData.put(tweet.tweetId, tweet.text);
			emotionLabels.put(tweet.tweetId, new ArrayList<Integer>(fixedAssignment));
		}
		
		return new LabeledData(emotionLabels, textData);
	}
	
	public static LabeledData getTweetsAsLabeledWithSpecificCategory (String unlabeledDataName, int assignedCategoryId) throws Exception
	{
		TweetData tweetData = DataRepositoryIndex.getTweetDataByName(unlabeledDataName);
		return DataSetFormatter.transformTweetDataIntoLabeledByAssigningCategory(tweetData, assignedCategoryId);
	}
}

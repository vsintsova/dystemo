/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.start;

import learners.given.ClassifierLearnerFactory.LearnerName;
import learners.given.ClassifierLearnerWeightedVoting.WeightedVotingLearnerParams;
import learning.parameters.AllLearningParameters;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.MultiLabelRefineParams;
import learning.parameters.FeatureExtractionParameters.DataNgramExtractionParams;
import learning.parameters.FeatureSelectionParameters;
import learning.parameters.FeatureExtractionParameters.FeatureType;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import learning.parameters.FeatureSelectionParameters.SelectionType;
import linguistic.NegationDetector.NegationTreatmentParams;
import linguistic.NegationDetector.NegationTreatmentType;
import linguistic.TermDetectionParameters;

public class StartLearningProcess {

	public static void run()
	{
		AllLearningParameters fullLearningParams = new AllLearningParameters();
		
		fullLearningParams.featureExtractionParams = new FeatureExtractionParameters();
		
		fullLearningParams.featureExtractionParams.useCache = true;
		fullLearningParams.featureExtractionParams.featureTypes = new FeatureType[1];
		fullLearningParams.featureExtractionParams.featureTypes[0] = FeatureType.Ngrams;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams = new DataNgramExtractionParams();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.maxN = 2;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.minTermOccurThreshold = 5;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.removeStopWords = true;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection = new TermDetectionParameters();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.ignoreHashtags = true;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams = new NegationTreatmentParams();
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams.lookBehindLength = 3;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.parametersOnNgramsDetection.treatNegationsParams.negTreatmentType = NegationTreatmentType.ToRemove;
		fullLearningParams.featureExtractionParams.dataNgramExtractionParams.applySevereNonOverlap = false;
		
		fullLearningParams.featureSelectionParams = new FeatureSelectionParameters();
		fullLearningParams.featureSelectionParams.selectionTypesToApply = new SelectionType[1];
		fullLearningParams.featureSelectionParams.selectionTypesToApply[0] = SelectionType.OnPolarity;
		fullLearningParams.featureSelectionParams.polaritySelectionParams = new BinarySelectionParams();
		fullLearningParams.featureSelectionParams.polaritySelectionParams.selectionAlg = FeatureSelectionAlgOption.PMI;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.scoreThreshold = 0.1;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.minOccurNum = 5;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.applySmoothing = true;
		fullLearningParams.featureSelectionParams.polaritySelectionParams.includeNeutral = false;
		
		fullLearningParams.learnerName = LearnerName.WeightedBalancedVoting;
		
		fullLearningParams.learnerParams = new WeightedVotingLearnerParams(false, false);
		fullLearningParams.learnerParams.multiLabelInitialRefinementParams = null;
		fullLearningParams.learnerParams.outputClassifierApplicationParameters = new ClassifierApplicationParameters();
		fullLearningParams.learnerParams.outputClassifierApplicationParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(true, true);
		fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams =  new MultiLabelRefineParams(0.9, 30);
		fullLearningParams.learnerParams.rebalancingParameters = null;
		fullLearningParams.learnerParams.specificLearnerParams = null;
		
		System.out.println("Parameters set: " + fullLearningParams.printParametersShort());
		
		
	}
}

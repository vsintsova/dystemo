package learning.parameters;

import classification.loaders.WekaClassifierFactory;
import learners.given.ClassifierLearnerFactory.LearnerName;
import learning.parameters.BinaryLearnerParameters.BinaryLearnerName;
import learning.parameters.BinaryLearnerParameters.BinaryLearnerParametersEnumerationList;
import utility.Pair;

/*
Copyright 2016 Valentina Sintsova

This file is part of Dystemo.

Dystemo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or 
(at your option) any later version.

Dystemo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

public class IndependentLearnerParameters extends ClassifierLearnerParams implements IParameters {
	/**
	 * 
	 * IncludeOut - the standard break: 1 for the objects with category, 0 for all others,
	 * IncludeIn - 1 for the objects with the category and the similar categories, 0 for all others,
	 * Exclude - 1 for the objects with the category, nothing (not included in the dataset) for the objects similar to the category, 0 for all others
	 */
	public enum CloseCategoryUseType {IncludeOut, IncludeIn, Exclude};
	public enum CloseCategoryCalculation {Similarity, Quadrant, Polarity, Own};// Own means the category will be similar only to itself //, Complex
	
	public boolean useCacheForIndependentRepresentation;

	public CloseCategoryUseType closeCategoryUseType;
	public CloseCategoryCalculation closeCategoryCalculation;
	
	public BinaryLearnerParameters binaryLearnerParameters;
	
	public boolean useDependentClassifierForOutput; // If false - will use the corresponding independent classifier, while if true - will generate the classifier that looks at the data altogether and selects the maximal weights.
	
	public IndependentLearnerParameters()
	{
		binaryLearnerParameters = new BinaryLearnerParameters();
	}
	
	public IndependentLearnerParameters(LearnerName curLearnerName)
	{
		binaryLearnerParameters = new BinaryLearnerParameters();
		
		String learnerName = curLearnerName.name();
		if (!learnerName.startsWith("Independent"))
		{
			System.out.print("Incorrect parameter setting for " + curLearnerName);
		}
		else
		{
			String binaryClassifierType = learnerName.substring("Independent".length());
			binaryLearnerParameters.binaryLearnerName = BinaryLearnerName.valueOf(binaryClassifierType);
		}
	}
	
	public ClassifierLearnerParams clone()
	{
		IndependentLearnerParameters newLearnerParams = new IndependentLearnerParameters();
		newLearnerParams.binaryLearnerParameters = this.binaryLearnerParameters;
		newLearnerParams.closeCategoryUseType = this.closeCategoryUseType;
		newLearnerParams.closeCategoryCalculation = this.closeCategoryCalculation;
		newLearnerParams.useDependentClassifierForOutput = this.useDependentClassifierForOutput;
		newLearnerParams.copyDataFromAnother(this);
		return newLearnerParams;
	}

	@Override
	public String printParametersShort()
	{
		String original = super.printParametersShort();
		StringBuilder sb = new StringBuilder();
		sb.append(
				binaryLearnerParameters.printParametersShort()
				);
		sb.append("-simUse-" + closeCategoryUseType);
		if (closeCategoryUseType != CloseCategoryUseType.IncludeOut)
			sb.append("-simType-" + closeCategoryCalculation);
		if (useDependentClassifierForOutput) {
			sb.append("-useDep");
		}
		
		return original + "," + sb.toString();
	}
	
	public static class IndependentLearnerParamsEnumerationList extends ClassifierLearnerParamsEnumerationList implements IParametersEnumerationList
	{
		public Pair<CloseCategoryUseType, CloseCategoryCalculation>[] closeCategoryTreatmentOptions;
		public BinaryLearnerParametersEnumerationList  binaryLearnerParametersEnumerationList;
		
		public boolean useCacheForIndependentRepresentation;
		public boolean useDependentClassifierForOutput; // only one option will be available.
		
		public IndependentLearnerParamsEnumerationList()
		{};
		
		public IndependentLearnerParamsEnumerationList(ClassifierLearnerParamsEnumerationList anotherList)
		{
			this.initialMultiLabelOptions = anotherList.initialMultiLabelOptions;
			this.extraParameters =  anotherList.extraParameters;
			this.rebalancingOptions =  anotherList.rebalancingOptions;
		};
		
		public IndependentLearnerParamsEnumerationList(IndependentLearnerParamsEnumerationList anotherList)
		{
			this((ClassifierLearnerParamsEnumerationList)anotherList);
			this.closeCategoryTreatmentOptions = anotherList.closeCategoryTreatmentOptions;
			this.binaryLearnerParametersEnumerationList = new BinaryLearnerParametersEnumerationList(anotherList.binaryLearnerParametersEnumerationList);
			this.useCacheForIndependentRepresentation = anotherList.useCacheForIndependentRepresentation;
		}
	}
}

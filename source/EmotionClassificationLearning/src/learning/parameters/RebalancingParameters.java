/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.parameters;


public class RebalancingParameters implements IParameters {

	/**
	 * 
	 */
	public enum BalanceWeightType {Prior, TermNum, AdaptedPrior, LogPrior, NormalizedTermNum, WordNum};
	
	public BalanceWeightType weightType;
	public double paramAdaptedBalanceRatio; // used only for the AdaptedPrior option
	
	public RebalancingParameters(BalanceWeightType weightType)
	{
		this.weightType = weightType;
	}
	
	@Override
	public String printParametersShort() {
		String res = "-blcT-"+weightType.ordinal();
		if (weightType == BalanceWeightType.AdaptedPrior)
			res += "-bWR-" + paramAdaptedBalanceRatio;
		return res;
	}

	public String printParamsLong()
	{
		String res = "BalanceType-" + weightType;
		if (weightType == BalanceWeightType.AdaptedPrior)
			res += "-BalanceWeightRatio-" + paramAdaptedBalanceRatio;
		return res;
	}
}

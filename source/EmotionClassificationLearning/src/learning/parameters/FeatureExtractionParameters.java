/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.parameters;

import linguistic.TermDetectionParameters;

public class FeatureExtractionParameters implements IParameters {
	
	public boolean useCache;
	public DataNgramExtractionParams dataNgramExtractionParams;
	public FeatureType[] featureTypes;
	
	public String givenFeatureListsNames; // this is used only to store the parameters of the run, hard to rewrite to process it properly because the lists should not be stored in learning module
	
	public boolean useGivenIndicatorsAlongWithDiscovered;
	
	public FeatureExtractionParameters clone()
	{
		FeatureExtractionParameters newParams = new FeatureExtractionParameters();
		newParams.useCache = this.useCache;
		newParams.givenFeatureListsNames = this.givenFeatureListsNames;
		newParams.featureTypes = this.featureTypes;
		newParams.dataNgramExtractionParams = new DataNgramExtractionParams(this.dataNgramExtractionParams);
		return newParams;
	}
	
	public static class DataNgramExtractionParams  implements IParameters 
	{
		public int maxN;
		public int minTermOccurThreshold;
		public boolean applySevereNonOverlap; // Not used. Always false.
		public boolean removeStopWords;//TODO: fix that it is not used yet
		public TermDetectionParameters parametersOnNgramsDetection;
		
		public DataNgramExtractionParams() {};
		public DataNgramExtractionParams(DataNgramExtractionParams initParams)
		{
			this.maxN = initParams.maxN;
			this.minTermOccurThreshold = initParams.minTermOccurThreshold;
			this.applySevereNonOverlap = initParams.applySevereNonOverlap;
			this.removeStopWords = initParams.removeStopWords;
			this.parametersOnNgramsDetection = initParams.parametersOnNgramsDetection;
		};
		
		@Override
		public String printParametersShort() {
			StringBuilder sb = new StringBuilder();
			sb.append("-maxN-");
			sb.append(maxN);
			sb.append("-minO-");
			sb.append(minTermOccurThreshold);
			if (applySevereNonOverlap)
			{
				sb.append("-sevNonOv-");
				sb.append(applySevereNonOverlap?1:0);
			}
			sb.append("-noSt-");
			sb.append(removeStopWords?1:0);
			
			sb.append(parametersOnNgramsDetection.printParametersShort());
			
			return sb.toString();
		}
		
		public abstract static class FeatureExtractionParamsEnumerationList implements IParametersEnumerationList
		{
			
		}
		
		public static class GivenFeaturesExtractionParamsEnumerationList extends FeatureExtractionParamsEnumerationList implements IParametersEnumerationList
		{
			public String[] givenFeatureNamesList;
		}
		
		public static class DataNgramExtractionParamsEnumerationList extends FeatureExtractionParamsEnumerationList implements IParametersEnumerationList
		{
			public int[] diffTermOccNumList;
			public int[] diffMaxNgramLengthList;
			public ExtraParamCombinationForNgramExtraction[] extraParamCombinations;
			
			public  DataNgramExtractionParamsEnumerationList (int[] diffTermOccNumList,
															int[] diffMaxNgramLengthList,
															ExtraParamCombinationForNgramExtraction[] extraParamCombinations)	
			{
				this.diffTermOccNumList = diffTermOccNumList;
				this.diffMaxNgramLengthList = diffMaxNgramLengthList;
				this.extraParamCombinations = extraParamCombinations;
			}
			
			public static class ExtraParamCombinationForNgramExtraction
			{
				public boolean applySevereNonOverlap;
				public boolean removeStopWords;
				public TermDetectionParameters termDetectionParameters;
				
				public  ExtraParamCombinationForNgramExtraction ( boolean removeStopWords, TermDetectionParameters termDetectionParameters,  boolean applySevereNonOverlap)
				{
					this.applySevereNonOverlap = applySevereNonOverlap;
					this.termDetectionParameters = termDetectionParameters;
					this.removeStopWords = removeStopWords;
				}
			}
		}
	}
	
	public enum FeatureType {Ngrams, GivenList}

	@Override
	public String printParametersShort() {
		StringBuilder sb = new StringBuilder();
		if (featureTypes == null || featureTypes.length == 0)
			sb.append("None");
		else
		{
			for (FeatureType featType : featureTypes)
			{
				sb.append(featType.name());
				sb.append(":");
				if (featType == FeatureType.Ngrams)
				{
					sb.append(dataNgramExtractionParams.printParametersShort());
				}
				else if (featType == FeatureType.GivenList)
				{
					sb.append("names-"+givenFeatureListsNames);
				}
			}
		}
		
		
		return sb.toString();
	};
	
	
	
	
}

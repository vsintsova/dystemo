/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.parameters;

import java.util.HashMap;
import java.util.Map;

import classification.loaders.WekaClassifierFactory.AvailableWekaClassifiers;

import learners.given.ClassifierLearnerWeightedBalancedVoting.WeightedBalancedVotingLearnerParams;
import learners.given.ClassifierLearnerWeightedVoting.WeightedVotingLearnerParams;
import learners.given.ClassifierLearnerFactory.LearnerName;
import learners.given.ClassifierLearnerForWeka.WekaCategoryClassifierLearnerParams;
import learning.parameters.BinaryLearnerParameters.BinaryLearnerName;
import learning.parameters.IndependentLearnerParameters.CloseCategoryCalculation;
import learning.parameters.IndependentLearnerParameters.CloseCategoryUseType;
import learning.parameters.RebalancingParameters.BalanceWeightType;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import linguistic.TermDetectionParameters;

public class DefaultAllLearningParameters {
	
	public static ClassifierLearnerParams getDefaultClassifierLearnerParamsForClassifier(LearnerName learnerName)
	{
		return defaultParametersSaved.get(learnerName);
	}
	
	static Map<LearnerName, ClassifierLearnerParams> defaultParametersSaved;
	
	static
	{
		//initialize default parameters
		 defaultParametersSaved = new HashMap<LearnerName, ClassifierLearnerParams>();
		/* 
		 ClassifierLearnerParams params = new ClassifierLearnerParams();
		 
		 params.multiLabelInitialRefinementParams = null;
		 params.multiLabelOutputRefinementParams = new MultiLabelRefineParams(0.9, 30); //30 for not taking into account the emotion limit (30 > 22 categories)
		 params.rebalancingParameters = null; 
		 //RebalancingParameters defRebalancingParameters = new RebalancingParameters(BalanceWeightType.TermNum);
		 params.specificLearnerParams = null;
		 */
		 
		 // GENERAL
		 MultiLabelRefineParams defMultiLabelInitialRefinementParams = null;
		 ClassifierApplicationParameters defOutputClassifierParameters = new ClassifierApplicationParameters();
		 defOutputClassifierParameters.multiLabelOutputRefinementParams = new MultiLabelRefineParams(0.9, 30); //30 for not taking into account the emotion limit (30 > 22 categories)
		 defOutputClassifierParameters.parametersOfTermDetectionForClassifier = new TermDetectionParameters(true, true);
		 RebalancingParameters defRebalancingParameters = null; 
		 //RebalancingParameters defRebalancingParameters = new RebalancingParameters(BalanceWeightType.TermNum);
		
		 // INDEPENDENT
		 CloseCategoryUseType defCloseCategoryUseType = CloseCategoryUseType.IncludeOut;
		 CloseCategoryCalculation defCloseCategoryCalculation = CloseCategoryCalculation.Own;
			
		 
		
		 for (LearnerName learnerName : LearnerName.values())
		 {
			 ClassifierLearnerParams params;
			 switch (learnerName)
			 {
			 	case None: params = null; break;
				case WeightedVoting: 
				{
					params = new WeightedVotingLearnerParams(false, false);
					params.multiLabelInitialRefinementParams = defMultiLabelInitialRefinementParams;
					params.outputClassifierApplicationParameters = defOutputClassifierParameters; 
					params.rebalancingParameters = defRebalancingParameters; 
					params.specificLearnerParams = null;
					break;
				}
				case WeightedBalancedVoting: 
				{
					RebalancingParameters defRebalancingParams = new RebalancingParameters(BalanceWeightType.TermNum);
					params = new WeightedBalancedVotingLearnerParams(defRebalancingParams, false, false);
					params.multiLabelInitialRefinementParams = defMultiLabelInitialRefinementParams;
					params.outputClassifierApplicationParameters = defOutputClassifierParameters; 
					params.specificLearnerParams = null;
					break;
				}
				case IndependentPMI:
				{
					IndependentLearnerParameters curParams = new IndependentLearnerParameters(learnerName);
					curParams.closeCategoryUseType = defCloseCategoryUseType;
					curParams.closeCategoryCalculation = defCloseCategoryCalculation;
					curParams.binaryLearnerParameters.binaryLexiconParams = "-S1"; // "-T0.1 - this should be an equivalent of feature selection option with PMI with the same threshold
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.No;
					params = curParams;
					params.multiLabelInitialRefinementParams = defMultiLabelInitialRefinementParams;
					params.outputClassifierApplicationParameters = defOutputClassifierParameters; 
					params.rebalancingParameters = defRebalancingParameters; 
					params.specificLearnerParams = null;
					break;
				}
				case IndependentWekaMNB:
				{
					IndependentLearnerParameters curParams = new IndependentLearnerParameters(learnerName);
					curParams.closeCategoryUseType = defCloseCategoryUseType;
					curParams.closeCategoryCalculation = defCloseCategoryCalculation;
					curParams.binaryLearnerParameters.binaryLexiconParams = ""; 
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.No;
					params = curParams;
					params.multiLabelInitialRefinementParams = defMultiLabelInitialRefinementParams;
					params.outputClassifierApplicationParameters = defOutputClassifierParameters; 
					params.rebalancingParameters = defRebalancingParameters; 
					params.specificLearnerParams = null;
					break;
				}
				case IndependentWekaLogReg:
				{
					IndependentLearnerParameters curParams = new IndependentLearnerParameters(learnerName);
					curParams.closeCategoryUseType = defCloseCategoryUseType;
					curParams.closeCategoryCalculation = defCloseCategoryCalculation;
					curParams.binaryLearnerParameters.binaryLexiconParams = ""; 
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.No;
					params = curParams;
					params.multiLabelInitialRefinementParams = defMultiLabelInitialRefinementParams;
					params.outputClassifierApplicationParameters = defOutputClassifierParameters; 
					params.rebalancingParameters = defRebalancingParameters; 
					params.specificLearnerParams = null;
					break;
				}
				case IndependentWekaSVM:
				{
					IndependentLearnerParameters curParams = new IndependentLearnerParameters(learnerName);
					curParams.closeCategoryUseType = defCloseCategoryUseType;
					curParams.closeCategoryCalculation = defCloseCategoryCalculation;
					curParams.binaryLearnerParameters.binaryLexiconParams = ""; 
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.No;
					params = curParams;
					params.multiLabelInitialRefinementParams = defMultiLabelInitialRefinementParams;
					params.outputClassifierApplicationParameters = defOutputClassifierParameters; 
					params.rebalancingParameters = defRebalancingParameters; 
					params.specificLearnerParams = null;
					break;
				}
				case CategoryWekaLogReg:
				{
					WekaCategoryClassifierLearnerParams curParams = new WekaCategoryClassifierLearnerParams(learnerName);
					params = curParams;
					params.multiLabelInitialRefinementParams = defMultiLabelInitialRefinementParams;
					params.outputClassifierApplicationParameters = defOutputClassifierParameters; 
					params.rebalancingParameters = defRebalancingParameters; 
					params.specificLearnerParams = null;
					break;
				}
				case CategoryWekaMNB:
				{
					WekaCategoryClassifierLearnerParams curParams = new WekaCategoryClassifierLearnerParams(learnerName);
					params = curParams;
					params.multiLabelInitialRefinementParams = defMultiLabelInitialRefinementParams;
					params.outputClassifierApplicationParameters = defOutputClassifierParameters; 
					params.rebalancingParameters = defRebalancingParameters; 
					params.specificLearnerParams = null;
					break;
				}
				case ClassifierLearnerForOneCategoryWithWekaLogReg:
				{
					IndependentLearnerParameters curParams = new IndependentLearnerParameters();
					curParams.closeCategoryUseType = defCloseCategoryUseType;
					curParams.closeCategoryCalculation = defCloseCategoryCalculation;
					curParams.binaryLearnerParameters.binaryLearnerName = BinaryLearnerName.WekaLogReg;
					curParams.binaryLearnerParameters.binaryLexiconParams = ""; 
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.No;
					params = curParams;
					params.multiLabelInitialRefinementParams = defMultiLabelInitialRefinementParams;
					params.outputClassifierApplicationParameters = defOutputClassifierParameters; 
					params.rebalancingParameters = defRebalancingParameters; 
					params.specificLearnerParams = null;
					break;
				}
				case ClassifierLearnerForOneCategoryWithPMI:
				{
					IndependentLearnerParameters curParams = new IndependentLearnerParameters();
					curParams.closeCategoryUseType = defCloseCategoryUseType;
					curParams.closeCategoryCalculation = defCloseCategoryCalculation;
					curParams.binaryLearnerParameters.binaryLearnerName = BinaryLearnerName.PMI;
					curParams.binaryLearnerParameters.binaryLexiconParams = ""; // "-T0.1 - this should be an equivalent of feature selection option with PMI with the same threshold
					curParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg =  FeatureSelectionAlgOption.No;
					params = curParams;
					params.multiLabelInitialRefinementParams = defMultiLabelInitialRefinementParams;
					params.outputClassifierApplicationParameters = defOutputClassifierParameters; 
					params.rebalancingParameters = defRebalancingParameters; 
					params.specificLearnerParams = null;
					break;
				}
				default: params = null;
			}
			defaultParametersSaved.put(learnerName, params);
		 }
		 LearnerName curLearnerName = LearnerName.WeightedVoting;
		 
		
	}
	
}

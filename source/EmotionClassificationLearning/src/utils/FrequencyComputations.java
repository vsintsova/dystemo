/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.util.List;
import java.util.Map;

import data.categories.Polarity;
import functionality.UtilArrays;
import functionality.UtilCollections;

public class FrequencyComputations {
	
	
	public static void extractFrequencies(
			Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> termData, Map<Integer, Double> objectWeights, 
			Map<String,Double> termFrequencies, 
			Map<String,Double> positiveFrequencies, Map<String,Double> negativeFrequencies)
	{
		if (objectWeights == null)
			{
				extractFrequencies(sentimentLabels, termData, termFrequencies, positiveFrequencies, negativeFrequencies);
				return;
			}
		// compute frequencies
				for(Integer key : sentimentLabels.keySet())
				{
					List<String> terms = termData.get(key);
					if (terms == null)
						continue;
					int sentimentLabel = sentimentLabels.get(key).intValue();		
					for(int i = 0; i < terms.size(); i++)
					{
						String term = terms.get(i);
						
						if(sentimentLabel == Polarity.POSITIVE)
						{
							UtilCollections.incrementIntValueInMap(positiveFrequencies, term, objectWeights.get(key));
						} 
						else if(sentimentLabel == Polarity.NEGATIVE)
						{
							UtilCollections.incrementIntValueInMap(negativeFrequencies, term, objectWeights.get(key));
						}
						else
							continue;
						
						UtilCollections.incrementIntValueInMap(termFrequencies, term, objectWeights.get(key)); // we count only the terms that appeared within one or another class!
					}
				}
	}
	
	public static void extractFrequenciesWithNeutral(
			Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> termData, Map<Integer, Double> objectWeights, 
			Map<String,Double> termFrequencies, 
			Map<String,Double> positiveFrequencies, Map<String,Double> negativeFrequencies, Map<String,Double> neutralFrequencies)
	{
		if (objectWeights == null)
			{
			extractFrequenciesWithNeutral(sentimentLabels, termData, termFrequencies, positiveFrequencies, negativeFrequencies, neutralFrequencies);
				return;
			}
		// compute frequencies
				for(Integer key : sentimentLabels.keySet())
				{
					List<String> terms = termData.get(key);
					int sentimentLabel = sentimentLabels.get(key).intValue();		
					for(int i = 0; i < terms.size(); i++)
					{
						String term = terms.get(i);
						
						if(sentimentLabel == Polarity.POSITIVE)
						{
							UtilCollections.incrementIntValueInMap(positiveFrequencies, term, objectWeights.get(key));
						} 
						else if(sentimentLabel == Polarity.NEGATIVE)
						{
							UtilCollections.incrementIntValueInMap(negativeFrequencies, term, objectWeights.get(key));
						}
						else if(sentimentLabel == Polarity.NEUTRAL)
							UtilCollections.incrementIntValueInMap(neutralFrequencies, term, objectWeights.get(key));
						else
							continue;
						
						UtilCollections.incrementIntValueInMap(termFrequencies, term, objectWeights.get(key)); // we count only the terms that appeared within one or another class!
					}
				}
	}
	
	
	public static void extractFrequenciesWithNeutral(
			Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> termData,  
			Map<String,Double> termFrequencies, 
			Map<String,Double> positiveFrequencies,
			Map<String,Double> negativeFrequencies,
			Map<String,Double> neutralFrequencies)
	{
		
		// compute frequencies
				for(Integer key : sentimentLabels.keySet())
				{
					List<String> terms = termData.get(key);
					int sentimentLabel = sentimentLabels.get(key).intValue();		
					for(int i = 0; i < terms.size(); i++)
					{
						String term = terms.get(i);
						
						if(sentimentLabel == Polarity.POSITIVE)
						{
							UtilCollections.incrementIntValueInMap(positiveFrequencies, term, 1.0);
						} 
						else if(sentimentLabel == Polarity.NEGATIVE)
						{
							UtilCollections.incrementIntValueInMap(negativeFrequencies, term, 1.0);
						}
						else if(sentimentLabel == Polarity.NEUTRAL)
							UtilCollections.incrementIntValueInMap(neutralFrequencies, term, 1.0);
						else
							continue;
						
						UtilCollections.incrementIntValueInMap(termFrequencies, term, 1.0); // we count only the terms that appeared within one or another class!
					}
				}
	}
	
	public static void extractFrequencies(
			Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> termData,  
			Map<String,Double> termFrequencies, 
			Map<String,Double> positiveFrequencies, Map<String,Double> negativeFrequencies)
	{
		// compute frequencies
				for(Integer key : sentimentLabels.keySet())
				{
					List<String> terms = termData.get(key);
					if (terms == null)
						continue;
					int sentimentLabel = sentimentLabels.get(key).intValue();		
					for(int i = 0; i < terms.size(); i++)
					{
						String term = terms.get(i);
						
						if(sentimentLabel == Polarity.POSITIVE)
						{
							UtilCollections.incrementIntValueInMap(positiveFrequencies, term, 1.0);
						} 
						else if(sentimentLabel == Polarity.NEGATIVE)
						{
							UtilCollections.incrementIntValueInMap(negativeFrequencies, term, 1.0);
						}
						else
							continue;
						
						UtilCollections.incrementIntValueInMap(termFrequencies, term, 1.0); // we count only the terms that appeared within one or another class!
					}
				}
	}
	
	
	public static double computeSumWeightsOfKeysForLabel(Map<Integer, Integer> objectLabels, Map<Integer, Double> objectWeights, Integer seekLabel)
	{
		if (objectWeights == null)
			return computeSumWeightsOfKeysForLabel(objectLabels, seekLabel);
		double result = 0.0;
		for (Map.Entry<Integer, Integer> entry : objectLabels.entrySet())
		{
				Integer foundValue = entry.getValue();
				if (foundValue.equals(seekLabel))
					result+= objectWeights.get(entry.getKey());
		}
		return result;
	}
	
	public static double computeSumWeightsOfKeysForLabel(Map<Integer, Integer> objectLabels, Integer seekLabel)
	{
		double result = 0.0;
		for (Map.Entry<Integer, Integer> entry : objectLabels.entrySet())
			{
				Integer foundValue = entry.getValue();
				if (foundValue.equals(seekLabel))
					result+= 1.0;
			}
		return result;
	}
	
	public static double[] computeSumWeightsForAllValues(Map<Integer, double[]> objectEmotionWeights) throws Exception {
		if (objectEmotionWeights == null || objectEmotionWeights.isEmpty()) return null;
		double[] totalEmotionWeights = new double[objectEmotionWeights.values().iterator().next().length];
		for (double[] weights : objectEmotionWeights.values()) {
			UtilArrays.sumTwoArraysInternal(totalEmotionWeights, weights);
		}
		return totalEmotionWeights;
	}
}

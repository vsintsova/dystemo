/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import functionality.UtilArrays;

public class EmotionDistributionAdoption {
	
	/**
	 * Sets the probabilities lower than threshold to zero. Then normalizes the distribution.
	 * @param distr
	 * @param threshold
	 */
	public static void dropLowProbabilitiesByThreshold(double[] distr, double threshold)
	{
		if (distr == null)
			return;
		for (int i = 0; i < distr.length; ++i)
		{
			if (distr[i] < threshold)
				distr[i] = 0.0;
		}
		UtilArrays.normalizeArrayInternal(distr);
	}
	
	/**
	 * Sets the probabilities lower than alpha * maximum to zero. Then normalizes the distribution.
	 * @param distr
	 * @param threshold
	 */
	public static void dropLowProbabilitiesByAlphaCut(double[] distr, double alpha)
	{
		if (distr == null)
			return;
		//find maximum:
		double threshold = alpha * UtilArrays.getMax(distr);
		dropLowProbabilitiesByThreshold(distr, threshold);
	}
	
	/** Removes all the categories (sets them to zero) if they are not in the alpha close set to the maximum and within the "limit" closest to the maximum (including maximum).
	 * Then normalizes the distribution to the initial sum.*/
	public static void leaveHighProbabilitiesByAlphaCutAndLimit (double[] distr, double alpha, int limit)
	{
		if (distr == null)
			return;
		
		double maximumValue = UtilArrays.getMax(distr);
		double sum = UtilArrays.getSum(distr);
		double threshold = alpha * maximumValue;
		
		int left = 0;
		for (int i = 0; i < distr.length; ++i)
		{
			if (distr[i] < threshold)
				distr[i] = 0.0;
			else
				left++;
		}
		
		if (left > limit && maximumValue > 0.0)
		{
			// need to delete some others (only "limit" maximal values will stay)
			List<Double> leftValues = new ArrayList<Double>();
			for (int i = 0; i < distr.length; ++i)
			{
				if (distr[i] > 0.0)
					leftValues.add(distr[i]);
			}
			
			Collections.sort(leftValues, Collections.reverseOrder());
			threshold = leftValues.get(limit - 1);
			
			for (int i = 0; i < distr.length; ++i)
			{
				if (distr[i] < threshold)
					distr[i] = 0.0;
			}
		}	
		UtilArrays.normalizeArrayInternal(distr);
		if (sum != 1.0 && sum != 0.0)
			UtilArrays.multiplyArrayInternal(distr, sum);
	}

	private static double epsilonDiff = 0.001;
	
	/** reweights the distribution in general, if the current sum is less than 1 (significantly)*/
	public static void reweightOnLowSum(double[] distr,
			double reweightCoefficient) {
		if (distr == null)
			return;
		double sum = UtilArrays.getSum(distr);
		if (sum < 1.0 - epsilonDiff && sum > 0.0)
			UtilArrays.multiplyArrayInternal(distr, reweightCoefficient / sum);
	}
	
	public static boolean zeroNonDominantEmotions(double[] distr,
			ClassLimiter limiter) {
		if (distr == null)
			return false;
		boolean wasChanged = false;
		for (int i = 0; i < distr.length; ++i)
		{
			if (limiter.isToRemove(i+1) && distr[i] > 0.0)
			{
				distr[i] = 0.0;
				wasChanged = true;
			}
		}
		return wasChanged;
	}
	
	
	public static List<Integer> findEmotionsGreaterThanThreshold(double[] distr, double threshold)
	{
		List<Integer> emotions = new ArrayList<Integer>();
		for (int i = 0; i < distr.length; ++i)
		{
			if (distr[i] > threshold)
				emotions.add(i+1);
		}
		return emotions;
	}
	
	public static List<Integer> findHighEmotionsByAlphaCutAndLimit(double[] distr, double alpha, int limit)
	{
		double[] distrCopy = new double[distr.length];
		System.arraycopy(distr,0,distrCopy,0,distr.length);
		leaveHighProbabilitiesByAlphaCutAndLimit(distrCopy, alpha, limit);
		
		return findEmotionsGreaterThanThreshold(distrCopy, 0.0);
	}
	
}

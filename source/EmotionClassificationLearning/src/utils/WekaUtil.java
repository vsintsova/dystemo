/*
    Copyright 2016 Valentina Sintsova, Marina Boia
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Vector;




import functionality.UtilCollections;
import weka.attributeSelection.GainRatioAttributeEval;
import weka.attributeSelection.Ranker;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;
import weka.filters.Filter;
import weka.filters.supervised.attribute.AttributeSelection;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.attribute.RemoveUseless;
import weka.filters.unsupervised.attribute.StringToWordVector;
import classification.definitions.classifiers.WeightedClassifierLexiconBased;
import classification.extraclassifiers.TwoLayerIndependentWeightedClassifier;
import classification.featurerepresentation.WekaAttributesSet;
import data.categories.Polarity;
import functionality.UtilCollections;

/**
 * Utility functions for loading instances in Weka format
 */
public class WekaUtil 
{	
	public static boolean performCrossValidation = false;
	
	static boolean useOnlyBooleanFeatures = false;
	
	/**
	 * Load instances assigning weights to the objects to make classes to have equal weight
	 * 
	 * @return data in Weka format
	 * @throws Exception
	 */
	public static Instances loadInstancesWithWeights(Map<Integer, Integer> sentimentLabels, Map<Integer, String> textData) throws Exception
	{
		return loadInstances(sentimentLabels, textData, true);
	}
	
	public static WekaAttributesSet detectAttributesFromTexts(Map<Integer, List<String>> textData)
	{
		// 1. create new attributes in dataset
		LinkedHashMap<String,Attribute> savedAttributeMap = new LinkedHashMap<String, Attribute>();
				
		for (Map.Entry<Integer, List<String>> textEntry : textData.entrySet() ) 
		{
			for (String term : textEntry.getValue())
			{
				if (term.equals(" the"))
				{
					int k = 0;
					k++;
				}
				if (!savedAttributeMap.containsKey(term))
				{
					Attribute curAttr = new Attribute(term);// need to be careful with quotes!
					savedAttributeMap.put(term, curAttr);
				}
			}
		}
		return new WekaAttributesSet(savedAttributeMap);
	}
	
	public static WekaAttributesSet detectAttributesFromTexts(List<Map<String, Double>> textData)
	{
		// 1. create new attributes in dataset
		LinkedHashMap<String,Attribute> savedAttributeMap = new LinkedHashMap<String, Attribute>();
				
		for (Map<String, Double> textEntry : textData)
		{
			for (String term : textEntry.keySet())
			{
				if (!savedAttributeMap.containsKey(term))
				{
					Attribute curAttr = new Attribute(term);// need to be careful with quotes!
					savedAttributeMap.put(term, curAttr);
				}
			}
		}
		return new WekaAttributesSet(savedAttributeMap);
	}
	
	public static Instances createExampleProblemBinary(WekaAttributesSet attributesSet)
	{
		FastVector attributes = createAllAttributesBinary(attributesSet);
		Instances instances = new Instances("relation", attributes, 1);
		instances.setClassIndex(attributes.size() - 1);
		return instances;
	}
	
	public static Instances createExampleProblemCategory(WekaAttributesSet attributesSet, List<Integer> categories)
	{
		FastVector attributes = createAllAttributesCategory(attributesSet, categories);
		Instances instances = new Instances("relation", attributes, 1);
		instances.setClassIndex(attributes.size() - 1);
		return instances;
	}
	
	public static Instances createExampleProblemNoClass(WekaAttributesSet attributesSet)
	{
		FastVector attributes = createAllAttributes(attributesSet, null);
		Instances instances = new Instances("relation", attributes, 1);
		return instances;
	}
	
	private static FastVector createAllAttributesBinary(WekaAttributesSet attributesSet)
	{
		// Create the class attribute
		FastVector classes = new FastVector(2);
		classes.addElement("-1");
		classes.addElement("1");
		Attribute classAttribute = new Attribute("_class", classes);
		
		return createAllAttributes(attributesSet, classAttribute);
	}
	
	private static FastVector createAllAttributesCategory(WekaAttributesSet attributesSet, List<Integer> categories)
	{
		// Create the class attribute
		FastVector classes = new FastVector(categories.size());
		for (Integer category : categories)
			classes.addElement(category.toString());
		Attribute classAttribute = new Attribute("_class", classes);
				
		return createAllAttributes(attributesSet, classAttribute);
	}
	
	private static FastVector createAllAttributes(WekaAttributesSet attributesSet, Attribute classAttribute)
	{
						
		// The attributes
		FastVector attributes = new FastVector(attributesSet.size() + 1);
				
		for (Map.Entry<String, Attribute> termEntry : attributesSet.getAttributeMap().entrySet())
			attributes.addElement(termEntry.getValue());
		
		if (classAttribute != null)
			attributes.addElement(classAttribute);
		
		return attributes;
	}
	
	public static Instances loadInstancesWithSpecificBinaryData(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> textData, 
			Map<Integer, Double> objectWeights, boolean setClassWeights, WekaAttributesSet attributesSet) throws Exception
	{
		
		FastVector attributes = createAllAttributesBinary(attributesSet);
		
				
		// Separately load the instances for each class
		HashMap<Integer, Instances> instancesMap = new HashMap<Integer, Instances>(); 
				
		int[] _classes = new int[] {Polarity.NEGATIVE, Polarity.POSITIVE};
				
			
		double firstClassWeightedSize = 0.0;
		int sumObjectCount = 0;
				
		double[] defaultAttValues = new double[attributes.size()];
		Arrays.fill(defaultAttValues, 0.0);
		
		Instance emptyInstance = new SparseInstance(1.0, defaultAttValues);
		
				
		for(int i = 0; i < _classes.length; i++)
		{
			List<Integer> ids = UtilCollections.findKeysWithSpecificValue(sentimentLabels,  _classes[i]);
					
			int classSize = ids.size();
			double classWeightedSize = FrequencyComputations.computeSumWeightsOfKeysForLabel(sentimentLabels, objectWeights, _classes[i]);
			if (i == 0)
				firstClassWeightedSize = classWeightedSize;
			double classWeight = (i==0) ? 1.0 : (1.0 * firstClassWeightedSize)/(1.0 * classWeightedSize); //first class gets the weight 1
					
			instancesMap.put(_classes[i],  new Instances("relation", attributes, classSize));
					
			
			for(int j = 0; j < classSize; j++)
			{
				List<String> objectTerms = textData.get(ids.get(j));
						
				if(objectTerms == null || objectTerms.size() == 0)
				{
					continue;
				}
				
				double instanceWeight = 1.0;
				if (objectWeights != null)
					instanceWeight = objectWeights.get(ids.get(j));
				
				if (setClassWeights)
				{
					instanceWeight *= classWeight;
				}
				
				Instance instance = new SparseInstance(emptyInstance);
				instance.setWeight(instanceWeight);
				for (String term : objectTerms)
				{
					Attribute curArg = attributesSet.getAttributeByName(term);
					if (useOnlyBooleanFeatures)
						instance.setValue(curArg,  1.0);
					else
					{
						double curValue  = instance.value(curArg);
						if (curValue >= 0.0)
						{
							instance.setValue(curArg,  curValue + 1.0);
						}
						else
						{
							int y = 0;
							y++;
						}
					}
				}
				
				
				
				
				/*
				double[] attValues = new double[objectTerms.size()];
				double[] attIndexes = new double[objectTerms.size()];
				
				int tInd = 0;
				for (String term : objectTerms)
				{
					attValues[tInd] = 1.0;
					attIndexes[tInd] = attributesSet.attributeIndexes.get(term);
					tInd++;
				}
				Instance instance = new SparseInstance(instanceWeight, attValues, attIndexes, attributes.size());*/
				
				/*
				Instance instance = new SparseInstance(attributes.size());
				
				for (String term : objectTerms)
					instance.setValue(attributesSet.savedAttributeMap.get(term),  1.0);
				//instance.se
				
				//replacing missing values with 0
				for (int attrInd = 0; attrInd < instance.numAttributes(); ++attrInd)
				{
					if (instance.isMissing(attrInd))
						instance.setValue(attrInd, 0.0);
				}*/
						
				instance.setValue((Attribute)attributes.lastElement(), _classes[i] == Polarity.POSITIVE ? "1" : "-1");
						
				instancesMap.get(_classes[i]).add(instance);
			}
					
			sumObjectCount += classSize;
					
			ids.clear();
		}
				
				// Merge the instances for each class
				Instances instances = new Instances("relation", attributes, sumObjectCount);
				
				instances.setClassIndex(attributes.size() - 1);
				
				for(int i = 0; i < _classes.length; i++)
				{
					for(int j = 0; j < instancesMap.get(_classes[i]).numInstances(); j++)
					{
						instances.add(instancesMap.get(_classes[i]).instance(j));
					}
				}
				
				// TODO: optionally make sure the classes are equally represented? Or adjust the weights for the classes!
				
				instancesMap.clear();
				
				return instances;
	}
	
	public static Instances loadInstancesWithSpecificBinaryDataWithFeatures(Map<Integer, Integer> sentimentLabels, Map<Integer, Map<Integer,Integer>> featureData, 
			Map<Integer, Double> objectWeights, boolean setClassWeights, WekaAttributesSet attributesSet) throws Exception
	{
		
		FastVector attributes = createAllAttributesBinary(attributesSet);
		
				
		// Separately load the instances for each class
		HashMap<Integer, Instances> instancesMap = new HashMap<Integer, Instances>(); 
				
		int[] _classes = new int[] {Polarity.NEGATIVE, Polarity.POSITIVE};
				
			
		double firstClassWeightedSize = 0.0;
		int sumObjectCount = 0;
				
				
		for(int i = 0; i < _classes.length; i++)
		{
			List<Integer> ids = UtilCollections.findKeysWithSpecificValue(sentimentLabels,  _classes[i]);
					
			int classSize = ids.size();
			double classWeightedSize = FrequencyComputations.computeSumWeightsOfKeysForLabel(sentimentLabels, objectWeights, _classes[i]);
			if (i == 0)
				firstClassWeightedSize = classWeightedSize;
			double classWeight = (i==0) ? 1.0 : (1.0 * firstClassWeightedSize)/(1.0 * classWeightedSize); //first class gets the weight 1
					
			instancesMap.put(_classes[i],  new Instances("relation", attributes, classSize));
					
			double[] defaultAttValues = new double[attributes.size()];
			Arrays.fill(defaultAttValues, 0.0);
			
			for(int j = 0; j < classSize; j++)
			{
				Map<Integer,Integer> objectAttributes = featureData.get(ids.get(j));
						
				if(objectAttributes == null || objectAttributes.size() == 0)
				{
					continue;
				}
				
				double instanceWeight = 1.0;
				if (objectWeights != null)
					instanceWeight = objectWeights.get(ids.get(j));
				
				if (setClassWeights)
				{
					instanceWeight *= classWeight;
				}
				
				Instance instance = new SparseInstance(instanceWeight, defaultAttValues);
				
				for (Map.Entry<Integer, Integer> attr : objectAttributes.entrySet())
					instance.setValue(attr.getKey(),  attr.getValue());
						
				instance.setValue((Attribute)attributes.lastElement(), _classes[i] == Polarity.POSITIVE ? "1" : "-1");
						
				instancesMap.get(_classes[i]).add(instance);
			}
					
			sumObjectCount += classSize;
					
			ids.clear();
		}
				
				// Merge the instances for each class
				Instances instances = new Instances("relation", attributes, sumObjectCount);
				
				instances.setClassIndex(attributes.size() - 1);
				
				for(int i = 0; i < _classes.length; i++)
				{
					for(int j = 0; j < instancesMap.get(_classes[i]).numInstances(); j++)
					{
						instances.add(instancesMap.get(_classes[i]).instance(j));
					}
				}
				
				// TODO: optionally make sure the classes are equally represented? Or adjust the weights for the classes!
				
				instancesMap.clear();
				
				return instances;
	}
	
	/**
	 * Loads instances without class assignment
	 * @param sentimentLabels
	 * @param featureData
	 * @param objectWeights
	 * @param setClassWeights
	 * @param attributesSet
	 * @return
	 * @throws Exception
	 */
	public static Instances loadInstancesWithFeaturesNoClass(Map<Integer, Map<Integer,Double>> featureData, 
			Map<Integer, Double> objectWeights, WekaAttributesSet attributesSet) throws Exception
	{
		
		FastVector attributes = createAllAttributesBinary(attributesSet);
		Attribute idAttr = new Attribute("<id>", (FastVector) null);
		attributes.addElement(idAttr);
		
		Instances finalProblem = new Instances("relation", attributes, featureData.size());
		
		// Separately load the instances for each class
		int sumObjectCount = 0;
		double[] defaultAttValues = new double[attributes.size()];
		Arrays.fill(defaultAttValues, 0.0);
				
		for (Map.Entry<Integer, Map<Integer, Double>> objFeatures : featureData.entrySet())
		{
			// set to be working with missing values
			
			Map<Integer,Double> objectAttributes = objFeatures.getValue();
						
			if(objectAttributes == null || objectAttributes.size() == 0)
			{
				continue;
			}
				
			double instanceWeight = 1.0;
			if (objectWeights != null)
				instanceWeight = objectWeights.get(objFeatures.getKey());
				
			Instance instance = new SparseInstance(instanceWeight, defaultAttValues);
				
			for (Map.Entry<Integer, Double> attr : objectAttributes.entrySet())
				instance.setValue(attr.getKey(),  attr.getValue());
			instance.setValue(idAttr, objFeatures.getKey().toString());
			
			finalProblem.add(instance);
		}
					
					
		return finalProblem;
	}
	
	/**
	 * Load instances
	 * 
	 * @return data in Weka format
	 * @throws Exception
	 */
	public static Instances loadInstances(Map<Integer, Integer> sentimentLabels, Map<Integer, String> textData) throws Exception
	{
		return loadInstances(sentimentLabels, textData, false);
	}
	
	/**
	 * Load instances
	 * 
	 * @return data in Weka format
	 * @throws Exception
	 */
	public static Instances loadInstances(Map<Integer, Integer> sentimentLabels, Map<Integer, String> textData, boolean setClassWeights) throws Exception
	{
		// The text attribute
		Attribute textAttribute = new Attribute("text", (FastVector) null);
		
		FastVector classes = new FastVector(2);
		
		classes.addElement("-1");
		 
		classes.addElement("1");
		 
		// The class attribute
		Attribute classAttribute = new Attribute("_class", classes);
		
		// The attributes
		FastVector attributes = new FastVector(2);
		
		attributes.addElement(textAttribute);
		
		attributes.addElement(classAttribute);
		
		
		// Separately load the instances for each class
		HashMap<Integer, Instances> instancesMap = new HashMap<Integer, Instances>(); 
		
		int[] _classes = new int[] {Polarity.NEGATIVE, Polarity.POSITIVE};
		
		// classes weight computation
		double[] _classWeights = new double[_classes.length];
		
		for(int i = 0; i < _classes.length; i++)
		{

			UtilCollections.countValueOccurrenceInMap(sentimentLabels, _classes[i]);
			
		}
		
		int firstClassSize = 0;
		int sumObjectCount = 0;
		
		
		for(int i = 0; i < _classes.length; i++)
		{
			List<Integer> ids = UtilCollections.findKeysWithSpecificValue(sentimentLabels,  _classes[i]);
			
			int classSize = ids.size();
			if (i == 0)
				firstClassSize = classSize;
			double classWeight = (i==0) ? 1.0 : (1.0 * firstClassSize)/(1.0 * classSize); //first class gets the weight 1
			
			instancesMap.put(_classes[i],  new Instances("relation", attributes, classSize));
			
			
			for(int j = 0; j < classSize; j++)
			{
				String objectText = textData.get(ids.get(j));
				
				if(objectText == null || objectText.length() == 0)
				{
					continue;
				}
				
				Instance instance = new DenseInstance(2);
				
				instance.setValue(textAttribute,  objectText);
				
				instance.setValue(classAttribute, _classes[i] == Polarity.POSITIVE ? "1" : "-1");
				
				
				if (setClassWeights)
				{
					instance.setWeight(classWeight);
				}
				
				instancesMap.get(_classes[i]).add(instance);
			}
			
			sumObjectCount += classSize;
			
			ids.clear();
		}
		
		// Merge the instances for each class
		Instances instances = new Instances("relation", attributes, sumObjectCount);
		
		instances.setClassIndex(1);
		
		for(int i = 0; i < _classes.length; i++)
		{
			for(int j = 0; j < instancesMap.get(_classes[i]).numInstances(); j++)
			{
				instances.add(instancesMap.get(_classes[i]).instance(j));
			}
		}
		
		// TODO: optionally make sure the classes are equally represented? Or adjust the weights for the classes!
		
		instancesMap.clear();
		
		return instances;
	}
	
	private static Attribute addAttributeToData (Instances instances, String attributeName)
	{
		Attribute curAttr = new Attribute(attributeName);
		instances.insertAttributeAt(curAttr, instances.numAttributes());
		return curAttr;
	}
	
	/** 
	 * Removes the first attribute from the data. In our dataset it should be text (before preprocessing)*/
	public static Instances removeTheFirstAttribute(Instances instances)
	{
		instances.deleteAttributeAt(0);
		return instances;
	}
	
	
	
	/**
	 * Prepare a set of instances by adding lexicon-based summative features. 
	 * To extract the features the first attribute of current instances is used.
	 * Similar in functionality to some data filter in WEKA
	 *  */
	public static Instances addSeedLexiconAttributes(Instances instances, WeightedClassifierLexiconBased emClassification)  throws Exception
	{
		
		// 1. create new attributes in dataset
		Map<String,Attribute> savedAttributeMap = new HashMap<String, Attribute>();
		
		int categoryNum = emClassification.getCategoriesData().getCategoryNum();
		List<String> allNewAttrNames = new ArrayList<String>();
		
		// for each emotion category:
		for (int catId = 1; catId < categoryNum + 1; ++catId)
		{
			allNewAttrNames.add("emCat"+catId);
		}
		
		// for each emotion quadrant:
		for (int quad = 1; quad < 5; ++quad)
		{
			allNewAttrNames.add("emQuad"+quad);
		}
		
		// for each emotion polarity:
		allNewAttrNames.add("emPositive");
		allNewAttrNames.add("emNegative");
		
		for (String attrName : allNewAttrNames)
		{
			Attribute curAttr = addAttributeToData(instances, attrName);
			savedAttributeMap.put(attrName, curAttr);
		}
		
		
		
		// 2. set the values for new attributes
		
		for (int instInd = 0; instInd < instances.numInstances(); ++instInd)
		{
			Instance curInst = instances.instance(instInd);
			String objectText = curInst.stringValue(0);
			try
			{
				Map<String, Double> attributeValues = TwoLayerIndependentWeightedClassifier.getUpLayerAttributeValuesFromWeights(emClassification, objectText);
				
				for (Map.Entry<String, Double> attrValue : attributeValues.entrySet())
					curInst.setValue(instances.attribute(attrValue.getKey()), attrValue.getValue());
			}
			catch(Exception e)
			{
				System.out.println("Wasn't able to update the attributes of instances!");
				e.printStackTrace();
				return null;
			}
		}
		
		
		//TODO: remove text attribute?
		//3. remove uninformative features (always 0 or never repeated?)
		RemoveUseless filter = new RemoveUseless();
        filter.setInputFormat(instances);
        instances = Filter.useFilter(instances, filter);
		
		return instances;
	}
	
	
	
	/**
	 * Prepare a set of instances by converting to a bag of words and removing the stop words
	 */
	public static Instances prepareInstances(Instances instances, int wordsToKeep, boolean doNotOperateOnPerClassBasis, int minNgramLength, int maxNgramLength) throws Exception
	{
		// Convert to a bag of words
		StringToWordVector stringToWordVector = new StringToWordVector();
		
		// Example command line with doNotOperateOnPerClassBasis = true
		//weka.filters.unsupervised.attribute.StringToWordVector -R first-last -W 1000 -prune-rate -1.0 -N 0 -stemmer weka.core.stemmers.NullStemmer -M 1 -O -tokenizer "weka.core.tokenizers.WordTokenizer -delimiters \" \\r\\n\\t.,;:\\\'\\\"()?!\""
		// Example command line with doNotOperateOnPerClassBasis = false
		//weka.filters.unsupervised.attribute.StringToWordVector -R first-last -W 1000 -prune-rate -1.0 -N 0 -stemmer weka.core.stemmers.NullStemmer -M 1 -tokenizer "weka.core.tokenizers.WordTokenizer -delimiters \" \\r\\n\\t.,;:\\\'\\\"()?!\""
		
		if (doNotOperateOnPerClassBasis) //Valentina: I removed negation here and reversed the description before
			stringToWordVector.setOptions(weka.core.Utils.splitOptions("-O"));
		
		StringBuilder tokenizerOptions = new StringBuilder();
		String wordDelimiters = " \\\\n\\\\t\\\\r";//.,;:\\\\\\\'\\\\\\\"()?!-������[]";
		//wordDelimiters = java.util.regex.Matcher.quoteReplacement(wordDelimiters).replaceAll("\\", "\\\\");
		tokenizerOptions.append("-tokenizer \"weka.core.tokenizers.");
		if (maxNgramLength == 1)
		{
			// use WordTokenizer
			tokenizerOptions.append("WordTokenizer -delimiters \\\"");
			tokenizerOptions.append(wordDelimiters);
			tokenizerOptions.append("\\\" \"");
		}
		else
		{
			// use NGramTokenizer
			tokenizerOptions.append("NGramTokenizer -delimiters \\\"");
			tokenizerOptions.append(wordDelimiters);
			tokenizerOptions.append("\\\" -max ");
			tokenizerOptions.append(maxNgramLength);
			tokenizerOptions.append(" -min ");
			tokenizerOptions.append(minNgramLength);
			tokenizerOptions.append("\"");
		}
		
		stringToWordVector.setOptions(weka.core.Utils.splitOptions(tokenizerOptions.toString()));
		
		// I am adding the option to keep a certain number of words, but, as I said, it works fishy
		if(wordsToKeep > 0)
		{
			stringToWordVector.setWordsToKeep(wordsToKeep);
		}
        
		stringToWordVector.setInputFormat(instances);
        
		instances = Filter.useFilter(instances, stringToWordVector);
        
		// Remove stop words
        Vector<Integer> indices = new Vector<Integer>();
        
        for(int i = 0; i < instances.numAttributes(); i++)
        {
        	String word = instances.attribute(i).name();
        	
        	if(word.length() < 2)// || TextUtil.isStopWord(word) || TextUtil.containsDigit(word))
        	{
        		indices.add(i);
        	}
        }
        
        instances = removeAttributes(instances, indices);
        
        indices.clear();
        
        return instances;
	}
	
	/** 
	 * preserves only top k features identified by the information gain ratio
	 * @param instances
	 * @param k
	 * @return
	 * @throws Exception
	 */
	public static Instances keepTopKAttributes(Instances instances, int k) throws Exception
	{
		Filter topKfilter = getFilterForTopKAttributes(instances, k);
		
		if (topKfilter == null)
			return instances;
		else
			return Filter.useFilter(instances, topKfilter);
	}
	
	/** 
	 * Returns the filter to find top k attributes using information gain ratio
	 */
	public static Filter getFilterForTopKAttributes(Instances instances, int k) throws Exception
	{
		if(k <= 0)
		{
			return null;
		}

		AttributeSelection as = new AttributeSelection();
		Ranker ranker = new Ranker();
		ranker.setNumToSelect(k);
		as.setEvaluator(new GainRatioAttributeEval());//new InfoGainAttributeEval()
		as.setSearch(ranker);
		as.setInputFormat(instances);
		
		return as;
	}
	
	
	/** 
	 * preserves only top k features identified by the information gain ratio
	 * @param instances
	 * @param k
	 * @return
	 * @throws Exception
	 */
	public static Instances keepTopKAttributesOld(Instances instances, int k) throws Exception
	{
		if(k <= 0)
		{
			return instances;
		}
		
		GainRatioAttributeEval gainRatioAttributeEvaluator = new GainRatioAttributeEval();
        
        gainRatioAttributeEvaluator.buildEvaluator(instances);
        
        HashMap<String, Double> attributes = new HashMap<String, Double>();
        
        for(int i = 0; i < instances.numAttributes(); i++)
        {
        	attributes.put(instances.attribute(i).name(), gainRatioAttributeEvaluator.evaluateAttribute(i));
        }
     
        Vector<String> sortedAttributes = UtilCollections.sortMapByValue(attributes);
      
        List<String> topKSortedAttributes = sortedAttributes.subList(0, Math.min(k, sortedAttributes.size()));
       
        Vector<Integer> indices = new Vector<Integer>();
        
        for(int i = 0; i < instances.numAttributes(); i++)
        {
        	if(!topKSortedAttributes.contains(instances.attribute(i).name()) && i != instances.classIndex())
        	{
        		indices.add(i);
        	}
        }
        
        instances = removeAttributes(instances, indices);
        
        indices.clear();
  
        return instances;
	}
	
	public static Instances removeAttributes(Instances instances, Vector<Integer> indices) throws Exception
	{
		 int[] indicesArray = new int[indices.size()];
	        
	     for(int i = 0; i < indicesArray.length; i++)
	     {
	    	 indicesArray[i] = indices.get(i);
	     }
	        
	     indices.clear();
	        
	     Remove remove = new Remove();
	        
	     remove.setAttributeIndicesArray(indicesArray);
	       
	     remove.setInputFormat(instances);
	        
	     instances = Remove.useFilter(instances, remove);
	    
	     return instances;
	}
	
	public static Instances merge(Vector<Instances> instancesVector)
	{
		Instances mergedInstances = null;
		
		for(int i = 0; i < instancesVector.size(); i++)
		{
			if(i == 0)
			{
				mergedInstances = new Instances(instancesVector.get(i));
			}
			else
			{
				for(int j = 0; j < instancesVector.get(i).numInstances(); j++)
				{
					mergedInstances.add(instancesVector.get(i).instance(j));
				}
			}
		}
		
		return mergedInstances;
	}
	
	public static void evaluate(Classifier wekaClassifier, Instances instances) throws Exception
	{
		System.out.println("Evaluating the classifier");
		
		Evaluation e = new Evaluation(instances);
        
        e.evaluateModel(wekaClassifier, instances);
        
        System.out.println("Done: " + e.pctCorrect() + " train accuracy");
        
        System.out.println(e.pctUnclassified() + " unclassified elements");
        
        System.out.println(e.weightedRecall() + " of recall");
        
        System.out.println(e.weightedPrecision() + " of precision");
        
        System.out.println();
	}
	
	
	public static void crossValidateModelFiltered(Classifier wekaClassifier, Instances instances, Filter usedFilter) throws Exception
	{

		System.out.println("Cross-validating  the classifier");
		
		Evaluation e = new Evaluation(instances);
        
		FilteredClassifier fc = new FilteredClassifier();
		fc.setFilter(usedFilter);
		fc.setClassifier(wekaClassifier);
		
		e.crossValidateModel(fc, instances, 10, new Random(498));
        
        System.out.println("Done: " + e.pctCorrect() + " train accuracy");
        
        System.out.println(e.pctUnclassified() + " unclassified elements");
        
        System.out.println(e.weightedRecall() + " of recall");
        
        System.out.println(e.weightedPrecision() + " of precision");
        
       // e.confusionMatrix();
        System.out.println(e.toSummaryString());
        System.out.println(e.toClassDetailsString());
        System.out.println(e.toMatrixString());
        System.out.println();
	}
	
	public static void crossValidateModel(Classifier wekaClassifier, Instances instances) throws Exception
	{

		System.out.println("Cross-validating  the classifier");
		
		Evaluation e = new Evaluation(instances);
        
		e.crossValidateModel(wekaClassifier, instances, 10, new Random(498));
        
        System.out.println("Done: " + e.pctCorrect() + " train accuracy");
        
        System.out.println(e.pctUnclassified() + " unclassified elements");
        
        System.out.println(e.weightedRecall() + " of recall");
        
        System.out.println(e.weightedPrecision() + " of precision");
        
       // e.confusionMatrix();
        System.out.println(e.toSummaryString());
        System.out.println(e.toClassDetailsString());
        System.out.println(e.toMatrixString());
        System.out.println();
	}

	/**
	 * This loads Weka Instances class from the general representation of the data.
	 * Each label for each object generates a separate instance. Only those objects that are present (by keys) in textData are included.
	 * @param sentimentLabels
	 * @param textData
	 * @param objectWeights
	 * @param attributesSet
	 * @param allCategories
	 * @return
	 */
	public static Instances loadInstancesWithSpecificCategoryData(
			Map<Integer, List<Integer>> sentimentLabels,
			Map<Integer, List<String>> textData,
			Map<Integer, Double> objectWeights, 
			WekaAttributesSet attributesSet, 
			List<Integer> allCategories, Set<Integer> nonConsideredCategories)
	{
		FastVector attributes = createAllAttributesCategory(attributesSet, allCategories);
		
		// count sum of objects to add
		int sumLabelObject = 0;
		for (List<Integer> labelList : sentimentLabels.values())
		{
			sumLabelObject += labelList.size();
		}
		
		Instances instances = new Instances("relation", attributes, sumLabelObject);
				
		instances.setClassIndex(attributes.size() - 1);
		
		double[] defaultAttValues = new double[attributes.size()];
		Arrays.fill(defaultAttValues, 0.0);
		
		Instance emptyInstance = new SparseInstance(1.0, defaultAttValues);
		
		// put all the objects
		for (Integer instKey : sentimentLabels.keySet())
		{
			List<String> objectTerms = textData.get(instKey);
			
			for (Integer label : sentimentLabels.get(instKey))
			{
				if (nonConsideredCategories.contains(label))
					continue;
				double instanceWeight = 1.0;
				if (objectWeights != null)
					instanceWeight = objectWeights.get(instKey);
				
				Instance instance = new SparseInstance(emptyInstance);
				instance.setWeight(instanceWeight);
				
				for (String term : objectTerms)
					instance.setValue(attributesSet.getAttributeByName(term),  1.0);
					instance.setValue((Attribute)attributes.lastElement(), label.toString());
				instances.add(instance);
			}
		}
		
		return instances;
	}
}
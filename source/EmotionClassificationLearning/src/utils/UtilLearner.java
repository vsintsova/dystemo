/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import functionality.UtilArrays;
import functionality.UtilCollections;

public class UtilLearner {
	
	/** For each term sums up the distributions of the tweets in which it appeared
	 * Note:
	 * the annotation should be normalized!
	 * The weights of annotation are included*/
	public static void computeTermClassFrequency(
			Map<Integer, double[]> annotatedTweetEmotions, 
			Map<Integer,List<String>> tweetRepresentation,
			Map<String, double[]> termClassFrequencies)
	{
		for (Map.Entry<Integer,List<String>> tweetEntry : tweetRepresentation.entrySet())
		{
			double[] tweetWeights = annotatedTweetEmotions.get(tweetEntry.getKey());
			if (tweetWeights == null) // it means that the classifier refused to classify this tweet, we don't consider it
				continue;
			for (String term : tweetEntry.getValue())
			{
				try {
					UtilCollections.sumArrayValueToMap(termClassFrequencies, term, tweetWeights);
				} catch (Exception e) {
					System.err.println("Error for summing the weights for the tweet with id = " + tweetEntry.getKey());
					e.printStackTrace();
				}
			}
		}
	}
	
	/**For each term, saves how many times it appears in each class (while considering a valid occurrence if the weight for the class is positive)
	 * Note: 
	 * the annotation should be normalized!
	 * The weights of annotation are not included - if >0 the word counts as occurred in the class*/
	public static void computeTermClassOccurrenceNumber(
			Map<Integer, double[]> annotatedTweetEmotions, 
			Map<Integer,List<String>> tweetRepresentation,
			Map<String, double[]> termClassFrequencies)
	{
		for (Map.Entry<Integer,List<String>> tweetEntry : tweetRepresentation.entrySet())
		{
			double[] tweetWeights = annotatedTweetEmotions.get(tweetEntry.getKey());
			if (tweetWeights == null) // it means that the classifier refused to classify this tweet, we don't consider it
				continue;
			
			double[] resArray = new double[tweetWeights.length];
			for (int i = 0; i < tweetWeights.length; ++i)
			{
				if (tweetWeights[i] > 0)
					resArray[i] = 1.0;
			}
			for (String term : tweetEntry.getValue())
			{
				try {
					UtilCollections.sumArrayValueToMap(termClassFrequencies, term, resArray);
				} catch (Exception e) {
					System.err.println("Error for summing the weights for the tweet with id = " + tweetEntry.getKey());
					e.printStackTrace();
				}
			}
		}
	}
	
	/** computes the strict (soft) frequencies of classes over all annotated documents.
	 * The classFrequencies parameter should be already initialized !
	 * the annotation should be normalized!*/
	public static void computeClassFrequency(
			Map<Integer, double[]> annotatedTweetEmotions, 
			double[] classFrequencies)
	{
		int annotatedDocumentNum = 0;
		
		for (Map.Entry<Integer, double[]> tweetEmotionEntry : annotatedTweetEmotions.entrySet())
		{
			double[] tweetWeights = tweetEmotionEntry.getValue();
			if (tweetWeights == null) // it means that the classifier refused to classify this tweet, we don't consider it
				continue;
			annotatedDocumentNum++;
			try {
				UtilArrays.addArray(classFrequencies, tweetWeights);  
			} catch (Exception e) {
				System.err.println("Error for summing the weights for the tweet with id = " + tweetEmotionEntry.getKey());
				e.printStackTrace();
			}
		}
			
	}
	
	
	/**
	 * Calculates the number of occurrences of each term in the tweet representation
	 * @param tweetRepresentation
	 * @return
	 */
	public static  Map<String,Integer> computeTermOccurrence(Map<Integer,List<String>> tweetRepresentation)
	{
		Map<String, Integer> occurTermMap = new HashMap<String, Integer>();
		
		// 1. compute the number of occurrences for each term
		for (Map.Entry<Integer,List<String>> reprEntry : tweetRepresentation.entrySet())
		{
			for (String term : reprEntry.getValue())
			{
				UtilCollections.incrementIntValueInMap(occurTermMap, term);
			}
		}
		return occurTermMap;
	}
	
	
	/**
	 * Calculates the number of occurrences of each term in the tweet representation, taking into account only those tweets given in the set
	 * @param tweetRepresentation
	 * @return
	 */
	public static  Map<String,Integer> computeTermOccurrence(Map<Integer,List<String>> tweetRepresentation, Set<Integer> tweetIdsToConsider)
	{
		Map<String, Integer> occurTermMap = new HashMap<String, Integer>();
		List<String> reprOfTweet;
		for (Integer twId : tweetIdsToConsider)
		{
			reprOfTweet = tweetRepresentation.get(twId);
			for (String term : reprOfTweet)
			{
				UtilCollections.incrementIntValueInMap(occurTermMap, term);
			}
		}
		return occurTermMap;
	}
	
	
	/* Computes for each term its inverse emotion frequency, analogously to inverse document frequency
	* @throws Exception 
	*/

	public static Map<String, Double> computeTermIEF(Map<String, double[]> phraseEmotionCounts) throws Exception {
		int emotionNum = phraseEmotionCounts.values().iterator().next().length;
		Map<String, Double> iefTermScores = new HashMap<String, Double>();

		for (Map.Entry<String, double[]> phraseEmotionsCount : phraseEmotionCounts.entrySet()) {
			int emotionPresenceCount = 0;
			double[] weights =  phraseEmotionsCount.getValue();
			for (int emInd = 0; emInd < emotionNum; ++emInd) {
					if (weights[emInd] > 0) ++emotionPresenceCount;
			} 

			double ief = Math.log((emotionNum + 1) / (1.0 * emotionPresenceCount)); 
			// + 1 is for smoothing, so that when all emotions are present the score is small but still positive
			iefTermScores.put(phraseEmotionsCount.getKey(), ief);
		}
		return iefTermScores;
	}
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import classification.definitions.WeightedClassifier;

import data.documents.Tweet;


import functionality.UtilArrays;
import functionality.UtilCollections;

public class TweetCorpusAnnotationProcessing {

	/** annotates only the emotional tweets - thus, all tweets where no terms were found will not have an entry in the result map
		*/
	public static Map<Integer, double[]> annotateTweetCorpus(List<? extends Tweet> textCorpus, WeightedClassifier classifier) throws Exception
	{
		Map<Integer, double[]> emotionRelations = new LinkedHashMap<Integer, double[]>();
		for (Tweet tweet : textCorpus)
		{
			double[] weights = classifier.findCategoryWeights(tweet.text);
			if (UtilArrays.getSum(weights) > 0)
				emotionRelations.put(tweet.tweetId, weights);
		}
		return emotionRelations;
	}
	
	/**
	 * If ignoreNeutral = true, then those tweets that were classified actively as Neutral will not be included. 
	 * If ignoreNeutral = false, then only those tweets were no features were found would be excluded.
	 * @param textCorpus
	 * @param classifier
	 * @param ignoreNeutral
	 * @return
	 * @throws Exception
	 */
	public static Map<Integer, double[]> annotateTweetCorpus(List<? extends Tweet> textCorpus, WeightedClassifier classifier, boolean ignoreNeutral) throws Exception
	{
		Map<Integer, double[]> emotionRelations = new LinkedHashMap<Integer, double[]>();
		int neutralId = classifier.getCategoriesData().neutralCategory();
		int neutralInd = classifier.getCategoriesData().getCategoryIndexById(neutralId);
		
		for (Tweet tweet : textCorpus)
		{
			double[] weights = classifier.findCategoryWeights(tweet.text);
			if (UtilArrays.getSum(weights) > 0)
			{
				if (!ignoreNeutral)
					emotionRelations.put(tweet.tweetId, weights);
				else
				{
					// check if dominant emotions contain Neutral
					List<Integer> dominantEms = UtilArrays.getIndexesOfMax(weights);
					if (!dominantEms.contains(neutralInd))
					{
						emotionRelations.put(tweet.tweetId, weights);
					}
				}
			}
				
		}
		return emotionRelations;
	}
	
	public static<T extends Object> void refineAnnotationWithAlphaLowerCut(Map<T, double[]> annotation, double alpha)
	{
		for (Map.Entry<T, double[]> entry : annotation.entrySet())
		{
			EmotionDistributionAdoption.dropLowProbabilitiesByAlphaCut(entry.getValue(), alpha);
		}
	}
	
	public static<T extends Object> void refineAnnotationWithAlphaHigherCutAndLimit(Map<T, double[]> annotation, double alpha, int limit)
	{
		for (Map.Entry<T, double[]> entry : annotation.entrySet())
		{
			EmotionDistributionAdoption.leaveHighProbabilitiesByAlphaCutAndLimit(entry.getValue(), alpha, limit);
		}
	}
	
	public static<T extends Object> void reweightLowProbObjectsInAnnotation(Map<T, double[]> annotation, double reweightCoefficient)
	{
		for (Map.Entry<T, double[]> entry : annotation.entrySet())
		{
			EmotionDistributionAdoption.reweightOnLowSum(entry.getValue(), reweightCoefficient);
		}
	}
	
	public static<T extends Object> void normalizeAnnotation(Map<T, double[]> annotation)
	{
		for (Map.Entry<T, double[]> entry : annotation.entrySet())
		{
			UtilArrays.normalizeArrayInternal(entry.getValue());
		}
	}
	
	public static Map<Integer, List<Integer>> getEmotionLabelsFromWeights(Map<Integer, double[]> annotatedTweetEmotions, double alphaCut, int emotionLimit)
	{
		Map<Integer, List<Integer>> resultLabels = new HashMap<Integer, List<Integer>>();
		for (Map.Entry<Integer, double[]> tweetEntry : annotatedTweetEmotions.entrySet())
		{
			resultLabels.put(tweetEntry.getKey(), EmotionDistributionAdoption.findHighEmotionsByAlphaCutAndLimit(tweetEntry.getValue(), alphaCut, emotionLimit));
		}
		return resultLabels;
	}
	
	/**
	 * This function will set to 0 the weights for all emotions found by limiter, and delete those objects that have 0.0 sum weight in the end
	 * @param annotation
	 * @param limiter
	 */
	public static<T extends Object> void dropNonDominantEmotionsFromAnnotation (Map<T, double[]> annotation, ClassLimiter limiter)
	{
		Set<T> toRemove = new HashSet<T>();
		for (Map.Entry<T, double[]> entry : annotation.entrySet())
		{
			double[] curWeights = entry.getValue();
			boolean wasChanged = EmotionDistributionAdoption. zeroNonDominantEmotions(curWeights, limiter);
			if (wasChanged)
			{
				if (UtilArrays.getSum(curWeights) == 0.0)
					toRemove.add(entry.getKey());
				else
					UtilArrays.normalizeArrayInternal(curWeights);
			}
		}
		UtilCollections.removeAllKeysFromMap(annotation, toRemove);
	}
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import learners.given.BinaryLexiconConstruction;
import learners.given.TermScoresLexiconConstruction;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams;


import utils.PMICalculation;

import data.categories.Polarity;

import functionality.UtilCollections;
import functionality.UtilString;

public class PMITermScoresLexiconConstruction extends TermScoresLexiconConstruction {

	/* parameters */
	double threshold = 0;
	int numToOccur = -1;
	boolean includeNeutral = false;
	boolean applySmoothing = false;
	boolean applyToAbsoluteSoAScores = true; // was always true by default
	boolean saveOnlyPositiveScores = false; // was always false by default
	boolean nullifyLowPositiveScores = false; // was always false by default
	
	@Override
	public String getParameters() {
		return  "-T" + threshold + "-N" + numToOccur + "-R" + (includeNeutral?1:0) + "-S" + (applySmoothing?1:0) + (saveOnlyPositiveScores ? "-P1" : "") + (nullifyLowPositiveScores ? "-F1" : "") + (applyToAbsoluteSoAScores?"-A1":"-A0") ;
	}
	
	public PMITermScoresLexiconConstruction() {};
	public PMITermScoresLexiconConstruction(String params) {
		setParameters(params);
	}
	
	public PMITermScoresLexiconConstruction(BinarySelectionParams params) {
		applySmoothing = params.applySmoothing;
		includeNeutral = params.includeNeutral;
		threshold = params.scoreThreshold;
		numToOccur = params.minOccurNum;
		saveOnlyPositiveScores = params.useOnlyPositive;
		nullifyLowPositiveScores = params.nullifyLowPositiveScores;
		applyToAbsoluteSoAScores  = params.applyToAbsoluteSoAScores ;
	}
	
	/** Parse the string of parameters in the following format: -T<threshold>-N<numToOccur>-L<ngramLength>'
	 * If any is omitted, the default value is taken*/
	@Override
	public void setParameters(String params) {
		String[] parsedParams = params.split("-");
		for (String param : parsedParams)
		{
			if (param.trim().length() == 0)
				continue;
			if (param.charAt(0) == 'T')
				threshold = Double.parseDouble(param.substring(1));
			if (param.charAt(0) == 'N')
				numToOccur = Integer.parseInt(param.substring(1));
			if (param.charAt(0) == 'R')
				includeNeutral = param.substring(1).equals("1");
			if (param.charAt(0) == 'S')
				applySmoothing = param.substring(1).equals("1");
			if (param.charAt(0) == 'P')
				saveOnlyPositiveScores = param.substring(1).equals("1");
			if (param.charAt(0) == 'F')
				nullifyLowPositiveScores = param.substring(1).equals("1");
			if (param.charAt(0) == 'A')
				applyToAbsoluteSoAScores = !param.substring(1).equals("0");
		}
	}
	
	private void RemoveNonPositiveScores(Map<String, Double> termScores) {
		Set<String> termsToRemove = new HashSet<String>();
		for (Map.Entry<String, Double> entry : termScores.entrySet()) {
			if (entry.getValue() <= 0.0)
				termsToRemove.add(entry.getKey());
		}
		UtilCollections.removeAllKeysFromMap(termScores, termsToRemove);
	}
	
	
	/**
	 * Use the computed sentiment labels in order to extract the subjectivity lexicon
	 * parameters:
	 * sentimentLabels - map with tweet ids as keys, and sentiment labels (1, -1, or 0) as values
	 * textData - map with tweet ids as keys, and tweet text as value
	*/
	public Map<String, Double> extractTermScoresFromRepresentation(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> textData, Map<Integer, Double> objectWeights) throws Exception
	{
		Map<String, Double> PMIScores = PMICalculation.extractPMIValues(sentimentLabels, textData, objectWeights, numToOccur, threshold, applySmoothing, includeNeutral, !applyToAbsoluteSoAScores, nullifyLowPositiveScores);
		
		// ! limit the infinity values by the absolute maximum + 1 over all category values
		double maximumPMIValue = 0.0;
		for (Map.Entry<String, Double> termEntry : PMIScores.entrySet())
			{
				Double curValue = termEntry.getValue();
				if (!curValue.isInfinite())
					if (Math.abs(curValue) > maximumPMIValue)
						maximumPMIValue = Math.abs(curValue);
			}
		
		for (Map.Entry<String, Double> termEntry : PMIScores.entrySet())
		{
			Double curValue = termEntry.getValue();
			if (curValue.isInfinite())
			{
				if (curValue > 0)
					PMIScores.put(termEntry.getKey(), maximumPMIValue + 1.0);
				else
					PMIScores.put(termEntry.getKey(), -maximumPMIValue - 1.0);
			}
		}
		
		if (saveOnlyPositiveScores) {
			RemoveNonPositiveScores(PMIScores);
		}
		
		return PMIScores;
	}
	
}

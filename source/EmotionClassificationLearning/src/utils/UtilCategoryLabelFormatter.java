/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import classification.ClassificationUtil;
import data.categories.CategoryUtil;
import data.categories.ICategoriesData;

public class UtilCategoryLabelFormatter {

	public static Map<Integer, double[]> transformStrictAnnotationsIntoWeighted(
			ICategoriesData categoriesData,
			Map<Integer, List<Integer>> strictCategoryAssignment)
	{
		Map<Integer, double[]> annotatedTweetEmotions = new HashMap<Integer, double[]>();
		for (Map.Entry<Integer, List<Integer>> categoryEntry : strictCategoryAssignment.entrySet())
		{
			
			double[] catWeights = CategoryUtil.transformCategoryListToWeightsWithCounts(categoryEntry.getValue(), categoriesData);
			annotatedTweetEmotions.put(categoryEntry.getKey(), catWeights);
		}
		return annotatedTweetEmotions;
	}
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.util.HashSet;
import java.util.Set;

import parameters.EmotionRecognitionParameters;
import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;

public class ClassLimiter {
	Set<Integer> removedClasses;
	
	public ClassLimiter(int[] classesToRemove)
	{
		removedClasses = new HashSet<Integer>();
		for (int classId : classesToRemove)
			removedClasses.add(classId);
	}
	
	public boolean isToRemove(int classId)
	{
		return removedClasses.contains(classId);
	}
	
	static ClassLimiter defaultCLassLimiter;
	public static ClassLimiter getDefaultClassLimiter()
	{
		if (defaultCLassLimiter == null)
		{
			int[] classesToRemove = CategoryProcessor.getNonDominantEmotions(
					EmotionRecognitionParameters.defaultEmotionCategoriesType);
			defaultCLassLimiter = new ClassLimiter(classesToRemove);
		}
		return defaultCLassLimiter;
	}
}

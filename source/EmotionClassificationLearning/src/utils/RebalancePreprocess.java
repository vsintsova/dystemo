/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import learning.parameters.RebalancingParameters;
import learning.parameters.RebalancingParameters.BalanceWeightType;

import utils.UtilLearner;
import functionality.UtilArrays;

public class RebalancePreprocess {


	private static double getEmotionWeight(int index, double[] prior, BalanceWeightType weightType)
	{
		if (weightType == BalanceWeightType.LogPrior)
		{
			return - Math.log(prior[index])/Math.log(2.0);
		}
		else
			return 1 / prior[index];
	}
	
	public static double[] computeEmotionClassWeights (
			Map<Integer, double[]> annotatedTweetEmotions, 
			Map<Integer,List<String>> tweetRepresentation,
			RebalancingParameters params
			)
	{
		 BalanceWeightType weightType = params.weightType;
		// possible weights:
		
		double[] emClassWeights = new double[annotatedTweetEmotions.values().iterator().next().length];
		
		double[] emotionPrior = new double[emClassWeights.length];
		
		if (weightType == BalanceWeightType.Prior || weightType == BalanceWeightType.AdaptedPrior || weightType == BalanceWeightType.LogPrior)
		{
			// 1) by the class prior (weighted with the tweet weights!)
			
			for (Map.Entry<Integer, double[]> tweetAnnotation : annotatedTweetEmotions.entrySet())
			{
				try {
					UtilArrays.addArray(emotionPrior, tweetAnnotation.getValue());
				} catch (Exception e) {
					System.out.println("Error in computing emotion prior");
					e.printStackTrace();
				}
			}
		}
		else if (weightType == BalanceWeightType.TermNum || weightType == BalanceWeightType.NormalizedTermNum)
		{
			// 2) by the number of words (weighted by the tweet weights!)
			for (Map.Entry<Integer, double[]> tweetAnnotation : annotatedTweetEmotions.entrySet())
			{
				try {
					if (!tweetRepresentation.containsKey(tweetAnnotation.getKey()))
						continue;
					
					UtilArrays.addArray(emotionPrior, 
							UtilArrays.multiplyArray(tweetAnnotation.getValue(), 
													1.0 * tweetRepresentation.get(tweetAnnotation.getKey()).size())); // add prior but multiplied by the number of found features
				} catch (Exception e) {
					System.out.println("Error in computing emotion prior");
					e.printStackTrace();
				}
			}
		}
		
		if (weightType == BalanceWeightType.NormalizedTermNum || weightType == BalanceWeightType.WordNum)
		{
			
			// compute the number of words per class (taking into account both tweet weights and emotion weights per tweet) - > might be hard to compute
			Map<String, double[]> wordWeights = new HashMap<String, double[]>();
			Map<String, double[]> wordOccurrences = new HashMap<String, double[]>();
			
			// compute the frequencies for each word
			UtilLearner.computeTermClassFrequency(annotatedTweetEmotions, tweetRepresentation, wordWeights);
			UtilLearner.computeTermClassOccurrenceNumber(annotatedTweetEmotions, tweetRepresentation, wordOccurrences);
			
			double[] emotionWordNums = new double[emClassWeights.length];
			for (Map.Entry<String, double[]> wordWeight : wordWeights.entrySet())
			{
				double[] occurrence = wordOccurrences.get(wordWeight.getKey());
				double[] weight = wordWeight.getValue();
				for (int i = 0; i < occurrence.length; ++i)
				{
					emotionWordNums[i] += (occurrence[i] > 0.0) ? weight[i]/occurrence[i] : 0.0;
				}
			}
			if (weightType == BalanceWeightType.NormalizedTermNum)
			{
				for (int i = 0; i < emClassWeights.length; ++i)
				{
					emotionPrior[i] /= (emotionWordNums[i] > 0.0) ? emotionWordNums[i] : 1.0;
				}
			}
			else if (weightType == BalanceWeightType.WordNum)
			{
				emotionPrior = emotionWordNums;
			}
			
		}
		
		if (weightType == BalanceWeightType.AdaptedPrior)
		{
			// 3) by the adapted class prior - we add the regularization parameter for each value - defined by the variable
			// TODO: check which values are there!
			// 4) by the adapted class prior - we add the regularization parameter for each value - defined as the average of values
			double priorAverage = UtilArrays.getAverage(emotionPrior);
			double regularizator = priorAverage / params.paramAdaptedBalanceRatio;
			
			for (int i = 0; i < emotionPrior.length - 1; ++i) // -1 because the last one is for the Other emotion
				emotionPrior[i] += regularizator;
		}
		
		
		
		UtilArrays.normalizeArrayInternal(emotionPrior);
		System.arraycopy(emotionPrior, 0, emClassWeights, 0, emotionPrior.length);
		return emClassWeights;
	}
	
	public static void rebalanceAnnotation(
			Map<Integer, double[]> annotatedTweetEmotions, Map<Integer,List<String>> tweetRepresentation,
			RebalancingParameters params) {
		
		// 1. reweight the objects of classification (tweets) to have the balanced distribution
		
		// 1.a compute the prior distribution
		double[] emClassWeights = computeEmotionClassWeights (annotatedTweetEmotions, tweetRepresentation, params);
		
		// 1.b multiply all the distributions by the corresponding weight
		for (Map.Entry<Integer, double[]> tweetAnnotation : annotatedTweetEmotions.entrySet())
		{
			double[] emotionWeights = tweetAnnotation.getValue();
			balanceDistributionWeight(emotionWeights, emClassWeights, params.weightType);
		}
	}
	
	private static void balanceDistributionWeight(double[] distr, double[] emClassWeights, BalanceWeightType weightType)
	{
		boolean multiplyPerObject = false;
		//boolean preserveInitialWeight = true; // only rebalance between categories
		
		if (multiplyPerObject)
		{
			// the full distribution is multiplied by the weight of the maximum category
			List<Integer> maxEmotionIndexes = UtilArrays.getIndexesOfMaxWithThreshold(distr, 0.0, true);
			if (maxEmotionIndexes.size() > 0)
			{
				double tweetWeight = 0;
				for (Integer maxIndex : maxEmotionIndexes)
					tweetWeight += getEmotionWeight(maxIndex, emClassWeights, weightType);
				tweetWeight /= maxEmotionIndexes.size();
				try {
					UtilArrays.multiplyArrayInternal(distr, tweetWeight);
				} catch (Exception e) {
					System.out.println("Error in computing the balancing tweet weights");
					e.printStackTrace();
				}
			}
		}
		else
		{
			// the value for each category is multiplied to corresponding weight
			for (int i=0; i < distr.length; ++i)
			{
				if (distr[i] != 0.0)
					distr[i] *= getEmotionWeight(i, emClassWeights, weightType);
			}
		}
	}

}

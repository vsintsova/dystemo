/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.given;

import java.util.List;
import java.util.Map;

import learners.models.WekaBasedModelConstruction;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import utils.WekaUtil;
import weka.core.Instances;
import classification.definitions.BinaryClassifier;
import classification.definitions.classifiers.BinaryClassifierWekaBased;
import classification.featurerepresentation.WekaAttributesSet;
import classification.loaders.WekaClassifierFactory;
import classification.loaders.WekaClassifierFactory.AvailableWekaClassifiers;

public class ClassifierLearnerBinaryForWeka 
{
	static FeatureSelectionAlgOption defaultFeatureSelectionOption = FeatureSelectionAlgOption.No;
	
	
	public static BinaryClassifier learnClassifier(AvailableWekaClassifiers classifierName, Map<Integer, List<String>> featureRepresentation, Map<Integer,Integer> labelData, FeatureSelectionAlgOption fsOption ) throws Exception
	{
		WekaAttributesSet attributesSet = WekaUtil.detectAttributesFromTexts(featureRepresentation);
		Instances exampleProblem = WekaUtil.createExampleProblemBinary(attributesSet);
		
		WekaBasedModelConstruction binLexConstruction = new WekaBasedModelConstruction(WekaClassifierFactory.getWekaClassifierByName(classifierName));
		
		weka.classifiers.Classifier categoryModel = null;
		if (fsOption == FeatureSelectionAlgOption.IG)
			categoryModel = binLexConstruction.extractModelFromRepresentationWithIGFiltering(labelData, featureRepresentation, null, attributesSet);
		else
			categoryModel = binLexConstruction.extractModelFromRepresentation(labelData, featureRepresentation, null, attributesSet);
			
		BinaryClassifierWekaBased resultClassifier = new BinaryClassifierWekaBased(categoryModel, attributesSet, exampleProblem);
		return resultClassifier;
	}
	
	public static BinaryClassifier learnClassifier(AvailableWekaClassifiers classifierName, Map<Integer, List<String>> featureRepresentation, Map<Integer,Integer> labelData ) throws Exception
	{
		return learnClassifier(classifierName, featureRepresentation, labelData, defaultFeatureSelectionOption);
	}
}

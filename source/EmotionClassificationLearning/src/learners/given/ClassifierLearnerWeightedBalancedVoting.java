/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.given;

import java.util.List;
import java.util.Map;

import utils.RebalancePreprocess;

import learners.given.ClassifierLearnerWeightedVoting.WeightedVotingLearnerParams;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.ClassifierLearnerParams;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureSelectionParameters;
import learning.parameters.RebalancingParameters;


import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams;
public class ClassifierLearnerWeightedBalancedVoting extends ClassifierLearnerWeightedVoting{
	
	public ClassifierLearnerWeightedBalancedVoting(
			List<String> indicatorCandidates,
			ClassifierLearnerParams learnerParams,
			FeatureExtractionParameters featureExtractionParams,
			FeatureSelectionParameters featureSelectionParams) {
		super(indicatorCandidates, learnerParams, featureExtractionParams,
				featureSelectionParams);
		
		if (learnerParams instanceof WeightedBalancedVotingLearnerParams)
		{
			curLearnerParams = (WeightedBalancedVotingLearnerParams)learnerParams;
			if (curLearnerParams.rebalancingParameters == null)
				System.out.print("Incorrect rebalancing parameter setting for ClassifierLearnerWeightedBalancedVoting");
		}
		else
		{
			System.out.print("Incorrect parameter setting for ClassifierLearnerWeightedBalancedVoting");
		}
	}
	
	public static class WeightedBalancedVotingLearnerParams extends WeightedVotingLearnerParams
	{
	
		public WeightedBalancedVotingLearnerParams(
				RebalancingParameters rebalancingParams,
				boolean doPolarityAmbiquityRefinementOnLexicon,
				boolean doDropLowProbabilitiesOnLexicon)
		{
			super(doPolarityAmbiquityRefinementOnLexicon,  doDropLowProbabilitiesOnLexicon);
			this.rebalancingParameters = rebalancingParams;
		}
		
		public WeightedBalancedVotingLearnerParams(
				WeightedBalancedVotingLearnerParams initialParams)
		{
			super(initialParams.doPolarityAmbiquityRefinementOnLexicon, initialParams.doDropLowProbabilitiesOnLexicon);
			this.rebalancingParameters = initialParams.rebalancingParameters;
			this.copyDataFromAnother(initialParams);
		}
		
		@Override
		public String printParametersShort()
		{
			String original = super.printParametersShort();
			
			StringBuilder sb = new StringBuilder();
			if (rebalancingParameters != null)
				sb.append(rebalancingParameters.printParametersShort());
			
			return original + "," + sb.toString();
		}
		
		
		@Override
		public ClassifierLearnerParams clone()
		{
			WeightedBalancedVotingLearnerParams newLearnerParams = new WeightedBalancedVotingLearnerParams(this.rebalancingParameters, this.doPolarityAmbiquityRefinementOnLexicon, this.doDropLowProbabilitiesOnLexicon);
			newLearnerParams.lowProbabilitiesThreshold = this.lowProbabilitiesThreshold;
			newLearnerParams.copyDataFromAnother(this);
			return newLearnerParams;
		}
		
		
	}
	
	@Override
	public WeightedClassifier learnClassifierOnRefinedData(
			Map<Integer, double[]> annotatedTweetEmotions) {
		
		if (annotatedTweetEmotions == null || annotatedTweetEmotions.size() == 0)
			return null;
		
		if (!learnerParams.doRebalance()) // otherwise it would be already rebalanced
			RebalancePreprocess.rebalanceAnnotation(annotatedTweetEmotions, tweetRepresentation, ((WeightedBalancedVotingLearnerParams)curLearnerParams).rebalancingParameters);
		
		return super.learnClassifierOnRefinedData(annotatedTweetEmotions);
	}
	
	

}

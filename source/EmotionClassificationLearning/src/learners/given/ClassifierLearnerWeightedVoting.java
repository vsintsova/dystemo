/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.given;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import utils.TweetCorpusAnnotationProcessing;
import utils.UtilLearner;

import learners.definitions.ClassifierLearnerWithInternalRepresentation;
import learners.given.ClassifierLearnerForWeka.WekaCategoryClassifierLearnerParams;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.ClassifierLearnerParams;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureSelectionParameters;

import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.WeightedClassifierLexiconBased;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams;
import classification.definitions.lexicons.WeightedEmotionLexicon;
import classification.loaders.WekaClassifierFactory;

import data.categories.CategoryProcessor;
import data.categories.CategoryUtil;

import functionality.UtilArrays;

public class ClassifierLearnerWeightedVoting extends ClassifierLearnerWithInternalRepresentation {

	public ClassifierLearnerWeightedVoting(List<String> indicatorCandidates,
			ClassifierLearnerParams learnerParams,
			FeatureExtractionParameters featureExtractionParams,
			FeatureSelectionParameters featureSelectionParams) {
		super(indicatorCandidates, learnerParams, featureExtractionParams,
				featureSelectionParams);
		
		if (learnerParams instanceof WeightedVotingLearnerParams)
		{
			curLearnerParams = (WeightedVotingLearnerParams)learnerParams;
		}
		else
		{
			System.out.print("Incorrect parameter setting for ClassifierLearnerWeightedVoting");
		}
	}

	WeightedVotingLearnerParams curLearnerParams;
	
	public static class WeightedVotingLearnerParams extends ClassifierLearnerParams
	{
		public boolean doPolarityAmbiquityRefinementOnLexicon;
		public boolean doDropLowProbabilitiesOnLexicon;
		public double lowProbabilitiesThreshold;
			
		public WeightedVotingLearnerParams() {};
		
		public WeightedVotingLearnerParams(
				boolean doPolarityAmbiquityRefinementOnLexicon,
				boolean doDropLowProbabilitiesOnLexicon
		) 
		{
			this.doPolarityAmbiquityRefinementOnLexicon = doPolarityAmbiquityRefinementOnLexicon;
			this.doDropLowProbabilitiesOnLexicon = doDropLowProbabilitiesOnLexicon;
		};
		
		@Override
		public String printParametersShort()
		{
			String original = super.printParametersShort();
			
			StringBuilder sb = new StringBuilder();
			
			sb.append("-LexAmb-" + (doPolarityAmbiquityRefinementOnLexicon?1:0));
			sb.append("-NoLowProb-" + (doDropLowProbabilitiesOnLexicon?1:0));
			if (doDropLowProbabilitiesOnLexicon)
				sb.append(":prT-" + lowProbabilitiesThreshold);
			
			return original + "," + sb.toString();
		}
		
		@Override
		public ClassifierLearnerParams clone()
		{
			WeightedVotingLearnerParams newLearnerParams = new WeightedVotingLearnerParams(this.doPolarityAmbiquityRefinementOnLexicon, this.doDropLowProbabilitiesOnLexicon);
			newLearnerParams.lowProbabilitiesThreshold = this.lowProbabilitiesThreshold;
			newLearnerParams.copyDataFromAnother(this);
			return newLearnerParams;
		}
	}

	
	
	
	@Override
	public WeightedClassifier learnClassifierOnRefinedData(
			Map<Integer, double[]> annotatedTweetEmotions) {
		
		Map<String, double[]> resultTermEmotionAssociation = new HashMap<String, double[]>();
		
		// compute the distribution for each word
		computeInternalFeatureRepresentation(annotatedTweetEmotions, tweetRepresentation, resultTermEmotionAssociation);
		
		if (curLearnerParams.doPolarityAmbiquityRefinementOnLexicon)
			refineLexiconFromAmbiguousTerms(resultTermEmotionAssociation);
		if (curLearnerParams.doDropLowProbabilitiesOnLexicon)
			dropLowProbablitiesInLexicon(resultTermEmotionAssociation);
		
		WeightedEmotionLexicon lexicon = new WeightedEmotionLexicon();
		lexicon.createSimpleFromGivenData(resultTermEmotionAssociation, CategoryProcessor.getCategoriesData(emotionCategoriesType));
		
		WeightedClassifierLexiconBased lexClassifier = new WeightedClassifierLexiconBased(lexicon);
		return lexClassifier;
	}
	
	public static void computeInternalFeatureRepresentation(
			Map<Integer, double[]> annotatedTweetEmotions, 
			Map<Integer, List<String>> tweetRepresentation, 
			Map<String, double[]> resultFeatureRepresentation)
	{
		// compute the frequencies for each word
		UtilLearner.computeTermClassFrequency(annotatedTweetEmotions, tweetRepresentation, resultFeatureRepresentation);
				
		// normalization step
		for (Map.Entry<String, double[]> termEntry : resultFeatureRepresentation.entrySet())
		{
			double[] weights = UtilArrays.normalizeArray(termEntry.getValue());
			resultFeatureRepresentation.put(termEntry.getKey(), weights);
		}
	}
	
	@Override
	public WeightedClassifier updateOutputClassifierForLearnerSpecificApplicationParameters(
			WeightedClassifier classifier,
			ClassifierApplicationParameters applicationParameters) {
		
		WeightedClassifierWrapperWithApplicationParams resClassifier;
		if (classifier instanceof WeightedClassifierWrapperWithApplicationParams)
		{
			resClassifier = (WeightedClassifierWrapperWithApplicationParams)classifier;
		}
		else
			resClassifier = new WeightedClassifierWrapperWithApplicationParams(classifier);
		resClassifier.curApplicationParams.alphaCut = 
				applicationParameters.multiLabelOutputRefinementParams != null ? applicationParameters.multiLabelOutputRefinementParams.alphaCut : 1.0;
		return resClassifier;
		
	}

	
	protected void refineLexiconFromAmbiguousTerms(Map<String, double[]> annotatedTermEmotions)
	{
		Set<String> ambTerms = CategoryUtil.detectAmbiguousTerms(annotatedTermEmotions, emotionCategoriesType.name());
		System.out.println("There were detected " + ambTerms.size() + " ambiguous terms");
		
		for (String term : ambTerms)
			annotatedTermEmotions.remove(term);
	}
	
	protected void dropLowProbablitiesInLexicon(
			Map<String, double[]> resultTermEmotionAssociation) {
		TweetCorpusAnnotationProcessing.refineAnnotationWithAlphaLowerCut(resultTermEmotionAssociation, curLearnerParams.lowProbabilitiesThreshold); 
	}

}

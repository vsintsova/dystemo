/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.given;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import learners.definitions.ClassifierLearnerForCategory;
import learning.parameters.ClassifierLearnerParams;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureSelectionParameters;

import utils.CategoriesBinarySplitting;
import utils.TweetCorpusAnnotationProcessing;
import utils.UtilBinaryLearnerLoader;
import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.BinaryClassifierLexiconBased;
import classification.definitions.classifiers.wrappers.CategoryClassifierWrapper;
import data.categories.CategoryProcessor;
import data.categories.ICategoriesData;
import functionality.UtilArrays;

public class ClassifierLearnerForOneCategoryOnIndependent extends ClassifierLearnerIndependentCategories implements ClassifierLearnerForCategory {

	
	public ClassifierLearnerForOneCategoryOnIndependent(
			List<String> indicatorCandidates,
			ClassifierLearnerParams learnerParams,
			FeatureExtractionParameters featureExtractionParams,
			FeatureSelectionParameters featureSelectionParams) {
		super(indicatorCandidates, learnerParams, featureExtractionParams,
				featureSelectionParams);
	}

	public int focusCategoryId = 0;

	@Override
	public WeightedClassifier learnClassifierOnRefinedData (
			Map<Integer, double[]> annotatedTweetEmotions) throws Exception { 
		
		if (focusCategoryId == 0)
		{
			System.out.println("Initialize the category first!");
			return null;
		}
		
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(emotionCategoriesType); 
		
		Map<Integer, List<Integer>> emotionLabels = 
				TweetCorpusAnnotationProcessing.
					getEmotionLabelsFromWeights(annotatedTweetEmotions, 
							curLearnerParams.multiLabelInitialRefinementParams != null ? curLearnerParams.multiLabelInitialRefinementParams.alphaCut : 1.0,
									curLearnerParams.multiLabelInitialRefinementParams != null ? curLearnerParams.multiLabelInitialRefinementParams.emotionLimit : categoriesData.getCategoryNum());
		
		
		Map<Integer, Double> annotatedTweetWeights = new LinkedHashMap<Integer, Double>();
		for (Map.Entry<Integer, double[]> tweetEntry : annotatedTweetEmotions.entrySet())
		{
			annotatedTweetWeights.put(tweetEntry.getKey(), UtilArrays.getSum(tweetEntry.getValue()));
		}
		
		BinaryLexiconConstruction binLexConstruction = 
				UtilBinaryLearnerLoader.
					getInstanceOfLexiconConstruction(
							curLearnerParams.binaryLearnerParameters.binaryLearnerName,
							curLearnerParams.binaryLearnerParameters.binaryLexiconParams);
		
		
		CategoryClassifierWrapper classifier = null;
		try {
			// constructing binary model for the selected category
			
				System.out.println();
				System.out.println("Started consructing the classifier for category " + focusCategoryId);
				System.out.println();
				
				Map<Integer,Integer> categoryLabelData = CategoriesBinarySplitting.
						getBinaryDataForCategory(
								CategoryProcessor.getCategoriesData(emotionCategoriesType), 
								focusCategoryId, emotionLabels, 
								curLearnerParams.closeCategoryUseType,
								curLearnerParams.closeCategoryCalculation);
					
				Map<String, Double> categoryWeightedLexicon = binLexConstruction.extractBinaryLexiconFromRepresentation(categoryLabelData, tweetRepresentation, annotatedTweetWeights);
			
				BinaryClassifierLexiconBased categoryClassifier = new BinaryClassifierLexiconBased(categoryWeightedLexicon);
				
				
				classifier = new CategoryClassifierWrapper(categoryClassifier, focusCategoryId);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return classifier;
	}
	
	@Override
	public void setCategoryId(int categoryId) {
		this.focusCategoryId = categoryId;
	}
	

}

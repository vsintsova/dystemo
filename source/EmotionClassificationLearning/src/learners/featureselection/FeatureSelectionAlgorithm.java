/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.featureselection;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import learning.parameters.FeatureSelectionParameters;
import learning.parameters.FeatureSelectionParameters.BinarySelectionParams;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import learning.parameters.FeatureSelectionParameters.SelectionType;
import utils.DataRepresentationRefinement;
import classification.ClassificationUtil;
import data.categories.CategoryProcessor.CategoryDataType;
import functionality.UtilArrays;

public class FeatureSelectionAlgorithm {

	public static Map<Integer,List<String>> selectFeaturesInRepresentation(
			FeatureSelectionParameters featureSelectionParams, CategoryDataType categoriesType,
			Map<Integer, double[]> annotatedTweetEmotions, Map<Integer,List<String>> originalTweetRepresentation) throws Exception
	{
		Map<Integer,List<String>> tweetRepresentation = null;
	
	//	DataRepresentationRefinement.printConfidenceScores(originalTweetRepresentation, annotatedTweetEmotions, annotatedTweetWeights, annotatedTweetPolarities, tweetData);
		boolean originalChanged = false;
		
		for (SelectionType selType : featureSelectionParams.selectionTypesToApply)
		{
			switch (selType)
			{
				case OnPolarity: {
					
					// build the polarity annotation for each tweet
					Map<Integer, Integer> annotatedTweetPolarities = new LinkedHashMap<Integer, Integer>();
					Map<Integer, Double> annotatedTweetWeights = new LinkedHashMap<Integer, Double>();
					for (Map.Entry<Integer, double[]> tweetEntry : annotatedTweetEmotions.entrySet())
					{
						annotatedTweetPolarities.put(tweetEntry.getKey(), ClassificationUtil.getOnePolarity(tweetEntry.getValue(), categoriesType.name()));
						annotatedTweetWeights.put(tweetEntry.getKey(), UtilArrays.getSum(tweetEntry.getValue()));
					}
					

					tweetRepresentation = selectFeaturesFromBinaryRepresentation(
									(originalChanged) ? tweetRepresentation : originalTweetRepresentation,
									annotatedTweetPolarities,
									annotatedTweetWeights, 
									featureSelectionParams.polaritySelectionParams
							);
			
					originalChanged = true;
					break;
				}
				case OnEmotionality:
				{
					// build the emotionality annotation for each tweet
					Map<Integer, Integer> annotatedTweetEmotionality = new LinkedHashMap<Integer, Integer>(); // +1 - if Emotional, -1 - if Neutral
					Map<Integer, Double> annotatedTweetWeights = new LinkedHashMap<Integer, Double>();
					for (Map.Entry<Integer, double[]> tweetEntry : annotatedTweetEmotions.entrySet())
					{
						int emotinalityLabel = ClassificationUtil.checkIfEmotional(tweetEntry.getValue(), categoriesType.name())?1:-1;
						annotatedTweetEmotionality.put(tweetEntry.getKey(), emotinalityLabel);
						annotatedTweetWeights.put(tweetEntry.getKey(), UtilArrays.getSum(tweetEntry.getValue()));
					}
					
					tweetRepresentation = selectFeaturesFromBinaryRepresentation(
							(originalChanged) ? tweetRepresentation : originalTweetRepresentation,
							annotatedTweetEmotionality,
							annotatedTweetWeights, 
							featureSelectionParams.emotionalitySelectionParams
					);
						
					originalChanged = true;
					break;
				}
				case OnOccurrence:
				{
					tweetRepresentation = (originalChanged) ? tweetRepresentation : new LinkedHashMap<Integer, List<String>>(originalTweetRepresentation);
					DataRepresentationRefinement.deleteLowFrequencyTermsFromRepresentation(
							tweetRepresentation,  annotatedTweetEmotions, featureSelectionParams.minOccurrenceThreshold);
					originalChanged = true;
					break;
				}
			}
		}
		
		if (!originalChanged)
			return originalTweetRepresentation;
		
		return tweetRepresentation;
	}
	
	
	
	public static Map<Integer,List<String>> selectFeaturesFromBinaryRepresentation(
										Map<Integer,List<String>> initialTweetRepresentation,
										Map<Integer, Integer> annotatedTweetPolarities,
										Map<Integer, Double> annotatedTweetWeights,
										BinarySelectionParams binaryFeatureSelectionParams
									) throws Exception
	{
		if (binaryFeatureSelectionParams.selectionAlg == FeatureSelectionAlgOption.No)
			return initialTweetRepresentation; // no changes
		
		if (binaryFeatureSelectionParams.selectionAlg == FeatureSelectionAlgOption.PMI)
		{	
			return DataRepresentationRefinement.
					detectAndDeletePolarityAmbiguousTermsFromRepresentationWithPMI(
						initialTweetRepresentation,
						annotatedTweetPolarities,
						annotatedTweetWeights, 
						binaryFeatureSelectionParams
						);
		}
		else if (binaryFeatureSelectionParams.selectionAlg == FeatureSelectionAlgOption.PMISimple)
		{
			return DataRepresentationRefinement.
					detectAndDeletePolarityAmbiguousTermsFromRepresentationWithPMISimple(
						initialTweetRepresentation,
						annotatedTweetPolarities,
						annotatedTweetWeights, 
						binaryFeatureSelectionParams.minOccurNum,
						binaryFeatureSelectionParams.scoreThreshold,
						binaryFeatureSelectionParams.applySmoothing
						);
		}
		else
			throw new Exception("Other feature selection methods for polarity besides PMI are not implemented!");
		
			
		
	}
}

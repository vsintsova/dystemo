/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.featureextraction;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import processing.TweetPreprocessing;

import linguistic.UtilText;
import functionality.UtilCollections;
import functionality.UtilString;

public class LinguisticLexiconRefinement {

	public static int maxNgamLength = 5; // parameter
	public static int minTermLength = 2; // parameter
	
	static Set<String> emoStopWords;
	
	
	static
	{
		setDefaultEmoStopWords();
	}
	
	public static void setDefaultEmoStopWords()
	{		
		String[] preprocessingResStopWords =
			{
				"<username>", "<int_num>", "<link>", "link", "<year>", "\\n", "\n", "rt", "vs", "<int_num><int_num>", 
				"ai", "<ext>", ".<username>", ".<int_num>", "<int_num>.", "<float_num>", "im", "<num>"
			};		
		
		String[] stopWords = {"no", "not", "or", "the", "a", "have", "do", "is", "and", "'s", "'ve", "'m", "'d", "'re", "'ll"};
		// need to delete 
		emoStopWords = new HashSet<String>();
		
		for (String word : stopWords)
			emoStopWords.add(word);
		for (String word : preprocessingResStopWords)
			emoStopWords.add(word);
	}
	
	
	
	
	public static void dropNotLexiconTerms(List<String> curTerms)
	{
		Set<String> termsToRemove = new HashSet<String>();
		for (String term : curTerms)
		{
			int wordLength = UtilString.getWordNumber(term);
			if ( (UtilText.containsOnlyPunctuations(term)) || (emoStopWords.contains(term)) || (term.length() < minTermLength) || (UtilText.containsOutDigit(term)) || (UtilText.isStopWord(term)))
				termsToRemove.add(term);
			else if (wordLength > 1)
				if (UtilText.containsOnlyStopWordsAndGiven(term, emoStopWords))
					termsToRemove.add(term);
		}
		//System.out.println("There were removed " + termsToRemove.size() + " non-lexicon terms");
		curTerms.removeAll(termsToRemove);
	}
	
	public static String dropNotLexiconTerms(String text)
	{
		List<String> tokens = TweetPreprocessing.tokenizeWithoutPreprocessing(text);
		dropNotLexiconTerms(tokens);
		return UtilCollections.join(tokens, " ");
	}
	
	/**
	 * Updates the stop words, the previously set will be removed
	 * @param stopWordsToUse
	 */
	public static void setUsedListOfEmoStopWords(Set<String> stopWordsToUse)
	{
		emoStopWords = new HashSet<String>(stopWordsToUse);
	}
	
	/**
	 * The new stop words will be added into the given basic stop words list.
	 * Note that this list includes rather domain-specific or preprocessing-specific stop words, such as <username>. 
	 * The detection of stop words will in any case additionally rely on more standard stop words enumerated in UtilText.stopWords
	 * @param additionalStopWordsToUse
	 */
	public static void addNewEmoStopWords(Set<String> additionalStopWordsToUse)
	{
		emoStopWords.addAll(additionalStopWordsToUse);
	}
	
	
	public static void removeGivenTerms(List<String> sequence, Set<String> givenTerms) {
		sequence.removeAll(givenTerms);
	}
	
	

	
}

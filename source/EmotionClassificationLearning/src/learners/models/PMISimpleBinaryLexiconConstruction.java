/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.models;

import java.util.List;
import java.util.Map;

import utils.PMICalculation;

/**
 * While constructing such lexicon, the term score is computed not as Strength of Association, but directly as PMI with the positive class.
 * @author Erian
 *
 */
public class PMISimpleBinaryLexiconConstruction extends PMIBinaryLexiconConstruction {

	
	public PMISimpleBinaryLexiconConstruction(String lexParams) {
		super(lexParams);
	}

	/**
	 * Use the computed sentiment labels in order to extract the subjectivity lexicon
	 * parameters:
	 * sentimentLabels - map with tweet ids as keys, and sentiment labels (1 or -1) as value
	 * textData - map with tweet ids as keys, and tweet text as value
	 * IMPORTANT: in this case the parameters of NgramLength and freqLim will be ignored!
	 */
	@Override
	public Map<String, Double> extractBinaryLexiconFromRepresentation(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> textData, Map<Integer, Double> objectWeights) throws Exception
	{
		Map<String, Double> PMIScores =  PMICalculation.extractPMIValuesWithSimple(sentimentLabels, textData, objectWeights, numToOccur, threshold, applySmoothing);
		return PMIScores;
	}
}

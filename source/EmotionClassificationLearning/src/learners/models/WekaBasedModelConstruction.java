/*
    Copyright 2016 Valentina Sintsova, Marina Boia
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.models;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import classification.featurerepresentation.WekaAttributesSet;
import utils.WekaUtil;
import weka.classifiers.Classifier;
import weka.classifiers.rules.ZeroR;
import weka.core.Instances;
import weka.filters.Filter;

public class WekaBasedModelConstruction {

	Classifier wekaClassifier = null;
	
	int topKAttributesToSave = 0; 	// The number of attributes to keep after ranking. If all, specify 0
	
	public WekaBasedModelConstruction(Classifier wekaClassifier)
	{
		this.wekaClassifier = wekaClassifier;
	}
	
	protected Classifier extractModelFromRepresentationOld(Instances instances) throws Exception
	{
		// Rank attributes 
		instances = keepTopKAttributes(instances);
				
		
		if (WekaUtil.performCrossValidation)
		{
			//if (this.topKAttributesToSave <= 0)
				WekaUtil.crossValidateModel(wekaClassifier, instances);
			/*else
			{
				Filter curFilter = getCurAttributeFilter(instances);
				WekaUtil.crossValidateModelFiltered(wekaClassifier, instances, curFilter);
			}*/
        	
		}

			
		// Build the classifier
		buildClassifier(instances);
		WekaUtil.evaluate(wekaClassifier, instances);
		
		return wekaClassifier;
	}
	
	public Classifier extractModelFromRepresentation(Instances instances) throws Exception
	{
		
		if (WekaUtil.performCrossValidation)
		{
			if (this.topKAttributesToSave <= 0)
				WekaUtil.crossValidateModel(wekaClassifier, instances);
			else
			{
				Filter curFilter = getCurAttributeFilter(instances);
				WekaUtil.crossValidateModelFiltered(wekaClassifier, instances, curFilter);
			}
        	
		}

		// Rank attributes 
		instances = keepTopKAttributes(instances);
			
		// Build the classifier
		buildClassifier(instances);
		//WekaUtil.evaluate(wekaClassifier, instances);
		
		return wekaClassifier;
	}
	
	/**
	 * Use the computed sentiment labels in order to extract the subjectivity lexicon
	 * parameters:
	 * sentimentLabels - map with tweet ids as keys, and sentiment labels (1 or -1) as value
	 * textData - map with tweet ids as keys, and tweet tokens (the word or phrases which will be used in classification) as value
	 */
	public Classifier extractModelFromRepresentation(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> textData, Map<Integer, Double> objectWeights, WekaAttributesSet attributesSet) throws Exception
	{
		// Load train data
		Instances instances = loadInstancesForSentiment(sentimentLabels, textData, objectWeights, attributesSet);
		return extractModelFromRepresentation(instances);
	}
	
	/**
	 * Use the computed sentiment labels in order to extract the subjectivity lexicon with information gain refinement first
	 * parameters:
	 * sentimentLabels - map with tweet ids as keys, and sentiment labels (1 or -1) as value
	 * textData - map with tweet ids as keys, and tweet tokens (the word or phrases which will be used in classification) as value
	 */
	public Classifier extractModelFromRepresentationWithIGFiltering(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> textData, Map<Integer, Double> objectWeights, WekaAttributesSet attributesSet) throws Exception
	{
		topKAttributesToSave = 10000;
		// Load train data
		Instances instances = loadInstancesForSentiment(sentimentLabels, textData, objectWeights, attributesSet);
		return extractModelFromRepresentation(instances);
	}
	
	/**
	 * Use the computed sentiment labels in order to extract the subjectivity lexicon
	 * parameters:
	 * sentimentLabels - map with tweet ids as keys, and sentiment labels (1 or -1) as value
	 * textData - map with tweet ids as keys, and tweet tokens (the word or phrases which will be used in classification) as value
	 */
	public Classifier extractModelFromRepresentationWithFeatures(Map<Integer, Integer> sentimentLabels, Map<Integer, Map<Integer, Integer>> featureData, Map<Integer, Double> objectWeights, WekaAttributesSet attributesSet) throws Exception
	{
		// Load train data
		Instances instances = loadInstancesForSentimentWithFeatures(sentimentLabels, featureData, objectWeights, attributesSet);
		return extractModelFromRepresentation(instances);
	}
	
	/**
	 * Use the emotion labels in order to construct the emotion recognition model
	 * parameters:
	 * emotionLabels - map with tweet ids as keys, and emotion labels (category ids) as value
	 * textData - map with tweet ids as keys, and tweet tokens (the word or phrases which will be used in classification) as value
	 * 
	 * nonConsideredCategories - those emotion categories that will be ignored in the evaluation step
	 */
	public Classifier extractModelFromRepresentation(
			Map<Integer, List<Integer>> emotionLabels, Map<Integer, List<String>> textData, Map<Integer, Double> objectWeights, 
			WekaAttributesSet attributesSet, List<Integer> emotionCategories, Set<Integer> nonConsideredCategories) throws Exception
	{
		// Load train data
		Instances instances = loadInstancesForEmotions(emotionLabels, textData, objectWeights, attributesSet, emotionCategories, nonConsideredCategories);
		return extractModelFromRepresentation(instances);
	}
	
	public static Instances loadInstancesForSentiment
		(Map<Integer, Integer> sentimentLabels, Map<Integer, List<String>> textData, Map<Integer, Double> objectWeights, WekaAttributesSet attributesSet) throws Exception
		{
			System.out.println("Loading train data");
			Date start = new Date();
			Instances instances = WekaUtil.loadInstancesWithSpecificBinaryData(sentimentLabels, textData, objectWeights, false, attributesSet);
		
			System.out.println("Done: " + instances.numAttributes() + " attributes");
			System.out.println(instances.numInstances() + " instances");
			System.out.println("Done in " + (new Date().getTime() - start.getTime())/60000.0 + " minutes; ");
			
			System.out.println();
		
			return instances;
		}
	
	protected Instances loadInstancesForEmotions
			(Map<Integer, List<Integer>> sentimentLabels,
			Map<Integer, List<String>> textData,
			Map<Integer, Double> objectWeights, WekaAttributesSet attributesSet, 
			List<Integer> categories, Set<Integer> nonConsideredCategories)
			throws Exception
	{
		System.out.println("Loading train data");
		Date start = new Date();
		
		Instances instances = WekaUtil.loadInstancesWithSpecificCategoryData(sentimentLabels, textData, objectWeights, attributesSet, categories, nonConsideredCategories);
		
		System.out.println("Done: " + instances.numAttributes() + " attributes");
		System.out.println(instances.numInstances() + " instances");
		System.out.println("Done in " + (new Date().getTime() - start.getTime())/60000.0 + " minutes; ");
		
		System.out.println();
		
		return instances;
	}
	
	protected Instances loadInstancesForSentimentWithFeatures
	(Map<Integer, Integer> sentimentLabels, Map<Integer, Map<Integer,Integer>> featureData, Map<Integer, Double> objectWeights, WekaAttributesSet attributesSet) throws Exception
	{
		System.out.println("Loading train data");
		Instances instances = WekaUtil.loadInstancesWithSpecificBinaryDataWithFeatures(sentimentLabels, featureData, objectWeights, false, attributesSet);
		
		System.out.println("Done: " + instances.numAttributes() + " attributes");
		System.out.println(instances.numInstances() + " instances");
		
		System.out.println();
		
		return instances;
	}
	
	protected Instances keepTopKAttributes(Instances instances) throws Exception
	{
		System.out.println("Ranking the attributes");
		
		Date start = new Date();
		
		instances = WekaUtil.keepTopKAttributes(instances, this.topKAttributesToSave);
		
		System.out.println("Done: " + instances.numAttributes() + " attributes, spent " + (new Date().getTime() - start.getTime())/60000.0 + " minutes ");
        
        System.out.println();
        
        return instances;
	}
	
	protected Filter getCurAttributeFilter(Instances instances)  throws Exception
	{
		return WekaUtil.getFilterForTopKAttributes(instances, this.topKAttributesToSave);
	}
	
	protected void buildClassifier(Instances instances)
	{
		System.out.println("Building the classifier");
        
		Date start = new Date();
		
		try {
			wekaClassifier.buildClassifier(instances);
		} catch (Exception e) {
			e.printStackTrace();
			// set the baseline classifier
			wekaClassifier = null; //new ZeroR();
			
		}
		        
		System.out.println("Done in " + (new Date().getTime() - start.getTime())/60000.0 + " minutes; ");
		        
		System.out.println();
	}
}

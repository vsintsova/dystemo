/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.definitions;

import java.util.List;
import java.util.Map;

import learners.featureextraction.FeatureExtractionAlgorithm;
import learners.featureextraction.RepresentationCache;
import learners.featureselection.FeatureSelectionAlgorithm;
import learning.parameters.AllLearningParameters;
import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.ClassifierLearnerParams;
import learning.parameters.FeatureExtractionParameters;
import learning.parameters.FeatureSelectionParameters;


import utils.RebalancePreprocess;
import utils.TweetCorpusAnnotationProcessing;
import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams.HierarchyApplyOption;

import functionality.UtilCollections;

import data.documents.Tweet;

public abstract class ClassifierLearnerWithInternalRepresentation extends ClassifierLearner{
	
	FeatureExtractionParameters featureExtractionParams;
	FeatureSelectionParameters featureSelectionParams;
	
	public ClassifierLearnerWithInternalRepresentation(List<String> indicatorCandidates, ClassifierLearnerParams learnerParams)
	{
		super(indicatorCandidates, learnerParams);
		
		featureExtractionParams = AllLearningParameters.currentSetup.featureExtractionParams;
		featureSelectionParams = AllLearningParameters.currentSetup.featureSelectionParams;
	}
	
	public ClassifierLearnerWithInternalRepresentation(List<String> indicatorCandidates, 
			ClassifierLearnerParams learnerParams, 
			FeatureExtractionParameters featureExtractionParams,
			FeatureSelectionParameters featureSelectionParams)
	{
		super(indicatorCandidates, learnerParams);
		this.featureExtractionParams = featureExtractionParams;
		this.featureSelectionParams = featureSelectionParams;
	}
	
	public Map<Integer,List<String>> originalTweetRepresentation = null; // the tweet representation before refinement
	protected Map<Integer,List<String>> tweetRepresentation = null;
	
	@Override
	public void initialize(List<? extends Tweet> tweetData)
	{
		super.initialize(tweetData);
		initializeTokenRepresentation();
		initializeNextIteration();
	}
	
	public void initializePredefinedTokenRepresentation(Map<Integer,List<String>> givenRepresentation)
	{
		tweetRepresentation = givenRepresentation;
		originalTweetRepresentation = tweetRepresentation;
	}
	
	private void initializeTokenRepresentation()
	{
		if (featureExtractionParams.useCache)
			initializeCachedTokenRepresentation();
		else
			initializeTokenRepresentationByAlg();
		originalTweetRepresentation = tweetRepresentation;
	}
	
	private void initializeTokenRepresentationByAlg()
	{
		tweetRepresentation = FeatureExtractionAlgorithm.extractFeaturesFromData(tweetData, featureExtractionParams, indicatorCandidates);
	}
	
	private void initializeCachedTokenRepresentation()
	{
		Map<Integer, List<String>> foundCachedData = RepresentationCache.getCurrentCachedRepresentation();
		if (foundCachedData != null)
			tweetRepresentation = UtilCollections.copyListMap(foundCachedData);
		else
		{
			initializeTokenRepresentationByAlg();
			RepresentationCache.saveNewCurrentRepresentation(UtilCollections.copyListMap(tweetRepresentation));
		}
	}
	
	boolean featuresSelected = false;
	
	protected void preselectFeatures(Map<Integer, double[]> annotatedTweetEmotions) throws Exception
	{
		if (!featuresSelected && indicatorCandidates == null &&
				featureSelectionParams.selectionTypesToApply != null)
		{
			tweetRepresentation = FeatureSelectionAlgorithm.selectFeaturesInRepresentation(featureSelectionParams, emotionCategoriesType, annotatedTweetEmotions, originalTweetRepresentation);
		}
		featuresSelected = true;
	}
	
	@Override
	public void initializeNextIteration()
	{
		featuresSelected = false;
	}
	
	@Override
	public WeightedClassifier learnClassifier(
			Map<Integer, double[]> annotatedTweetEmotions) throws Exception 
	{
		if (annotatedTweetEmotions == null || annotatedTweetEmotions.size() == 0)
			return null;
		
		
		refineAnnotation(annotatedTweetEmotions);
		
		preselectFeatures(annotatedTweetEmotions);
		
		if (learnerParams.doRebalance())
			RebalancePreprocess.rebalanceAnnotation(annotatedTweetEmotions, tweetRepresentation, learnerParams.rebalancingParameters);
		
		WeightedClassifier classifier = learnClassifierOnRefinedData(annotatedTweetEmotions);
		
		classifier = updateOutputClassifierForApplicationParameters(classifier, learnerParams.outputClassifierApplicationParameters);
		return classifier;
	}
	
	abstract protected WeightedClassifier learnClassifierOnRefinedData(Map<Integer, double[]> annotatedTweetEmotions) throws Exception;
	
	@Override
	public void clear() 
	{
		if (tweetRepresentation != null)
			tweetRepresentation.clear();
		if (originalTweetRepresentation != null)
			originalTweetRepresentation.clear();
		tweetRepresentation = null;
		originalTweetRepresentation = null;
	}
	
	protected void refineAnnotation(Map<Integer, double[]> annotatedTweetEmotions)
	{	
		if (learnerParams.multiLabelInitialRefinementParams != null)
		{
			// need to do initial refinement
			try
			{
				TweetCorpusAnnotationProcessing.refineAnnotationWithAlphaHigherCutAndLimit(annotatedTweetEmotions, learnerParams.multiLabelInitialRefinementParams.alphaCut, learnerParams.multiLabelInitialRefinementParams.emotionLimit);
			}
			catch (Exception e)
			{
				System.out.println("Was not able to refine annotation data!");
				e.printStackTrace();
			}
		}
	}
	
	public abstract WeightedClassifier updateOutputClassifierForLearnerSpecificApplicationParameters(WeightedClassifier classifier, ClassifierApplicationParameters applicationParameters);
	
	public WeightedClassifier updateOutputClassifierForApplicationParameters(WeightedClassifier classifier, ClassifierApplicationParameters applicationParameters)
	{
		if (applicationParameters.parametersOfTermDetectionForClassifier.requiresChangeInDetection())
			classifier.setupTermDetectionParameters(applicationParameters.parametersOfTermDetectionForClassifier);
		
		classifier = updateOutputClassifierForLearnerSpecificApplicationParameters(classifier, applicationParameters);
		
		if ( applicationParameters.hierarchyOption != HierarchyApplyOption.None && (classifier instanceof WeightedClassifierWrapperWithApplicationParams))
		{
			((WeightedClassifierWrapperWithApplicationParams) classifier).curApplicationParams.hierarchyOption = applicationParameters.hierarchyOption;
		}
		return classifier;
	}
	
	
	
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learners.definitions;

import java.util.List;
import java.util.Map;

import learning.parameters.ClassifierApplicationParameters;
import learning.parameters.ClassifierLearnerParams;

import parameters.EmotionRecognitionParameters;
import utils.UtilCategoryLabelFormatter;

import classification.definitions.WeightedClassifier;
import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;
import data.documents.Tweet;


/** updates the classifier using annotatedTweetEmotions (for the train data),
 *  indicatorCandidates (for the list of the possible indicators),
 *   and the tweetData (for the texts)*/
public abstract class ClassifierLearner 
{
	/* GIVEN INPUT PARAMETERS */
	// a list of terms which will be considered emotional. If null - all occurred terms (except stop-words) will be considered as potentially emotional.
	List<String> indicatorCandidates = null;
	
	/* INTERNAL USED VARIABLES */
	List<? extends Tweet> tweetData = null;
	Map<Integer, double[]> annotatedTweetEmotions = null;
	
	protected CategoryDataType emotionCategoriesType = EmotionRecognitionParameters.defaultEmotionCategoriesType;
	protected ClassifierLearnerParams learnerParams;
	
	public CategoryDataType getEmotionCategoriesType()
	{
		return emotionCategoriesType;
	}

	/**
	 * If emotion categories is undefined, the default from EmotionRecognitionParameters.defaultEmotionCategoriesType will be loaded.
	 * @param learnerParams
	 */
	public ClassifierLearner (ClassifierLearnerParams learnerParams)
	{
		this.learnerParams = learnerParams;
	}
	
	public ClassifierLearner (ClassifierLearnerParams learnerParams, CategoryDataType emotionCategoriesType)
	{
		this.learnerParams = learnerParams;
		this.emotionCategoriesType = emotionCategoriesType;
	}
	
	public ClassifierLearner (List<String> indicatorCandidates, ClassifierLearnerParams learnerParams)
	{
		this.indicatorCandidates = indicatorCandidates;
		this.learnerParams = learnerParams;
	}
	
	public ClassifierLearner (List<String> indicatorCandidates, ClassifierLearnerParams learnerParams, CategoryDataType emotionCategoriesType)
	{
		this.indicatorCandidates = indicatorCandidates;
		this.learnerParams = learnerParams;
		this.emotionCategoriesType = emotionCategoriesType;
	}
	
	public void initialize(List<? extends Tweet> tweetData)
	{
		clear();
		this.tweetData = tweetData;
	}
	
	/** Learns a classifier using the given data
	 * @throws Exception */
	public abstract WeightedClassifier learnClassifier(Map<Integer, double[]> annotatedTweetEmotions) throws Exception;
	
	/** Learns a classifier using the given data. 
	 * It first transforms the given labels into the weighted format.
	 * @throws Exception */
	public WeightedClassifier learnClassifierOnStrictLabels (Map<Integer, List<Integer>> annotatedTweetEmotions) throws Exception
	{
		Map<Integer, double[]> weightedTweetEmotions = UtilCategoryLabelFormatter.transformStrictAnnotationsIntoWeighted(
				CategoryProcessor.getCategoriesData(emotionCategoriesType), annotatedTweetEmotions);
		return this.learnClassifier( weightedTweetEmotions);
	}


	public abstract void initializeNextIteration();

	public void clear()
	{
		this.tweetData = null;
		this.annotatedTweetEmotions = null;
	}
	
	public abstract WeightedClassifier updateOutputClassifierForApplicationParameters(WeightedClassifier classifier, ClassifierApplicationParameters applicationParameters);
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers.wrappers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

import learning.parameters.BinaryLearnerParameters.ProbabilisticOutputTreatment;

import classification.definitions.classifiers.WeightedClassifierWekaBasedIndependentFully;
import classification.featurerepresentation.WekaAttributesSet;

import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;

public class ClassifierWekaBasedIndependentFullyWithProbabilities
	extends WeightedClassifierWekaBasedIndependentFully
{

	private static final long serialVersionUID = -232244894575168142L;

	public ClassifierWekaBasedIndependentFullyWithProbabilities(
			List<Classifier> wekaClassifiersForCategories,
			List<WekaAttributesSet> attributesSets,
			List<Instances> correspondingBinaryProblems) {
		super(wekaClassifiersForCategories, attributesSets, correspondingBinaryProblems);
	}
	
	public ClassifierWekaBasedIndependentFullyWithProbabilities(
			WeightedClassifierWekaBasedIndependentFully initial) {
		super(initial);
	}

	ProbabilisticOutputTreatment curProbParams;
	
	public void setProbabilityLimit(double limit)
	{
		curProbParams = new ProbabilisticOutputTreatment ();
		curProbParams.useProbabilities = true;
		curProbParams.probabilityThreshold = limit;
	}
	
	@Override
	public double findSpecificClassWeight (Classifier wekaClassifierForCategory, Instance inst) throws Exception  
	{
		double classValue = 0.0;
		if (wekaClassifierForCategory != null)
		{
			double[] weights = wekaClassifierForCategory.distributionForInstance(inst); 
			if (weights[1] >= curProbParams.probabilityThreshold)
				classValue = 1.0;
		}
		return (classValue > 0) ? 1.0 : 0.0;
	}
	
	public static ClassifierWekaBasedIndependentFullyWithProbabilities readFromBinaryFile(String filename) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(filename));
		Object classifier = ois.readObject();	
		ois.close();
		
		ClassifierWekaBasedIndependentFullyWithProbabilities resClassifier = (ClassifierWekaBasedIndependentFullyWithProbabilities)classifier;
		
		return resClassifier;
	}
}

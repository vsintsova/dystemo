/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers.wrappers;

import java.io.Serializable;
import java.util.List;

import linguistic.TermDetectionParameters;
import classification.definitions.WeightedClassifier;

public class WeightedClassifierWrapper extends WeightedClassifier  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3454938652166329694L;
	WeightedClassifier internalWeightedClassifier;
	
	public WeightedClassifierWrapper(WeightedClassifier classifier)
	{
		internalWeightedClassifier = classifier;
		this.emotionCategoriesType = classifier.getCategoriesData().getCategoriesType();
	}
	
	@Override
	public String getName()
	{
		return internalWeightedClassifier.getName();
	}
	
	@Override
	public double[] findCategoryWeights(String text, boolean toPreprocessText)
			throws Exception {
		return internalWeightedClassifier.findCategoryWeights(text, toPreprocessText);
	}

	@Override
	public void printToTextFile(String filename) {
		internalWeightedClassifier.printToTextFile(filename);
		//TODO: add printing of the wrapper parameters in the beginning ?
	}

	@Override
	public List<String> getFeatureNames() {
		return internalWeightedClassifier.getFeatureNames();
	}

	@Override
	public int getFeatureNumber() {
		return internalWeightedClassifier.getFeatureNumber();
	}

	@Override
	public WeightedClassifier clone() {
		return new WeightedClassifierWrapper(internalWeightedClassifier.clone());
	}

	@Override
	public void clear() {
		internalWeightedClassifier.clear();
		internalWeightedClassifier = null;
	}

	@Override
	public void setupTermDetectionParameters(
			TermDetectionParameters termDetectionParameters) {
		internalWeightedClassifier.setupTermDetectionParameters(termDetectionParameters);
	}

}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers.wrappers;

import java.util.List;

import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapper;

import data.categories.CategoryUtil;
import data.categories.ICategoriesData;


import utils.EmotionDistributionAdoption;

public class WeightedClassifierWrapperWithApplicationParams extends WeightedClassifierWrapper
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5759139109819716433L;

	public WeightedClassifierWrapperWithApplicationParams(
			WeightedClassifier classifier) {
		super(classifier);
	}

	public enum HierarchyApplyOption {None, PolarityBased, QuadrantBased};
	
	public class ApplicationParams
	{
		public double alphaCut = 1.0; // 1.0 corresponds to the choice of only the maximal weighted categories
		public HierarchyApplyOption hierarchyOption = HierarchyApplyOption.None;
		
	}
	
	public ApplicationParams curApplicationParams = new ApplicationParams();
	
	@Override
	public List<Integer> getDominantCategories(double[] weights) {
		
		if (curApplicationParams.hierarchyOption == HierarchyApplyOption.None)
		{
			return EmotionDistributionAdoption.findHighEmotionsByAlphaCutAndLimit(weights, curApplicationParams.alphaCut, 30);
		}
		else
		{
			double[] distrCopy = new double[weights.length];
			System.arraycopy(weights,0,distrCopy,0,weights.length);
			EmotionDistributionAdoption.leaveHighProbabilitiesByAlphaCutAndLimit(distrCopy, curApplicationParams.alphaCut, 30);
			
			// remove all weights from all categories not in the hierarchically dominant polarity or quadrant
			List<Integer> polarities = CategoryUtil.getDominantPolarityList(distrCopy, emotionCategoriesType);
			List<Integer> quadrants = CategoryUtil.getDominantQuadrantList(distrCopy, emotionCategoriesType);
			ICategoriesData categoriesData = this.getCategoriesData();
			for (int catId = 1; catId <= categoriesData.getCategoryNum(); ++catId)
			{
				if ( curApplicationParams.hierarchyOption == HierarchyApplyOption.PolarityBased &&
						!polarities.contains(categoriesData.getCategoryPolarity(catId)) )
					distrCopy[categoriesData.getCategoryIndexById(catId)] = 0.0;
				if ( curApplicationParams.hierarchyOption == HierarchyApplyOption.QuadrantBased &&
						!quadrants.contains(categoriesData.getCategoryQuadrant(catId)) )
					distrCopy[categoriesData.getCategoryIndexById(catId)] = 0.0;
			}
			
			return EmotionDistributionAdoption.findEmotionsGreaterThanThreshold(distrCopy, 0.0);
		}
		
		
	}

}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.evaluation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Vector;

import utility.Pair;

import classification.evaluation.ComplexEvaluation.EvaluationResult;
import data.documents.Tweet;
import functionality.UtilArrays;


public class EvaluationUtil {
	
	static boolean useFullRandom = false; // whether to change the splits between runs or not
	
	public static Vector<List<Integer>> separateIndexesRandomlyForCrossValidation(int size, int nFolds)
	{
		return separateIndexesRandomlyForCrossValidation(size, nFolds, useFullRandom);
	}
	
	private static List<Integer> getAllIndexesListInRandomOrder(int size, boolean useFullRandom)
	{
		List<Integer> allIds = new ArrayList<Integer>();
		for (int i = 0; i < size; ++i)
			allIds.add(i);
		
		Random rand;
		
		if (useFullRandom)
			rand = new Random();
		else
			rand = new Random(290931L);
		
		Collections.shuffle(allIds, rand);
		
		return allIds;
	}
	
	public static Vector<List<Integer>> separateIndexesRandomlyForCrossValidation(int size, int nFolds, boolean useFullRandom)
	{
		List<Integer> allIds = getAllIndexesListInRandomOrder(size, useFullRandom);
		
		int foldSize = size / nFolds;
		Vector<List<Integer>> resultFolds = new Vector<List<Integer>>();
		// For each fold
		for(int i = 0; i < nFolds; i++)
		{	
			int foldStart = i * foldSize;
			int foldEnd = (i < (nFolds - 1)) ? (i + 1) * foldSize : size;
			
			List<Integer> foldIds = new ArrayList<Integer>();
			for(int j = foldStart; j < foldEnd; j++)
			{		
				foldIds.add(allIds.get(j));
			}
					
			resultFolds.add(foldIds);
		}
		
		return resultFolds;
	}
	
	/** Randomly selects the indexes up to 'size' in the amount of subpartPercentag*size 
	 * useFullRandom: whether to change the split between runs or not;
	 * 
	 * Returns: the pair, of which the first element is the list of selected indexes; and the second - the remaining indexes**/
	public static Pair<List<Integer>, List<Integer>> selectIndexesSubpartRandomly (int size, double subpartPercentage, boolean useFullRandom)
	{
		List<Integer> allIds = getAllIndexesListInRandomOrder(size, useFullRandom);
		
		long subpartSize = Math.round(subpartPercentage * size);
		List<Integer> randIds = UtilArrays.subList(allIds, 0, (int)subpartSize - 1);
		List<Integer> remainIds = UtilArrays.subList(allIds, (int)subpartSize, size - 1);
		return new Pair(randIds, remainIds);
	}
	
	
	
	/** Returns those objects that are not in the indexes within the given fold indexes **/
	public static<T extends Object> List<T> getTrainDataForFold(List<T> allData, Vector<List<Integer>> folds, int foldId)
	{
		if (foldId < 0 || foldId >= folds.size())
			return null;
		
		List<T> trainData = new ArrayList<T>();
		
		for (int i = 0; i < folds.size(); ++i)
		{
			if (i == foldId)
				continue;
			trainData.addAll(applyIdsList(allData, folds.get(i)));
		}
		
		return trainData;
	}
	
	/** Returns those objects that are within the indexes within the given fold indexes **/
	public static<T extends Object> List<T> getTestDataForFold(List<T> allData, Vector<List<Integer>> folds, int foldId)
	{
		if (foldId < 0 || foldId >= folds.size())
			return null;
		
		List<T> testData = applyIdsList(allData, folds.get(foldId));
		
		return testData;
	}
	
	/**
	 * Returns the sublist of 'allData' with the elements at the indices specified in 'ids'
	 * @param allData
	 * @param ids The indexes to find
	 * @return
	 */
	public static<T extends Object> List<T> applyIdsList(List<T> allData, List<Integer> ids)
	{
		List<T> subData = new ArrayList<T>();
		for (Integer id : ids)
			subData.add(allData.get(id));
		return subData;
	}
	
	public static Map<Integer, List<Integer>> findSelectedAssignmentSubset(Map<Integer, List<Integer>> allCategoryAssignment, List<Tweet> selectedTweets)
	{
		Map<Integer, List<Integer>>  selectedAssignment = new HashMap<Integer, List<Integer>> ();
		for (Tweet tweet : selectedTweets)
		{
			selectedAssignment.put(tweet.tweetId, allCategoryAssignment.get(tweet.tweetId));
		}
		return selectedAssignment;
	}
	
	public static void printEvaluationResults (EvaluationResult evalResult)
	{
		System.out.println("Evaluation Results: ");
		System.out.println(evalResult.toString());
		System.out.println();
	}
	
}

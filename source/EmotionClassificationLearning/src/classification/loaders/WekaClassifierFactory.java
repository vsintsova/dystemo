/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.loaders;

import weka.classifiers.bayes.NaiveBayesMultinomial;
import weka.classifiers.functions.LibLINEAR;
import weka.classifiers.trees.RandomForest;

public class WekaClassifierFactory {
	public enum AvailableWekaClassifiers {NaiveBayesMultinomial, LogisticRegression, RandomForest, DecisionTree, SVM};
	
	public static weka.classifiers.Classifier getWekaClassifierByName (AvailableWekaClassifiers classifierType)
	{
		return  getWekaClassifierByName (classifierType, null);
	
	}
	
	public static AvailableWekaClassifiers getWekaClassifierByNameFromLearnerName(String classifierName)
	{
		AvailableWekaClassifiers wekaClassifierType = null;
		
		switch (classifierName)
		{
			case "WekaLogReg":  wekaClassifierType = AvailableWekaClassifiers.LogisticRegression; break;
			case "WekaSVM": wekaClassifierType = AvailableWekaClassifiers.SVM; break;
			case "WekaMNB": wekaClassifierType = AvailableWekaClassifiers.NaiveBayesMultinomial; break;
		}
		
		return wekaClassifierType;
	}
	
	public static boolean fl_supportsProbabilisticOutput(AvailableWekaClassifiers classifierType)
	{
		return (classifierType == AvailableWekaClassifiers.LogisticRegression || classifierType == AvailableWekaClassifiers.NaiveBayesMultinomial);
	}
	
	public static weka.classifiers.Classifier getWekaClassifierByName (AvailableWekaClassifiers classifierType, String classifierParams)
	{
		weka.classifiers.Classifier resClassifier = null;
		switch (classifierType)
		{
			case NaiveBayesMultinomial: 
			{
				NaiveBayesMultinomial mnbClassifier = new NaiveBayesMultinomial();
				if (classifierParams != null)
				{
					try {
						mnbClassifier.setOptions(weka.core.Utils.splitOptions(classifierParams));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				resClassifier = mnbClassifier;
				
				break;
			}
			case LogisticRegression: 
			{
				LibLINEAR libClassifier = new LibLINEAR();
				try {
					String parametersToSet = "-S 0" + (classifierParams!=null?classifierParams:""); // -P if with probabilities; -S 0: to set the Logistic Regression mode
					 //-W 1.0
					libClassifier.setOptions(weka.core.Utils.splitOptions(parametersToSet)); 
					libClassifier.setProbabilityEstimates(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				resClassifier = libClassifier;
				break;
			}
			case RandomForest:
			{
				RandomForest tmpClassifier = new RandomForest();
				if (classifierParams != null)
				{
					try {
						tmpClassifier.setOptions(weka.core.Utils.splitOptions(classifierParams));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				resClassifier = tmpClassifier;
				break;
			}
			case DecisionTree:
			{
				weka.classifiers.trees.J48 tmpClassifier = new weka.classifiers.trees.J48();
				if (classifierParams != null)
				{
					try {
						tmpClassifier.setOptions(weka.core.Utils.splitOptions(classifierParams));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				resClassifier = tmpClassifier;
				break;
			}
			case SVM:
			{
				
				LibLINEAR libClassifier = new LibLINEAR();
				try {
					String extraOptions = "";//"-W \"1 1000\" -C 2.0";
					String parametersToSet = "-S 4" + (classifierParams!=null?classifierParams:extraOptions); // -S 1 - L2-loss SVM, 4 - multi-class SVM by Crammer and Singer
					//-W 1.0
					libClassifier.setOptions(weka.core.Utils.splitOptions(parametersToSet)); 
					//libClassifier.setOptions(weka.core.Utils.splitOptions("-S 4")); // 1 - L2-loss SVM, 4 - multi-class SVM by Crammer and Singer
					if (classifierParams == null)
					{
						System.out.println("Options of SVM: " + extraOptions);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				resClassifier = libClassifier; 
		       
				break;
			}
			default: break;
		}
		return resClassifier;
	}
}

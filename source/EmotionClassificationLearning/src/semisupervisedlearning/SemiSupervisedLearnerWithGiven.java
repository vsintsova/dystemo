/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package semisupervisedlearning;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import learners.definitions.ClassifierLearner;
import semisupervisedlearning.parameters.SSLFrameworkParameters;
import classification.definitions.WeightedClassifier;
import data.documents.Tweet;

/*
 * This class allows to add labeled tweets into the semi-supervised learning
 */
public class SemiSupervisedLearnerWithGiven extends SemiSupervisedLearner {

	public SemiSupervisedLearnerWithGiven(SSLFrameworkParameters params) {
		super(params);
	}


	List<Tweet> givenTweets = new ArrayList<Tweet>();
	Map<Integer, double[]> givenAnnotatedTweetEmotions = null;
	
	public void storeAdditionalTweetEmotions(List<Tweet> givenTweets, Map<Integer, double[]> givenAnnotatedTweetEmotions)
	{
		this.givenTweets = givenTweets;
		this.givenAnnotatedTweetEmotions = givenAnnotatedTweetEmotions ;
	}
	
	@Override
	public void initialize(List<? extends Tweet> unlabeledTweetData, 
			   WeightedClassifier initialClassifier, 
			   ClassifierLearner classifierLearner) throws Exception
	{
		super.initializeOnlyEssential(unlabeledTweetData, initialClassifier, classifierLearner);
		List<Tweet> jointTweetData;
		if (tweetData == null)
			jointTweetData = givenTweets;
		else
		{
			jointTweetData = new ArrayList<Tweet>(tweetData);
			jointTweetData.addAll(givenTweets); //TODO: replace the ids!
		}
		this.classifierLearner.initialize(jointTweetData);
	}
	
	@Override
	protected void annotateDataSimply(WeightedClassifier classifier)
	{
		super.annotateDataSimply(classifier);
		for (Map.Entry<Integer, double[]> tweetEntry : givenAnnotatedTweetEmotions.entrySet())
		{
			annotatedTweetEmotions.put(tweetEntry.getKey(), tweetEntry.getValue()); 
		}
	}
}

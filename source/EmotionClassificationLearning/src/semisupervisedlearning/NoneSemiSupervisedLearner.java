/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package semisupervisedlearning;

import java.util.List;

import semisupervisedlearning.parameters.SSLFrameworkParameters;

import learners.definitions.ClassifierLearner;


import data.documents.Tweet;
import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams.HierarchyApplyOption;

/**
 * This learner is essentially doing nothing. It returns the unchanged copy of the initial classifier. 
 * However, if the application parameters can be set in initial classifier and such parameters are specified in the SSLParameters, they will be set to initial classifier. Run with unset parameters if undesired.
 * @author Erian
 *
 */
public class NoneSemiSupervisedLearner extends SemiSupervisedLearner{

	public NoneSemiSupervisedLearner(SSLFrameworkParameters params) {
		super(params);
		// TODO Auto-generated constructor stub
	}

	@Override
	public WeightedClassifier learn() throws Exception
	{
		WeightedClassifier resClassifier = initialClassifier.clone();
		
		classifierLearner.updateOutputClassifierForApplicationParameters(resClassifier, params.initialClassifierApplicationParams);// TODO: check if it works fine
		
		/*if (resClassifier instanceof WeightedClassifierWrapperWithApplicationParams)
		{
			if (SSLParameters.internalAllLearningParameters.learnerParams.multiLabelOutputRefinementParams != null)
				((WeightedClassifierWrapperWithApplicationParams)resClassifier).curApplicationParams.alphaCut = SSLParameters.internalAllLearningParameters.learnerParams.multiLabelOutputRefinementParams.alphaCut;
			//TOOD: add the negation here too!
			if (params.outputClassifierApplicationParams.hierarchyOption != HierarchyApplyOption.None)
			{
				((WeightedClassifierWrapperWithApplicationParams)resClassifier).curApplicationParams.hierarchyOption = params.outputClassifierApplicationParams.hierarchyOption;
			}
			
		}*/
		return resClassifier;
	}
	
	@Override
	public void initialize(List<? extends Tweet> unlabeledTweetData, 
			   WeightedClassifier initialClassifier, 
			   ClassifierLearner classifierLearner)
	{
		this.initialClassifier = initialClassifier;
		this.classifierLearner = classifierLearner; // implied to be null
		
		this.tweetData = unlabeledTweetData; // implied to be null
	}
	
	
}

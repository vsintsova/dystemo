/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package semisupervisedlearning;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import learners.definitions.ClassifierLearner;

import semisupervisedlearning.annotation.AnnotationCache;
import semisupervisedlearning.parameters.SSLFrameworkParameters;
import utils.ClassLimiter;
import utils.TextProcessingUtil;
import utils.TweetCorpusAnnotationProcessing;

import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.WeightedClassifierLexiconBased;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams;
import classification.definitions.classifiers.wrappers.WeightedClassifierWrapperWithApplicationParams.HierarchyApplyOption;

import data.documents.Tweet;
import functionality.UtilCollections;

public class SemiSupervisedLearner {
	
	/* GIVEN INPUT PARAMETERS */
	// data which will be used for learning
	
	// a given initial classifier, which will be used to annotate all data in the beginning
	WeightedClassifier initialClassifier = null;
	
	// a class storing the rules for the classifier update based on the provided data
	ClassifierLearner classifierLearner = null;
	
	/* INTERNAL USED VARIABLES */
	List<? extends Tweet> tweetData = null;
	Map<Integer, double[]> annotatedTweetEmotions = null;
	protected int iteration = 0;
	
	
	/* SET PARAMETERS */
	
	SSLFrameworkParameters params;
	
	public SemiSupervisedLearner(SSLFrameworkParameters params)
	{
		this.params = params;
	}
	
	
	protected void initializeOnlyEssential(List<? extends Tweet> unlabeledTweetData, 
			   WeightedClassifier initialClassifier, 
			   ClassifierLearner classifierLearner)
	{
		this.initialClassifier = initialClassifier;
		this.classifierLearner = classifierLearner;
		
		this.tweetData = unlabeledTweetData;
		
	}
	
	public void initialize(List<? extends Tweet> unlabeledTweetData, 
			   WeightedClassifier initialClassifier, 
			   ClassifierLearner classifierLearner) throws Exception
	{
		initializeOnlyEssential(unlabeledTweetData, initialClassifier, classifierLearner);
		this.classifierLearner.initialize(tweetData);
		
		//params = SSLParameters.sslFrameworkParameters;
	}
	
	public void setNewLearning(WeightedClassifier initialClassifier)
	{
		iteration = 0;
		this.initialClassifier = initialClassifier;
	}
	

	List<WeightedClassifier> savedConstructedClassifiersPerIteration;
	
	/** learns a classifier over the data via iterations*/
	public WeightedClassifier learn() throws Exception
	{
		WeightedClassifier curClassifier = initialClassifier;
		iteration = 0;
		while (iteration < params.maxIterationNum) //TODO: add some condition until which we do iterations in learning
		{
			annotateData(curClassifier);
			// the refinement was moved inside the classifier learner! 
			// if (params.refineAnnotation)
			//	refineAnnotation();
			
			if (params.limitToDominantEmotions)
				dropNonDominantEmotions();
			
			classifierLearner.initializeNextIteration();
			curClassifier = classifierLearner.learnClassifier(annotatedTweetEmotions);
			
			if (params.preserveInitialClassifierIfPossible)
				addInitialClassifierDataIfPossible(curClassifier);
			
			//save the result classifier
			if (params.evaluateLexiconsInIterations)
			{
				if (iteration == 0)
					savedConstructedClassifiersPerIteration = new ArrayList<WeightedClassifier>();
				savedConstructedClassifiersPerIteration.add(curClassifier);
				//TODO: print the classifiers after each iteration after the learning process
				/*
				curClassifier.printToTextFile("log/trained-classifier-"+ (iteration) + (SSLParameters.evaluateLexiconsInIterations?SSLEvaluationSaver.currentRunNum:"") +".txt");
				System.out.println("Test current classifier in iteration " + iteration);
				SSLEvaluationSaver.evaluateClassifierForIterationOverAllTestFiles(curClassifier, (iteration + 1));
				*/
			}
			
			iteration++;
		}
	
		return curClassifier;
	}
	
	private void dropNonDominantEmotions() 
	{
		ClassLimiter limiter = ClassLimiter.getDefaultClassLimiter();
		TweetCorpusAnnotationProcessing.dropNonDominantEmotionsFromAnnotation(annotatedTweetEmotions, limiter);
	}
	
	private void removeTweetsWithNegationsFromAnnotated()
	{
		// first, detect those tweets where negations are present among the selected ones
		for (Tweet tw : tweetData)
		{
			if (annotatedTweetEmotions.containsKey(tw.tweetId))
			{
				// check if this tweet contains negations
				if (TextProcessingUtil.isTextWithNegations(tw.text))
				{
					annotatedTweetEmotions.remove(tw.tweetId);
				}
			}
		}
		
	}

	/** annotated the given tweet data using the classifier*/
	protected void annotateData(WeightedClassifier classifier)
	{
		getCachedAnnotation();
		if (annotatedTweetEmotions == null)
			annotateDataSimply(classifier);
		
		if (params.dropTweetsWithNegationsInLearning)
		{
			// need to identify all tweets with negation terms and remove them
			removeTweetsWithNegationsFromAnnotated();
		}
	}
	
	protected void annotateDataSimply(WeightedClassifier classifier)
	{
		try
		{
			annotatedTweetEmotions = TweetCorpusAnnotationProcessing.annotateTweetCorpus(tweetData, classifier, params.ignoreNeutralInAnnotation); 
			TweetCorpusAnnotationProcessing.normalizeAnnotation(annotatedTweetEmotions);
			//EventDataFormatter.printNodesInfoIntoFIle("log/application-result" + iteration + ".csv", annotatedTweetEmotions);
		}
		catch (Exception e)
		{
			System.out.println("Was not able to annotate data with current classifier!");
			e.printStackTrace();
		}
	}
	
	protected void getCachedAnnotation()
	{
		annotatedTweetEmotions = null;
		if (params.useCacheForAnnotation && iteration == 0)
		{
			String dataClassifierName = initialClassifier.getName() + "_" + params.unlabeledDataName;
			Map<Integer, double[]> foundCachedData = AnnotationCache.getCachedAnnotation(dataClassifierName);
			if (foundCachedData != null)
				annotatedTweetEmotions = UtilCollections.copyArrayMap(foundCachedData);
			else
			{
				annotateDataSimply(initialClassifier);
				AnnotationCache.saveNewAnnotation(dataClassifierName, UtilCollections.copyArrayMap(annotatedTweetEmotions));
			}	
		}
	}
	
	protected void addInitialClassifierDataIfPossible(WeightedClassifier classifier)
	{
		if ((initialClassifier instanceof WeightedClassifierLexiconBased) && (classifier instanceof WeightedClassifierLexiconBased))
		{
			WeightedClassifierLexiconBased lexClassifier = (WeightedClassifierLexiconBased) classifier;
			WeightedClassifierLexiconBased inlexClassifier = (WeightedClassifierLexiconBased) initialClassifier;
			lexClassifier.addExtraLexiconTerms(inlexClassifier.getAllTermsData());
		}
	}

	public void clear() {
		tweetData.clear();
		annotatedTweetEmotions.clear();
		classifierLearner.clear();
	}
}

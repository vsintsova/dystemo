/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package semisupervisedlearning;

import java.util.ArrayList;
import java.util.List;

import semisupervisedlearning.parameters.SSLFrameworkParameters;

import learners.definitions.ClassifierLearner;

import classification.definitions.WeightedClassifier;

import data.documents.Tweet;
import functionality.UtilArrays;

public class SemiSupervisedLearnerWithNeutral extends SemiSupervisedLearner
{
	public SemiSupervisedLearnerWithNeutral(SSLFrameworkParameters params) {
		super(params);
	}

	List<? extends Tweet> neutralTweetData = null;
	
	public void setNeutralData(List<? extends Tweet> neutralTweetData)
	{
		this.neutralTweetData = neutralTweetData;
	}
	
	protected List<Tweet> initializeWithNeutralAndReturnJointData(List<? extends Tweet> unlabeledTweetData, 
			   WeightedClassifier initialClassifier, 
			   ClassifierLearner classifierLearner) throws Exception
	{
		if (neutralTweetData == null)
			throw new Exception("Neutral data should be specified prior to initializing. Call setNeutralData method");
		this.initialClassifier = initialClassifier;
		this.classifierLearner = classifierLearner;
		
		this.tweetData = unlabeledTweetData;
		
		List<Tweet> jointTweetData = new ArrayList<Tweet>(tweetData);
		jointTweetData.addAll(neutralTweetData);
		return jointTweetData;
	}
	
	@Override
	public void initialize(List<? extends Tweet> unlabeledTweetData, 
			   WeightedClassifier initialClassifier, 
			   ClassifierLearner classifierLearner) throws Exception
	{
		
		List<Tweet> jointTweetData = initializeWithNeutralAndReturnJointData(unlabeledTweetData, initialClassifier, classifierLearner);
		this.classifierLearner.initialize(jointTweetData);
	}
	
	@Override
	protected void annotateDataSimply(WeightedClassifier classifier)
	{
		super.annotateDataSimply(classifier);
		int neutralInd = classifier.getCategoriesData().getCategoryIndexById(
				classifier.getCategoriesData().neutralCategory());
		try
		{
			for (Tweet tweet : neutralTweetData)
			{
				if (params.ignoreInitialAnnotationInNeutralTweets)
				{
					double[] neutralWeights = new double[classifier.getCategoriesData().getCategoryNum()];
					neutralWeights[neutralInd] = 1.0;
					annotatedTweetEmotions.put(tweet.tweetId, neutralWeights);
				}
				else
				{
					double[] weights = classifier.findCategoryWeights(tweet.text);
					if (UtilArrays.getSum(weights) == 0) // no terms are found
					{
						weights[neutralInd] = 1.0;
						annotatedTweetEmotions.put(tweet.tweetId, weights);
					}
					
					else 
					{
						// if neutral is found among the dominant emotions, also include this tweet as being Neutral
						List<Integer> dominantEms = UtilArrays.getIndexesOfMax(weights);
						if (dominantEms.contains(neutralInd))
						{
							weights = new double[weights.length];
							weights[neutralInd] = 1.0;
							annotatedTweetEmotions.put(tweet.tweetId, weights);
						}
					}
				}
			}
		}
		catch (Exception e)
		{
			System.out.println("Was not able to annotate neutral data with current classifier!");
			e.printStackTrace();
		}
	}
}

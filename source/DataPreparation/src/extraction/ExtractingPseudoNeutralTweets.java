/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package extraction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import processing.TweetPreprocessing;
import data.DatabaseWrapper;
import data.documents.Tweet;
import datapreparation.helpers.CleaningNeutralDataHelper;
import datapreparation.helpers.DataExtractionHelpers;
import datapreparation.helpers.DataExtractionHelpers.TweetWithPseudoNeutralInfo;

public class ExtractingPseudoNeutralTweets {

	/**
	 * Processes all the tweets in the initial table to find which of them contain URLs 
	 * (as detected by <link> presence in the preprocessed text). 
	 * It saves the found tweets into the resulting table.
	 * 
	 * The tweets in the initial table are assumed to be preprocessed.
	 * 
	 * This function will also compute some additional information on the tweet: 
	 * number of real words and whether some emotional cues are present. 
	 * We will later consider only those tweets as pseudo-neutral that do not contain any listed emotional cue.
	 * This extracted information will be stored in the resulting table. 
	 * 
	 * The resulting table should have the following columns format:
		  `id` bigint(20) NOT NULL,
		  `preprocessed_text` varchar(1000) NOT NULL,
		  `refined_text` varchar(1000) DEFAULT NULL,
		  `hasEmoCue` bit(1) DEFAULT NULL,
		  `forTest` bit(1) DEFAULT b'0',
		  `tSize` int(11) DEFAULT NULL,
		  `realWordNum` int(11) DEFAULT NULL
	 * 
	 * (The refined_text column will contain the preprocessed text of the tweet without the URL if toSaveRefinedText is true).
	 * 
	 * @param initialTable The name of the initial table from the database.
	 * @param resultingTable The name of the resulting table in the database.
	 * @param idColumn The name of the tweet id column in the initial table.
	 * @param preprocessedColumn The name of the preprocessed tweet text column in the initial table.
	 * @throws Exception
	 */
	public static void ExtractPseudoNeutralTweetsFromDatabaseTable(
			String initialTable, String resultingTable,
			String idColumn, String preprocessedColumn, boolean toSaveRefinedText) throws Exception
	{		
		int lastReturnedSize = 1;
		String emotionCuePresentColumnName = "hasEmoCue";

		int maxOneBenchSize = 100000;
		int offset = 0;
		boolean doOnce = false;
	
		while (lastReturnedSize > 0)
		{
			try
			{
				Date start = new Date();
				List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabaseUsingIdLimits(
						initialTable, offset, offset + maxOneBenchSize, idColumn, preprocessedColumn);
				
				lastReturnedSize = curBench.size();
				if (lastReturnedSize != 0)
				{
					List<TweetWithPseudoNeutralInfo> preprocessRes = new ArrayList<TweetWithPseudoNeutralInfo>();
					for (int twInd = 0; twInd < curBench.size(); ++twInd)
					{
						Tweet tweet = curBench.get(twInd);
						
						boolean hasURL = TweetPreprocessing.containsLinkInPreprocessed(tweet.text);
						
						if ( hasURL )
						{
							TweetWithPseudoNeutralInfo tw = new TweetWithPseudoNeutralInfo();
							tw.tweetId = tweet.tweetId;
							tw.text = tweet.text;
							tw.hasURL = true;
							tw.containsEmotionCues = CleaningNeutralDataHelper.isEmotionalCuePresent(tweet.text);
							if (toSaveRefinedText)
								tw.noURLText = TweetPreprocessing.removeAllLinksAfterPreprocessing(tweet.text);
							preprocessRes.add(tw);
						}
					}
					DataExtractionHelpers.InsertExtractedPseudoNeutralDataInTheDatabase(resultingTable, preprocessRes, emotionCuePresentColumnName, toSaveRefinedText);
					
					preprocessRes.clear();
					
					Date finish = new Date();
					System.out.println(finish.toString() + " : Processed offset is " + offset + ", found " + lastReturnedSize + 
							" entries, last EntryID = " + curBench.get(curBench.size() - 1).tweetId + 
							" It took " + (finish.getTime()-start.getTime())/60000.0 + " minutes; ");
					curBench = null;
					start = finish;
				}
			}
			catch(Exception e)
			{
				System.out.println("Error during data processing from offset " + offset + ": " + e.getMessage() + "\n\r Stack:" );
				e.printStackTrace();
				lastReturnedSize = 1;
			}
			offset += maxOneBenchSize;
			if (doOnce)
				lastReturnedSize = 0;
		}
	}
}

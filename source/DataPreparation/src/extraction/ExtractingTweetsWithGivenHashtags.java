/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package extraction;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import datapreparation.helpers.DataExtractionHelpers;
import datapreparation.helpers.DataExtractionHelpers.TweetWithExtraInfoOnIndicators;
import datapreparation.helpers.DataExtractionHelpers.TweetWithIndicatorEndingInfo;
import datapreparation.helpers.HashtagsEmojisDetailsHelper;
import processing.TweetPreprocessing;
import classification.definitions.lexicons.CategoryLexiconUnrestricted;
import data.DatabaseWrapper;
import data.documents.Tweet;

public class ExtractingTweetsWithGivenHashtags {

	/**
	 * Processes all the tweets in the initial table to find which of them contain given hashtags. 
	 * Saves the found tweets with the corresponding emotions into the resulting table.
	 * 
	 * The tweets in the initial table are assumed to be preprocessed.
	 * 
	 * This function will also compute some additional information on the tweet: 
	 * number of real words, whether the detected hashtags are at the end of the text, 
	 * and whether there are several detected hashtags. 
	 * This information will be stored in the resulting table. 
	 * Note that categoryId column will contain the combined information on the associated emotion to the detected hashtag. 
	 * If several given hashtags would appear in the text, the entry is repeated for each of them.
	 * 
	 * The resulting table should have the following columns format:
		  `id` bigint(20) NOT NULL,
		  `preprocessed_text` varchar(1000) NOT NULL,
		  `refined_text` varchar(1000) DEFAULT NULL,
		  `hashtag` varchar(100) DEFAULT NULL,
		  `categoryId` varchar(100) DEFAULT NULL,
		  `endPosition` bit(1) DEFAULT NULL,
		  `severalGivenHashtags` bit(1) DEFAULT NULL,
		  `forTest` bit(1) DEFAULT b'0',
		  `tSize` int(11) DEFAULT NULL
	 * 
	 * (The refined_text column will contain the text of the tweet without the given hashtags if toSaveRefinedText is true).
	 * 
	 * @param initialTable The name of the initial table from the database.
	 * @param resultingTable The name of the resulting table in the database.
	 * @param idColumn The name of the tweet id column in the initial table.
	 * @param preprocessedColumn The name of the preprocessed tweet text column in the initial table.
	 * @param hashtagLexicon The mapping of the given emotional hashtags to emotions, e.g. EmotionLexiconsGivenFactory.getLexiconOfGivenGEWHashtags()
	 * @throws Exception
	 */
	public static void ExtractHashtagsFromDatabaseTable(
			String initialTable, String resultingTable,
			String idColumn, String preprocessedColumn,
			CategoryLexiconUnrestricted hashtagLexicon,
			boolean toSaveRefinedText) throws Exception
	{		
		int lastReturnedSize = 1;
		String indicatorColumnName = "hashtag";
		String severalGivenIndicatorsColumnName = "severalGivenHashtags";

		int maxOneBenchSize = 100000;
		int offset = 0;
		boolean doOnce = false;
	
		Map<String, Set<Integer>> hashtagCategoryMap = hashtagLexicon.getMapTermToCategories();
		
		
		while (lastReturnedSize > 0)
		{
			try
			{
				Date start = new Date();
				List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabaseUsingIdLimits(
						initialTable, offset, offset + maxOneBenchSize, idColumn, preprocessedColumn);
				
				lastReturnedSize = curBench.size();
				if (lastReturnedSize != 0)
				{
					List<TweetWithExtraInfoOnIndicators> preprocessRes = new ArrayList<TweetWithExtraInfoOnIndicators>();
					for (int twInd = 0; twInd < curBench.size(); ++twInd)
					{
						Tweet tweet = curBench.get(twInd);
						
						List<String> hashtagsFound = TweetPreprocessing.getHashTags(tweet.text);
						Set<String>  setOfFoundGivenHashtags = new HashSet<String>();
						for (String hashtag : hashtagsFound)
						{
							 if (hashtagCategoryMap.containsKey(hashtag))
								 setOfFoundGivenHashtags.add(hashtag); 
						}
						
						if ( setOfFoundGivenHashtags.size() > 0)
						{
							TweetWithIndicatorEndingInfo tw = new TweetWithIndicatorEndingInfo();
							tw.tweetId = tweet.tweetId;
							tw.text = tweet.text;
							tw.extraIndicatorsFound =  setOfFoundGivenHashtags;
							tw.isIndicatorEnding = HashtagsEmojisDetailsHelper.defineIfHashtagsAreEndingInTweet(tweet.text, setOfFoundGivenHashtags);
							if (toSaveRefinedText)
								tw.noGivenIndicatorsText = HashtagsEmojisDetailsHelper.removeFoundHashtagsInTweet(tweet.text, setOfFoundGivenHashtags);
							preprocessRes.add(tw);
						}
					}
					DataExtractionHelpers.InsertExtractedIndicatorsDataInTheDatabase(resultingTable, preprocessRes, hashtagCategoryMap,  indicatorColumnName, severalGivenIndicatorsColumnName, toSaveRefinedText);
					
					preprocessRes.clear();
					
					Date finish = new Date();
					System.out.println(finish.toString() + " : Processed offset is " + offset + ", found " + lastReturnedSize + 
							" entries, last EntryID = " + curBench.get(curBench.size() - 1).tweetId + 
							" It took " + (finish.getTime()-start.getTime())/60000.0 + " minutes; ");
					curBench = null;
					start = finish;
				}
			}
			catch(Exception e)
			{
				System.out.println("Error during data processing from offset " + offset + ": " + e.getMessage() + "\n\r Stack:" );
				e.printStackTrace();
				lastReturnedSize = 1;
			}
			offset += maxOneBenchSize;
			if (doOnce)
				lastReturnedSize = 0;
		}
	}
	

	
	

}

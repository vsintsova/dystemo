/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package datapreparation.helpers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import linguistic.UtilText;

import processing.TweetPreprocessing;

public class HashtagsEmojisDetailsHelper {
	
	/**
	 * 
	 * @param tweetText
	 * @param foundHashtags should be without the hashtag symbol
	 * @return
	 */
	public static Map<String, Boolean> defineIfHashtagsAreEndingInTweet(String tweetText, Set<String> foundHashtags)
	{
		Map<String, Boolean> resEndingMap = new HashMap<String,Boolean>();
		
		List<String> tokens = TweetPreprocessing.tokenizeWithoutPreprocessing( tweetText.toLowerCase());
		
		for (String hashtag : foundHashtags)
		{
			boolean isCurHashtagEnding = defineIfHashtagIsEndingInTweet(tokens, hashtag);
			resEndingMap.put(hashtag, isCurHashtagEnding);
		}
		
		return resEndingMap;
	}
	
	public static String removeFoundHashtagsInTweet(String tweetText, Set<String> foundHashtags)
	{
		StringBuilder sb = new StringBuilder();
		List<String> tokens = TweetPreprocessing.tokenizeWithoutPreprocessing(tweetText.toLowerCase());
		
		for (String token : tokens) {
			if (!token.startsWith("#") || !foundHashtags.contains(token.substring(1)))
				sb.append(token + " ");
		}
		if (sb.length() > 0)
			return sb.toString().substring(0, sb.length() - 1);
		else
			return "";
	}
	
	public static boolean defineIfHashtagIsEndingInTweet(String tweetText, String hashtag)
	{
		List<String> tokens = TweetPreprocessing.tokenizeWithoutPreprocessing( tweetText.toLowerCase());
		return  defineIfHashtagIsEndingInTweet(tokens, hashtag);
	}
	
	public static boolean defineIfHashtagIsEndingInTweet(List<String> tweetTokens, String hashtag)
	{
		boolean hashTagInEnd = false;
		boolean hashTagFound = false;
		for (String token :  tweetTokens)
		{
			if (!hashTagFound && token.startsWith("#") && token.substring(1).equals(hashtag))
			{
				hashTagFound = true;
				hashTagInEnd = true;
				continue;
			}
			if (hashTagFound && !token.startsWith("#") && !token.startsWith("@") && !token.equals("<username>") && !UtilText.containsOnlyPunctuations(token) && !token.equals("<link>") && !token.startsWith("<emoji_") && !token.startsWith("<emot_")) // we consider the hashtag as ending even if some other hashtags are appearing afterwards
			{
				hashTagInEnd = false;
				break;
			}
		}
		return hashTagInEnd;
	}
	
	public static void initializeForEmojis()
	{
		emojiUnicode = TweetPreprocessing.getEmojiDatabase();
		
		emojiUnicodeToDescrMap = new HashMap<String, String>();
		
		for(int j=0;j<emojiUnicode.length;j++)
 		{
			emojiUnicodeToDescrMap.put(emojiUnicode[j][0].toLowerCase(), "<emoji_"+emojiUnicode[j][1]+">");
 		}
	}
	
	static String[][] emojiUnicode = null;
	/**
	 * The map assigning emoji unicode (lowercased) to the corresponding replacement name, e.g. <emoji_face1>
	 */
	static Map<String, String> emojiUnicodeToDescrMap = null;
	
	
	public static Map<String, Boolean> defineIfEmojisAreEndingInTweet(String tweetText, Set<String> foundEmojis, boolean useEmojisUnicodeWithPreprocessedText)
	{
		if (useEmojisUnicodeWithPreprocessedText && emojiUnicode == null)
			initializeForEmojis();
		
		Map<String, Boolean> resEndingMap = new HashMap<String,Boolean>();
		
		List<String> tokens = TweetPreprocessing.tokenizeWithoutPreprocessing( tweetText.toLowerCase());
		
		
		for (String emoji : foundEmojis)
		{
			String emoji_text;
			if (useEmojisUnicodeWithPreprocessedText)
				emoji_text = emojiUnicodeToDescrMap.get(emoji);
			else
				emoji_text = emoji;
		
			boolean isCurIndicatorEnding = defineIfEmojiIsEndingInTweet(tokens, emoji_text);
			resEndingMap.put(emoji, isCurIndicatorEnding);
		}
		
		return resEndingMap;
	}
	

	public static boolean defineIfEmojiIsEndingInTweet(List<String> tweetTokens, String emoji)
	{
		boolean emojiInEnd = false;
		boolean emojiFound = false;
		for (int tokenInd = tweetTokens.size() - 1; tokenInd >= 0; --tokenInd)
		{
			String token =  tweetTokens.get(tokenInd);
			if (!emojiFound && token.equals(emoji))
			{
				emojiFound = true;
				emojiInEnd = true;
				break;
			}
			else if (token.startsWith("#") || token.startsWith("@") || token.equals("<username>") || token.equals("<link>") || token.startsWith("<emoji_") || token.startsWith("<emot_")) // we consider the hashtag as ending even if some other hashtags are appearing afterwards
			{
				continue;
			}
			else
			{
				break;
			}
		}
		return emojiInEnd;
	}
	
	public static String removeFoundEmojisInTweet(String tweetText, Set<String> foundEmojis)
	{
		StringBuilder sb = new StringBuilder();
		List<String> tokens = TweetPreprocessing.tokenizeWithoutPreprocessing(tweetText.toLowerCase());
		
		for (String token : tokens) {
			if (!token.startsWith("<emoji") || !foundEmojis.contains(token))
				sb.append(token + " ");
		}
		if (sb.length() > 0)
			return sb.toString().substring(0, sb.length() - 1);
		else
			return "";
	}
}

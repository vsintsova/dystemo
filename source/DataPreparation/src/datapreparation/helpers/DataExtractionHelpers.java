/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package datapreparation.helpers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import processing.TweetPreprocessing;
import utility.Pair;
import data.DatabaseWrapper;
import data.documents.Tweet;
import functionality.UtilCollections;

public class DataExtractionHelpers {


	public static class TweetWithExtraInfoOnIndicators extends Tweet
	{
		public Set<String> extraIndicatorsFound;
		public int numberOfGeneralIndicatorsFound;
		public String noGivenIndicatorsText;
	}
	
	public static class TweetWithIndicatorEndingInfo extends TweetWithExtraInfoOnIndicators
	{
		public Map<String, Boolean> isIndicatorEnding; //will save whether the indicator given is at the end position in the text (at least in one of the occurrences)
	}
	
	public static class TweetWithPseudoNeutralInfo extends Tweet
	{
		public boolean containsEmotionCues;
		public boolean hasURL;
		public String noURLText;
	}
	
	/**
	 * Get tweets from the database. Besides the standard information on the id and text of the tweet, 
	 * this function extracts the value of the given additional column. 
	 * 
	 * @param tablename
	 * @param whereCondition
	 * @param idColumn
	 * @param textColumn
	 * @param extraInfoColumn
	 * @param minId
	 * @param maxId
	 * @return
	 * @throws Exception
	 */
	public static Pair<List<Tweet>, List<String>> getTweetsAndExtraInfoFromDatabase(
			String tablename, String whereCondition, String idColumn, String textColumn,
			String extraInfoColumn, int minId, int maxId) throws Exception
	{
		Connection conn =  DatabaseWrapper.getCurrentConnection();
		List<Tweet> allTweets = new ArrayList<Tweet>();
		List<String> extraInfoData = new ArrayList<String>();
		try
		{
		Statement stat = conn.createStatement();
		String curWhereCondition =  idColumn + ">=" + minId + " AND " + idColumn + " < " + maxId + " AND " + whereCondition;
		
		ResultSet rs = stat.executeQuery("select * from "+tablename+" where " + curWhereCondition+ ";");
		while (rs.next()) 
		{
			Tweet tw = new Tweet();
			tw.tweetId = rs.getInt(idColumn);
			tw.text = rs.getString( textColumn);
			String extraInfo = rs.getString(extraInfoColumn);
			
			allTweets.add(tw);
			extraInfoData.add(extraInfo);
		}
		rs.close();
		stat.close();
		}
		catch(Exception e)
		{
		throw e;
		}
		finally{
		conn.close();
		} 
		
		return new Pair(allTweets, extraInfoData);
	}
	

	public static void InsertExtractedIndicatorsDataInTheDatabase(
			String tablename, List<TweetWithExtraInfoOnIndicators> tweetData, 
			Map<String, Set<Integer>> indicatorToCategoryMap,
			String indicatorColumnName,
			String severalGivenIndicatorsColumnName,
			boolean toSaveRefinedText) throws Exception
	{
		Connection conn =  DatabaseWrapper.getCurrentConnection();
		try
		 {
		    PreparedStatement prep = conn.prepareStatement(
				      "INSERT INTO "+tablename +
				      " (`id`, `text`,  `"+indicatorColumnName+"`, `categoryId`, `"+severalGivenIndicatorsColumnName+"`, "
				      		+ "`endPosition`, `realWordNum` "+ (toSaveRefinedText?", `refined_text`":"") +") " +
				      " VALUES (?, ?, ?, ?, ?, ?, ?"+ (toSaveRefinedText?", ?":"") +")");
		    
		    for (TweetWithExtraInfoOnIndicators tweet :  tweetData)
			  {	 
		    	int tweetLen = TweetPreprocessing.getNumberOfRealWords(tweet.text, false, false); // we will compute the length of the tweet only in term of non-bad words (without reference to the dictionary)
	    		
		    	for (String extraIndicator : tweet.extraIndicatorsFound)
		    	{
		    		prep.setInt(1, tweet.tweetId);
					prep.setString(2, tweet.text);
					prep.setString(3, extraIndicator);
					prep.setString(4,  UtilCollections.join(indicatorToCategoryMap.get(extraIndicator), "," ));
					prep.setInt(5, (tweet.extraIndicatorsFound.size() > 1) ? 1: 0);
					prep.setInt(6, ((TweetWithIndicatorEndingInfo)tweet).isIndicatorEnding.get(extraIndicator)?1:0);
					prep.setInt(7, tweetLen);
					if (toSaveRefinedText)
						prep.setString(8, tweet.noGivenIndicatorsText);
					prep.addBatch();
		    	}
			  }
			  
			  conn.setAutoCommit(false);
			  prep.executeBatch();
			  conn.setAutoCommit(true);
		 }
		 catch (Exception e)
		 {
		    throw e;
		 }
		 finally{
		    conn.close();
		 } 
	}
	
	public static void InsertExtractedPseudoNeutralDataInTheDatabase(
			String tablename, List<TweetWithPseudoNeutralInfo> tweetData,
			String emotionCuePresentColumnName,
			boolean toSaveRefinedText) throws Exception
	{
		Connection conn =  DatabaseWrapper.getCurrentConnection();
		try
		 {
		    PreparedStatement prep = conn.prepareStatement(
				      "INSERT INTO "+tablename+
				      " (`id`, `text`,  `"+emotionCuePresentColumnName+"`, `realWordNum` "+ 
				    		  (toSaveRefinedText?", `refined_text`":"") +") " +
				      " VALUES (?, ?, ?, ?"+ (toSaveRefinedText?", ?":"") +")");
		    
		    for (TweetWithPseudoNeutralInfo tweet : tweetData)
			  {	 
		    	int tweetLen = TweetPreprocessing.getNumberOfRealWords(tweet.text, false, false); // we will compute the length of the tweet only in term of non-bad words (without reference to the dictionary)
	    		
		    	
		    		prep.setInt(1, tweet.tweetId);
					prep.setString(2, tweet.text);
					prep.setInt(3, tweet.containsEmotionCues ? 1 : 0);
					prep.setInt(4, tweetLen);
					if (toSaveRefinedText)
						prep.setString(5, tweet.noURLText);
					prep.addBatch();
		    	
			  }
			  
			  conn.setAutoCommit(false);
			  prep.executeBatch();
			  conn.setAutoCommit(true);
		 }
		 catch (Exception e)
		 {
		    throw e;
		 }
		 finally{
		    conn.close();
		 } 
	}
	
	
	
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/


package preprocessing;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cleaning.TextCleaning;
import processing.TweetPreprocessing;
import data.DataRepositoryIndex;
import data.DatabaseWrapper;
import data.LabeledData;
import data.documents.Tweet;
import data.loading.DataFilesFormatReader;
import data.loading.DataFilesFormatSaver;
import functionality.UtilCollections;

public class AdditionalDataCleaning {

	/**
	 * This function works only for the first 1000000 tweets in the table when no condition is given. Requires rewrite if to be applied to larger tables.
	 * @param tablename
	 * @param textColumn
	 * @param idColumn
	 * @param realWNumColumn
	 * @throws Exception
	 */
	public static void computeWordNumForAllTweets(String tablename, String textColumn, String idColumn, String realWNumColumn) throws Exception
	{
		computeWordNumForAllTweets(tablename, textColumn, idColumn, realWNumColumn, null);
	}
	
	/**
	 * This function works only for the first 1000000 tweets in the table when no condition is given. Requires rewrite if to be applied to larger tables.
	 * @param tablename
	 * @param textColumn
	 * @param idColumn
	 * @param realWNumColumn
	 * @param whereCondition
	 * @throws Exception
	 */
	public static void computeWordNumForAllTweets(String tablename, String textColumn, String idColumn, String realWNumColumn, String whereCondition) throws Exception
	{
		List<Tweet> curBench;
		if (whereCondition != null)
			curBench = DatabaseWrapper.getTweetsFromDatabaseWithCondition(tablename, whereCondition, idColumn, textColumn);
		else
			curBench = DatabaseWrapper.getTweetsFromDatabase(tablename, 0, 1000000, idColumn, textColumn);
		
		Map<Integer, String> preprocessRes = new HashMap<Integer, String>();
		for (Tweet tweet : curBench)
		{
			int tweetLen = TweetPreprocessing.getNumberOfRealWords(tweet.text, false, false); // we will compute the length of the tweet only in term of non-bad words (without reference to the dictionary)
			preprocessRes.put(tweet.tweetId, Integer.toString(tweetLen));
		}
		
		DatabaseWrapper.saveTweetPreprocessingResults(preprocessRes, tablename, idColumn, realWNumColumn);
	}
	

	public static void updateTestDataTextsWithNewPreprocessing(String testDataFileOrig, String testDataFileFinal, String databaseTablename) throws Exception
	{
		// read test data:
		Map<Integer, List<Integer>> emotionLabels = new HashMap<Integer, List<Integer>>();
		Map<Integer, String> textData = new HashMap<Integer, String> ();
		DataFilesFormatReader.readLabeledDataFromFile(testDataFileOrig, emotionLabels, textData);
		
		//save result tweets in a datafile
		// format: tweetId<tab>tweetText<tab>categoriesListInComma
		File tempFile = new File( testDataFileFinal);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
						
		for (Map.Entry<Integer, String> twEntry : textData.entrySet())
		{
			// Get original text of the tweet
			Tweet tweet = DatabaseWrapper.getTweetById(databaseTablename, twEntry.getKey());
			
			String newText = TweetPreprocessing.preprocessTextNovel(tweet.text);
			
			StringBuilder sb = new StringBuilder();
			sb.append(tweet.tweetId);
			sb.append("\t");
			sb.append(newText);
			sb.append("\t");
			sb.append(UtilCollections.join(emotionLabels.get(tweet.tweetId), ","));
			pw.println(sb.toString());
			
		}
		pw.close();
	}

	
	/**
	 * Get the preprocessed text of the the same data as in the original file, but with the novel preprocessing.
	 * @param dataFileOrig
	 * @param dataFileFinal
	 * @param databaseTablename
	 * @param idColumn
	 * @param textColumn
	 * @throws Exception
	 */
	public static void updateUnlabeledDataTextsWithNewPreprocessing(String dataFileOrig, String dataFileFinal, String databaseTablename, String idColumn, String textColumn) throws Exception
	{
		// read data:
		List<Tweet> tweetData = new ArrayList<Tweet>();
		DataFilesFormatReader.readTextDataFromFile(dataFileOrig, tweetData);
		
		// preprocess data:
		for (int twInd = 0; twInd < tweetData.size(); ++twInd)
		{
			Tweet initTweet = tweetData.get(twInd);
			Tweet tweet = DatabaseWrapper.getTweetById(databaseTablename, initTweet.tweetId, idColumn, textColumn);
			initTweet.text = TweetPreprocessing.preprocessTextNovel(tweet.text);
		}
		
		// save data:
		DataFilesFormatSaver.printTweetDataIntoFileAsGiven(dataFileFinal, tweetData);
	}
	
	/**
	 * 
	 * @param dataName
	 * @param minRealWordNum By default, we used 3
	 * @throws Exception
	 */
	public static void refineTweetDataFromShortTextsAndQuotes(String dataName, int minRealWordNum) throws Exception
	{
		LabeledData fullLabelData = DataRepositoryIndex.getLabeledDataByName(dataName);
		Map<Integer, String> tweetData = fullLabelData.textData;
		Map<Integer, List<Integer>> categoryAssignment = fullLabelData.emotionLabels;
		
		TextCleaning.toRemoveShortTweets(tweetData, minRealWordNum);
		TextCleaning.toRemoveTweetsWithQuotes(tweetData);
		
		DataFilesFormatSaver.printLabeledDataIntoFile("output/labeledData_" + dataName + "_noLong.txt", categoryAssignment, tweetData);
	}

}

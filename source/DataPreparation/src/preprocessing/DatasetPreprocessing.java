/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package preprocessing;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import processing.TweetPreprocessing;
import data.DatabaseWrapper;
import data.documents.Tweet;

public class DatasetPreprocessing {

	/**
	 * Preprocesses the original text of the tweets and saves the results in the database 
	 * by updating the cleanColumn of the corresponding tweets.
	 * Prior to calling this function, ensure that correct database connection is set for DatabaseWrapper
	 * 
	 * @param tablename The name of the table in the database
	 * @param idColumnName The column name of the column with tweet id 
	 * @param textColumnName The column name of the column with original tweet text
	 * @param cleanColumnName The column name of the column with the resultant preprocessed tweet text
	 * @throws Exception
	 */
	public static void preprocessTextInDatabaseTable (String tablename, String idColumnName, String textColumnName, String cleanColumnName) throws Exception
	{
		preprocessTextInDatabaseTable(tablename, idColumnName, textColumnName, cleanColumnName, true);
	}
	
	/**
	 * Preprocesses the original text of the tweets and saves the results in the database 
	 * by updating the cleanColumn of the corresponding tweets.
	 * Prior to calling this function, ensure that correct database connection is set for DatabaseWrapper
	 * 
	 * @param tablename The name of the table in the database
	 * @param idColumnName The column name of the column with tweet id 
	 * @param textColumnName The column name of the column with original tweet text
	 * @param cleanColumnName The column name of the column with the resultant preprocessed tweet text
	 * @param useNovelPreprocessing Whether to use new or old preprocessing.
	 * @throws Exception
	 */
	public static void preprocessTextInDatabaseTable (String tablename, String idColumnName, String textColumnName, String cleanColumnName, boolean useNovelPreprocessing) throws Exception
	{
		int lastReturnedSize = 1;

		int maxOneBenchSize = 50000;
		int offset = 0;
		boolean doOnce = false;
		
		System.out.println("Started preprocessing of tweet texts in the table " + tablename + " from offset " + offset);
		
		while (lastReturnedSize > 0)
		{
			try
			{
				Date start = new Date();
				List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabase(tablename, offset, maxOneBenchSize, idColumnName, textColumnName);
				lastReturnedSize = curBench.size();
				if (lastReturnedSize != 0)
				{
					Map<Integer, String> preprocessRes = new HashMap<Integer, String>();
					for (Tweet tweet : curBench)
					{
						String preprocRes = (useNovelPreprocessing? TweetPreprocessing.preprocessTextNovel(tweet.text) : TweetPreprocessing.preprocessText(tweet.text));
						preprocessRes.put(tweet.tweetId, preprocRes);
						
					}
					
					DatabaseWrapper.saveTweetPreprocessingResults(preprocessRes, tablename, idColumnName, cleanColumnName);
					preprocessRes.clear();
					
					Date finish = new Date();
					System.out.println(finish.toString() + " : Processed offset is " + offset + ", found " + lastReturnedSize + 
							" entries, last EntryID = " + curBench.get(curBench.size() - 1).tweetId + 
							" It took " + (finish.getTime()-start.getTime())/60000.0 + " minutes; ");
					curBench = null;
					start = finish;
				}
			}
			catch(Exception e)
			{
				System.out.println("Error during data processing from offset " + offset + ": " + e.getMessage() + "\n\r Stack:" );
				e.printStackTrace();
				lastReturnedSize = 1;
			}
			offset += maxOneBenchSize;
			if (doOnce)
				lastReturnedSize = 0;
		}
	}
	
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package preprocessing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cleaning.TextCleaning;
import processing.TweetPreprocessing;
import data.DatabaseWrapper;
import data.documents.Tweet;

public class DataPreprocessingForLabeling {

	public static boolean needToDecodeSpecialSymbols = false; // was true for other hashtagged data
	
	/** This process will replace only usernames and remove the given emotional hashtags.
	 * Note: With this method only the first 1000000 tweets will be processed! (otherwise, rewrite is needed)
	 * @throws Exception **/
	public static void preprocessTableDataForLabelingOnHashtags(String tablename, String initialTextColumn, String idColumn, String resultTextColumn) throws Exception
	{
		List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabase(tablename, 0, 1000000, idColumn, initialTextColumn);
		
		
		Map<Integer, String> preprocessRes = new HashMap<Integer, String>();
		for (Tweet tweet : curBench)
		{
			String preprocRes = TweetPreprocessing.replaceAllUserNames(tweet.text);
			preprocRes = TweetPreprocessing.getInitial(preprocRes);
			preprocRes = TweetPreprocessing.separateHashtagsFromOtherSymbols(preprocRes);
			preprocessRes.put(tweet.tweetId, preprocRes);
		}
		
		TextCleaning.toRemoveGivenEmotionHashTags(preprocessRes, true);
		
		// remove all emojis
		TextCleaning.toRemoveAllEmojis(preprocessRes);
		
		if (needToDecodeSpecialSymbols)
			TextCleaning.decodeSpecialSymbols(preprocessRes);
		
		DatabaseWrapper.saveTweetPreprocessingResults(preprocessRes, tablename, idColumn, resultTextColumn);
		
	}
	
	public static void preprocessTableDataForLabelingSimple(String tablename, String initialTextColumn, String idColumn, String resultTextColumn) throws Exception
	{
		List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabase(tablename, 0, 1000000, idColumn, initialTextColumn);
		
		
		Map<Integer, String> preprocessRes = new HashMap<Integer, String>();
		for (Tweet tweet : curBench)
		{
			String preprocRes = TweetPreprocessing.replaceAllUserNames(tweet.text);
			preprocRes = TweetPreprocessing.getInitial(preprocRes);
			preprocRes = TweetPreprocessing.separateHashtagsFromOtherSymbols(preprocRes);
			preprocessRes.put(tweet.tweetId, preprocRes);
		}
		
		if (needToDecodeSpecialSymbols)
			TextCleaning.decodeSpecialSymbols(preprocessRes);
		
		DatabaseWrapper.saveTweetPreprocessingResults(preprocessRes, tablename, idColumn, resultTextColumn);
		
	}
	
	public static void preprocessTableDataForLabelingOnLinks(String tablename, String initialTextColumn, String idColumn, String resultTextColumn) throws Exception
	{
		List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabase(tablename, 0, 1000000, idColumn, initialTextColumn);
		
		
		Map<Integer, String> preprocessRes = new HashMap<Integer, String>();
		for (Tweet tweet : curBench)
		{
			String preprocRes = TweetPreprocessing.replaceAllUserNames(tweet.text);
			preprocRes = TweetPreprocessing.getInitial(preprocRes);
			preprocRes = TweetPreprocessing.separateHashtagsFromOtherSymbols(preprocRes);
			preprocessRes.put(tweet.tweetId, preprocRes);
		}
		
		TextCleaning.toRemoveLinksFromTextsUnpreprocessed(preprocessRes);
		
		if (needToDecodeSpecialSymbols)
			TextCleaning.decodeSpecialSymbols(preprocessRes);
		
		DatabaseWrapper.saveTweetPreprocessingResults(preprocessRes, tablename, idColumn, resultTextColumn);
		
	}
	
	/** This process will replace only usernames and remove the given emotional hashtags.
	 * Note: With this method only the first 1000000 tweets will be processed! (otherwise, rewrite is needed)
	 * @throws Exception **/
	public static void preprocessTableDataForLabelingOnEmojis(String tablename, String initialTextColumn, String idColumn, String resultTextColumn) throws Exception
	{
		preprocessTableDataForLabelingOnEmojis(tablename, initialTextColumn, idColumn, resultTextColumn, null);
	}
	
	/** This process will replace only usernames and remove the given emotional hashtags.
	 * Note: With this method only the first 1000000 tweets will be processed! (otherwise, rewrite is needed)
	 * @throws Exception **/
	public static void preprocessTableDataForLabelingOnEmojis(String tablename, String initialTextColumn, String idColumn, String resultTextColumn, String whereCondition) throws Exception
	{
		List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabaseWithCondition(tablename, whereCondition, idColumn, initialTextColumn, 0, 1000000);
		
		
		Map<Integer, String> preprocessRes = new HashMap<Integer, String>();
		for (Tweet tweet : curBench)
		{
			String preprocRes = TweetPreprocessing.replaceAllUserNames(tweet.text);
			preprocRes = TweetPreprocessing.getInitial(preprocRes);
			preprocessRes.put(tweet.tweetId, preprocRes);
		}
		
		TextCleaning.toRemoveGivenEmojis(preprocessRes, true);
		
		DatabaseWrapper.saveTweetPreprocessingResults(preprocessRes, tablename, idColumn, resultTextColumn);
		
	}
	
	
	/** This process will replace only usernames and remove all the emojis from the text
	 * Note: With this method only the first 1000000 tweets will be processed! (otherwise, rewrite is needed)
	 * @throws Exception **/
	public static void preprocessTableDataByRemovingAllEmojis(String tablename, String initialTextColumn, String idColumn, String resultTextColumn, String whereCondition, int offset) throws Exception
	{
		List<Tweet> curBench = DatabaseWrapper.getTweetsFromDatabaseWithCondition(tablename, whereCondition, idColumn, initialTextColumn, offset, 1000000);
		
		Map<Integer, String> preprocessRes = new HashMap<Integer, String>();
		for (Tweet tweet : curBench)
		{
			preprocessRes.put(tweet.tweetId, TweetPreprocessing.getInitial(tweet.text));
		}
		
		TextCleaning.toRemoveAllEmojis(preprocessRes);
		
		DatabaseWrapper.saveTweetPreprocessingResults(preprocessRes, tablename, idColumn, resultTextColumn);
	}
			
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.featurerepresentation;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import data.documents.TweetFeaturesExtended;

/**
 * This is the class for representing additional features of the text, such as length, number of exclamation marks, ...
 *
 */
public class SyntacticFeaturesSet {

	public final static Map<String,Integer> mapNameToIndex;
	
	static 
	{
		mapNameToIndex = new LinkedHashMap<String,Integer>();
		mapNameToIndex.put("<ftLen>", 1);
		mapNameToIndex.put("<ftEmot>", 2);
		mapNameToIndex.put("<ftExcl>", 3);
		mapNameToIndex.put("<ftQstn>", 4);
		mapNameToIndex.put("<ftTabs>", 5);
		mapNameToIndex.put("<ftAlong>", 6);
	}
	
	public static Map<Integer,Integer> parseFeatureValues (TweetFeaturesExtended tweet)
	{
		Map<Integer,Integer> resFeatures = new HashMap<Integer,Integer>();
		resFeatures.put(1, tweet.length);
		resFeatures.put(2, (tweet.isEmoticonPresent)?1:0);
		resFeatures.put(3, tweet.numberOfExclMarks);
		resFeatures.put(4, tweet.numberOfQuestionMarks);
		resFeatures.put(5, tweet.numberOfTabWords);
		resFeatures.put(6, tweet.numberOfAlongations);
		
		return resFeatures;
	}
}

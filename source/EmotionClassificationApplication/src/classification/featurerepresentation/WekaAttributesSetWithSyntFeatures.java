/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.featurerepresentation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import weka.core.Attribute;

import classification.detectors.TermDetectorWithProcessing;

public class WekaAttributesSetWithSyntFeatures extends WekaAttributesSet implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6053948135146100801L;
	
	public WekaAttributesSetWithSyntFeatures (LinkedHashMap<String,Attribute> savedAttributeMap)
	{
		super(savedAttributeMap);
		addSyntacticFeatures();
	}
	
	public WekaAttributesSetWithSyntFeatures (WekaAttributesSet origSet)
	{
		this(origSet.savedAttributeMap);
	}
	
	
	private void addSyntacticFeatures()
	{
		syntFeaturesOffset = this.size() - 1;
		
		for (Map.Entry<String, Integer> syntFeat : SyntacticFeaturesSet.mapNameToIndex.entrySet())
		{
			String featName = syntFeat.getKey();
			Attribute curAttr = new Attribute(featName);
			savedAttributeMap.put(featName, curAttr);
			attributeIndexes.put(featName, syntFeaturesOffset + syntFeat.getValue());
		}
	}
	/*
	public WekaAttributesSetWithSyntFeatures (List<String> attributeNames)
	{
		super(attributeNames);
		addSyntacticFeatures();
	}*/
	
	public int size()
	{
		return getAttributeIndexes().size();
	}
	
	int syntFeaturesOffset = 0;
	
	public Map<Integer,Integer> updateSyntFeatureIndexes(Map<Integer,Integer> syntFeatures)
	{
		Map<Integer,Integer> newSyntFeatures = new HashMap<Integer,Integer>();
		for (Map.Entry<Integer, Integer> feat : syntFeatures.entrySet())
		{
			 newSyntFeatures.put(feat.getKey() + syntFeaturesOffset, feat.getValue());
		}
		return newSyntFeatures;
	}
	
}

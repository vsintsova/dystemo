/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.lexicons;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;
import functionality.UtilCollections;

/** Lexicon where each term corresponds to only one of the categories
 * (one term to one category)**/
public class CategoryLexicon {
	
	Map<String, Integer> mapTermToCategory;
	
	Map<Integer, Set<String>> mapCategoryToTerms;
	
	public Map<String, Integer> getMapTermToCategory()  throws Exception
	{
		return mapTermToCategory;
	}
	
	public Map<Integer, Set<String>> mapCategoryToTerms()
	{
		if (mapCategoryToTerms == null)
			createMapCategoryToTerms();
		return mapCategoryToTerms;
	}
	
	public Set<String> getAllLexiconTerms()
	{
		return mapTermToCategory.keySet();
	}
	
	/** 
	 * Lexicon where each term corresponds to only one of the categories.
	 * This constructor creates an empty lexicon.
	 * */
	public CategoryLexicon() 
	{
		mapTermToCategory = new HashMap<String, Integer>();
	};
	
	/** 
	 * Lexicon where each term corresponds to only one of the categories
	 * */
	public CategoryLexicon (String mapFile) throws Exception
	{
		this(mapFile, true);
	}
	
	/** 
	 * Lexicon where each term corresponds to only one of the categories
	 * */
	public CategoryLexicon (String mapFile, boolean toLowerCase) throws Exception
	{
		mapTermToCategory = new HashMap<String, Integer>();
		BufferedReader input =  new BufferedReader(new InputStreamReader(new FileInputStream(mapFile), "UTF-8"));
		
    	
		//CSVReader reader = new CSVReader(new FileReader(mapFile),  '\t');
		String [] nextLine;
		String line;
		while (( line = input.readLine()) != null)
	        
		//while ((nextLine = reader.readNext()) != null) 
		{
			nextLine = line.split("\t");
			if (nextLine[0].startsWith("#"))
				continue;
			mapTermToCategory.put(toLowerCase ? nextLine[0].toLowerCase() : nextLine[0], Integer.parseInt(nextLine[1])); 
		}
		//reader.close();
		input.close();
	}
	
	public CategoryLexicon (Map<Integer, Set<String>> mapCategoryToTerms) throws Exception
	{
		this.mapCategoryToTerms = mapCategoryToTerms;
		mapTermToCategory = new HashMap<String, Integer>();
		for (Integer category : mapCategoryToTerms.keySet())
		{
			Set<String> categoryTerms = mapCategoryToTerms.get(category);
			for (String term : categoryTerms)
			{
				mapTermToCategory.put(term, category);
			}
		}
	}
	
	public Set<String> getLexiconTermsForCategory(int categoryId)
	{
		return mapCategoryToTerms.get(categoryId);
	}
	
	private void deleteNonUnigramTerms()
	{
		Set<String> tmpToDelete = new HashSet<String>();
		
		for (String term : mapTermToCategory.keySet())
		{
			if (term.indexOf(' ') != -1)
				tmpToDelete.add(term);
		}
		for (String term : tmpToDelete)
		{
			mapTermToCategory.remove(term);
		}
	}
	
	private void extendTermsWithHashTags()
	{
		Map<String, Integer> tmpMapTermToCategory = new HashMap<String, Integer>();
		
		for (Map.Entry<String, Integer> termEntry : mapTermToCategory.entrySet())
		{
			String term =  termEntry.getKey();
			if (term.startsWith("#"))
				continue;
			String newHashTerm = "#" + term;
			if (!mapTermToCategory.containsKey(newHashTerm))
				 tmpMapTermToCategory.put(newHashTerm, termEntry.getValue());
		}
		mapTermToCategory.putAll(tmpMapTermToCategory);
	}
	
	public void createMapCategoryToTerms()
	{
		mapCategoryToTerms = new HashMap<Integer, Set<String>>();
		for (Map.Entry<String, Integer> termEntry : mapTermToCategory.entrySet())
		{
			UtilCollections.appendNewObjectToValueInMap(mapCategoryToTerms, termEntry.getValue(), termEntry.getKey());
		}
	}
	
	public void printFileInCategoryToLineFormat(String filename) throws Exception
	{
		deleteNonUnigramTerms();
		extendTermsWithHashTags();
		createMapCategoryToTerms();
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		for (Map.Entry<Integer, Set<String>> catEntry : mapCategoryToTerms.entrySet())
		{
			pw.println(catEntry.getKey() + " " + UtilCollections.join(catEntry.getValue(), " "));
		}	
		pw.close();
	}
	
	public void saveToMapFile(String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		UtilCollections.printToFileMap(this.mapTermToCategory, filename);
	}
	
	public static void rewriteInCatToLineFormat(String initialMapFile, String resFilename) throws Exception
	{
		CategoryLexicon cl = new CategoryLexicon(initialMapFile);
		cl.printFileInCategoryToLineFormat(resFilename);
	}

	public void saveToMapFileInCategoryOrder(String filename)
			throws Exception {
		throw new Exception("Not implemented!");
	}

	
	public static CategoryLexicon readCategoryLexiconFromFile(String filename) throws Exception
	{
		return new CategoryLexicon (filename);
	}
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers;

import java.util.List;

import linguistic.TermDetectionParameters;

import classification.ClassificationUtil;
import classification.definitions.BinaryClassifier;
import classification.definitions.BinaryClassifierInterface;
import classification.definitions.WeightedClassifier;
import classification.detectors.TermDetectorUtil;
import classification.detectors.TermDetectorWithNegations;
import data.categories.CategoryProcessor.CategoryDataType;

import processing.TweetPreprocessing;



public class WeightedClassifierIndependentFully extends WeightedClassifier {

	List<? extends BinaryClassifierInterface> categoryClassifiers;
	
	public WeightedClassifierIndependentFully (List<? extends BinaryClassifierInterface> categoryClassifiers)
	{
		this.categoryClassifiers = categoryClassifiers;
	}
	
	@Override
	public List<Integer> getDominantCategories(double[] weights) {
		return ClassificationUtil.getMaximalCategoriesIds(weights, this.emotionCategoriesType);
	}

	@Override
	public double[] findCategoryWeights(String text) throws Exception {
		return findCategoryWeights(text, defaultPreprocessing);
	}

	@Override
	public double[] findCategoryWeights(String text, boolean toPreprocessText)
			throws Exception {
		double[] resWeights = new double[this.getCategoriesData().getCategoryNum()];
		String classifiedText = "";
		if (toPreprocessText)
			classifiedText = TweetPreprocessing.preprocessText(text);
		else
			classifiedText = text;
		for (int catInd = 0; catInd < categoryClassifiers.size(); ++catInd)
		{		
			double classValue = categoryClassifiers.get(catInd).findClassIndex(classifiedText, false);
			resWeights[catInd] = (classValue > 0)?1.0:0.0;
		}
		
		return resWeights;
	}

	@Override
	public void printToTextFile(String filename) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<String> getFeatureNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getFeatureNumber() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public WeightedClassifier clone() {
		WeightedClassifier resClassifier = new WeightedClassifierIndependentFully(categoryClassifiers);
		resClassifier.defaultPreprocessing = this.defaultPreprocessing;
		return resClassifier;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setupTermDetectionParameters(TermDetectionParameters termDetectionParameters){

		if (termDetectionParameters.requiresChangeInDetection())
		{
			for (int catInd = 0; catInd < categoryClassifiers.size(); ++catInd)
			{
				((BinaryClassifier)(categoryClassifiers.get(catInd))).setupTermDetectionParameters(termDetectionParameters);
			}
		}
		
	}

}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers;

import weka.classifiers.Classifier;
import weka.core.Instance;
import classification.definitions.WeightedClassifier;

public abstract class AbstractWeightedClassifierWekaBased extends WeightedClassifier {

	/**
	 * This returns the computed weight for this category and this instance
	 * @param text
	 * @param toPreprocessText
	 * @return
	 * @throws Exception
	 */
	public double findSpecificClassWeight (Classifier wekaClassifierForCategory, Instance inst) throws Exception
	{
		double classValue = 0.0;
		if (wekaClassifierForCategory != null)
			classValue = wekaClassifierForCategory.classifyInstance(inst);
		return (classValue > 0)?1.0:0.0;
	}
}

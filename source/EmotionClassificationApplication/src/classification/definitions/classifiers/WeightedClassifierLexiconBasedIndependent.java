/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers;

import java.util.Map;

import classification.definitions.lexicons.WeightedEmotionLexicon;

import functionality.UtilArrays;

/** Serves as the classification for cases when the lexicon terms can have negative weights.
 * This will return the weight 1 to all emotions that has positive weight in the end //TODO: think how to normalize!!!
 * */
public class WeightedClassifierLexiconBasedIndependent extends WeightedClassifierLexiconBased{

	public boolean SVMBased = false;
	
	public WeightedClassifierLexiconBasedIndependent (WeightedEmotionLexicon lexicon)
	{
		super(lexicon);
	}
	
	public WeightedClassifierLexiconBasedIndependent (WeightedEmotionLexicon lexicon, boolean SVMBased)
	{
		super(lexicon);
		this.SVMBased = SVMBased;
	}
	
	
	/** Returns the sum of the weights of all found terms*/
	public double[] getTermSumCategoryWeights(String text, boolean toPreprocess) throws Exception
	{
		double[] categoryWeights = new double[lexicon.getCategoriesToUseNum()];
		
		Map<String, Integer> foundTerms = lexicon.findEmotionalTermsInText(text, toPreprocess);
		for (String term : foundTerms.keySet())
		{
			if (lexicon.containsTerm(term))
				categoryWeights = UtilArrays.sumTwoArrays(categoryWeights, 
													UtilArrays.multiplyArray(lexicon.getWeightForTerm(term), foundTerms.get(term)));
		}
		return categoryWeights;
	}
	
	@Override
	/** Returns the sum of the weights of all found terms*/
	public double[] findCategoryWeights(String text, boolean preprocessText) throws Exception
	{
		double[] categorySumWeights = getTermSumCategoryWeights(text, preprocessText);
		
		if (SVMBased)
		{
			// need to add bias!
			double[] biasWeight = lexicon.getWeightForTerm("<bias>");
			for (int i = 0; i < categorySumWeights.length;++i)
				categorySumWeights[i] += biasWeight[i];
		}
		
		double[] resultCategoryWeights = new double[categorySumWeights.length];
		
		for (int i = 0; i < categorySumWeights.length; ++i)
			 resultCategoryWeights[i] = (categorySumWeights[i] > 0)?1.0:0.0;
		
		return resultCategoryWeights;
	}

	
	public double[] getTermSumCategoryWeights(String text) throws Exception {
		return  getTermSumCategoryWeights(text, getDefaultPreprocessing());
	}
}

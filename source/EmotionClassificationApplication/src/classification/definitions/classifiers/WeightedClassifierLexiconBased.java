/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions.classifiers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import linguistic.TermDetectionParameters;

import classification.definitions.WeightedClassifier;
import classification.definitions.lexicons.WeightedEmotionLexicon;
import classification.detectors.TermDetectorUtil;

import functionality.UtilArrays;

public class WeightedClassifierLexiconBased extends WeightedClassifier {

	public WeightedEmotionLexicon lexicon;
	public boolean defaultPreprocessing = false;
	
	public WeightedClassifierLexiconBased (WeightedEmotionLexicon lexicon)
	{
		this.lexicon = lexicon;
		this.emotionCategoriesType = lexicon.categoriesData.getCategoriesType();
	}
	
	/** Returns the sum of the weights of all found terms*/
	public double[] findCategoryWeights(String text, boolean preprocessText) throws Exception
	{
		if (lexicon == null)
		{
			throw new Exception("Lexicon wasn't initialized correctly!");
		}
		double[] categoryWeights = new double[lexicon.getCategoriesToUseNum()];
		
		Map<String, Integer> foundTerms = lexicon.findEmotionalTermsInText(text, preprocessText);
		for (String term : foundTerms.keySet())
		{
			if (lexicon.containsTerm(term))
				categoryWeights = UtilArrays.sumTwoArrays(categoryWeights, 
													UtilArrays.multiplyArray(lexicon.getWeightForTerm(term), foundTerms.get(term)));
		}
		return categoryWeights;
	}
	
	
	/** Returns the sum of the weights of all found terms*/
	public double[] findCategoryWeights(String text) throws Exception
	{
		return findCategoryWeights(text, getDefaultPreprocessing());
	}
	
	public Map<String, double[]> findCategoryWeightsReasons(String text) throws Exception
	{
		Map<String, double[]> resfoundTerms = new HashMap<String, double[]>();
		
		Map<String, Integer> foundTerms = lexicon.findEmotionalTermsInText(text);
		for (String term : foundTerms.keySet())
		{
			resfoundTerms.put(term, lexicon.getWeightForTerm(term));
		}
		return resfoundTerms;
	}

	
	@Override
	public String toString()
	{
		return "WeightedClassifier-" + lexicon.name;
	}
	
	public WeightedEmotionLexicon getInternalLexicon()
	{
		return lexicon;
	}

	@Override
	public void printToTextFile(String filename)
	{
		
		try {
			lexicon.saveToTextFile(filename);
		} catch (IOException e) {
			System.out.println("No success in printing the lexicon into a file " + filename);
			e.printStackTrace();
		}
		
	}
	
	
	@Override
	public List<String> getFeatureNames() {
		return new ArrayList<String>(lexicon.allTerms());
	}
	
	@Override
	public WeightedClassifier clone() {
		return new WeightedClassifierLexiconBased(lexicon);
	}

	@Override
	public int getFeatureNumber() {
		
		return lexicon.lexiconData.size();
	}


	@Override
	public void clear() {
		lexicon.clear();
		lexicon = null;
	}
	
	public void addExtraLexiconTerms(Map<String, double[]> extraTermsData)
	{
		lexicon.addTerms(extraTermsData);
	}
	
	public Map<String, double[]> getAllTermsData()
	{
		return lexicon.getAllTermsData();
	}


	@Override
	public void setupTermDetectionParameters(TermDetectionParameters termDetectionParameters){

		if (termDetectionParameters.requiresChangeInDetection())
		{
			 lexicon.tDetector = TermDetectorUtil.updateTermDetectorForParameters(termDetectionParameters,  lexicon.tDetector);
		}
		
	}
	
}

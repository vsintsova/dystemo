/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.definitions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import linguistic.TermDetectionParameters;
import linguistic.TermDetector;

import parameters.EmotionRecognitionParameters;

import classification.ClassificationUtil;
import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;
import data.categories.ICategoriesData;
import data.documents.Tweet;
import functionality.UtilArrays;
import functionality.UtilCollections;


public abstract class WeightedClassifier implements WeightedClassifierInterface {
	
	public boolean defaultPreprocessing = false;
	private String classifierName;
	public boolean returnNoEmotionWhenNoFeaturesFound = true; // when no features are found in the text, it returns NoEmotion category among dominant categories if true, it returns empty set if false 
	
	protected CategoryDataType emotionCategoriesType = EmotionRecognitionParameters.defaultEmotionCategoriesType;

	/** Returns the weights for each emotion
	 * @throws Exception */
	public double[] findCategoryWeights(String text) throws Exception
	{
		return findCategoryWeights(text, defaultPreprocessing);
	}
	
	@Override
	public abstract double[] findCategoryWeights(String text, boolean toPreprocessText) throws Exception;
	
	@Override
	public double[] findCategoryWeights(Tweet tweet, boolean preprocessText)
			throws Exception {
		return findCategoryWeights(tweet.text, preprocessText);
	}

	@Override
	public double[] findCategoryWeights(Tweet tweet) throws Exception {
		return findCategoryWeights(tweet.text);
	}
	
	public abstract void printToTextFile(String filename);
	
	public abstract List<String> getFeatureNames();
	
	public abstract int getFeatureNumber();
	
	@Override
	public ICategoriesData getCategoriesData()
	{
		return CategoryProcessor.getCategoriesData(emotionCategoriesType);
	}
	
	public String getName()
	{
		return classifierName;
	}
	
	public void setName(String name)
	{
		this.classifierName = name;
	}
	
	@Override
	public abstract WeightedClassifier clone();
	
	public abstract void clear();
	
	@Override
	public List<Integer> getDominantCategories(double[] weights) {
		List<Integer> dominantCategoriesId = ClassificationUtil.getMaximalCategoriesIds(weights, emotionCategoriesType);
		if (returnNoEmotionWhenNoFeaturesFound)
		{
			// add neutral category if needed
			if (UtilArrays.getSum(weights) == 0.0)
			{
				dominantCategoriesId.add(CategoryProcessor.neutralCategory(getCategoriesData().getCategoriesType()));//add neutral category
			}
			
		}
		return dominantCategoriesId;
	}

	public abstract void setupTermDetectionParameters(TermDetectionParameters termDetectionParameters);
		

	/** returns the polarities with maximal sum weights or Neutral (0) if no weights are given */
	public List<Integer> getDominantPolarity(double[] categoryWeights)
	{
		if (UtilArrays.getSum(categoryWeights) == 0.0)
		{
			ArrayList<Integer> result  = new ArrayList<Integer>();
			result.add(0);
			return result;
		}
		Map<Integer,Double> polaritiesWeights = new HashMap<Integer,Double>();
		ICategoriesData categoriesData = getCategoriesData();
		for (int i = 0; i < categoryWeights.length; ++i)
		{
			int polarity = categoriesData.getCategoryPolarity(categoriesData.getCategoryId(i));
			UtilCollections.incrementIntValueInMap(polaritiesWeights, polarity, categoryWeights[i]);
		}
		return UtilCollections.findKeysWithMaxValues(polaritiesWeights);
	}
	
	public List<Integer> getDominantQuadrant(double[] categoryWeights)
	{
		if (UtilArrays.getSum(categoryWeights) == 0.0)
		{
			ArrayList<Integer> result  = new ArrayList<Integer>();
			result.add(0);
			return result;
		}
		Map<Integer,Double> quadrantsWeights = new HashMap<Integer,Double>();
		ICategoriesData categoriesData = getCategoriesData();
		for (int i = 0; i < categoryWeights.length; ++i)
		{
			int quadrant = categoriesData.getCategoryQuadrant(categoriesData.getCategoryId(i));
			UtilCollections.incrementIntValueInMap(quadrantsWeights, quadrant, categoryWeights[i]);
		}
		return UtilCollections.findKeysWithMaxValues(quadrantsWeights);
	}
	
	public Map<Integer,Double> getQuadrantWeights(double[] categoryWeights)
	{
		Map<Integer,Double> quadrantsWeights = new HashMap<Integer,Double>();
		ICategoriesData categoriesData = getCategoriesData();
		for (int i = 0; i < categoryWeights.length; ++i)
		{
			int quadrant = categoriesData.getCategoryQuadrant(categoriesData.getCategoryId(i));
			UtilCollections.incrementIntValueInMap(quadrantsWeights, quadrant, categoryWeights[i]);
		}
		return quadrantsWeights;
	}
	

	public Map<Integer,Double> getPolarityWeights(double[] categoryWeights)
	{
		Map<Integer,Double> polarityWeights = new HashMap<Integer,Double>();
		ICategoriesData categoriesData = getCategoriesData();
		
		for (int i = 0; i < categoryWeights.length; ++i)
		{
			int polarity = categoriesData.getCategoryPolarity(categoriesData.getCategoryId(i));
			UtilCollections.incrementIntValueInMap(polarityWeights, polarity, categoryWeights[i]);
		}
		return polarityWeights;
	}
	
	public boolean getDefaultPreprocessing() {
		return defaultPreprocessing;
	}

	public void setDefaultPreprocessing(boolean defaultPreprocessing) {
		this.defaultPreprocessing = defaultPreprocessing;
	}
	
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import data.categories.CategoryProcessor;
import data.categories.CategoryProcessor.CategoryDataType;
import data.categories.ICategoriesData;

import functionality.UtilArrays;
import functionality.UtilCollections;


public class ClassificationUtil {
	
	/**
	 * Returns the ids of categories having maximal weight (expect if all are 0 - empty list is returned in this case)
	 * @param weights
	 * @param catDataType
	 * @return
	 */
	public static List<Integer> getMaximalCategoriesIds(double[] weights, CategoryDataType catDataType)
	{
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		
		List<Integer> dominantCatsIndexes = UtilArrays.getIndexesOfMaxWithThreshold(weights, 0.0, true);
		List<Integer> dominantCatsIds = new ArrayList<Integer>();
		for (Integer catInd: dominantCatsIndexes)
			dominantCatsIds.add(categoriesData.getCategoryId(catInd));
		return dominantCatsIds;
	}
	
	/**
	 * Returns the ids of categories having maximal weight (expect if all are 0 - empty list is returned in this case)
	 * @param weights
	 * @param catDataType
	 * @return
	 */
	public static List<Integer> getMaximalCategoriesIds(double[] weights, String catDataType)
	{
		return getMaximalCategoriesIds(weights, CategoryProcessor.getCategoryDataTypeByString(catDataType));
	}
	
	/**
	 * This transforms the list-based labels list into the map of labels (each object having only of label)
	 * This works only for the binary -1/1 labels!
	 * @param labels
	 * @return
	 */
	public static  Map<Integer,Integer> getLabelData (Map<Integer, List<Integer>> labels )
	{
		Map<Integer,Integer> labelData = new HashMap<Integer, Integer>();
		for (Map.Entry<Integer, List<Integer>> labelEntry : labels.entrySet())
		{
			if (labelEntry.getValue().contains(1))
				labelData.put(labelEntry.getKey(), 1);
			else if (labelEntry.getValue().contains(-1))
				labelData.put(labelEntry.getKey(), -1);

		}
		return labelData;
	}
	
	// below are new methods from ClassificationApplication
	

	public static Map<Integer,Double> getQuadrantWeights(double[] categoryWeights, String categoryRef)
	{
		Map<Integer,Double> quadrantsWeights = new HashMap<Integer,Double>();
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesDataByName(categoryRef);
		
		for (int i = 0; i < categoryWeights.length; ++i)
		{
			int quadrant = categoriesData.getCategoryQuadrant(categoriesData.categoriesToUse()[i]);
			UtilCollections.incrementIntValueInMap(quadrantsWeights, quadrant, categoryWeights[i]);
		}
		return quadrantsWeights;
	}
	

	public static Map<Integer,Double> getPolarityWeights(double[] categoryWeights, String categoryRef)
	{
		Map<Integer,Double> polarityWeights = new HashMap<Integer,Double>();
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesDataByName(categoryRef);
		
		for (int i = 0; i < categoryWeights.length; ++i)
		{
			int polarity = categoriesData.getCategoryPolarity(categoriesData.categoriesToUse()[i]);
			UtilCollections.incrementIntValueInMap(polarityWeights, polarity, categoryWeights[i]);
		}
		return polarityWeights;
	}
	
	public static Integer getOnePolarity(double[] categoryWeights, String categoryRef)
	{
		Map<Integer,Double> polarityWeights = getPolarityWeights(categoryWeights, categoryRef);
		List<Integer> dominantPolarities = UtilCollections.findKeysWithMaxValues(polarityWeights);
		if (dominantPolarities.size() == 1)
			return dominantPolarities.get(0);
		else
			return -3;
	}
	
	public static List<Integer> getDominantCategories(double[] categoryWeights, String categoryRef) throws Exception
	{
		List<Integer> dominantCatsIndexes = UtilArrays.getIndexesOfMaxWithThreshold(categoryWeights, 0.0, true);
		List<Integer> dominantCatsIds = new ArrayList<Integer>();
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesDataByName(categoryRef);
		
		for (Integer catInd: dominantCatsIndexes)
			dominantCatsIds.add( categoriesData.getCategoryId(catInd));

		if (UtilArrays.getSum(categoryWeights) == 0.0)
		{
			dominantCatsIds.add(categoriesData.neutralCategory());//add neutral category
		}
		
		return dominantCatsIds;
	}
	
	/**
	 * returns true if the weights show emotionality presence, or false - if the weights indicate Neutral (based on the presence of neutral category within the dominant ones)
	 * @param categoryWeights
	 * @param categoryRef
	 * @return
	 * @throws Exception
	 */
	public static boolean checkIfEmotional (double[] categoryWeights, String categoryRef) throws Exception
	{
		 return !getDominantCategories(categoryWeights, categoryRef)
				 .contains(
						 CategoryProcessor.getCategoriesDataByName(categoryRef).neutralCategory()
						 );
	}
	
	public static double[] getNeutralWeightAllocation(String categoryRef)
	{
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesDataByName(categoryRef);
		double[] resWeights = new double[categoriesData.getCategoryNum()];
		Arrays.fill(resWeights, 0.0);
		resWeights[categoriesData.getCategoryIndexById(categoriesData.neutralCategory())] = 1.0;
		return resWeights;
	}
	
	public static double[] createNewWeightsForEmotionCategories(CategoryDataType catDataType)
	{
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		return new double[categoriesData.getCategoryNum()];
	}
}

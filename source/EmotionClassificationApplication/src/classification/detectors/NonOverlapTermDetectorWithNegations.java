/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.detectors;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import linguistic.NegationDetector;
import linguistic.NegationDetector.NegationTreatmentParams;


/**
 * This detector both removes the overlapped terms and processes negations of the found terms.
 * 
 */
public class NonOverlapTermDetectorWithNegations extends NonOverlapTermDetector{

	private static final long serialVersionUID = 7707626154314101053L;

	public NonOverlapTermDetectorWithNegations(Collection<String> terms, boolean toPreprocess) {
		super(terms, toPreprocess);
		this.savePositions = true;
		this.saveTokenList = true;
	}
	
	public NonOverlapTermDetectorWithNegations(TermDetectorWithProcessing tdetector) {
		super(tdetector);
		this.savePositions = true;
		this.saveTokenList = true;
	}
	
	public NonOverlapTermDetectorWithNegations(TermDetectorWithProcessing tdetector, NegationTreatmentParams negParams) {
		super(tdetector);
		this.savePositions = true;
		this.saveTokenList = true;
		this.negParams = negParams;
	}
	
	//parameters
	NegationTreatmentParams negParams;
	
	@Override
	protected Map<String, Integer> findEmotionalTermsForTokens(List<String> tokens)
	{
		Map<String, Integer> occurrences = super.findEmotionalTermsForTokens(tokens);
		Map<String, List<int[]>> foundPositions = getLastPositionsOfFoundTerms();
		
		return NegationDetector.treatNegationsInFoundTerms(negParams, occurrences, tokens, foundPositions);
	}
}

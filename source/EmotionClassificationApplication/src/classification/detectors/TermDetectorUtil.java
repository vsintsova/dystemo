/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classification.detectors;

import linguistic.TermDetectionParameters;

public class TermDetectorUtil {

	public static TermDetectorWithProcessing updateTermDetectorForParameters(TermDetectionParameters termDetectorParameters, TermDetectorWithProcessing tDetector)
	{
		if (termDetectorParameters.toSearchForNonOverlap && termDetectorParameters.toTreatNegations())
			tDetector = new NonOverlapTermDetectorWithNegations(tDetector, termDetectorParameters.treatNegationsParams);
		else if (!termDetectorParameters.toSearchForNonOverlap && termDetectorParameters.toTreatNegations())
			tDetector = new TermDetectorWithNegations(tDetector, termDetectorParameters.treatNegationsParams);
		
		if (termDetectorParameters.ignoreHashtags)
			tDetector.ignoreHahstagSymbols = true;
		
		return tDetector;
	}
}

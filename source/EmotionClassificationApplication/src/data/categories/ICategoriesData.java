/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.categories;

import java.io.Serializable;
import java.util.List;

import data.categories.CategoryProcessor.CategoryDataType;


public interface ICategoriesData extends Serializable{
	
	public CategoryDataType getCategoriesType();
	
	public int getCategoryNum();
	
	public String getCategoryName(int catId);
	
	public String getCategoryNameByIndex(int catIndex);
	
	public int getPolarityIndex(int polarityLabel);
	
	
	public double getPolaritySimilarity(int cat1, int cat2);
	
	public double getCategorySimilarity(int cat1, int cat2);
	
	/**
	 * Returns the id of the neutral category
	 * @return
	 */
	public int neutralCategory();
	
	/**
	 * Returns the id of the context-dependent category (or Other category)
	 * @return
	 */
	public int contextCategory();
	public int notAgreedCategory();
	
	public boolean isEmotionalCategory(int categoryId);
	
	
	//should already check that all have the same polarity and number of occurrences
	public int getMostGeneralCategory(List<Integer> categories);
	public boolean hasSamePolarity(List<Integer> categories);
		
		
	public int getCategoryPolarity(int categoryId);
	public int getCategoryQuadrant(int categoryId);
	
	public int[] categoriesToUse();
	
	public int[] emotionalCategoriesToUse();
	
	public int[] getNonDominantEmotions();

	public List<String> getCategoriesEnumeration();

	public int getCategoryIndexByName(String categoryName);
	public int getCategoryIndexById(int id);
	public int getCategoryId(int categoryIndex);

	boolean isAmbiguous(int[] categoryData);
	boolean isAmbiguous(double[] categoryData);
	boolean isAmbiguous(double[] categoryData, double ambLevel);
	
}

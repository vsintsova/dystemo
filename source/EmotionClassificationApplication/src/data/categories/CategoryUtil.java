/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.categories;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import data.categories.CategoryProcessor.CategoryDataType;
import functionality.UtilArrays;
import functionality.UtilCollections;

public class CategoryUtil {
	
	/** returns the polarity with maximal sum weights, Neutral (0) if no weights are given, and NotDefined (-2) when several polarities have highest weights */
	public static int getDominantPolarityUnique(double[] categoryWeights, CategoryDataType catDataType)
	{
		if (UtilArrays.getSum(categoryWeights) == 0.0)
			return Polarity.NEUTRAL; 
		
		List<Integer> domPolarities = getDominantPolarityListFull (categoryWeights, catDataType);
		if (domPolarities.size() == 1)
			return domPolarities.get(0);
		else
			return Polarity.UNDEFINED;
	}
	
	/**
	 * Returns the list of dominant polarities, without separation of the actual values
	 * @param categoryWeights
	 * @param catDataType
	 * @return
	 */
	private static List<Integer> getDominantPolarityListFull(double[] categoryWeights,  CategoryDataType catDataType)
	{
		Map<Integer,Double> polaritiesWeights = new HashMap<Integer,Double>();
		
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		for (int i = 0; i < categoryWeights.length; ++i)
		{
			int polarity = categoriesData.getCategoryPolarity(categoriesData.categoriesToUse()[i]);
			UtilCollections.incrementIntValueInMap(polaritiesWeights, polarity, categoryWeights[i]);
		}
		return UtilCollections.findKeysWithMaxValues(polaritiesWeights);
	}
	
	/** returns the polarities with maximal sum weights or Neutral (0) if no weights are given */
	public static List<Integer> getDominantPolarityList(double[] categoryWeights, CategoryDataType catDataType)
	{
		if (UtilArrays.getSum(categoryWeights) == 0.0)
		{
			ArrayList<Integer> result  = new ArrayList<Integer>();
			result.add(Polarity.NEUTRAL);
			return result;
		}
		
		return getDominantPolarityListFull (categoryWeights, catDataType);
	}
	
	/** returns the polarities with maximal sum weights or Neutral (0) if no weights are given */
	public static List<Integer> getDominantPolarityList(double[] categoryWeights, String categoryDataType)
	{
		return getDominantPolarityList(categoryWeights, CategoryProcessor.getCategoryDataTypeByString(categoryDataType));
	}
	
	
	public static List<Integer> getDominantQuadrantList(double[] categoryWeights, CategoryDataType catDataType)
	{
		if (UtilArrays.getSum(categoryWeights) == 0.0)
		{
			ArrayList<Integer> result  = new ArrayList<Integer>();
			result.add(Quadrant.NEUTRAL);
			return result;
		}
		Map<Integer,Double> quadrantsWeights = new HashMap<Integer,Double>();
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		
		for (int i = 0; i < categoryWeights.length; ++i)
		{
			int quadrant = categoriesData.getCategoryQuadrant(categoriesData.categoriesToUse()[i]);
			UtilCollections.incrementIntValueInMap(quadrantsWeights, quadrant, categoryWeights[i]);
		}
		return UtilCollections.findKeysWithMaxValues(quadrantsWeights);
	}
	
	public static List<Integer> getDominantQuadrantList(double[] categoryWeights, String categoryDataType)
	{
		return getDominantQuadrantList(categoryWeights, CategoryProcessor.getCategoryDataTypeByString(categoryDataType));
	}
	
	
	public static List<Integer> getCategoryQuadrants(Collection<Integer> categoryLabels, CategoryDataType catDataType)
	{
		return getCategoryQuadrants(categoryLabels, CategoryProcessor.getCategoriesData(catDataType));
	}
	
	public static List<Integer> getCategoryQuadrants(Collection<Integer> categoryLabels, ICategoriesData categoriesData)
	{
		List<Integer> tweetQuadrants = new ArrayList<Integer>();
		for (Integer category : categoryLabels)
		{
			tweetQuadrants.add(categoriesData.getCategoryQuadrant(category));
		}
		return tweetQuadrants;
	}
	
	public static List<Integer> getCategoryPolarities(Collection<Integer> categoryLabels, CategoryDataType catDataType)
	{
		return getCategoryPolarities(categoryLabels, CategoryProcessor.getCategoriesData(catDataType));
	}
	
	public static List<Integer> getCategoryPolarities(Collection<Integer> categoryLabels, ICategoriesData categoriesData)
	{
		List<Integer> tweetPolarities = new ArrayList<Integer>();
		for (Integer category : categoryLabels)
		{
			tweetPolarities.add(categoriesData.getCategoryPolarity(category));
		}
		return tweetPolarities;
	}
	
	public static Set<String> detectAmbiguousTerms(Map<String, double[]> termsData, ICategoriesData categoriesData)
	{
		Set<String> ambTerms = new HashSet<String>();
		for (Map.Entry<String, double[]> termEntry : termsData.entrySet())
		{
			if (categoriesData.isAmbiguous(termEntry.getValue()))
				ambTerms.add(termEntry.getKey());
		}
		return ambTerms;
	}
	
	public static Set<String> detectAmbiguousTerms(Map<String, double[]> termsData, ICategoriesData categoriesData, double ambLevel)
	{
		Set<String> ambTerms = new HashSet<String>();
		for (Map.Entry<String, double[]> termEntry : termsData.entrySet())
		{
			if (categoriesData.isAmbiguous(termEntry.getValue(), ambLevel))
				ambTerms.add(termEntry.getKey());
		}
		return ambTerms;
	}
	
	public static Set<String> detectAmbiguousTerms(Map<String, double[]> termsData, CategoryDataType catDataType)
	{
		return detectAmbiguousTerms(termsData, CategoryProcessor.getCategoriesData(catDataType));
	}
	
	public static Set<String> detectAmbiguousTerms(Map<String, double[]> termsData, String catDataTypeRef)
	{
		return detectAmbiguousTerms(termsData, CategoryProcessor.getCategoriesDataByName(catDataTypeRef));
	}
	
	public static double[] createEmptyWeights(CategoryDataType catDataType)
	{
		return  new double[CategoryProcessor.getCategoriesData(catDataType).getCategoryNum()];
	}
	
	/**
	 * 
	 * @param weights
	 * @param categoryId
	 * @return
	 */
	public static double[] addCategoryToWeights(double[] weights, int categoryId, CategoryDataType catDataType)
	{
		double[] newWeights = weights;
		if (weights == null)
			newWeights = createEmptyWeights(catDataType);
		newWeights[CategoryProcessor.getCategoriesData(catDataType).getCategoryIndexById(categoryId)]++;
		return newWeights;	
	}
	
	/**
	 * Produces normalized weights, where each category in the list has an equal weight (or higher if the same id appears several times) and then the resultant array is normalized by the sum.
	 * @param categories
	 * @param catDataType
	 * @return
	 */
	public static double[] transformCategoryListToWeights(List<Integer> categories, CategoryDataType catDataType)
	{
		double[] weights = null;
		for (Integer categoryId : categories) {
			weights = addCategoryToWeights(weights, categoryId, catDataType);
		}
		UtilArrays.normalizeArrayInternal(weights);
		return weights;
	}
	
	/**
	 * Produces unnormalized weights corresponding to the given list of categories, where each enumerated category has a weight of 1.0
	 * @param categoriesIds
	 * @param categoriesData
	 * @return
	 */
	public static double[] transformCategoryListToWeightsWithCounts(List<Integer> categoriesIds, ICategoriesData categoriesData)
	{
		double[] catWeights = new double[categoriesData.getCategoryNum()];
		for (Integer catId : categoriesIds)
		{
			if (catWeights[categoriesData.getCategoryIndexById(catId)] == 0.0)
				catWeights[categoriesData.getCategoryIndexById(catId)] = 1.0;
		}
		return catWeights;
	}
	
	/**
	 * Produces unnormalized weights corresponding to the given list of categories, where each enumerated category has a weight of 1.0
	 * @param categoriesIds
	 * @param catDataType
	 * @return
	 */
	public static double[] transformCategoryListToWeightsWithCounts(List<Integer> categoriesIds, CategoryDataType catDataType)
	{
		ICategoriesData categoriesData = CategoryProcessor.getCategoriesData(catDataType);
		return transformCategoryListToWeightsWithCounts(categoriesIds, categoriesData);
	}
	
	public static List<String> getCategoriesNamesFromIds(List<Integer> categoryIds, ICategoriesData categoriesData)
	{
		 List<String> names = new ArrayList<String>();
		 for (Integer catId : categoryIds) {
			 names.add(categoriesData.getCategoryName(catId));
		 }
		 return names;
	}
	
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package data.categories;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import au.com.bytecode.opencsv.CSVReader;
import data.categories.CategoryProcessor.CategoryDataType;


public class CategoriesDataOLD implements ICategoriesData{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5799736851027576528L;

	public static final int[] categoryReordering = {4,3,1,2,5,6,10,9,7,8,11,12,13,14};//gives the indexes (1-start) of categories in interface ordering

	static
	{
		try {
			readCategoriesNames();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static final int neutralCategory = 13;
	
	public static final int categoryNum = 18;
	
	static Map<Integer, String> categoriesNames;
	
	public static void readCategoriesNames() throws IOException
	{
		categoriesNames = new HashMap<Integer,String>();
		String fileWithCats = CategoriesDataOLD.class.getClassLoader().getResource("emotionModels/OLDCategories.txt").getPath();
		fileWithCats = fileWithCats.replace("%20", " ");
		CSVReader reader = new CSVReader(new FileReader(fileWithCats),  '\t');
		
		String [] nextLine;
		 while ((nextLine = reader.readNext()) != null) {
			 String catName = nextLine[0];
			 String catIndexStr = nextLine[1];
			 categoriesNames.put(Integer.parseInt(catIndexStr), catName);
		 }
		 reader.close();
		 categoriesNames.put(-3, "NotAgreed");
	}
	
	public String getCategoryName(int catId)
	{
		return categoriesNames.get(catId);
	}
	
	
	public double getPolaritySimilarity(int cat1, int cat2)
	{
		if (cat1 == cat2)
			return 1.0;
		int catMin = Math.min(cat1, cat2);
		if (catMin == -2)
			return 0.5;
		return 0.0;
		/*
		int catMax = Math.max(cat1, cat2);
		if (catMin == -1 && catMax == 1)
			return 0.0;*/
		
	}
	
	public double getCategorySimilarity(int cat1, int cat2)
	{
		if (cat1 == cat2)
			return 1.0;
		else if (getCategoryPolarity(cat1) != getCategoryPolarity(cat2))
			return 0.0;
		else
		{
			int polarity = getCategoryPolarity(cat1);
			
			int[] positiveOrder = {17,6,5,1,2,4,3};
			int[] negativeOrder = {18,12,11,7,8,10,9};
			if (polarity == 1)
			{
				int ind1 = -1;
				int ind2 = -1;
				for (int i = 0; i < positiveOrder.length; ++i)
				{
					if (positiveOrder[i] == cat1)
						ind1 = i;
					else if (positiveOrder[i] == cat2)
						ind2 = i;
				}
				return (6 - Math.abs(ind1 - ind2))/10.0;
			}
			if (polarity == -1)
			{
				int ind1 = -1;
				int ind2 = -1;
				for (int i = 0; i < negativeOrder.length; ++i)
				{
					if (negativeOrder[i] == cat1)
						ind1 = i;
					else if (negativeOrder[i] == cat2)
						ind2 = i;
				}
				return (6 - Math.abs(ind1 - ind2))/10.0;
			}
			return 0.1;
		}
	}
		
	
	//should already check that all have the same polarity and number of occurrences
	public int getMostGeneralCategory(List<Integer> categories)
		{
			int polarity = getCategoryPolarity(categories.get(0));
			
			int[] positiveOrder = {17,6,5,1,2,4,3};
			int[] negativeOrder = {18,12,11,7,8,10,9};
			if (polarity == 1)
			{
				for (int i = 0; i < positiveOrder.length; ++i)
				{
					if (categories.contains(positiveOrder[i]))
						return positiveOrder[i];
				}
			}
			if (polarity == -1)
			{
				for (int i = 0; i < negativeOrder.length; ++i)
				{
					if (categories.contains(negativeOrder[i]))
						return negativeOrder[i];
				}
			}
			return -1;
		}
		
		public boolean hasSamePolarity(List<Integer> categories)
		{
			int firstPolarity = getCategoryPolarity(categories.get(0));
			boolean allSamePolarity = true;
				for (Integer cat : categories)
				{
					if (getCategoryPolarity(cat) != firstPolarity)
					{
						allSamePolarity = false;
						break;
					}
				}
			return allSamePolarity;
		}
		
		
		public int getCategoryPolarity(int category)
		{
			if ((category >= 1 && category <= 6) || category == 17)
				return 1;
			if ((category >= 7 && category <= 12) || category == 18)
				return -1;
			if (category == 13)
				return 0;
			if (category == 14)
				return -2;
			return -3;
		}
		
		
		
		@Override
		public int getCategoryNum() {
			return categoryNum;
		}

		@Override
		public int neutralCategory() {
			// TODO Auto-generated method stub
			return 13;
		}

		@Override
		public int contextCategory() {
			// TODO Auto-generated method stub
			return 14;
		}
		
		@Override
		public int notAgreedCategory() {
			// TODO Auto-generated method stub
			return 19;
		}

		@Override
		public int[] categoriesToUse() {
			// TODO Auto-generated method stub
			 int[] categoriesToUse = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
			return categoriesToUse;
		}

		@Override
		public boolean isAmbiguous(int[] categoryData) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isAmbiguous(double[] categoryData) {
			// TODO Auto-generated method stub
			return false;
		}
		
		@Override
		public String getCategoryNameByIndex(int catIndex) {
			
			return getCategoryName(categoriesToUse()[catIndex]);
		}

		@Override
		public int getCategoryQuadrant(int category) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public CategoryDataType getCategoriesType() {
			return CategoryDataType.OLD;
		}
		
		@Override
		public List<String> getCategoriesEnumeration() {
			List<String> catList = new ArrayList<String>();
			for (int catId : categoriesToUse())
			{
				catList.add(categoriesNames.get(catId));
			}
			
			return catList;
		}
		
		@Override
		public int getCategoryIndexByName(String categoryName) {
			List<String> categoryEnumeration = getCategoriesEnumeration();
			return categoryEnumeration.indexOf(categoryName);
		}
		
		@Override
		public int getCategoryIndexById(int id) {
			return id - 1;
		}

		@Override
		public int getCategoryId(int categoryIndex) {
			return categoriesToUse()[categoryIndex];
		}
		
		@Override
		public int getPolarityIndex(int polarityLabel) {
			switch(polarityLabel)
			{
				case 0: return 0;
				case 1: return 1;
				case -1: return 2;
				case -2: return 3;
				case -3: return 4;
			}
			return -1;
		}

		@Override
		public boolean isAmbiguous(double[] categoryData, double ambLevel) {
			// TODO Auto-generated method stub
			return false;
		}
		
		@Override
		public boolean isEmotionalCategory(int categoryId) {
			return (categoryId <= 12 || categoryId == 17 || categoryId == 18);
		}

		private final int[] emotionalCategories = {1,2,3,4,5,6,7,8,9,10,11,12,17,18};
		

		@Override
		public int[] emotionalCategoriesToUse() {
			return emotionalCategories;
		}

		@Override
		public int[] getNonDominantEmotions() {
			// TODO Auto-generated method stub
			return null;
		}
}

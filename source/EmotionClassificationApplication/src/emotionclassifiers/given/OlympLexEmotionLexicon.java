/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
    
     ------------------------------------
    
    Note that the given classifiers are provided for exemplary and research purposes. 
    Please refer to LEXICONS_LICENSING.txt file to read the terms of use for the provided lexicons.

*/

package emotionclassifiers.given;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

import classification.definitions.lexicons.WeightedEmotionLexicon;
import data.categories.CategoryProcessor.CategoryDataType;
import functionality.UtilFiles;

/**
 * The OlympLex emotion lexicon, generated via crowdsourcing process. 
 * There are several versions: 
 * 1.0: basic initial version. It preprocessed only to replace emoticons, but elongations are not merged. 
 * 1.1: refined version, where we removed some non-representative terms (listed in the resource file termLists/ambiguousOlympLexTerms).
 * 1.1-prep: refined and additionally preprocessed version. We merged the elongated lexicon terms into the same entry.
 * 
 * By default the last version will be loaded.
 */
public class OlympLexEmotionLexicon extends WeightedEmotionLexicon
{
	private static final long serialVersionUID = -882433888756510912L;
	static String OlympLexTextFileName = "OlympLex-1.0-lexicon.txt";
	static String OlympLexTextFileNameRefined = "OlympLex-1.1-lexicon.txt";
	static String OlympLexTextFileNameRefinedAndPreprocessed = "OlympLex-1.1-prep-lexicon.txt";
	
	/** 
	 * If true: both ambiguous terms will be removed, and all the emoticons will be assigned 0.5 weight.
	 */
	static boolean toRefineAdditionally = false;
	static boolean toUseNonOverlap = false;
	static boolean toUsePreprocessedVersion = true;
	
	
	private static String getResourceLexiconFileName(String filename) throws Exception
	{
		return OlympLexEmotionLexicon.class.getClassLoader().getResource("givenLexicons/" + filename).toURI().getPath();
	}
	
	private static String getCurrentLexiconName() {
		if (toUsePreprocessedVersion)
		{
			return OlympLexTextFileNameRefinedAndPreprocessed;
		} else
			return OlympLexTextFileNameRefined; 
	}
	
	public OlympLexEmotionLexicon() throws Exception
	{
		super(WeightedEmotionLexicon.loadFromTextFile(getResourceLexiconFileName(getCurrentLexiconName()),  CategoryDataType.GEW,  toUseNonOverlap));
		
		if (toRefineAdditionally)
		{
			refineLexicon();
		}
		this.name = "OlympLex"+ (toRefineAdditionally?"Refined":"");
		System.out.println("OlympLex version 1.1 loaded with preprocessing="+ toUsePreprocessedVersion +", with refinement=" + toRefineAdditionally + " and with noOverlap=" + toUseNonOverlap);
	}
	
	
	public Set<String> getAllEmoTerms()
	{
		return allTerms();
	}
	
	/** removes ambiguous terms and downweights emoticons 
	 * @throws URISyntaxException */
	private void refineLexicon() throws URISyntaxException
	{
		List<String> ambiguousTerms = UtilFiles.getContentLines(
				OlympLexEmotionLexicon.class.getClassLoader().getResource("termLists/ambiguousOlympLexTerms.txt").toURI().getPath());
		String emotoPattern = "<emot\\d{1,2}>";
		
		removeTerms(ambiguousTerms);
		reweightTerms(emotoPattern, 0.5);
	}
}

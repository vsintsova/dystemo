/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
    
    ------------------------------------
    
    Note that the given classifiers are provided for exemplary and research purposes. 
    Please refer to LEXICONS_LICENSING.txt file to read the terms of use for the provided lexicons.

*/

package emotionclassifiers.given;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import classification.definitions.WeightedClassifier;
import classification.definitions.classifiers.WeightedClassifierLexiconBased;
import classification.definitions.lexicons.WeightedEmotionLexicon;
import data.categories.CategoriesDataGEW;
import data.categories.ICategoriesData;
import functionality.UtilCollections;

public class GALCClassifierLoader {

	private static String getResourceLexiconFileName()
	{
		String filename = GALCClassifierLoader.class.getClassLoader().getResource(
				"givenLexicons/" + "GALC-lexicon.txt").getPath();
		filename = filename.replace("%20", " "); // to ensure correct extraction of the file when the white spaces are present in the path
		return filename;
	}
	
	private static String getResourceRevisedInstancesLexiconFileName() throws Exception
	{
		return GALCClassifierLoader.class.getClassLoader().getResource("givenLexicons/" + "GALC-R-instances-lexicon.txt").toURI().getPath();
	}
	
	public static WeightedClassifierLexiconBased getGALCClassifier() throws Exception
	{
		WeightedEmotionLexicon lexicon = WeightedEmotionLexicon.readFromFileWithCategories(getResourceLexiconFileName(), "GEW");
		lexicon.name = "GALC-Lex";
		lexicon.tDetector.usePatterns = true;
		return new WeightedClassifierLexiconBased(lexicon);
	}
	
	public static WeightedClassifierLexiconBased getGALCRevisedInstancesClassifier() throws Exception
	{
		WeightedEmotionLexicon lexicon = WeightedEmotionLexicon.readFromFileWithCategories(
				getResourceRevisedInstancesLexiconFileName(), "GEW");
		lexicon.name = "GALC-R-Lex";
		lexicon.tDetector.usePatterns = false;
		return new WeightedClassifierLexiconBased(lexicon);
	}
	
	
	public static void main(String[] args) throws Exception
	{
		WeightedClassifier galcClassifier = GALCClassifierLoader.getGALCClassifier();
		String testText = "I love it so much! I cannot describe how happy I am!";
		double[] weights = galcClassifier.findCategoryWeights(testText);
		
		List<Integer> categories = galcClassifier.getDominantCategories(weights);
		List<String> categoriesNames = new ArrayList<String>();
		ICategoriesData categoriesData = new CategoriesDataGEW();
		for (Integer categoryId : categories)
			categoriesNames.add(categoriesData.getCategoryName(categoryId));
		
		System.out.println("Text: \"" + testText + "\"");
		System.out.println("Emotions found: " + UtilCollections.join(categoriesNames, ", "));
	}
}

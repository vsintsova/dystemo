/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
    
    ------------------------------------
    
    Note that the given classifiers are provided for exemplary and research purposes. 
    Please refer to LEXICONS_LICENSING.txt file to read the terms of use for the provided lexicons.

*/

package emotionclassifiers.given;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import classification.definitions.lexicons.CategoryLexiconUnrestricted;

public class EmotionLexiconsGivenFactory {

	/**
	 * Returns the lexicon of the used GEW hashtags, each associated to one emotion category.
	 * The hashtag symbol (#) is not present.
	 * @return
	 * @throws Exception
	 */
	public static CategoryLexiconUnrestricted getLexiconOfGivenGEWHashtags() throws Exception
	{
		return getLexiconOfGivenGEWHashtags(false);
	}
	
	public static CategoryLexiconUnrestricted getLexiconOfGivenGEWHashtags(boolean withHashtagSymbol) throws Exception
	{
		String lexFilePath = EmotionLexiconsGivenFactory.class.getClassLoader().getResource("givenLexicons/GALC-based-hashtags-lexicon.txt").toURI().getPath();
		CategoryLexiconUnrestricted lexicon =  CategoryLexiconUnrestricted.readCategoryLexiconFromFile(lexFilePath);
		
		if (withHashtagSymbol)
		{
			Map<Integer, Set<String>> mapCategoryToTerms = lexicon.getMapCategoryToTerms();
			Map<Integer, Set<String>> newMapCategoryToTerms = new HashMap<Integer, Set<String>>();
			for (Map.Entry<Integer,Set<String>> categoryEntry :  mapCategoryToTerms.entrySet())
			{
				Set<String> newTermSet = new HashSet<String>();
				for (String term : categoryEntry.getValue())
				{
					newTermSet.add("#" + term);
				}
				newMapCategoryToTerms.put(categoryEntry.getKey(), newTermSet);
			}
			lexicon = new CategoryLexiconUnrestricted(newMapCategoryToTerms);
		}
		return lexicon;
	}
	
	/**
	 * Loads the lexicon of preprocessed emojis associated with GEW emotions.
	 * @return
	 * @throws Exception
	 */
	public static CategoryLexiconUnrestricted getLexiconOfGivenGEWEmojis() throws Exception
	{
		String lexFilePath = EmotionLexiconsGivenFactory.class.getClassLoader().getResource("givenLexicons/emoji-gew-preprocessed.txt").toURI().getPath();
		CategoryLexiconUnrestricted lexicon =  CategoryLexiconUnrestricted.readCategoryLexiconFromFile(lexFilePath);
		return lexicon;
	}	
	
}

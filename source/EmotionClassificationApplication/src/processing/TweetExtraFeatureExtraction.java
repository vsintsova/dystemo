/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package processing;

import data.documents.Tweet;
import data.documents.TweetFeaturesExtended;

public class TweetExtraFeatureExtraction {

	public static TweetFeaturesExtended computeTweetFeatures(Tweet tweet)
	{
		TweetFeaturesExtended resTweet = new TweetFeaturesExtended(tweet);
		resTweet.isEmoticonPresent = TweetPreprocessing.containsEmoticon(tweet.text);
		resTweet.length = tweet.text.length();
		resTweet.numberOfAlongations =  TweetPreprocessing.getNumberOfTimesRepeatedCharacters(tweet.text);
		resTweet.numberOfExclMarks =  TweetPreprocessing.getNumberOfPunctuationSigns(tweet.text, "!");
		resTweet.numberOfQuestionMarks =  TweetPreprocessing.getNumberOfPunctuationSigns(tweet.text, "?");
		resTweet.numberOfTabWords =  TweetPreprocessing.getNumberOfTabWords(tweet.text);
		return resTweet;
	}
}

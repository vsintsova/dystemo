/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package processing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import functionality.UtilCollections;
import functionality.UtilString;
import linguistic.StanfordAnnotator;
import linguistic.UtilText;
import utility.Pair;

public class TweetPreprocessing {
	
	static String[][] emojiUnicode = null; 

	static Map<String,String> emojiUnicodeMap = null;
	
	static String wordEndPattern = "(?=(\\s|\u00A0|$|@|#))";
	
	public TweetPreprocessing()
	{
		sannot = new StanfordAnnotator();
	}
	
	/**
	 * Removes the URLs from the text (unpreprocessed).
	 * @param tweetText
	 * @return
	 */
	public static String removeAllLinks(String tweetText)
	{
		return tweetText.replaceAll(TweetPatterns.linkRegEx+wordEndPattern, "");
	}
	
	/**
	 * Removes the URLs from the processed text (where URL is marked as <link>).
	 * @param tweetText
	 * @return
	 */
	public static String removeAllLinksAfterPreprocessing(String tweetText)
	{
		return tweetText.replace("<link>", "").replaceAll("\\s+", " ").trim();
	}
	
	/** replaces the links in the text with <link>*/
	static private String replaceAllLinks(String tweetText)
	{
		return tweetText.replaceAll(TweetPatterns.linkRegEx+wordEndPattern, "<link>");
	}
	
	/** replaces the usernames in the text with <username>*/
	public static String replaceAllUserNames(String tweetText)
	{
		return tweetText.replaceAll(TweetPatterns.usernameRegEx+"(?=(\\b|\\s|\u00A0|$|@|#))", " <username>");
	}
	
	/**
	 * surrounds all the found hashtags with white spaces
	 * @param tweetText
	 * @return
	 */
	static private String distinguishAllHashtags(String tweetText)
	{
		return tweetText.replaceAll("(" + TweetPatterns.hashtagRegEx + ")"+"(?=(\\s|\\b|\u00A0|$|[[\\p{Punct}]&&[^_]]))", "$1 ");
	}
	
	static public String preprocessForURLSearch(String tweetText)
	{
		// delete links
		tweetText = removeAllLinks(tweetText);
		
		//replace special symbols
		tweetText = getInitial(tweetText);
		return tweetText;
	}
	
	static public String separateHashtagsFromOtherSymbols(String tweetText)
	{
		return tweetText.replaceAll("(?<=[\\S])#", " #");
	}
	
	static public String preprocessForLanguageDetection(String tweetText)
	{
		// delete links 
		tweetText = removeAllLinks(tweetText);
		
		// other simple correction
		tweetText = getInitial(tweetText);
		
		// delete usernames
		tweetText = tweetText.replaceAll(TweetPatterns.usernameRegEx+wordEndPattern, "");
		
		// delete hashtags (cause usually out of vocabulary anyway)
		tweetText = tweetText.replaceAll(TweetPatterns.hashtagRegEx+"(?=(\\s|\\b|\u00A0|$|[[\\p{Punct}]&&[^_]]))", "");
		
		// delete retweet signs
		tweetText = tweetText.replaceAll("(?<=(\\s|\u00A0|^))[rR][tT](?=(\\s|\u00A0|$|@))", "");
		
		return tweetText;
	}
	
	static public boolean containsUserMention(String tweetText)
	{
		return Pattern.compile(TweetPatterns.usernameRegEx+wordEndPattern).matcher(tweetText).find();
	}
	
	/**
	 * Replaces symbols &lt; &gt; &amp; with their normal parts
	 * @param tweetText
	 * @return
	 */
	static public String getInitial(String tweetText)
	{
		tweetText = tweetText.replaceAll("&lt;", "<");
		tweetText = tweetText.replaceAll("&gt;", ">");
		tweetText = tweetText.replaceAll("&amp;", "&");
		return tweetText;
	}
	
	/**
	 * Note that this function will also lowercase the full text. (Not optimal implementation) 
	 * @param text
	 * @return
	 */
	static public String replaceEmoticons(String text)
	{
		String tweetText = new String(text.toLowerCase());
		for(String[] emoticon: emoticons)
		{
			if (tweetText.matches(".*" + Pattern.quote(emoticon[0].toLowerCase())+"(?=\\b|\\s|\u00A0|$).*"))
			{
				tweetText = tweetText.replaceAll(Pattern.quote(emoticon[0].toLowerCase())+"(?=(\\b|\\s|\u00A0|$))", emoticon[1]); //replace it for not finding the part of it in the next emoticons
			}
		}
		return tweetText;
	}
	
	/**
	 * This function will create the extra white spaces before and after the detected emoji (Not optimal implementation)
	 * @param text
	 * @return
	 */
	static public String replaceEmojis(String text, boolean textIsEscaped)
	{
		String tweetText = new String(text);
		try 
		{
			if (emojiUnicode == null)
				TweetPreprocessing.loadEmojisUnicodes();			
			//2. detect emojis - put them as tokens
			for(String[] emoji: emojiUnicode)
			{
				if (textIsEscaped) {
					tweetText = tweetText.replaceAll("(?i)\\Q" + emoji[0] + "\\E", " <emoji_"+emoji[1]+"> ");
				} else {
					tweetText = tweetText.replace(emoji[0], " <emoji_"+emoji[1]+"> "); 
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return tweetText;
	}
	
	static public Set<String> getAllEmojisNames() throws Exception {
		Set<String> emojis = new HashSet<String>();
		if (emojiUnicode == null)
			TweetPreprocessing.loadEmojisUnicodes();
		for(String[] emoji: emojiUnicode)
		{
			emojis.add("<emoji_"+emoji[1]+">");
		}
		return emojis;
	}
	
	static public String removeEmojis(String text, boolean textIsEscaped)
	{
		String tweetText = replaceEmojis(text, textIsEscaped);
		tweetText = tweetText.replaceAll("<emoji_([\\w&&[^>]]+)>", "");
		return tweetText.replaceAll("\\s+", " ");
	}
	
	StanfordAnnotator sannot;
	
	public static List<String> tokenizeWithoutLemmatization(String tweetText)
	{
		tweetText = preprocessText(tweetText);
		List<String> tokens = new ArrayList<String>();
		tokens.addAll(Arrays.asList(tweetText.split("\\s+")));
		return tokens;
	}
	
	public static List<String> tokenizeWithoutPreprocessing(String tweetText)
	{
		List<String> tokens = new ArrayList<String>();
		tokens.addAll(Arrays.asList(tweetText.trim().split("\\s+")));
		return tokens;
	}
	
	static Map<String, String> negationAbbreviationReplacement;
	static {
		negationAbbreviationReplacement = new HashMap<String, String>();
		negationAbbreviationReplacement.put("cant", "ca nt");
		negationAbbreviationReplacement.put("dont", "do nt");
		negationAbbreviationReplacement.put("isnt", "is nt");
		negationAbbreviationReplacement.put("wont", "wo nt");
		negationAbbreviationReplacement.put("havent", "have nt");
		negationAbbreviationReplacement.put("hasnt", "has nt");
		negationAbbreviationReplacement.put("shouldnt", "should nt");
		negationAbbreviationReplacement.put("cannot", "can not");
		negationAbbreviationReplacement.put("hadnt", "had nt");
		negationAbbreviationReplacement.put("werent", "were nt");
		negationAbbreviationReplacement.put("doesnt", "does nt");
		negationAbbreviationReplacement.put("didnt", "did nt");
		negationAbbreviationReplacement.put("arent", "are nt");
		negationAbbreviationReplacement.put("couldnt", "could nt");
		negationAbbreviationReplacement.put("wasnt", "was nt");
		negationAbbreviationReplacement.put("neednt", "need nt");
		negationAbbreviationReplacement.put("mustnt", "must nt");
		negationAbbreviationReplacement.put("wouldnt", "would nt");
		negationAbbreviationReplacement.put("oughtnt", "ought nt");
		negationAbbreviationReplacement.put("mightnt", "might nt");
		negationAbbreviationReplacement.put("wanna", "wan na");
		negationAbbreviationReplacement.put("gonna", "gon na");
		negationAbbreviationReplacement.put("gotta", "got ta");
	}
	
	public static String replaceNegationAbbreviations(String tweetText) {
		List<String> tokens = tokenizeWithoutPreprocessing(tweetText);
		for (int ind = 0; ind < tokens.size(); ++ind) {
			if (negationAbbreviationReplacement.containsKey(tokens.get(ind))) {
				tokens.set(ind, negationAbbreviationReplacement.get(tokens.get(ind)));
			}
		}
		return UtilCollections.join(tokens, " ");
	}
	
	static PreprocessingParameters defaultPreprocessingParametersInitial;
	static PreprocessingParameters defaultPreprocessingParametersNovel;
	
	static {
		defaultPreprocessingParametersInitial = new PreprocessingParameters(); //new PreprocessingParameters(true, false, true, true, true, false, false, false, false, false)
		
		defaultPreprocessingParametersInitial.textIsEscaped = true;
		defaultPreprocessingParametersInitial.parseEmoji = false;
		defaultPreprocessingParametersInitial.parseEmoticons = true;
		defaultPreprocessingParametersInitial.toRemoveHashtags = true;
		defaultPreprocessingParametersInitial.removeQuotes = true;
		defaultPreprocessingParametersInitial.replaceLineSeparatorsIntoSpecific = false;
		defaultPreprocessingParametersInitial.replaceAlongations = false;
		defaultPreprocessingParametersInitial.replaceURLSpecials = false;
		defaultPreprocessingParametersInitial.replaceNumberEntries = false;
		defaultPreprocessingParametersInitial.replaceLongQuotes = false;
		
		defaultPreprocessingParametersNovel = new PreprocessingParameters(); //new PreprocessingParameters(true, true, true, false, false, true, true, true, true, true)
		
		defaultPreprocessingParametersNovel.textIsEscaped = true;
		defaultPreprocessingParametersNovel.parseEmoji = true;
		defaultPreprocessingParametersNovel.parseEmoticons = true;
		defaultPreprocessingParametersNovel.toRemoveHashtags = false;
		defaultPreprocessingParametersNovel.removeQuotes = false;
		defaultPreprocessingParametersNovel.replaceLineSeparatorsIntoSpecific = true;
		defaultPreprocessingParametersNovel.replaceAlongations = true;
		defaultPreprocessingParametersNovel.replaceURLSpecials = true;
		defaultPreprocessingParametersNovel.replaceNumberEntries = true;
		defaultPreprocessingParametersNovel.replaceLongQuotes = true;
		
	}
	
	/**
	 * This is the default processing of the text. The special symbols should be escaped for the correct preprocessing.
	 * @param tweetText
	 * @return
	 */
	public static String preprocessText(String tweetText)
	{
		return preprocessText(tweetText, defaultPreprocessingParametersInitial);
	}
	
	/**
	 * This is the updated default processing of the text. The special symbols should be escaped for the correct preprocessing.
	 * @param tweetText
	 * @return
	 */
	public static String preprocessTextNovel(String tweetText)
	{
		return preprocessText(tweetText, defaultPreprocessingParametersNovel);
	}
	
	public static String preprocessOneTokenAfterParsing(String token) {
		if (token.equals("John")) return "<username>"; // to ensure the same representation of the username occurrences
		return preprocessText(token, new PreprocessingParameters(true, false, false, true, false, false, true, false, true, false));
	}
	
	public static String replaceLineSeparators(String tweetText, boolean replaceBySpecific) 
	{
		if(replaceBySpecific)
		{
			tweetText = tweetText.replace("\n", " \\n ");
			tweetText = tweetText.replace("\t", " \\t ");
			tweetText = tweetText.replace(System.getProperty("line.separator"), " \\n ");
		}
		else
		{
			tweetText = tweetText.replace("\n", " . ");
			tweetText = tweetText.replace("\t", " . ");
			tweetText = tweetText.replace(System.getProperty("line.separator"), " . ");
		}
		return tweetText;
	}
	
	public static class PreprocessingParameters {
		boolean textIsEscaped;
		boolean parseEmoji;
		boolean parseEmoticons;
		boolean toRemoveHashtags;
		boolean removeQuotes;
		boolean replaceLineSeparatorsIntoSpecific;
		boolean replaceAlongations;
		boolean replaceURLSpecials;
		boolean replaceNumberEntries;
		boolean replaceLongQuotes;
		boolean replaceNonStandardNegations; // not used yet!
		
		public PreprocessingParameters(boolean textIsEscaped, boolean parseEmoji, boolean parseEmoticons, boolean toRemoveHashtags, 
			boolean removeQuotes, boolean replaceLineSeparatorsIntoSpecific, boolean replaceAlongations,
			boolean replaceURLSpecials, boolean replaceNumberEntries, boolean replaceLongQuotes) {
			this.textIsEscaped = textIsEscaped;
			this.parseEmoji = parseEmoji;
			this.parseEmoticons = parseEmoticons;
			this.toRemoveHashtags = toRemoveHashtags;
			this.removeQuotes = removeQuotes;
			this.replaceLineSeparatorsIntoSpecific = replaceLineSeparatorsIntoSpecific;
			this.replaceAlongations = replaceAlongations;
			this.replaceURLSpecials = replaceURLSpecials;
			this.replaceNumberEntries = replaceNumberEntries;
			this.replaceLongQuotes = replaceLongQuotes;
		}
		
		public PreprocessingParameters() {};
		
		//TODO: run tests for correct preprocessing!
	}
	
	public static String preprocessText(String tweetText, PreprocessingParameters params)
	{
		String initialText = tweetText;
		if (params.replaceURLSpecials)
			tweetText = getInitial(tweetText);
		
		tweetText = tweetText.replace("\u2026", "... <ext> ");
		if (params.parseEmoji)
		{
			try {
				tweetText = replaceEmojis(tweetText, params.textIsEscaped); //Faster
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		tweetText = tweetText.replace("Â ", " ");
		tweetText = tweetText.replace('\u00A0', ' ');
		
		tweetText = tweetText.replace("&lt ;", " '<'");
		tweetText = tweetText.replace("&gt ;", " '>'");
		
		
		
		//5. delete all hashtags! to make it usual text (firststep to avoid problems when emoticons are followed by hashtag
		if (params.toRemoveHashtags)
			tweetText = tweetText.replaceAll("#","");
		else
		{
			tweetText = tweetText.replaceAll("(?<=[a-z0-9>])#(?=[a-z<])", " #");
		}
	
		//7. replace EOL and other problematic symbols with extra full stops
		tweetText = replaceLineSeparators(tweetText, params.replaceLineSeparatorsIntoSpecific);
				
		
		//2. replace usernames and links with placeholders
		if (!params.toRemoveHashtags) tweetText = distinguishAllHashtags(tweetText);
		tweetText = replaceAllLinks(tweetText);
		tweetText = replaceAllUserNames(tweetText);
		tweetText = tweetText.toLowerCase(); // we'll not ditinguish the emoticons in upper and lower cases!
		
				
		tweetText = tweetText.replaceAll("\\){3,}", "))");
		tweetText = tweetText.replaceAll("\\({3,}", "((");
		tweetText = tweetText.replaceAll("\\*{3,}", "**");
						
		if (params.parseEmoticons) {
				//1. detect all emoticons - put them as tokens
				for(String[] emoticon: emoticons)
				{
					if (tweetText.matches(".*" + Pattern.quote(emoticon[0].toLowerCase())+"(?=\\b|\\s|\u00A0|$).*"))
					{
						tweetText = tweetText.replaceAll(Pattern.quote(emoticon[0].toLowerCase())+"(?=(\\b|\\s|\u00A0|$))", " " + emoticon[1] + " "); //replace it for not finding the part of it in the next emoticons
					}
				}
		}
						
		//3. detect punctuation repetitions
		tweetText = tweetText.replaceAll("((\\!+\\?+)|(\\?+\\!+)){2,}[\\?\\!]*", "-?!-*");		
		tweetText = tweetText.replaceAll("\\?{2,}", "?*");
		tweetText = tweetText.replaceAll("\\!{2,}", "!*");
		tweetText = tweetText.replaceAll("\\.{4,}", "...* ");
		tweetText =	tweetText.toLowerCase();
		
		if (params.replaceNumberEntries)
		{
			// need to replace numbers, but not those that are describing emoji or emoticons
			tweetText = replaceNotKnownNumbers(tweetText);
			tweetText =	tweetText.toLowerCase();
		}
		
		
		
				
		//4. separate punctiation signs from words (or hashtags, or numbers!)
		String punctuationSigns = "?!,;:-\"()[]/{}“”*|~=+"; // . will be processed below
		for (int i = 0; i <  punctuationSigns.length(); ++i)
		{
			char c = punctuationSigns.charAt(i);
			tweetText = tweetText.replaceAll("(?<=[a-z0-9>])\\" + c, " " + c);
			tweetText = tweetText.replaceAll("\\" + c + "(?=[a-z0-9<#])", c + " ");
		}
		
		// not really needed
		//tweetText = tweetText.replaceAll("\\>(?=[a-z0-9<#])", "> ");
		//tweetText = tweetText.replaceAll("(?<=[a-z0-9>])\\<", " <");
		
		tweetText = separateDotCorrectly(tweetText);
		
		// separate punctuation signs from some
		String signsToSeparate1 = "?!.,;:'*";
		String signsToSeparate2 = "\"()[]/{}'“”";
		for (int i1 = 0; i1 <  signsToSeparate1.length(); ++i1)
			for (int i2 = 0; i2 <  signsToSeparate2.length(); ++i2)
		{
			char c1 = signsToSeparate1.charAt(i1);
			char c2 = signsToSeparate2.charAt(i2);
			tweetText = tweetText.replace(c1 + "" + c2, c1 + " " + c2);
			tweetText = tweetText.replace(c2 + "" + c1, c2 + " " + c1);
		}
		
				
		// 8. separate emoticons from everything (was added later!)
		tweetText = tweetText.replaceAll("(<emot[0-9]+>)(\\w)", "$1 $2");
		tweetText = tweetText.replaceAll("(\\w)(<emot[0-9]+>)", "$1 $2");
		
		tweetText = tweetText.trim();
		if(params.removeQuotes)
		{
			//6. delete covering quotes
			if (tweetText.length() > 0)
			{
				if (tweetText.charAt(0) == '"')
					tweetText = tweetText.substring(1);
				if (tweetText.length() > 0)
				{
					if (tweetText.charAt(tweetText.length() - 1) == '"')
						tweetText = tweetText.substring(0, tweetText.length() - 1);
				}
			}
		}
		
		tweetText = TweetPreprocessing.treatOtherUnicodeCharacters(tweetText, params.textIsEscaped);
		
		// separate the 's from the words (in order to better be able to detect concepts)
		tweetText = separatePrimeCorrectly(tweetText);
		tweetText = separateAmpersCorrectly(tweetText);
		
		tweetText = separateHashtagsFromOtherSymbols(tweetText); //was added afterwards only!
		
		if (params.replaceAlongations)
			tweetText = TweetPreprocessing.normalizeText(tweetText);		
		
		tweetText = tweetText.replaceAll("\\s+", " ");
		
		tweetText = tweetText.trim();
		
		if (params.replaceLongQuotes)
			tweetText = replaceLongQuotes(tweetText, 4);
		
		if (tweetText.length() == 0)
		{
			int k = 0;
			k++;
		}
		
		return tweetText;
	}
	
	private static< T extends Object> int findElemInList(List<T> list, T elem, int startInd)
	{
		for (int ind = startInd; ind < list.size(); ++ind)
		{
			if (list.get(ind).equals(elem))
				return ind;
		}
		return -1;
	}
	
	/** Replaces long quotes with the placeholder <quote>. It will not replace the quote is it cover the full text only
	 * @param tweetText The text where the quotes are being replaced. It should be already preprocessed (i.e. the quote symbols \" are separated from other tokens)
	 * @param qouteLength The minimum length of quotes in words in order to be replaced
	 * @return new text with quotes replacements
	 */
	public static String replaceLongQuotes(String tweetText, int minQouteLength)
	{
		if (!tweetText.contains("\""))
			return tweetText;
		
		List<String> tokens = tokenizeWithoutPreprocessing(tweetText);
		boolean allQuotesReplaced = false;
		int nextToWatchIndex = 0;
		
		StringBuilder sb = new StringBuilder();
		
		while (!allQuotesReplaced)
		{
			int quoteStartInd =  findElemInList(tokens, "\"", nextToWatchIndex);
			
			if (quoteStartInd >= 0)
			{
				// there is a quote in the text
				
				// copy all the previous text before the quote
				for (int ind = nextToWatchIndex; ind < quoteStartInd; ++ind)
				{
					sb.append(tokens.get(ind) + " ");
				}
				int quoteLen = 0;
				boolean endFound = false;
				for (int ind = quoteStartInd + 1; ind < tokens.size(); ++ind)
				{
					if (tokens.get(ind).equals("\""))
					{
						// end of the quote
						endFound = true;
						break;
					}
					quoteLen++;
				}
				if (!endFound)
				{
					// try to find end of the quote as "<link>"
					quoteLen = 0;	
					for (int ind = quoteStartInd + 1; ind < tokens.size(); ++ind)
					{
						if (tokens.get(ind).equals("<link>"))
						{
							// end of the quote
							endFound = true;
							break;
						}
						quoteLen++;
					}
				}
				
				if (endFound && quoteStartInd == 0 && quoteLen == tokens.size() - 2)
				{
					endFound = false; // we will not replace such quotes
				}
				
				// replace the quote if end was found and the condition of length is fulfilled
				if (endFound && quoteLen >=  minQouteLength)
				{
					sb.append("<quote> ");
					nextToWatchIndex = quoteStartInd + quoteLen + 2;
					if (!tokens.get(nextToWatchIndex - 1).equals("\"")) // meaning that quote wasn't ending was '\"
						sb.append(tokens.get(nextToWatchIndex - 1) + " ");
				}
				else
				{
					if (!endFound)
					{
						allQuotesReplaced = true;
						// copy the current 'quote' as is until the end of the tweet as the quote end wasn't found
						for (int ind = quoteStartInd; ind < tokens.size(); ++ind)
						{
							sb.append(tokens.get(ind) + " ");
						}
					}
					else
					{
						// copy the current 'quote' as is until the end of the tweet as the quote end wasn't found
						for (int ind = quoteStartInd; ind < quoteStartInd + quoteLen + 2; ++ind)
						{
							sb.append(tokens.get(ind) + " ");
						}
						nextToWatchIndex = quoteStartInd + quoteLen + 2;
					}
				}
				if (nextToWatchIndex >= tokens.size())
					allQuotesReplaced = true;
			}
			else
			{
				// copy until the end
				allQuotesReplaced = true;
				for (int ind = nextToWatchIndex; ind < tokens.size(); ++ind)
				{
					sb.append(tokens.get(ind) + " ");
				}
			}
		}
			
		return sb.toString().substring(0, sb.length() - 1);
	}
	
	public static String doBasicPreprocessing(String tweetText, boolean toRemoveHashtags)
	{
		tweetText = tweetText.replace(" ", " ");
		tweetText = tweetText.replace('\u00A0', ' ');
	
		if (toRemoveHashtags)
			tweetText = tweetText.replaceAll("#","");
	
		tweetText = tweetText.replace('\n', ' ');
		//tweetText = tweetText.replace('\r', ' ');
		tweetText = tweetText.replace('\t', ' ');
		tweetText = tweetText.toLowerCase();
		
		//4. separate punctiation signs from words (or hashtags!)
		String punctuationSigns = "?!.,;:-\"()[]/{}“”*|~";
		for (int i = 0; i <  punctuationSigns.length(); ++i)
		{
			char c = punctuationSigns.charAt(i);
			tweetText = tweetText.replaceAll("(?<=[a-z>])\\" + c, " " + c);
			tweetText = tweetText.replaceAll("\\" + c + "(?=[a-z<#])", c + " ");
		}
		
		return tweetText;
	}
	
	public static String normalizeText(String tweetText)
	{
		StringBuilder sb = new StringBuilder();
		for (String token : tokenizeWithoutPreprocessing(tweetText))
		{
			String newToken =  UtilText.replaceAlongationsInWord(token);
			newToken = replaceHahaPatternsAlongations(newToken);
			sb.append(" " + newToken); 
		}
		if (sb.length() > 0)
			return sb.substring(1);		
		else
			return "";
		//return UtilText.replaceRepeatedCharacters(tweetText);
	}
	
	/**
	 * This will replace accents with standard symbols and remove all other non-ASCII symbols. 
	 * Notice that if the emojis were not replaced before with normal characters, they will be removed!
	 * Notice also that the text is considered to be properly escaped, and thus will be escaped once more, including quotes (" -> \").
	 * @param tweetText
	 * @param textIsEscaped
	 * @return
	 */
	public static String treatOtherUnicodeCharacters(String tweetText, boolean textIsEscaped) {
		if (textIsEscaped) {
			// need to unescape first!
			tweetText = UtilString.unescapeSpecialSymbols(tweetText);
		}
		// put into the normal form (remove accents) 
		tweetText = Normalizer.normalize(tweetText, Normalizer.Form.NFD);
		tweetText = tweetText.replaceAll("[^\\p{ASCII}]", ""); // this one removes emojis when they are not processed, e.g. when text is not escaped!!!
		if (textIsEscaped) {
			// put back as escaped
			tweetText = UtilString.escapeSpecialSymbols(tweetText);
		}
		return tweetText;
	}
	
	public static String removeAllNonAlphaNumericLetters(String text, boolean leaveCommonPunctuationSigns)
	{
		return  removeAllNonAlphaNumericLetters(text, leaveCommonPunctuationSigns, true);
	}
	
	public static String removeAllNonAlphaNumericLetters(String text, boolean leaveCommonPunctuationSigns, boolean leaveWhiteSpaceInstead)
	{
		String replacement = "";
		if (leaveWhiteSpaceInstead)
			replacement = " ";
		String textWithRemovedSigns = text.replace("\\n", replacement);
		textWithRemovedSigns = UtilText.replaceNonAlphaNumericCharacters(textWithRemovedSigns, leaveCommonPunctuationSigns, replacement);
		textWithRemovedSigns = textWithRemovedSigns.replaceAll("\\s+", " ").trim();
		return textWithRemovedSigns;
	}
	
	public static String replaceNotKnownNumbers(String tweetText)
	{
		
		StringBuilder sb = new StringBuilder();
		for (String token : tokenizeWithoutPreprocessing(tweetText))
		{
			if (token.contains("<emot") || token.contains("<emoji"))
				sb.append(" " + token);
			else
			{
				String newToken = UtilText.replaceNumbersFully(token);
				//newToken = newToken.replace(">", "> ");
				sb.append(" " + newToken);
			}
		}
		if (sb.length() > 0)
			return sb.substring(1);		
		else
			return "";
	}
	
	public static String separateDotCorrectly(String tweetText)
	{
		StringBuilder sb = new StringBuilder();
		boolean prevPointWasAttachedInTheSameWord = false;
		for (int ind = 0; ind < tweetText.length(); ++ind)
		{
			if (tweetText.charAt(ind) == '.')
			{
				int prevLen = lengthOfPrevAlphaNumericToken(tweetText, ind);
				if ( prevLen > 1)
				{
					sb.append(" ");
				}
				else if (prevLen == 1)
					prevPointWasAttachedInTheSameWord = true;
				
				sb.append(".");
				
				int nextLen = lengthOfNextAlphaNumericToken(tweetText, ind);
				
				if (nextLen > 1 || (nextLen == 1 && !prevPointWasAttachedInTheSameWord) )
				{
					sb.append(" ");
				}
			}
			else if (tweetText.charAt(ind) == ' ')
			{
				prevPointWasAttachedInTheSameWord = false;
				sb.append(" ");
			}
			else
				sb.append(tweetText.charAt(ind));
		}
		
		return sb.toString();
	}
	
	static HashSet<String> nonProcessedContinuationsForPrime;
	
	static
	{
		nonProcessedContinuationsForPrime = new HashSet<String>();
		nonProcessedContinuationsForPrime.add("s");
		nonProcessedContinuationsForPrime.add("ve");
		nonProcessedContinuationsForPrime.add("m");
		nonProcessedContinuationsForPrime.add("d");
		nonProcessedContinuationsForPrime.add("re");
		nonProcessedContinuationsForPrime.add("t"); //for n't
		nonProcessedContinuationsForPrime.add("ll");
	}
	
	/**
	 * Separates ' symbol from the text more correctly by introducing some exclusions.
	 * @param tweetText
	 * @return
	 */
	private static String separatePrimeCorrectly(String tweetText)
	{
		StringBuilder sb = new StringBuilder();
		tweetText = tweetText.replace("'s", " 's");
		tweetText = tweetText.replace("'ve", " 've");
		tweetText = tweetText.replace("'m", " 'm");
		tweetText = tweetText.replace("'d", " 'd");
		tweetText = tweetText.replace("'re", " 're");
		tweetText = tweetText.replace("'ll", " 'll");
		
		tweetText = tweetText.replace("n't", " n't");
		for (int ind = 0; ind < tweetText.length(); ++ind)
		{
			if (tweetText.charAt(ind) == '\'')
			{
				String prevToken = getPrevAlphaNumericToken(tweetText, ind);
				
				if ( prevToken.length() > 0 && !prevToken.equals("n"))
				{
					sb.append(" ");
				}
				
				
				sb.append("\'");
				String nextToken = getNextAlphaNumericToken(tweetText, ind);
				
				if (nextToken.length() > 0 && !nonProcessedContinuationsForPrime.contains(nextToken))
				{
					sb.append(" ");
				}
			}
			else
				sb.append(tweetText.charAt(ind));
		}
		
		return sb.toString();
	}
	
	/**
	 * Separates & symbol from the text
	 * @param tweetText
	 * @return
	 */
	public static String separateAmpersCorrectly(String tweetText)
	{
		StringBuilder sb = new StringBuilder();
		for (int ind = 0; ind < tweetText.length(); ++ind)
		{
			if (tweetText.charAt(ind) == '&')
			{
				int prevLen = lengthOfPrevAlphaNumericToken(tweetText, ind);
				int nextLen = lengthOfNextAlphaNumericToken(tweetText, ind);
				
				if ((nextLen + prevLen > 0) && (prevLen == 0 || prevLen > 1) && (nextLen == 0 || nextLen > 1))
				{
					sb.append(" & ");
				}
				else
					sb.append("&");
			}
			else 
				sb.append(tweetText.charAt(ind));
		}
		
		return sb.toString();
	}
	
	private static int lengthOfPrevAlphaNumericToken (String tweetText, int startPos)
	{
		int len = 0;
		if (startPos == 0)
			return 0;
		int ind = startPos - 1;
		while (ind >= 0 && Character.isLetterOrDigit (tweetText.charAt(ind)))
		{
			len++;
			ind--;
		}
		return len;
	}
	
	private static int lengthOfNextAlphaNumericToken (String tweetText, int startPos)
	{
		int len = 0;
		if (startPos ==  tweetText.length() - 1)
			return 0;
		int ind = startPos + 1;
		while (ind <= tweetText.length() - 1 && Character.isLetterOrDigit (tweetText.charAt(ind)))
		{
			len++;
			ind++;
		}
		return len;
	}
	
	private static String getNextAlphaNumericToken (String tweetText, int startPos)
	{
		if (startPos ==  tweetText.length() - 1)
			return "";
		int ind = startPos + 1;
		StringBuilder sb = new StringBuilder();
		while (ind <= tweetText.length() - 1 && Character.isLetterOrDigit (tweetText.charAt(ind)))
		{
			sb.append(tweetText.charAt(ind));
			ind++;
		}
		return sb.toString();
	}
	
	private static String getPrevAlphaNumericToken (String tweetText, int startPos)
	{
		if (startPos == 0)
			return "";
		int ind = startPos - 1;
		StringBuilder sb = new StringBuilder();
		while (ind >= 0 && Character.isLetterOrDigit (tweetText.charAt(ind)))
		{
			sb.insert(0, tweetText.charAt(ind));
			ind--;
		}
		return sb.toString();
	}
	
	
	public static String replaceHahaPatternsAlongations(String word)
	{
		String regex = "(ha|hi|he|ho)\\1+h?";//"((ha)|(hi)|(he)|(ho))\\1+h?";
			
		Pattern	repeatedLaugthPattern = Pattern.compile(regex);
		
		StringBuilder newWord = new StringBuilder();
		
		Matcher m = repeatedLaugthPattern.matcher(word);
		
		int offset = 0;
		
		while(m.find())
		{
			newWord.append(word.substring(offset, m.start()));
			
			newWord.append(m.group().substring(0,2));
			
			offset = m.end();
		}
		newWord.append(word.substring(offset));
		if (offset > 0)
			newWord.append("*");
		
		return newWord.toString();
	}
	
	public static String extraPreprocessing(String tweetText)
	{
		tweetText = tweetText.replace("&lt ;", " '<'");
		tweetText = tweetText.replace("&gt ;", " '>'");
		
		String punctuationSigns = "*&";
		for (int i = 0; i <  punctuationSigns.length(); ++i)
		{
			char c = punctuationSigns.charAt(i);
			tweetText = tweetText.replaceAll("(?<=[a-z>])\\" + c, " " + c);
			tweetText = tweetText.replaceAll("\\" + c + "(?=[a-z<])", c + " ");
		}
		
		tweetText = tweetText.replace("'s", " 's");
		tweetText = tweetText.replace("'ve", " 've");
		tweetText = tweetText.replace("'m", " 'm");
		tweetText = tweetText.replace("'d", " 'd");
		tweetText = tweetText.replace("'re", " 're");
		

		tweetText = tweetText.replaceAll("\\.{4,}", "...");
		tweetText = tweetText.replaceAll("\\){3,}", "))");
		tweetText = tweetText.replaceAll("\\({3,}", "((");
		tweetText = tweetText.replaceAll("\\*{3,}", "**");
		
		
		String fullPunctuationSigns = "?!.,;:-\"()[]/{}ÒÓ*&";
		for (int i = 0; i <  fullPunctuationSigns.length(); ++i)
		{
			char c = punctuationSigns.charAt(i);
			tweetText = tweetText.replaceAll("(?<=[a-z0-9>])\\" + c, " " + c);
			tweetText = tweetText.replaceAll("\\" + c + "(?=[a-z0-9<])", c + " ");
		}
		
		// separate punctuation signs from some
		String signsToSeparate1 = "?!.,;:";
		String signsToSeparate2 = "\"()[]/{}ÒÓ*&";
		for (int i1 = 0; i1 <  signsToSeparate1.length(); ++i1)
			for (int i2 = 0; i2 <  signsToSeparate2.length(); ++i2)
		{
			char c1 = signsToSeparate1.charAt(i1);
			char c2 = signsToSeparate2.charAt(i2);
			tweetText = tweetText.replace(c1 + c2 +"", c1 + " " + c2);
			tweetText = tweetText.replace(c2 + c1 +"", c2 + " " + c1);
		}
		
		// replace some extra emoticons
		String[][] emoticonsNew =
			{
			{":\\","<EMOT88>"},
			{":))","<EMOT89>"},
			{":((","<EMOT90>"}
			};
		for (String[] emoticon: emoticonsNew)
		{
			if (tweetText.matches(".*" + Pattern.quote(emoticon[0].toLowerCase())+"(?=\\b|\\s|\u00A0|$).*"))
			{
				tweetText = tweetText.replaceAll(Pattern.quote(emoticon[0].toLowerCase())+"(?=(\\b|\\s|\u00A0|$))", emoticon[1].toLowerCase()); //replace it for not finding the part of it in the next emoticons
			}
		}
		
		tweetText = tweetText.replaceAll("\\s+", " ");
		
		
		// TODO: test link replacement
		if (tweetText.contains("http"))
		{
			int j = 0;
			j++;
			tweetText = replaceAllLinks(tweetText);
		}
		
		return tweetText;
	}
	
	/** tokenizes text with lemmatization*/
	public List<String> tokenize(String tweetText)
	{
		List<String> tokens = new ArrayList<String>();
		tweetText = preprocessText(tweetText);
		StanfordAnnotator.AnnotationData adata = sannot.getAnnotation(tweetText);
		
		//4. separate words as tokens
		tokens.addAll(Arrays.asList(adata.lemmatizedText.split("\\s+")));
		return tokens;
	}
	
	/** 
	 * Get the list of emojis unicode from file.
	 * @throws Exception
	 */
	public static void loadEmojisUnicodes() throws Exception
	{
			String filePath = TweetPreprocessing.class.getClassLoader().getResource("full_emoji_database-updated.txt").toURI().getPath();
			BufferedReader reader = new BufferedReader(new FileReader(filePath));
			 String line = null;
			 String[] splitted;
			 
			 List<String[]> emojiData = new ArrayList<String[]>();
			 
			 while ((line = reader.readLine()) != null) 
			{
				splitted=line.split("\t", -1);	
				String[] curEm = new String[2];
				curEm[0] = splitted[0];
				curEm[1] = splitted[1];
				emojiData.add(curEm);
				if (splitted[4].length() > 0)
				{
					curEm = new String[2];
					curEm[0] = splitted[4];
					curEm[1] = splitted[1];
					emojiData.add(curEm);
				}
				
				
			}
			emojiUnicode = new String[emojiData.size()][2];
			int i=0;
			
			emojiUnicodeMap = new HashMap<String, String>();
			for (String[] emoji : emojiData)
			{
				emojiUnicode[i][0]=emoji[0];
				emojiUnicode[i][1]=emoji[1];
				i++;
				emojiUnicodeMap.put(emoji[0].toLowerCase(), emoji[1]);
			}
			reader.close();
	}	
	
	public static String[][] getEmoticonsDatabase()
	{
		return emoticons;
	}
	
	/**
	 * Returns the list of emojis in the following format:
	 * String[emoji_num][2] : for each emoji: 0th entry - unicode, 1st entry - assigned name (e.g. 'face1')
	 * @return
	 */
	public static String[][] getEmojiDatabase()
	{
		if (emojiUnicode == null)
			try {
				loadEmojisUnicodes();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return emojiUnicode;
	}
	
	static String[][] emoticons = {
		{"(.V.)","<EMOT1>"},
		{" O:-)","<EMOT2>"},
		{" X-D","<EMOT3>"},
		{" B^D","<EMOT4>"},
		{" O-)","<EMOT5>"},
		{" o_O","<EMOT6>"},
		{" O.o","<EMOT7>"},
		{" B-)","<EMOT8>"},
		{":'-(","<EMOT9>"},
		{":'-)","<EMOT10>"},
		{" X3 ","<EMOT11>"},
		{":-()","<EMOT12>"},
		{">:]","<EMOT13>"},
		{":-)","<EMOT14>"},
		{":o)","<EMOT15>"},
		{":c)","<EMOT16>"},
		{":^)","<EMOT17>"},
		{">:D","<EMOT18>"},
		{":-D","<EMOT19>"},
		{"8-D","<EMOT20>"},
		{"x-D","<EMOT21>"},
		{" xD","<EMOT22>"},
		{" XD","<EMOT23>"},
		{"=-D","<EMOT24>"},
		{"=-3","<EMOT25>"},
		{":-(","<EMOT26>"},
		{":=(","<EMOT27>"},
		{">:(","<EMOT28>"},
		{":-<","<EMOT29>"},
		{"8-[","<EMOT30>"},
		{":=@","<EMOT31>"},
		{":-?","<EMOT32>"},
		{":-$","<EMOT33>"},
		{":-S","<EMOT34>"},
		{":B)","<EMOT35>"},
		{"%-)","<EMOT36>"},
		{">.<","<EMOT37>"},
		{":_(","<EMOT38>"},
		{":'(","<EMOT39>"},
		{":')","<EMOT40>"},
		{"*-*","<EMOT41>"},
		{":-|","<EMOT42>"},
		{":-O","<EMOT43>"},
		{":=o","<EMOT44>"},
		{"*.*","<EMOT45>"},
		{":-P","<EMOT46>"},
		{":-p","<EMOT47>"},
		{"^_^","<EMOT48>"},
		{"</3","<EMOT49>"},
		{";-)","<EMOT50>"},
		{"-_-","<EMOT87>"},
		{":))","<EMOT89>"},
		{":((","<EMOT90>"},
		{":)","<EMOT51>"},
		{":]","<EMOT52>"},
		{":3","<EMOT53>"},
		{":>","<EMOT54>"},
		{"=]","<EMOT55>"},
		{"8)","<EMOT56>"},
		{"=)","<EMOT57>"},
		{":}","<EMOT58>"},
		{":D","<EMOT59>"},
		{"=D","<EMOT60>"},
		{"=3","<EMOT61>"},
		{":(","<EMOT62>"},
		{":[","<EMOT63>"},
		{":c","<EMOT64>"},
		{"):","<EMOT65>"},
		{"=(","<EMOT66>"},
		{":{","<EMOT67>"},
		{":@","<EMOT68>"},
		{"=$","<EMOT69>"},
		{"=S","<EMOT70>"},
		{":|","<EMOT71>"},
		{"=\\","<EMOT72>"},
		{":o","<EMOT73>"},
		{":o","<EMOT74>"},
		{":O","<EMOT75>"},
		{":p","<EMOT76>"},
		{":P","<EMOT77>"},
		{"=P","<EMOT78>"},
		{"<3","<EMOT79>"},
		{";)","<EMOT80>"},
		{":/","<EMOT81>"},
		{"(:","<EMOT82>"},
		{";d","<EMOT83>"},
		{":d","<EMOT84>"},
		{";]","<EMOT85>"},
		{"=d","<EMOT86>"},
		{":\\","<EMOT88>"}, 
		{"=[","<EMOT91>"},
		{":x","<EMOT92>"},
		{":*","<EMOT93>"} // next one 94!
	};
	
	
	public static int getNumberOfPunctuationSigns(String text, String punctuationSigns)
	{
		int result = 0;
		for (int i = 0; i <  punctuationSigns.length(); ++i)
		{
			char c = punctuationSigns.charAt(i);
			Matcher matcher = 
					Pattern.compile("\\" + c).matcher(text);
			while (matcher.find()) {
				result++;
            }
		}
		return result;
	}
	
	/**
	 * Considers that the word is from preprocessed text. 
	 * Returns true only if word is not a punctuation mark, username, stop word, or hashtag; and its length is at least 2 symbols; and in the dictionary
	 */
	public static boolean isWordReal(String word)
	{
		return isWordReal(word,true, true);
	}
	/**
	 * Considers that the word is from preprocessed text. 
	 * Returns true only if word is not a punctuation mark, username, stop word, or hashtag; and its length is at least 2 symbols; and in the dictionary
	 */
	public static boolean isWordReal(String word, boolean useLemmas, boolean useDictionary)
	{
		boolean isRealWord = !( word.length() < 2 || 
								UtilText.containsOnlyPunctuations(word) ||
								UtilText.isStopWord(word) ||
								word.equals("<username>") || 
								word.startsWith("#") ||
								word.startsWith("<emoji_") ||
								word.startsWith("<emot") ||
								word.equals("\n") ||
								word.equals("<int_num>") || word.equals("<quote>") || word.equals("<link>")
							  );
		
		// dictionary check
		if (isRealWord && useDictionary)
		{
			boolean isDictWord = UtilText.isWNDictionaryWord(word);
			if (!isDictWord && useLemmas)
			{
				// check if lemma is dict word
				// TODO: potential speed up: first check is stemmed form is different, and only then check lemmatized one
				String lemma = UtilText.lemmatizeText(word);
				if (!word.equals(lemma))
				{
					isDictWord = UtilText.isWNDictionaryWord(lemma);
				}
			}
			isRealWord = isDictWord;
		}
		
		return isRealWord;
	}
	
	/**
	 * Computes number of words based on simple split of text into words using a white space
	 * @param text
	 * @return
	 */
	public static int getNumberOfWords(String text)
	{
		List<String> tokens = tokenizeWithoutPreprocessing(text);
		return tokens.size();
	}
	
	/** this works on the preprocessed text. It counts the number of tokens that are not punctuation marks, usernames, or hashtags; not a stop word and is in the English dictionary (or lemmatized version is in the dictionary)**/
	public static int getNumberOfRealWords(String text)
	{
		return getNumberOfRealWords(text, true, true);
	}

	/** this works on the preprocessed text. It counts the number of tokens that are not punctuation marks, usernames, or hashtags; not a stop word and is in the English dictionary (or lemmatized version is in the dictionary)**/
	public static int getNumberOfRealWords(String text, boolean useLemmas, boolean useDictionary)
	{
		List<String> tokens = tokenizeWithoutPreprocessing(text);
		
		int realWordNum = 0;
		
		for (String token : tokens)
		{
			if (isWordReal(token, useLemmas, useDictionary))
				realWordNum++;
		}
		
		return realWordNum;
	}
	
	public static  int getNumberOfTabWords(String text)
	{
		int result = 0;
		Vector<String> tokens = getTokens(text);
		for (String token : tokens)
		{
			if (token.length() > 3 && isTabSpelled(token))
				result++;
		}
		
		return result;
	}
	
	public static boolean isTabSpelled(String word)
	{
		for (int i = 0; i < word.length(); ++i)
			if (!Character.isUpperCase(word.charAt(i)))
				return false;	
		return true;
	}
	
	public static int getNumberOfTimesRepeatedCharacters(String text)
	{	
		String regex = "([\\w\\!\\?])\\1{2,}";
		Matcher m = Pattern.compile(regex).matcher(text);
		
		int result = 0;
		
		while(m.find())
		{
			result++;
		}
		
		return result;
	}
	
	/**
	 * This splits the tokens in the text based on the letters and digits only
	 * @param text
	 * @return
	 */
	public static Vector<String> getTokens(String text)
	{
		Vector<String> tokens = new Vector<String>();
		
		String regex = "[\\w]+";
		
		Matcher m = Pattern.compile(regex).matcher(text);
		
		while(m.find())
		{
			tokens.add(m.group());
		}
		
		return tokens;
	}
	
	/** Returns all symbolic tokens, including usernames and hashtags */
	public static Vector<String> getWordTokens(String text)
	{
		Vector<String> tokens = new Vector<String>();
		
		String regex = "[#@]?[\\w]+";
		
		Matcher m = Pattern.compile(regex).matcher(text);
		
		while(m.find())
		{
			tokens.add(m.group());
		}
		
		return tokens;
	}
	
	/** Returns all the hashtags which appeared in the tweet text. 
	 *  Hashtag symbol (#) is not included: if "#happy" found, "happy" is returned
	 * */
	public static List<String> getHashTags(String text)
	{
		List<String> tokens = new ArrayList<String>();
		
		String regex = "#[\\w]+";
		
		Matcher m = Pattern.compile(regex).matcher(text);
		
		while(m.find())
		{
			tokens.add(m.group().substring(1));
		}
		
		return tokens;
	}
	
	/**
	 * Extracts the present emojis from the preprocessed text. 
	 * This function assumes that emojis are separate tokens in the form <emoji_NAME>
	 * @param text
	 * @return
	 */
	public static List<String> getEmojisFromPreprocessed(String text)
	{
		List<String> tokens = tokenizeWithoutPreprocessing(text);
		List<String> foundEmojis = new ArrayList<String>();
		for (String token : tokens) {
			if (token.startsWith("<emoji_"))
				foundEmojis.add(token);
		}
		return foundEmojis;
	}
	
	
	public static int countEmotionIntensifiers(String text)
	{
		int par1 = getNumberOfPunctuationSigns(text, "!");
		int par2 = getNumberOfTimesRepeatedCharacters(text);
		int par3 = getNumberOfTabWords(text);
		
		return (par1 + par2 + par3);
	}
	
	public static boolean containsEmotionIntensifiers(String text)
	{
		return (countEmotionIntensifiers(text) > 0);
	}
	
	public static int getLinkStart (String text)
	{
		return UtilText.getPatternFirstStart(text, TweetPatterns.linkRegEx);
	}
	
	public static String getLinkString (String text)
	{
		return UtilText.getPatternInstance(text, TweetPatterns.linkRegEx);
	}
	
	public static Pair<Integer, Integer> getUserNameStartEnd (String text, int startSearchIndex)
	{
		return UtilText.getPatternFirstStartEnd(text, TweetPatterns.usernameRegEx, startSearchIndex);
	}
	
	public static Pair<Integer, Integer> getUserNameStartEnd (String text)
	{
		return UtilText.getPatternFirstStartEnd(text, TweetPatterns.usernameRegEx);
	}
	
	public static boolean containsEmoticon(String text)
	{
		text = text.toLowerCase();
		boolean containsEmoticon = false;
		for(String[] emoticon: emoticons)
		{
			if (text.contains(emoticon[0].toLowerCase()))
			{
				containsEmoticon = true;
				break;
			}
		}
		return containsEmoticon;
	}
	
	/**
	 * Detects whether <link> (URL) is present in the preprocessed text.
	 * @param text
	 * @return
	 */
	public static boolean containsLinkInPreprocessed(String text)
	{
		return text.contains("<link>");
	}
	


}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package processing;

import java.util.List;
import java.util.Map;

import data.documents.Tweet;

public class PreprocessingProcessor {

	public static void preprocessAllTweets(List<? extends Tweet> tweets)
	{
		for (Tweet tweet : tweets)
		{
			tweet.text = TweetPreprocessing.preprocessTextNovel(tweet.text);
		}
	}
	

	public static void preprocessAllTweets(Map<Integer, String> textData)
	{
		for (Integer twId : textData.keySet())
		{
			String newText = TweetPreprocessing.preprocessTextNovel(textData.get(twId));
			textData.put(twId, newText);
		}
	}
	
	public static void preprocessAllTexts(List<String> textData)
	{
		for (int i = 0; i < textData.size(); ++i) {
			String newText = TweetPreprocessing.preprocessTextNovel(textData.get(i));
			textData.set(i, newText);
		}
	}
}

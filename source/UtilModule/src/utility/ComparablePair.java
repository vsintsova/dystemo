/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package utility;

public class ComparablePair<N, V> extends Pair<N,V> implements Comparable {
/**
	 * 
	 */
	private static final long serialVersionUID = -8294684492115910990L;
	boolean compareByFirst = true;
	
	public ComparablePair(N name, V value) {
		super(name, value);
	}
	public ComparablePair(Pair<N,V> pair)
	{
		this(pair.first, pair.second);
	}
	
	@Override
	public int compareTo(Object arg0) {
		if (compareByFirst)
		{
			N i1 = this.getFirst();
			N i2 = ((Pair<N,V>)arg0).getFirst();
			return ((Comparable) i1).compareTo(i2);
		}
		else
		{
			V i1 = this.getSecond();
			V i2 = ((Pair<N,V>)arg0).getSecond();
			return ((Comparable) i1).compareTo(i2);
		}
	}

}

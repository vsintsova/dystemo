/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package functionality;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

public class UtilArrays {
	public static int getSum(int[] array)
	{
		int sum = 0;
		for (int i = 0; i < array.length; ++i)
		{
			sum += array[i];
		}
		return sum;
	}
	
	public static double getSum(double[] array)
	{
		double sum = 0.0;
		for (int i = 0; i < array.length; ++i)
		{
			sum += array[i];
		}
		return sum;
	}
	
	public static double getSum(Double[] array)
	{
		double sum = 0.0;
		for (int i = 0; i < array.length; ++i)
		{
			sum += array[i];
		}
		return sum;
	}
	
	/**
	 * This includes the ending index and starting index
	 * @param array
	 * @param startInd
	 * @param endInd
	 * @return
	 */
	public static double getSumOfSubArray(double[] array, int startInd, int endInd)
	{
		if (!checkSubArrayBounds(array, startInd, endInd))
			return Double.NaN;
		
		double sum = 0.0;
		for (int i = startInd; i <= endInd; ++i)
		{
			sum += array[i];
		}
		return sum;
	}
	
	/** returns the result of multiplication*/
	public static double[] multiplyArray(double[] array, double multiplier)
	{
		if (array == null)
			return null;
		double[] resArray = new double[array.length];
		for (int i = 0; i < array.length; ++i)
		{
			resArray[i] = array[i] * multiplier;
		}
		return resArray;
	}
	
	/** multiplies the values in the array with the multiplier (the array itself changes)*/
	public static void multiplyArrayInternal(double[] array, double multiplier)
	{
		if (array == null)
			return;
		for (int i = 0; i < array.length; ++i)
		{
			array[i] = array[i] * multiplier;
		}
	}
	
	
	public static double[] multiplyArray(int[] array, double multiplier)
	{
		if (array == null)
			return null;
		double[] resArray = new double[array.length];
		for (int i = 0; i < array.length; ++i)
		{
			resArray[i] = array[i] * multiplier;
		}
		return resArray;
	}
	
	public static double[] castIntArrayToDouble(int[] array)
	{
		return multiplyArray(array, 1.0);
	}
	
	public static int[] sumTwoArrays(int[] array1, int[] array2) throws Exception
	{
		if (array1.length != array2.length)
			throw new Exception("Arrays of different lenghts cannot be sum up");
		
		int[] resArray = new int[array1.length];
		for (int i = 0; i < array1.length; ++i)
		{
			resArray[i] = array1[i] + array2[i];
		}
		return resArray;
	}
	
	/** 
	 * The second array is added to the first one
	 * @param array1
	 * @param array2
	 * @throws Exception
	 */
	public static void sumTwoArraysInternal(int[] array1, int[] array2) throws Exception
	{
		if (array1.length != array2.length)
			throw new Exception("Arrays of different lenghts cannot be sum up");
		
		for (int i = 0; i < array1.length; ++i)
		{
			array1[i] = array1[i] + array2[i];
		}
	}
	
	/** 
	 * The second array is added to the first one
	 * @param array1
	 * @param array2
	 * @throws Exception
	 */
	public static void sumTwoArraysInternal(double[] array1, double[] array2) throws Exception
	{
		if (array2 == null) // nothing to add!
			return;
		if (array1.length != array2.length)
			throw new Exception("Arrays of different lenghts cannot be sum up");
		
		for (int i = 0; i < array1.length; ++i)
		{
			array1[i] = array1[i] + array2[i];
		}
	}

	/** returns the sum of two arrays*/
	public static double[] sumTwoArrays(double[] array1,
			double[]  array2) throws Exception {
		if (array1.length != array2.length)
			throw new Exception("Arrays of different lenghts cannot be sum up");
		
		double[] resArray = new double[array1.length];
		for (int i = 0; i < array1.length; ++i)
		{
			resArray[i] = array1[i] + array2[i];
		}
		return resArray;
	}
	
	/** add all the elements of the second array into the first one*/
	public static void addArray(double[] array1,
			double[]  array2) throws Exception {
		if (array1.length != array2.length)
			throw new Exception("Arrays of different lenghts cannot be sum up");
		
		for (int i = 0; i < array1.length; ++i)
		{
			 array1[i] = array1[i] + array2[i];
		}
	}
	
	public static<V  extends Comparable<V>> V getMax(V[] array)
	{
		V maximalValue = null;
		for (int index = 0; index < array.length; ++index)
		{
			V curValue = array[index];
			if (maximalValue == null)
				maximalValue = curValue;
			else
			{
				int comp = curValue.compareTo(maximalValue);
				if (comp > 0)
					maximalValue = curValue;
			}
		}
		return maximalValue;
	}
	
	public static<V  extends Comparable<V>> V getMaxInSubarray(V[] array, int startInd, int endInd)
	{
		if (!checkSubArrayBounds(array, startInd, endInd))
			return null;
		
		V maximalValue = null;
		for (int index = startInd; index <= endInd; ++index)
		{
			V curValue = array[index];
			if (maximalValue == null)
				maximalValue = curValue;
			else
			{
				int comp = curValue.compareTo(maximalValue);
				if (comp > 0)
					maximalValue = curValue;
			}
		}
		return maximalValue;
	}
	

	public static double getMaxInSubarray(double[] array, int startInd,
			int endInd) {
		if (!checkSubArrayBounds(array, startInd, endInd))
			return Double.NaN;
		
		Double maximalValue = -Double.MAX_VALUE;
		for (int index = startInd; index <= endInd; ++index)
		{
			Double curValue = array[index];
			if (maximalValue == null)
				maximalValue = array[index];
			else
			{
				int comp = curValue.compareTo(maximalValue);
				if (comp > 0)
					maximalValue = curValue;
			}
		}
		return maximalValue;
	}
	
	public static<V  extends Comparable<V>> List<Integer> getIndexesOfMax(V[] array)
	{
		V maximalValue = null;
		for (int index = 0; index < array.length; ++index)
		{
			V curValue = array[index];
			if (maximalValue == null)
				maximalValue = curValue;
			else
			{
				int comp = curValue.compareTo(maximalValue);
				if (comp > 0)
					maximalValue = curValue;
			}
		}
		
		List<Integer> foundKeys = new ArrayList<Integer>();
		for (int index = 0; index < array.length; ++index)
		{
			if (array[index].compareTo(maximalValue) == 0)
				foundKeys.add(index);
		}
		
		return foundKeys;
	}
	
	public static List<Integer> getIndexesOfMax(double[] array)
	{
		double threshold = -Double.MAX_VALUE;
		return getIndexesOfMaxWithThreshold(array, threshold, false);
	}
	
	public static List<Integer> getIndexesOfMin(double[] array)
	{
		double threshold = Double.MAX_VALUE;
		return getIndexesOfMinWithThreshold(array, threshold, false);
	}
	
	public static List<Integer> getIndexesOfMaxInSubArray(double[] array, int startInd, int endInd)
	{
		double threshold = -Double.MAX_VALUE;
		return getIndexesOfMaxWithThresholdInSubArray(array, threshold, false, startInd, endInd);
	}
	
	public static List<Integer> getIndexesOfMaxWithThreshold(double[] array, double threshold, boolean isStrongThreshold)
	{
		return getIndexesOfMaxWithThresholdInSubArray(array, threshold, isStrongThreshold, 0, array.length - 1);
	}
	
	public static List<Integer> getIndexesOfMinWithThreshold(double[] array, double threshold, boolean isStrongThreshold)
	{
		return getIndexesOfMinWithThresholdInSubArray(array, threshold, isStrongThreshold, 0, array.length - 1);
	}
	
	
	public static List<Integer> getIndexesOverThreshold(double[] array, double threshold, boolean isStrongThreshold)
	{
		return getIndexesOverThresholdInSubArray(array, threshold, isStrongThreshold, 0, array.length - 1);
	}
	
	private static boolean checkSubArrayBounds(double[] array, int startInd, int endInd)
	{
		if (startInd < 0 || startInd >= array.length || endInd < 0 || endInd >= array.length || startInd > endInd)
			return false;
		return true;
	}
	
	private static boolean checkSubArrayBounds(int[] array, int startInd, int endInd)
	{
		if (startInd < 0 || startInd >= array.length || endInd < 0 || endInd >= array.length || startInd > endInd)
			return false;
		return true;
	}
	
	private static<T extends Object> boolean checkSubArrayBounds(List<T> array, int startInd, int endInd)
	{
		if (startInd < 0 || startInd >= array.size() || endInd < 0 || endInd >= array.size() || startInd > endInd)
			return false;
		return true;
	}
	
	private static<T extends Object> boolean checkSubArrayBounds(T[] array, int startInd, int endInd)
	{
		if (startInd < 0 || startInd >= array.length || endInd < 0 || endInd >= array.length || startInd > endInd)
			return false;
		return true;
	}
	
	private static<T extends Object> boolean checkIndexWithinBounds(int arraySize, int ind)
	{
		if (ind < arraySize && ind >= 0)
			return true;
		return false;
	}
	
	
	public static List<Integer> getIndexesOverThresholdInSubArray(double[] array, double threshold, boolean isStrongThreshold, int startInd, int endInd)
	{
		if (!checkSubArrayBounds(array, startInd, endInd))
			return null;
		List<Integer> foundKeys = new ArrayList<Integer>();
		
		for (int index = startInd; index <= endInd; ++index)
		{
			Double curValue = array[index];
			if (curValue.compareTo(threshold) < 0)
				continue;
			if (curValue.compareTo(threshold) == 0 && isStrongThreshold)
				continue;
			
			// current value is over the threshold
			
			foundKeys.add(index);
		}
	
		return foundKeys;
	}
	
	public static List<Integer> getIndexesOfMaxWithThresholdInSubArray(double[] array, double threshold, boolean isStrongThreshold, int startInd, int endInd)
	{
		if (!checkSubArrayBounds(array, startInd, endInd))
			return null;
		
		Double maximalValue = null;
		for (int index = startInd; index <= endInd; ++index)
		{
			Double curValue = array[index];
			if (curValue.compareTo(threshold) < 0)
				continue;
			if (curValue.compareTo(threshold) == 0 && isStrongThreshold)
				continue;
			if (maximalValue == null)
				maximalValue = curValue;
			else
			{
				int comp = curValue.compareTo(maximalValue);
				if (comp > 0)
					maximalValue = curValue;
			}
		}
		
		List<Integer> foundKeys = new ArrayList<Integer>();
		if (maximalValue != null)
		{
			for (int index = startInd; index <= endInd; ++index)
			{
				if (array[index] == maximalValue)
					foundKeys.add(index);
			}
		}
		return foundKeys;
	}
	
	public static List<Integer> getIndexesOfMinWithThresholdInSubArray(double[] array, double threshold, boolean isStrongThreshold, int startInd, int endInd)
	{
		if (!checkSubArrayBounds(array, startInd, endInd))
			return null;
		
		Double minimalValue = null;
		for (int index = startInd; index <= endInd; ++index)
		{
			Double curValue = array[index];
			if (curValue.compareTo(threshold) > 0)
				continue;
			if (curValue.compareTo(threshold) == 0 && isStrongThreshold)
				continue;
			if (minimalValue == null)
				minimalValue = curValue;
			else
			{
				int comp = curValue.compareTo(minimalValue);
				if (comp < 0)
					minimalValue = curValue;
			}
		}
		
		List<Integer> foundKeys = new ArrayList<Integer>();
		if (minimalValue != null)
		{
			for (int index = startInd; index <= endInd; ++index)
			{
				if (array[index] == minimalValue)
					foundKeys.add(index);
			}
		}
		return foundKeys;
	}
	
	public static int[] parseIntegerArrayFromString (String arrayStr, String separator) throws Exception
	{
		StringTokenizer st = new StringTokenizer(arrayStr, separator);
		int[] array = new int[st.countTokens()];
		
		int index = 0;
		try
		{
			while (st.hasMoreTokens())
			{
				String curElem = st.nextToken();
				array[index] = Integer.parseInt(curElem);
				index++;
			}
		}
		catch (Exception e)
		{
			throw new Exception("Unparsable string to integer", e);
		}
		
		return array;
	}
	
	public static double[] parseDoubleArrayFromString (String arrayStr, String separator) throws Exception
	{
		StringTokenizer st = new StringTokenizer(arrayStr, separator);
		double[] array = new double[st.countTokens()];
		
		int index = 0;
		try
		{
			while (st.hasMoreTokens())
			{
				String curElem = st.nextToken();
				array[index] = Double.parseDouble(curElem);
				index++;
			}
		}
		catch (Exception e)
		{
			throw new Exception("Unparsable string to integer", e);
		}
		
		return array;
	}
	
	/** divides all array elements on the sum of all elements*/
	public static double[] normalizeArray(double[] array)
	{
		double sum = UtilArrays.getSum(array);
		double[] result = new double[array.length];
		if (sum > 0.0)
			result = UtilArrays.multiplyArray(array, 1/sum);
		else
			Arrays.fill(result, 0.0);
		return result;
	}
	
	/** divides all array elements on the sum of all elements*/
	public static void normalizeArrayInternal(double[] array)
	{
		double sum = UtilArrays.getSum(array);
		if (sum == 1.0)
			return;
		if (sum > 0.0)
			UtilArrays.multiplyArrayInternal(array, 1/sum);
		else
			Arrays.fill(array, 0.0);
	}

	public static double[] normalizeArray(int[] array) {
		double sum = UtilArrays.getSum(array);
		double[] result = new double[array.length];
		if (sum > 0)
			result = UtilArrays.multiplyArray(array, 1/sum);
		else
			Arrays.fill(result, 0.0);
		return result;
	}
	
	public static<T extends Object> String join(T[] array, String separator)
	{
		StringBuilder sb = new StringBuilder();
		boolean firstElemPrinted = false;
		for (T item : array)
		{
			if (firstElemPrinted)
				sb.append(separator);
			sb.append(item.toString());
			firstElemPrinted = true;
		}
		return sb.toString();
	}
	
	public static String join(double[] array, String separator)
	{
		StringBuilder sb = new StringBuilder();
		for (double item : array)
		{
			if (sb.length() > 0)
				sb.append(separator);
			sb.append(item);
		}
		return sb.toString();
	}
	
	public static String joinInShortFormat(double[] array, String separator)
	{
		StringBuilder sb = new StringBuilder();
		for (double item : array)
		{
			if (sb.length() > 0)
				sb.append(separator);
			sb.append(UtilString.formatNumberInShort(item));
		}
		return sb.toString();
	}
	
	public static String join(int[] array, String separator)
	{
		StringBuilder sb = new StringBuilder();
		for (int item : array)
		{
			if (sb.length() > 0)
				sb.append(separator);
			sb.append(item);
		}
		return sb.toString();
	}
	
	public static<T extends Object> int findFirstWithLinearSearch(T[] array, T searchElement)
	{
		int foundIndex = -1;
		for (int index = 0; index < array.length; ++index)
		{
			if (array[index] == searchElement)
				foundIndex = index;
		}
		return foundIndex;
	}
	
	public static int findFirstWithLinearSearch(int[] array, int searchElement)
	{
		int foundIndex = -1;
		for (int index = 0; index < array.length; ++index)
		{
			if (array[index] == searchElement)
				foundIndex = index;
		}
		return foundIndex;
	}

	public static<T extends Object> T getRandElement(List<T> coll) {
		int maxIndex = coll.size() - 1;
		int minIndex = 0;
	
		int chosenIndex = minIndex + (int)(Math.random() * ((maxIndex - minIndex) + 1));
		return coll.get(chosenIndex);
	}

	public static double getMax(double[] array) {
		double maximalValue = -Double.MAX_VALUE;
		for (int index = 0; index < array.length; ++index)
		{
			double curValue = array[index];
			if (curValue > maximalValue)
				maximalValue = curValue;
		}
		return maximalValue;
		
	}
	
	public static double getMax(int[] array) {
		double maximalValue = -Double.MAX_VALUE;
		for (int index = 0; index < array.length; ++index)
		{
			double curValue = array[index];
			if (curValue > maximalValue)
				maximalValue = curValue;
		}
		return maximalValue;
		
	}
	
	public static double getMin(double[] array) {
		double minimalValue = Double.MAX_VALUE;
		for (int index = 0; index < array.length; ++index)
		{
			double curValue = array[index];
			if (curValue < minimalValue)
				minimalValue = curValue;
		}
		return minimalValue;
		
	}
	
	public static double getMin(int[] array) {
		double minimalValue = Double.MAX_VALUE;
		for (int index = 0; index < array.length; ++index)
		{
			double curValue = array[index];
			if (curValue < minimalValue)
				minimalValue = curValue;
		}
		return minimalValue;
		
	}
	
	public static double getAverage(double[] array) {
		double sum = getSum(array);
		return sum / (1.0 * array.length);
		
	}
	
	public static<T extends Object> List<T> subArray(T[] array, int startInd, int endInd)
	{
		if (!checkSubArrayBounds(array, startInd, endInd))
			return null;
		
		List<T> foundElems = new ArrayList<T>();
		for (int i = startInd; i <= endInd; ++i)
		{
			foundElems.add(array[i]);
		}
		return foundElems;
	}
	
	public static<T extends Object> String[] subArray(String[] array, int startInd, int endInd)
	{
		if (!checkSubArrayBounds(array, startInd, endInd))
			return null;
		
		String[] foundElems = new String[endInd - startInd + 1];
		for (int i = startInd; i <= endInd; ++i)
		{
			foundElems[i - startInd] = array[i];
		}
		return foundElems;
	}
	
	public static<T extends Object> String[] subArray(String[] array, int startInd)
	{
		return subArray(array, startInd, array.length - 1);
	}
	
	/**
	 * Extracts a subarray corresponding to the specified indices (the order of indices is preserved).
	 * Will return null if one given index is out of array limits.
	 * @param array
	 * @param indices_to_return
	 * @return
	 */
	public static<T extends Object> List<T> subArrayFixed(T[] array, int[] indices_to_return)
	{
		List<T> foundElems = new ArrayList<T>();
		for (int i : indices_to_return)
		{
			if (!checkIndexWithinBounds(array.length, i))
				return null;
			foundElems.add(array[i]);
		}
		return foundElems;
	}
	
	/**
	 * Returns the sublist including the endInd
	 * @param list
	 * @param startInd
	 * @param endInd
	 * @return
	 */
	public static<T extends Object> List<T> subList(List<T> list, int startInd, int endInd)
	{
		if (!checkSubArrayBounds(list, startInd, endInd))
			return null;
		
		List<T> foundElems = new ArrayList<T>();
		for (int i = startInd; i <= endInd; ++i)
		{
			foundElems.add(list.get(i));
		}
		return foundElems;
	}
	
	public static void multiplyArraysByElementsInternally(double[] array1, double[] array2) throws Exception
	{
		if (array1.length != array2.length)
			throw new Exception("Arrays of different lenghts cannot be sum up");
		
		for (int i = 0; i < array1.length; ++i)
		{
			 array1[i] = array1[i] * array2[i];
		}
	}
	
	
	/** increments the values in the array on the incrementValue (the array itself changes)*/
	public static void incrementArrayInternal(double[] array, double incrementValue)
	{
		if (array == null)
			return;
		for (int i = 0; i < array.length; ++i)
		{
			array[i] += incrementValue;
		}
	}
	
	public static<T extends Object> Set<T> getRandSubset(List<T> coll, int subsetSize) {
		int maxIndex = coll.size() - 1;
		int minIndex = 0;
	
		Set<T> subset = new HashSet<T>();
		while (subset.size() < subsetSize)
		{
			int chosenIndex = minIndex + (int)(Math.random() * ((maxIndex - minIndex) + 1));
			subset.add(coll.get(chosenIndex));
		}
		return subset;
	}
	
	public static int[] copyArray(int[] array)
	{
		int[] newArray = new int[array.length];
		System.arraycopy(array, 0, newArray, 0, array.length);
		return newArray;
	}
	
	public static double[] copyArray(double[] array)
	{
		return Arrays.copyOf(array, array.length);
	}
	
	public static<T extends Object> T[] copyArray(T[] array)
	{
		return Arrays.copyOf(array, array.length);
	}

	public static double[] subArray(double[] array, int startInd, int endInd) {
		if (!checkSubArrayBounds(array, startInd, endInd))
			return null;
		
		double[] newArray = new double[endInd - startInd + 1];
		System.arraycopy(array, startInd, newArray, 0, newArray.length);
		return newArray;
	}
	
	/** This will create a sub array, with endInd included **/
	public static int[] subArray(int[] array, int startInd, int endInd) {
		if (!checkSubArrayBounds(array, startInd, endInd))
			return null;
		
		int[] newArray = new int[endInd - startInd + 1];
		System.arraycopy(array, startInd, newArray, 0, newArray.length);
		return newArray;
	}

	
}

/*
    Copyright 2016 Valentina Sintsova, Sudheendra Hangal
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package functionality;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.Map.Entry;

import utility.ComparablePair;
import utility.Pair;



public class UtilCollections {
	
	public static<T extends Object> void printMap(Map<T, Integer> map, PrintWriter pw) {
		Set<T> itMod = map.keySet();
		for (T s : itMod) {
			pw.print(s+"\t"+map.get(s));
			pw.println();
		}
		pw.close();
	}
	/** increments the integer value for specified key in the map or creates the new entry with value 1 */
	public static<T extends Object> void incrementIntValueInMap(Map<T, Integer> map, T key)
	{
		Integer curNum = map.get(key);
		if (curNum == null)
			curNum = 0;
		curNum++;
		map.put(key, curNum);
	}
	
	/** increments the integer value for specified key in the map or creates the new entry with value incValue */
	public static<T extends Object> void incrementIntValueInMap(Map<T, Integer> map, T key, int incValue)
	{
		Integer curNum = map.get(key);
		if (curNum == null)
			curNum = 0;
		curNum+=incValue;
		map.put(key, curNum);
	}
	
	/** increments the integer value for specified key in the map or creates the new entry with value incValue */
	public static<T extends Object> void incrementIntValueInMap(Map<T, Double> map, T key, double incValue)
	{
		Double curNum = map.get(key);
		if (curNum == null)
			curNum = 0.0;
		curNum+=incValue;
		map.put(key, curNum);
	}

	/** appends a new element newValue into a set for specified key in the map or creates the new link for key with set containing newValue */
	public static<T,V extends Object> void appendNewObjectToValueInMap(Map<T, Set<V>> map, T key, V newValue)
	{
		Set<V> curSet = map.get(key);
		if (curSet == null)
		{
			curSet = new HashSet<V>();
			map.put(key, curSet);
		}
		curSet.add(newValue);
	}
	
	/** appends a new element newValue into a set for specified key in the map or creates the new link for key with set containing newValue */
	public static<T,V extends Object> void appendNewObjectToListInMap(Map<T, List<V>> map, T key, V newValue)
	{
		List<V> curSet = map.get(key);
		if (curSet == null)
		{
			curSet = new ArrayList<V>();
			map.put(key, curSet);
		}
		curSet.add(newValue);
	}
	
	/** list should be sorted before sending here!*/
	public static<T extends Object> void deleteDublicates(List<T> list)
	{
		for (int ind = list.size() - 2; ind >= 0; --ind)
		{
			if (list.get(ind).equals(list.get(ind + 1)))
				list.remove(ind + 1);
		}
	}
	
	/** list should be sorted before sending here!*/
	public static<T extends Object> void removeRepeatedInSortedList(List<T> list)
	{
		deleteDublicates(list);
	}
	
	/** creates a new list from the original, but without the elements to delete*/
	public static<T extends Object> List<T> removeElementsAndCreateNew(List<T> origList, Collection<T> elemsToDelete)
	{
		List<T> newList = new ArrayList<T>();
		for (T elem : origList)
		{
			if (!elemsToDelete.contains(elem))
				newList.add(elem);
		}
		return newList;
	}
	
	
	/** 
	 * A format line is the following:
	 * <key1>	<key1-setEl1>
	 * <key1>	<key1-setEl2>
	 * <key2>	<key1-setEl1>
	 * ...
	 * */
	public static<T,V extends Object> void printToFileMapObjectToSetWithRepeatedFirstLines(Map<T, Set<V>> map, String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		for (Map.Entry<T, Set<V>> mapEntry : map.entrySet())
		{
			for (V elem : mapEntry.getValue())
			{
				pw.println(mapEntry.getKey().toString() + "\t" + elem.toString());
			}
		}
		pw.close();
	}
	
	/** 
	 * A format line is the following:
	 * <key1>	<key1-setEl1>
	 * <key1>	<key1-setEl2>
	 * <key2>	<key1-setEl1>
	 * ...
	 * */
	public static<T,V extends Object> void printToFileMapObjectToListWithRepeatedFirstLines(Map<T, List<V>> map, String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		for (Map.Entry<T, List<V>> mapEntry : map.entrySet())
		{
			for (V elem : mapEntry.getValue())
			{
				pw.println(mapEntry.getKey().toString() + "\t" + elem.toString());
			}
		}
		pw.close();
	}
	
	public static<T,V extends Object> void printToFileMapObjectToSet(Map<T, Set<V>> map, String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		for (Map.Entry<T, Set<V>> mapEntry : map.entrySet())
		{
			StringBuilder sb = new StringBuilder();
			sb.append(mapEntry.getKey().toString());
			sb.append("\t");
			sb.append(UtilCollections.join(mapEntry.getValue(), "\t"));
			pw.println(sb.toString());
		}
		pw.close();
	}
	
	public static<T extends Object> void printToFileMapObjectToArray(Map<T, int[]> map, String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		for (Map.Entry<T, int[]> mapEntry : map.entrySet())
		{
			StringBuilder sb = new StringBuilder();
			sb.append(mapEntry.getKey().toString());
			sb.append("\t");
			int[] array = mapEntry.getValue();
			for (int i = 0; i < array.length; ++i)
			{
				sb.append(array[i]);
				sb.append("\t");
			}
			pw.println(sb.toString());
		}
		pw.close();
	}
	
	public static<T extends Object> void printToFileMapObjectToDoubleArray(Map<T, double[]> map, String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		for (Map.Entry<T, double[]> mapEntry : map.entrySet())
		{
			StringBuilder sb = new StringBuilder();
			sb.append(mapEntry.getKey().toString());
			double[] array = mapEntry.getValue();
			for (int i = 0; i < array.length; ++i)
			{
				sb.append("\t");
				sb.append(array[i]);
			}
			pw.println(sb.toString());
		}
		pw.close();
	}
	
	public static<T,S extends Object> void printToFileMap(Map<T, S> map, String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		for (Map.Entry<T, S> mapEntry : map.entrySet())
		{
			StringBuilder sb = new StringBuilder();
			sb.append(mapEntry.getKey().toString());
			sb.append("\t");
			sb.append(mapEntry.getValue().toString());
			pw.println(sb.toString());
		}
		pw.close();
	}
	
	public static<T,S extends Object> void printToFileMapLarge(Map<T, S> map, String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		int objCount = 0;
		for (Map.Entry<T, S> mapEntry : map.entrySet())
		{
			pw.println(mapEntry.getKey().toString() + "\t" + mapEntry.getValue().toString());
			objCount++;
			if (objCount % 500000 == 0) pw.flush();
		}
		pw.close();
	}
	
	
	public static<T,S extends Object> void printMapToOpenFile(Map<T, S> map, PrintWriter pw) throws FileNotFoundException, UnsupportedEncodingException
	{
		for (Map.Entry<T, S> mapEntry : map.entrySet())
		{
			StringBuilder sb = new StringBuilder();
			sb.append(mapEntry.getKey().toString());
			sb.append("\t");
			sb.append(mapEntry.getValue().toString());
			pw.println(sb.toString());
		}
	}
	
	public static<T,S extends Object> String joinMap(Map<T, S> map, String entrySeparator, String valueSeparator)
	{
		StringBuilder sb = new StringBuilder();
		
		for (Map.Entry<T, S> mapEntry : map.entrySet())
		{
			sb.append(mapEntry.getKey().toString());
			sb.append(valueSeparator);
			sb.append(mapEntry.getValue().toString());
			sb.append(entrySeparator);
		}
		return sb.toString();
	}
	
	public static<T,S extends Object> void printToFileMap(Map<T, S> map, String caption, String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		pw.println(caption);
		for (Map.Entry<T, S> mapEntry : map.entrySet())
		{
			StringBuilder sb = new StringBuilder();
			sb.append(mapEntry.getKey().toString());
			sb.append("\t");
			sb.append(mapEntry.getValue().toString());
			pw.println(sb.toString());
		}
		pw.close();
	}
	
	public static<T,S extends Object> Set<S> accumulateAllValueElements(Map<T, List<S>> map)
	{
		Set<S> result = new HashSet<S>();
		for (Map.Entry<T, List<S>> mapEntry : map.entrySet())
		{
			result.addAll(mapEntry.getValue());
		}
		return result;
	}
	
	public static<T extends Object> Map<T, Integer> computeFrequencies(List<T> list)
	{
		Map<T, Integer> result = new HashMap<T, Integer>();
		for (T item : list)
			incrementIntValueInMap(result, item);
		return result;
	}
	
	
	public static<T extends Object> void removeAllObjectsAfterIndex(List<T> list, int firstRemoveIndex)
	{
		if (list == null)
			return;
		for (int index = list.size() - 1; index >= firstRemoveIndex; --index)
		{
			list.remove(index);
		}
	}
	
	
	
	public static<T extends Object> String join(Collection<T> list, String separator)
	{
		if (list == null)
			return null;
		StringBuilder sb = new StringBuilder();
		for (T item : list)
		{
			if (sb.length() > 0)
				sb.append(separator);
			sb.append(item.toString());
		}
		return sb.toString();
	}
	
	public static<T extends Object> String join(T[] array, String separator)
	{
		StringBuilder sb = new StringBuilder();
		for (T item : array)
		{
			if (sb.length() > 0)
				sb.append(separator);
			sb.append(item.toString());
		}
		return sb.toString();
	}
	
	/** prints List to file where each element is placed on a new line 
	 * @throws UnsupportedEncodingException 
	 * @throws FileNotFoundException */
	public static<T extends Object> void printListToFile(Collection<T> list, String filename) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		for (T item : list)
		{
			pw.println(item.toString());
		}
		pw.close();
	}
	
	/** prints List to file where each element is placed on a new line 
	 * @throws UnsupportedEncodingException 
	 * @throws FileNotFoundException */
	public static<T extends Object> void printListToFile(Collection<T> list, String filename, String caption) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		pw.println(caption);
		for (T item : list)
		{
			pw.println(item.toString());
		}
		pw.close();
	}
	
	public static<T extends Object,V  extends Comparable<V>> List<T> findKeysWithMaxValues(Map<T,V> map)
	{
		V maximalValue = null;
		for (Map.Entry<T, V> entry : map.entrySet())
		{
			V curValue = entry.getValue();
			if (maximalValue == null)
				maximalValue = curValue;
			else
			{
				int comp = curValue.compareTo(maximalValue);
				if (comp > 0)
					maximalValue = curValue;
			}
		}
		
		List<T> foundKeys = new ArrayList<T>();
		for (Map.Entry<T, V> entry : map.entrySet())
		{
			if (entry.getValue().compareTo(maximalValue) == 0)
				foundKeys.add(entry.getKey());
		}
		
		return foundKeys;
	}
	
	public static<T extends Object,V  extends Comparable<V>> List<T> findKeysWithSpecificValue(Map<T,V> map, V value)
	{
		List<T> foundKeys = new ArrayList<T>();
		for (Map.Entry<T, V> entry : map.entrySet())
		{
			if (entry.getValue().compareTo(value) == 0)
				foundKeys.add(entry.getKey());
		}
		
		return foundKeys;
	}
	
	public static<T,V extends Object> int countValueOccurrenceInMap(Map<T,V> map, V value)
	{
		int result = 0;
		for (Map.Entry<T, V> entry : map.entrySet())
		{
			V foundValue = entry.getValue();
			if (value == null &&  foundValue== null)
				result++;
			else if (foundValue.equals(value))
				result++;
		}
		return result;
	}
	
		
	public static<T extends Object> Long sumFrequenceValues(Map<T,Integer> map)
	{
		Long sum = 0L;
		for (Map.Entry<T, Integer> entry : map.entrySet())
		{
			sum += entry.getValue();
		}
		
		return sum;
	}

	public static void sumArrayValueToMap(Map<String, double[]> map, String key, double[] addArray) throws Exception
	{
		double[] curArray = map.get(key);
		if (curArray == null)
		{
			
			map.put(key, UtilArrays.copyArray(addArray));
			return;
		}
		curArray = UtilArrays.sumTwoArrays(curArray, addArray);
		map.put(key, curArray);
	}
	
	public static void sumArrayValueToMap(Map<String, int[]> map, String key, int[] addArray) throws Exception
	{
		int[] curArray = map.get(key);
		if (curArray == null)
		{
			map.put(key, UtilArrays.copyArray(addArray));
			return;
		}
		curArray = UtilArrays.sumTwoArrays(curArray, addArray);
		map.put(key, curArray);
	}
	
	public static<T extends Object> Set<T> intersection(Collection<T> list1, Collection<T> list2)
	{
		Set<T> coll = new HashSet<T>(list1);
		coll.retainAll(list2);
		return coll;
	}
	
	public static<T extends Object> List<T> intersection(List<T> list1, List<T> list2)
	{
		List<T> coll = new ArrayList<T>(list1);
		coll.retainAll(list2);
		return coll;
	}
	
	public static<T extends Object> Set<T> intersection(Set<T> list1,Set<T> list2)
	{
		Set<T> coll = new HashSet<T>(list1);
		coll.retainAll(list2);
		return coll;
	}
	
	/** Detects if listToContain contains any of the elements of listForSearch*/
	public static<T extends Object> boolean containsAny(List<T> listToContain, List<T> listForSearch)
	{
		for (T elem : listForSearch)
		{
			if (listToContain.contains(elem))
				return true;
		}
		return false;
	}
	
	public static<T extends Object> Set<T> transformArrayToSet(T[] array)
	{
		Set<T> result = new HashSet<T>();
		for (T element : array)
		{
			result.add(element);
		}
		return result;
	}
	
	public static<T extends Object> List<T> transformArrayToList(T[] array)
	{
		return Arrays.asList(array);
	}
	
	public static<T,V extends Object> void removeAllKeysFromMap(Map<T, V> map, Collection<T> keysToRemove) 
	{
		for (T key: keysToRemove)
		{
			map.remove(key);
		}
	}
	
	/**
	 * Retains in the map only those keys that are in the keysToRetain set.
	 * @param map
	 * @param keysToRetain
	 */
	public static<T,V extends Object> void retainAllKeysFromMap(Map<T, V> map, Collection<T> keysToRetain) 
	{
		/*Set<T> keysToRemove = new HashSet<T> ();
		for (T key : map.keySet())
			if (!keysToRetain.contains(key))
				keysToRemove.add(key);*/
		Set<T> keysToRemove = new HashSet<T> ( map.keySet());
		//keysToRemove.removeAll(new HashSet<T>(keysToRetain));
		keysToRemove.removeAll( keysToRetain);
		removeAllKeysFromMap(map, keysToRemove);
	}
	
	/**
	 * Removes from the map all keys that have value less than a specified minimum
	 * @param map
	 * @param minValue
	 */
	public static<T extends Object> void removeFromIntegerMapAllEntriesWithLowValues(Map<T, Integer> map, int minValue) 
	{
		Set<T> keysToRemove = new HashSet<T>();
		for (Map.Entry<T, Integer> entry : map.entrySet())
		{
			if (entry.getValue() < minValue)
				keysToRemove.add(entry.getKey());
		}
		removeAllKeysFromMap(map, keysToRemove);
	}
	
	/** increments all the integer values in the first map according to the second map. If no key exists in the first map, it's added. */
	public static<T extends Object> void addFrequencyMap(Map<T, Integer> map, Map<T, Integer> extensionMap)
	{
		for (T key : extensionMap.keySet())
		{
			incrementIntValueInMap(map, key, extensionMap.get(key));
		}
		
	}
	
	public static<T extends Object> void addFrequencyMapForDouble(Map<T, Double> map, Map<T, Double> extensionMap)
	{
		for (T key : extensionMap.keySet())
		{
			incrementIntValueInMap(map, key, extensionMap.get(key));
		}
	}

	/**
	 * Both maps are assumed to have the same keys.
	 * @param map
	 * @param extensionMap
	 * @throws Exception 
	 */
	public static<T extends Object> void addDoubleArrayMap(Map<T, double[]> map, Map<T, double[]> extensionMap) throws Exception
	{
		for (T key : extensionMap.keySet())
		{
			UtilArrays.addArray(map.get(key), extensionMap.get(key));
		}
	}
	
	public static<T extends Comparable<T>> List<T> sortAndRemoveRepeated (List<T> initialList)
	{
		List<T> resultList = new ArrayList<T>(initialList);
		Collections.sort(resultList);
		deleteDublicates(resultList);
		return resultList;
		
	}
	
	/** Sort map by value (descending order=false; ascending order=true)*/
	public static Map<String, Integer> sortMapByValueInteger(Map<String, Integer> unsortedMap, final boolean order){
        List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(unsortedMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Entry<String, Integer>>()
        {
            public int compare(Entry<String, Integer> o1,Entry<String, Integer> o2){
                if (order)
                    return o1.getValue().compareTo(o2.getValue());
                else
                    return o2.getValue().compareTo(o1.getValue());
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Integer> sortedMap = new LinkedHashMap<String,Integer>();
        for (Entry<String, Integer> entry : list){
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

	/** takes in a map K,V and returns a List of Pairs <K,V> sorted by (descending) value */
	public static<K,V extends Comparable<? super V>> Vector<K> sortMapByValue(Map<K,V> map)
	{
		List<Pair<K,V>> linkList = new ArrayList<Pair<K,V>>();
		
		for (Map.Entry<K,V> e: map.entrySet())
			linkList.add(new Pair<K,V>(e.getKey(), e.getValue()));
		
		sortPairsBySecondElement(linkList);
		
		Vector<K> result = new Vector<K>();
		
		for (Pair<K,V> link : linkList)
			result.add(link.getFirst());
		
		return result;
	}
	
	/** takes in a map K,V and sorts it in descending order of corresponding values. Returns LinkedHashMap*/
	public static<K,V extends Comparable<? super V>> LinkedHashMap<K,V> sortMapByValueAndReturnNewOne(Map<K,V> map)
	{
		List<Pair<K,V>> linkList = new ArrayList<Pair<K,V>>();
		
		for (Map.Entry<K,V> e: map.entrySet())
			linkList.add(new Pair<K,V>(e.getKey(), e.getValue()));
		
		sortPairsBySecondElement(linkList);
		
		LinkedHashMap<K,V> resultMap = new LinkedHashMap<K,V>();
		
		for (Pair<K,V> link : linkList)
			resultMap.put(link.getFirst(), link.getSecond());
		
		return resultMap;
	}
	
	/** takes in a map K,V and sorts it in descending order of corresponding values. Returns LinkedHashMap*/
	public static<K,T, S extends Comparable<? super S>> LinkedHashMap<K,Pair<S,T>> sortPairMapByFirstPairValueAndReturnNewOne(Map<K,Pair<S,T>> map)
	{
		List<Pair<K,ComparablePair<S,T>>> linkList = new ArrayList<Pair<K,ComparablePair<S,T>>>();
		
		for (Map.Entry<K,Pair<S,T>> e: map.entrySet())
			linkList.add(new Pair<K,ComparablePair<S,T>>(e.getKey(), new ComparablePair<S,T>(e.getValue())));
		
		sortPairsBySecondElement(linkList);
		
		LinkedHashMap<K,Pair<S,T>> resultMap = new LinkedHashMap<K,Pair<S,T>>();
		
		for (Pair<K,ComparablePair<S,T>> link : linkList)
			resultMap.put(link.getFirst(), link.getSecond());
		
		return resultMap;
	}
	
	/** takes in a map K,V and sorts it in ascending order of keys. Returns LinkedHashMap */
	public static<K extends Comparable<? super K>,V > LinkedHashMap<K,V> sortMapByKeyAndReturnNewOne(Map<K,V> map)
	{
		List<K> keys = new ArrayList<K>(map.keySet());
		Collections.sort(keys);
					
		LinkedHashMap<K,V> resultMap = new LinkedHashMap<K,V>();
		
		for (K key : keys)
			resultMap.put(key, map.get(key));
		
		return resultMap;
	}
	
	
	/** sorts in increasing order of first element of pair */
	public static<S extends Comparable<? super S>,T > void sortPairsByFistElement(List<Pair<S,T>> input)
	{
		Collections.sort (input, new Comparator<Pair<S,?>>() {
			public int compare (Pair<S,?> p1, Pair<S,?> p2) {
				S i1 = p1.getFirst();
				S i2 = p2.getFirst();
				return i1.compareTo(i2);
			}
		});
	}
	
	/** sorts in decreasing order of second element of pair */
	public static<S,T extends Comparable<? super T>> void sortPairsBySecondElement(List<Pair<S,T>> input)
	{
		Collections.sort (input, new Comparator<Pair<?,T>>() {
			public int compare (Pair<?,T> p1, Pair<?,T> p2) {
				T i1 = p1.getSecond();
				T i2 = p2.getSecond();
				return i2.compareTo(i1);
			}
		});
	}
	
	/** sorts in increasing order of second element of pair */
	public static<S,T extends Comparable<? super T>> void sortPairsBySecondElementIncreasing(List<Pair<S,T>> input)
	{
		Collections.sort (input, new Comparator<Pair<?,T>>() {
			public int compare (Pair<?,T> p1, Pair<?,T> p2) {
				T i1 = p1.getSecond();
				T i2 = p2.getSecond();
				return i1.compareTo(i2);
			}
		});
	}
	
	/** sorts in either decreasing or increasing order of second element of pair. 
	 * if increasingOrder is true, return increasing order. Otherwise - decreasing. */
	public static<S,T extends Comparable<? super T>> void sortPairsBySecondElement(List<Pair<S,T>> input, final boolean increasingOrder)
	{
		Collections.sort (input, new Comparator<Pair<?,T>>() {
			public int compare (Pair<?,T> p1, Pair<?,T> p2) {
				T i1 = p1.getSecond();
				T i2 = p2.getSecond();
				if (increasingOrder)
					return i1.compareTo(i2);
				else
					return i2.compareTo(i1);
			}
		});
	}

	/** reads the map with keys as String and values as double[] in tab-formatted file (first column is key, and all others - values in the array). 
	 * The method presumes that each key is present only once in a file (otherwise, only the last entry of the key will be saved)*/
	public static Map<String, double[]> readMapDoubleArrayFromFile(String filename) throws Exception
	{
		Map<String, double[]> resultMap = new HashMap<String, double[]>();
		List<String> dataLines = UtilFiles.getContentLines(filename);
		for (String line : dataLines)
		{
			String[] parsedLine = line.split("\t");
			String key = parsedLine[0];
			double[] valueArray = new double[parsedLine.length - 1];
			for (int i = 0; i < valueArray.length; ++i) {
				valueArray[i] = Double.parseDouble(parsedLine[i + 1]);
			}
			
			resultMap.put(key, valueArray);
		}
		return resultMap;
	}
	
	/** reads the map with keys as String and values as Double in tab-formatted file (first 2 columns). 
	 * The method presumes that each key is present only once in a file (otherwise, only the last entry of the key will be saved)*/
	public static Map<String, Double> readMapDoubleFromFile(String filename) throws Exception
	{
		Map<String, Double> resultMap = new HashMap<String, Double>();
		List<String> dataLines = UtilFiles.getContentLines(filename);
		for (String line : dataLines)
		{
			String[] parsedLine = line.split("\t");
			String key = parsedLine[0];
			String value = parsedLine[1];
			double dblValue = Double.parseDouble(value);
			
			resultMap.put(key, dblValue);
		}
		return resultMap;
	}
	
	/** reads the hash map with keys as String and values as Integer in tab-formatted file (first 2 columns). 
	 * The method presumes that each key is present only once in a file (otherwise, only the last entry of the key will be saved)*/
	public static Map<String, Integer> readMapIntFromFile(String filename) throws Exception
	{
		return readMapIntFromFile(filename, false);
	}
	
	/** reads the map with keys as String and values as Integer in tab-formatted file (first 2 columns). 
	 * The method presumes that each key is present only once in a file (otherwise, only the last entry of the key will be saved)
	 * if preserveOrder == true -> LinkedHashMap is created*/
	public static Map<String, Integer> readMapIntFromFile(String filename, boolean preserveOrder) throws Exception
	{
		Map<String, Integer> resultMap = null;
		if (preserveOrder)
			resultMap = new LinkedHashMap<String, Integer>();
		else
			resultMap = new HashMap<String, Integer>();
		List<String> dataLines = UtilFiles.getContentLines(filename, false);
		String[] parsedLine;
		int intValue;
		for (String line : dataLines)
		{
			parsedLine = line.split("\t");
			intValue = Integer.parseInt(parsedLine[1]);
			resultMap.put(parsedLine[0], intValue);
		}
		dataLines.clear();
		return resultMap;
	}

	public static <T extends Object> boolean nullOrEmpty(Collection<T> coll) {
		return (coll == null || coll.size() == 0);
	}

	public static <K extends Object> Map<K,double[]> copyArrayMap (Map<K, double[]> initialMap)
	{
		Map<K,double[]> resMap = new HashMap<K,double[]>(initialMap.size());
		for (Map.Entry<K, double[]> mapEntry : initialMap.entrySet())
		{
			double[] cpArray = new double[mapEntry.getValue().length];
			System.arraycopy(mapEntry.getValue(), 0, cpArray, 0, mapEntry.getValue().length);
			resMap.put(mapEntry.getKey(), cpArray);
		}
		
		return resMap;
	}
	
	public static <K,T extends Object> Map<K,List<T>> copyListMap (Map<K, List<T>> initialMap)
	{
		Map<K,List<T>> resMap = new HashMap<K,List<T>>(initialMap.size());
		for (Map.Entry<K, List<T>> mapEntry : initialMap.entrySet())
		{
			List<T> cpList = new ArrayList<T>(mapEntry.getValue());
			resMap.put(mapEntry.getKey(), cpList);
		}
		
		return resMap;
	}
	
	public static <T extends Object> HashSet<T> createHashSetFromArray(T[] array)
	{
		HashSet<T> res = new HashSet<T>();
		for (T elem : array)
			res.add(elem);
		return res;
	}
	
	public static <T extends Object> List<T> createListFromArray(T[] array)
	{
		List<T> res = new ArrayList<T>();
		for (T elem : array)
			res.add(elem);
		return res;
	}
	
	public static <T,V extends Object> Map<T,V> joinMaps(Map<T,V> map1, Map<T,V> map2)
	{
		Map<T,V> newMap = new HashMap<T,V>(map1);
		newMap.putAll(map2);
		return newMap;
	}
	
	public static <V extends Object> void addArrayToList(List<V> listToUpdate, V[] array)
	{
		for (V elem : array)
			listToUpdate.add(elem);
	}
	
	/**
	 * Sort a list of strings[] according to the field indicated in 'index'
	 * @param arr
	 * @param index
	 * @return
	 */
	public static List<String[]> listMergeSort(List<String[]> arr, int index){
	     List<String[]> left = new ArrayList<String[]>();
	     List<String[]> right =  new ArrayList<String[]>();

	     if(arr.size()<=1)
	         return arr;
	     else{   
	         int middle = arr.size()/2;
	         for (int i = 0; i<middle; i++) 
	             left.add(arr.get(i));
	         for (int j = middle; j<arr.size(); j++)
	             right.add(arr.get(j));
	         left = listMergeSort(left, index);
	         right = listMergeSort(right, index);
	         return merge(left, right, index);
	         
	     }   
	}
	
	private static List<String[]> merge(List<String[]> left, List<String[]> right, int index){
	     List<String[]> result = new ArrayList<String[]>();
	     int indexLeft=index;
	     int indexRight=index;
	     while (left.size() > 0 && right.size() > 0){
	    	 if (index==-1) {
	    		 indexLeft=left.get(0).length;
	    		 indexRight=right.get(0).length;
	    	 }
	    	 if(Integer.parseInt(left.get(0)[indexLeft]) >= Integer.parseInt(right.get(0)[indexRight])){
	    		 result.add(left.get(0));
		         left.remove(0);
		     }
		     else{
		         result.add(right.get(0));
		         right.remove(0);
		     }
	     }
	     if (left.size()>0) {
	         for(int i=0; i<left.size(); i++)
	             result.add(left.get(i));
	     }
	     if (right.size()>0) {
	         for(int i=0; i<right.size(); i++)
	             result.add(right.get(i));
	     }
	     return result;
	}
	
	public static<T extends Object> List<T> subListExcludingOneElem(List<T> list, int excludedElemIndex) throws Exception
	{
		if (excludedElemIndex < 0 || excludedElemIndex >= list.size()) {
			throw new Exception("Index of excluded element is out of array bounds");
		}
		
		List<T> foundElems = new ArrayList<T>(list);
		foundElems.remove(excludedElemIndex);
		return foundElems;
	}
	
	public static<T extends Object> List<T> subListExcludingSomeElems(List<T> list, int[] excludedElemIndices) throws Exception
	{		
		List<T> foundElems = new ArrayList<T>(list);
		Arrays.sort(excludedElemIndices);
		for (int i = excludedElemIndices.length - 1; i >= 0; --i) {
			if (excludedElemIndices[i] < 0 || excludedElemIndices[i] >= list.size()) {
				throw new Exception("Index of excluded element is out of array bounds");
			}
			foundElems.remove(excludedElemIndices[i]);
		}
		return foundElems;
	}
	
}

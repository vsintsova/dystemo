/*
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package functionality;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.csvreader.CsvReader;

import au.com.bytecode.opencsv.CSVReader;

/**
 * 
 * 
 * Some functions from this files are 
 * 
 */
public class UtilFiles {

	static public List<String> getContentLines(File aFile, boolean toTrimLines)
	{
		List<String> lines = new ArrayList<String>();
		  try {
		    BufferedReader input =  new BufferedReader(new InputStreamReader(new FileInputStream(aFile), "UTF-8"));
	    		
	    	String line = null; //not declared within while loop
	        
	        while (( line = input.readLine()) != null){
	        	lines.add(toTrimLines?line.trim():line);
	        }
	        input.close();
	    }
	    catch (IOException ex){
	      ex.printStackTrace();
	    }
		return lines;
	}
	
	static public List<String> getContentLines(String filename, boolean toTrimLines)
	{
		List<String> lines = new ArrayList<String>();
		  try {
			File aFile = new File(filename);
				
		    BufferedReader input =  new BufferedReader(new InputStreamReader(new FileInputStream(aFile), "UTF-8"));
	    		
	    	String line = null; //not declared within while loop
	        
	        while (( line = input.readLine()) != null){
	        	lines.add(toTrimLines?line.trim():line);
	        }
	        input.close();
	    }
	    catch (IOException ex){
	      ex.printStackTrace();
	    }
		return lines;
	}
	
	static public List<String> getContentLines(File aFile)
	{
		return getContentLines(aFile, true);
	}
	
	static public List<String> getContentLines(String filename)
	{
		return getContentLines(filename, true);
	}
	
	static private String getContentsFromReader(BufferedReader input)
	{
		StringBuilder contents = new StringBuilder();
		 
		try
		{
			try {
		        String line = null; //not declared within while loop
		       
		        while (( line = input.readLine()) != null){
		          contents.append(line);
		          contents.append(System.getProperty("line.separator"));
		        }
		      }
		      finally {
		        input.close();
		      }
		}
	    catch (IOException ex){
	      ex.printStackTrace();
	    }
    
		 return contents.toString();
	}
	
	
	static public String getContents(File aFile) {
	    StringBuilder contents = new StringBuilder();
	    
	    try {
	      BufferedReader input =  new BufferedReader(new FileReader(aFile));
	      contents.append(getContentsFromReader(input));
	    }
	    catch (IOException ex){
	      ex.printStackTrace();
	    }
	    
	    return contents.toString();
	}
	
	static public String getContents(String filename) {
	    StringBuilder contents = new StringBuilder();
	    
	    try {
	      BufferedReader input =  new BufferedReader(new FileReader(new File(filename)));
	      contents.append(getContentsFromReader(input));
	    }
	    catch (IOException ex){
	      ex.printStackTrace();
	    }
	    
	    return contents.toString();
	  }
	
	/**
	 * This uses the standard split, and thus allow to ignore the separation by quotes.
	 * This takes \t as a separator.
	 * @param filename
	 * @param firstLine
	 * @param toLowerCase
	 * @return
	 * @throws IOException
	 */
	static public List<String> getFirstParameterLinesNoCSV(String filename, int firstLine, boolean toLowerCase) throws IOException
	{
		return getFirstParameterLinesNoCSV(filename, firstLine, toLowerCase, "\t");
	}
	
	/**
	 * This uses the standard split, and thus allow to ignore the separation by quotes
	 * @param filename
	 * @param firstLine
	 * @param toLowerCase
	 * @return
	 * @throws IOException
	 */
	static public List<String> getFirstParameterLinesNoCSV(String filename, int firstLine, boolean toLowerCase, String separator) throws IOException
	{
		List<String> lines = new ArrayList<String>();
		  try {
		    BufferedReader input =  new BufferedReader(new InputStreamReader(new FileInputStream(filename), "UTF-8"));
	    		
	    	String line = null; 
	    	for (int i = 0; i < firstLine; ++i)
			{
	    		input.readLine();
			}   	
	    	
	        while (( line = input.readLine()) != null)
	        {
	        	String term = line.trim().split(separator)[0];
				if (toLowerCase)
					term = term.toLowerCase();
				lines.add(term);
	        }
	        input.close();
	    }
	    catch (IOException ex){
	      ex.printStackTrace();
	    }
		return lines;
	}
	
	
	/** imply that data in the file are tabulated; extracts what is before the first tab. It uses CSVReader to parse the file
	 * firstLine - index of first line to read (starting from 0)
	 * @throws IOException */
	static public List<String> getFirstParameterLines(String filename, int firstLine, boolean toLowerCase) throws IOException
	{
		 CSVReader reader = new CSVReader(new FileReader(filename),  '\t');
		 List<String> resultLines = new ArrayList<String>();
		 String [] nextLine;
		 for (int i = 0; i < firstLine; ++i)
		 {
			 reader.readNext();
		 }
		 while ((nextLine = reader.readNext()) != null) 
			{
				String term = nextLine[0];
				if (toLowerCase)
					term = term.toLowerCase();
				resultLines.add(term);
			}
		reader.close();
		return resultLines;
	}
	
	/** imply that data in the file are tabulated; extracts what is before the first tab
	 * firstLine - index of first line to read (starting from 0)
	 * By default, puts the read text into lowercase
	 * @throws IOException */
	static public List<String> getFirstParameterLines(String filename, int firstLine) throws IOException
	{
		return getFirstParameterLines(filename, firstLine, true);
	}
	
	/** returns all the lines of the file, splitted into an array based on the given separator (using CSVReader, which requires escaping of quotes!)
	 * firstLine - index of first line to read (starting from 0)
	 * @throws IOException */
	static public List<String[]> getParsedLinesWithCSVOld(String filename, char separator, int firstLine) throws IOException
	{
		 CSVReader reader = new CSVReader(new FileReader(filename), separator);
		 List<String[]> resultLines = new ArrayList<String[]>();
		 String [] nextLine;
		 for (int i = 0; i < firstLine; ++i)
		 {
			 reader.readNext();
		 }
		 while ((nextLine = reader.readNext()) != null) 
			{
				resultLines.add(nextLine);
			}
		reader.close();
		return resultLines;
	}
	
	/** returns all the lines of the file, splitted into an array based on the given separator (using CSVReader, which requires escaping of quotes!)
	 * firstLine - index of first line to read (starting from 0)
	 * @throws IOException */
	static public List<String[]> getParsedLinesWithCSV(String filename, char separator, int firstLine) throws IOException
	
	{
		CsvReader reader = new CsvReader(filename,
                separator /* delimiter */ );

		List<String[]> resultLines = new ArrayList<String[]>();
		
		int lineInd = 0;
		while (lineInd != firstLine && reader.readRecord()) 
		{
			lineInd++;
		}
		
		while (reader.readRecord()) 
		{
			// full row, you can use regex to find 
			// any rows you specifically want
			//String row = reader.getRawRecord();  
			
			// get value of the first field
			//String col = reader.get(0);          
			
			// gets array of fields
			String[] cols = reader.getValues(); 
			resultLines.add(cols);
		}
		
		reader.close();
		return resultLines;
	}
	
	/** returns all the lines of the file, splitted into an array based on the given separator 
	 * firstLine - index of first line to read (starting from 0)
	 * @throws IOException */
	static public List<String[]> getParsedLines(String filename, char separator, int firstLine) throws IOException
	{
		String strSeparator = separator + "";
		return getParsedLines(filename, strSeparator, firstLine);
	}
	
	/** returns all the lines of the file, splitted into an array based on the given separator 
	 * firstLine - index of first line to read (starting from 0)
	 * @throws IOException */
	static public List<String[]> getParsedLines(String filename, String separator, int firstLine) throws IOException
	{
		 List<String> dataLines = UtilFiles.getContentLines(filename, false);	
		 return UtilString.parseTable(dataLines, separator, firstLine);
	}
	
	static public void writeAdditionalDataToFileFromNewLine(String filename, String data) throws IOException
	{
		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
		pw.println(data);
		pw.close();
	}
	
	static public void writeToFile(String filename, String data) throws FileNotFoundException, UnsupportedEncodingException
	{
		File tempFile = new File(filename);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		pw.print(data);
		pw.close();
	}
	
	static public void writeSerializableDataToFile(Object objectToWrite, String filename)
	{
		 FileOutputStream fos = null;
		    ObjectOutputStream out = null;
		    try {
		        fos = new FileOutputStream(filename,false);
		        out = new ObjectOutputStream(fos);
		        out.writeObject(objectToWrite);
		        out.close();
		        //System.out.println("Object Persisted");
		    } catch (IOException ex) {
		        ex.printStackTrace();
		    }
	}
	
	static public Object readSerializableDataFromFile(String filename)
	{
		Object readObject = null;
	
		FileInputStream fos;
		try 
		{
			fos = new FileInputStream(filename);
			ObjectInputStream oos = new ObjectInputStream(fos);
			readObject =  oos.readObject();
		 	fos.close();
		}
		catch (Exception ex)
		{
			 ex.printStackTrace();
		}
		return readObject;
	}
	
	static public String[] getFilesInTheDirectory(String directoryPath)
	{
		File folder = new File(directoryPath);
		return folder.list();
	}
	
	static public boolean checkIfFileExists(String filePath) 
	{
		return checkIfFileExists(new File(filePath));
	}

	static public boolean checkIfFileExists(File aFile) 
	{
		if (aFile == null) 
			return false;
		return aFile.exists();
	}
		
	public static void CheckIfExistsOrCreate(String filename) throws IOException {
		File aFile = new File(filename);
		if (!aFile.exists())
			aFile.createNewFile();
	}
	
	static public void checkAvailabilityToWrite(File aFile) throws FileNotFoundException
	{
		if (aFile == null) {
		      throw new IllegalArgumentException("File should not be null.");
		    }
		    if (!aFile.exists()) {
		      throw new FileNotFoundException ("File does not exist: " + aFile);
		    }
		    if (!aFile.isFile()) {
		      throw new IllegalArgumentException("Should not be a directory: " + aFile);
		    }
		    if (!aFile.canWrite()) {
		      throw new IllegalArgumentException("File cannot be written: " + aFile);
		    }
	}

  /**
  * Change the contents of text file in its entirety, overwriting any
  * existing text.
  *
  * This style of implementation throws all exceptions to the caller.
  *
  * @param aFile is an existing file which can be written to.
  * @throws IllegalArgumentException if param does not comply.
  * @throws FileNotFoundException if the file does not exist.
  * @throws IOException if problem encountered during write.
  */
  static public void setContents(File aFile, String aContents)
                                 throws FileNotFoundException, IOException {
	checkAvailabilityToWrite(aFile);

    //use buffering
    Writer output = new BufferedWriter(new FileWriter(aFile));
    try {
      //FileWriter always assumes default encoding is OK!
      output.write( aContents );
    }
    finally {
      output.close();
    }
  }
	
	public static void setContents(String filename, String contents) throws FileNotFoundException, IOException {
		File file = new File(filename);
		setContents(file, contents);
	}
	
	/**
	 * This is a very simplistic implementation of file copying, without any check for exception cases or memory concerns
	 * @param oldFilename
	 * @param newFilename
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public static void copyFile(String origFilename, String newFilename) throws FileNotFoundException, IOException {
		String prevContent = getContents(origFilename);
		setContents(newFilename, prevContent);
	}
}

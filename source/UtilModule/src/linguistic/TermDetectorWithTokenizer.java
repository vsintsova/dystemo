/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.util.List;
import java.util.Map;

public class TermDetectorWithTokenizer extends TermDetector {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1764598343073284634L;

	/**
	 * OnWhiteSpaces = separate tokens based on the white space or other similar separators;
	 * WithWordLetters = only the alpha-numeric characters will form the words. Any other character is considered as a separator.
	 *
	 */
	public enum TokenizationType {OnWhiteSpaces,
								  WithWordLetters}
	
	public TokenizationType tokenizationType =  TokenizationType.OnWhiteSpaces;
	
	public TermDetectorWithTokenizer(List<String> termList) {
		super(termList);
	}

	@Override
	public Map<String, Integer> findTermsInText(String text)
	{
		List<String> tokens = null;
		switch (tokenizationType)
		{
			case OnWhiteSpaces: tokens = UtilText.getAllTokens(text); break;
			case WithWordLetters: tokens = UtilText.getTokens(text); break;
			default: break;
		}
	
		return findTermsForTokens(tokens);
	}
}

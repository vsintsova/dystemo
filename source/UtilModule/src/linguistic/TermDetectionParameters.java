/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import functionality.UtilString;
import linguistic.NegationDetector.NegationTreatmentParams;
import linguistic.NegationDetector.NegationTreatmentType;

public class TermDetectionParameters {

	public NegationTreatmentParams treatNegationsParams; // how to treat appeared negations
	public boolean ignoreHashtags; // whether to extract words with hashtags both as with hashtag and without
	public boolean toSearchForNonOverlap; // wasn't used before! If set to true, it will create NonOverlapTermDetector
	
	
	public String printValues(String separator, boolean putShortName)
	{
		StringBuilder sb = new StringBuilder();
		sb.append(putShortName?"-iHash-":""); //ignoreHashtags?
		sb.append(ignoreHashtags?1:0);
		sb.append(separator);
		sb.append(putShortName?"tNeg-":"");
		boolean toTreatNegations = toTreatNegations();
		sb.append(toTreatNegations?1:0);
		sb.append(separator);
		if ( toTreatNegations )
		{
			sb.append(treatNegationsParams.printParametersShort());
			sb.append(separator);
		}
		if (toSearchForNonOverlap) {
			sb.append(putShortName?"-inOver-":"");
			sb.append(toSearchForNonOverlap?1:0);
		}
		return sb.toString();
	}
	
	public String printParametersShort() {
		return printValues("-", true);
	}
	
	public boolean toTreatNegations()
	{
		return treatNegationsParams!= null && treatNegationsParams.negTreatmentType != NegationTreatmentType.None;
	}
	
	public boolean requiresChangeInDetection()
	{
		return toTreatNegations() || ignoreHashtags ||  toSearchForNonOverlap;
	}
	
	public TermDetectionParameters() {};
	
	public TermDetectionParameters(boolean toTreatNegationsInDefaultWay, boolean toIgnoreHashtags) {
		this.ignoreHashtags = toIgnoreHashtags;
		if (toTreatNegationsInDefaultWay)
			this.treatNegationsParams = NegationDetector.defaultNegationParams;
		else
			this.treatNegationsParams = null;
	};
	
	public TermDetectionParameters(NegationTreatmentParams negationTreatParams, boolean toIgnoreHashtags) {
		this.ignoreHashtags = toIgnoreHashtags;
		this.treatNegationsParams = negationTreatParams;
	};
	
	public TermDetectionParameters(TermDetectionParameters initial) {
		this(null, initial.ignoreHashtags);
		if (initial.treatNegationsParams != null)
			this.treatNegationsParams = new NegationTreatmentParams(initial.treatNegationsParams);
			
	}
	
	public TermDetectionParameters(NegationTreatmentParams negationTreatParams, boolean toIgnoreHashtags, boolean toSearchForNonOverlap) {
		this(negationTreatParams, toIgnoreHashtags);
		this.toSearchForNonOverlap = toSearchForNonOverlap;
	};
	
	
	public static TermDetectionParameters defaultEmptyTreatmentParams;
	
	static
	{
		defaultEmptyTreatmentParams = new TermDetectionParameters();
		defaultEmptyTreatmentParams.ignoreHashtags = false;
		defaultEmptyTreatmentParams.treatNegationsParams = null;
	}
	
	public static TermDetectionParameters parseParametersStr(String paramStr)
	{
	   //TODO: check if non-overlap detector is set
		boolean toTreatNegations = Integer.parseInt(UtilString.getPatternEntry("tNeg-([\\w]+?)(?=-|$)", paramStr)) > 0;//
		NegationTreatmentParams negTreatmentParams;
		if (toTreatNegations)
		{
			negTreatmentParams = new NegationTreatmentParams();
			negTreatmentParams.negTreatmentType = NegationTreatmentType.values()[Integer.parseInt(UtilString.getPatternEntry("-negType-([\\w]+?)-", paramStr))];
			negTreatmentParams.lookBehindLength = Integer.parseInt(UtilString.getPatternEntry("-negLens-([\\w]+?)-", paramStr));
		}
		else
			negTreatmentParams = null;
		boolean toIgnoreHashtags = Integer.parseInt(UtilString.getPatternEntry("iHash-([\\w]+?)(?=-|$)", paramStr)) > 0;//
		return new TermDetectionParameters(negTreatmentParams, toIgnoreHashtags);
	}
	
}

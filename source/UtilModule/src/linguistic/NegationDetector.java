/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import functionality.UtilCollections;

public class NegationDetector {
	
	/**
	 * None: all ngrams will be detected regardless of negations;
	 * 
	 * ToIncludeWithNegation: if negation terms before the ngram, it will include this negation term into the ngram obligatory (and only once).
	 * Important - for the negated ngrams the number of words can be +1 to maximal possible n (the negation word is not included in the count);
	 * 
	 * ToRemove: if negation terms before the ngram, this ngram is excluded
	 * 
	 * ToReplace: if the term is found as negated, it will be included as not_term
	 * 
	 * Example: I will not go there
	 * For bigrams with negation detected just before:
	 * None: I, will, not, go, there, I will, will not, not go, go there
	 * ToIncludeWithNegation:  I, will, not, there, I will, will not, not go, not go there
	 * ToRemove: I, will, not, there, I will, will not
	 * ToReplace:  I, will, not, not_go, there, I will, will not, not go, not_go there
	 */
	public enum NegationTreatmentType {None, ToIncludeWithNegation, ToRemove, ToReplace};
	
	// some previous
	//This method will return all the ngrams, but for those that were negated (the negation term before it), it will include this negation term into the ngram obligatory (and only once).
	// * Important - for the negated ngrams the number of words can be +1 to maximal possible n (the negation word is not included in the count)
	
	public static NegationTreatmentParams defaultNegationParams;
	
	static
	{
		defaultNegationParams = new NegationTreatmentParams();
		defaultNegationParams.negTreatmentType = NegationTreatmentType.ToRemove;
		defaultNegationParams.lookBehindLength = 1;
	}
	

	public static class NegationTreatmentParams implements Serializable
	{
		public NegationTreatmentType negTreatmentType; // what to do with the found negated terms; 
		
		public int lookBehindLength; // in how many terms back to search for negations. Should be positive. Before was set to 1.
	
		public boolean toRemoveNotKnownNegatedTerms; // was always false by default. If true - the corresponding TermDetector should remove the terms that are not within the original input.
		
		public String printParametersShort() {
			if (negTreatmentType != NegationTreatmentType.None)
			{
				StringBuilder sb = new StringBuilder();
				if (lookBehindLength != 1)
					sb.append("-negLens-" + lookBehindLength);
				if (negTreatmentType != NegationTreatmentType.ToRemove)
					sb.append("-negType-" + negTreatmentType.ordinal()); // = negation replacement is on
				if (toRemoveNotKnownNegatedTerms)
					sb.append("-unkNegToRem-1");
				return sb.toString();
			}
			else 
				return "";
		}
		
		public NegationTreatmentParams () {};
		
		public NegationTreatmentParams (NegationTreatmentType negTreatmentType, int lookBehindLength) {
			this.lookBehindLength = lookBehindLength;
			this.negTreatmentType = negTreatmentType;
		};
		
		public NegationTreatmentParams(NegationTreatmentParams initial)
		{
			this(initial.negTreatmentType, initial.lookBehindLength, initial.toRemoveNotKnownNegatedTerms);
		}
		
		public NegationTreatmentParams (NegationTreatmentType negTreatmentType, int lookBehindLength, boolean toRemoveNotKnownNegatedTerms) {
			this.lookBehindLength = lookBehindLength;
			this.negTreatmentType = negTreatmentType;
			this.toRemoveNotKnownNegatedTerms = toRemoveNotKnownNegatedTerms;
		};
		
		
	}
	
	// just temporary variables to save space
	static boolean negationFound;
	static int prevInd;
		
	public static Map<String, Integer> treatNegationsInFoundTerms(NegationTreatmentParams negParams, Map<String, Integer> foundTerms, List<String> tokens, Map<String, List<int[]>> foundPositions)
	{
		if (negParams.negTreatmentType == NegationTreatmentType.None)
			return foundTerms;
		
		
		for (String term : foundPositions.keySet())
		{
			List<int[]> positions = foundPositions.get(term);
			for (int[] position : positions)
			{
				negationFound = false;
				prevInd = position[0] - 1;
				while (prevInd >= 0 && prevInd >= position[0] - negParams.lookBehindLength)
				{
					if (UtilText.isNegationWord(tokens.get(prevInd).toLowerCase()))
					{
						negationFound = true;
						break;
					}
					prevInd--;
				}
				
				if (negationFound)
				{
					// need to drop this occurrence from the data
					UtilCollections.incrementIntValueInMap(foundTerms, term, -1);
					
					if (negParams.negTreatmentType == NegationTreatmentType.ToIncludeWithNegation)
					{
						StringBuilder constructedTerm = new StringBuilder(); // all between negation and the term
						for (int i = prevInd; i < position[0]; ++i)
							constructedTerm.append(tokens.get(i) + " ");
						constructedTerm.append(term);
						UtilCollections.incrementIntValueInMap(foundTerms, constructedTerm.toString(), 1);
					}
					else if (negParams.negTreatmentType == NegationTreatmentType.ToReplace)
					{
						// need to add a changed version of this term
						UtilCollections.incrementIntValueInMap(foundTerms, "not_"+term, 1);
					}
					
				}
			}
			
		}
		
		List<String> termsToRemove = new ArrayList<String>();
		for (String  term : foundTerms.keySet() )
			if (foundTerms.get(term) <= 0)
				termsToRemove.add(term);
		
		UtilCollections.removeAllKeysFromMap(foundTerms, termsToRemove);
		return foundTerms;
	}
	
	

}

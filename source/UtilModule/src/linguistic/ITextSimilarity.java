/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.util.List;
import java.util.Set;

import utility.Pair;

public interface ITextSimilarity {
	
	public double getMaxSimilarityValue();
	public double getMinSimilarityValue();
	
	public double getSimilarityScore(String text1, String text2);
	
	public <T extends Object> double getSimilarityScore(Set<T> set1, Set<T> set2);
	
	/** The lists should be sorted before applying this function (sorting function not included for speeding up) + this method suggesting no dublicates present!*/
	public <T extends Object & Comparable<T>>  double getSimilarityScore(List<T> list1, List<T> list2);
	
	/** The lists should be sorted by the first element of the pair before applying this function (sorting function not included for speeding up) + this method suggesting no dublicates present!*/
	public <T extends Object & Comparable<T>>  double getSimilarityScoreFromFeatures(List<Pair<T, Double>> list1, List<Pair<T, Double>> list2);
	
}

/*
    Based on the examples from Stanford CoreNLP.
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations.OriginalTextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import functionality.UtilString;

public class StanfordAnnotator {
	protected StanfordCoreNLP pipeline;

	public StanfordAnnotator() {
        // Create StanfordCoreNLP object properties, with POS tagging
        // (required for lemmatization), and lemmatization
        Properties props;
        props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma");

        // StanfordCoreNLP loads a lot of models, so you probably
        // only want to do this once per execution
        this.pipeline = new StanfordCoreNLP(props);
    }

	
	 public AnnotationData getAnnotation(String documentText)
	    {
	        List<String> lemmas = new LinkedList<String>();
	        List<String> postags = new LinkedList<String>();


	        // create an empty Annotation just with the given text
	        Annotation document = new Annotation(documentText);

	        // run all Annotators on this text
	        this.pipeline.annotate(document);

	        // Iterate over all of the sentences found
	        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
	        for(CoreMap sentence: sentences) {
	            // Iterate over all tokens in a sentence
	            for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
	                // Retrieve and add the lemma for each word into the
	                // list of lemmas
	                lemmas.add(token.get(LemmaAnnotation.class).toLowerCase());
	                String realWord = token.get(OriginalTextAnnotation.class).toLowerCase();
	                String posTag = token.get(PartOfSpeechAnnotation.class);
	                //double idfValue = token.get(IDFAnnotation.class);
	                postags.add(realWord + "/"+posTag);
	            }
	        }
	        
	        String lemmaStr = UtilString.generateStringFromStringList(lemmas);
	        String postagStr = UtilString.generateStringFromStringList(postags);
			
			AnnotationData ad = new AnnotationData();
			ad.lemmatizedText = lemmaStr;
			ad.posTaggedText = postagStr;
	        return ad;
	    }
	 
	 public class AnnotationData
	 {
		 public String lemmatizedText;
		 public String posTaggedText;
	 }
	    
}

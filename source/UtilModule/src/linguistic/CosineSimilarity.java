/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import utility.Pair;

public class CosineSimilarity implements ITextSimilarity{

	@Override
	public double getMaxSimilarityValue() {
		return 1.0;
	}

	@Override
	public double getMinSimilarityValue() {
		return 0.0;
	}

	@Override
	public double getSimilarityScore(String text1, String text2) 
	{
		//TODO: get token set from texts!
		String[] tokens1 = text1.split(" +");
		String[] tokens2 = text2.split(" +");
		
		return getSimilarityScore(new HashSet<String>(Arrays.asList(tokens1)), new HashSet<String>(Arrays.asList(tokens2)));
	}
	
	
	@Override
	public <T extends Object> double getSimilarityScore(Set<T> set1, Set<T> set2)
	{
		if (set1 == null || set1.size() == 0 || set2 == null || set2.size() == 0)
			return 0.0;
		
		Set<T> union = new HashSet<T>(set1);
		union.addAll(set2);
		Set<T> intersect = new HashSet<T>(set1);
		intersect.retainAll(set2);
		return (1.0)*intersect.size() /  ( Math.sqrt(set1.size()) * Math.sqrt(set2.size())  );
	}


	@Override
	public <T extends Object & Comparable<T>> double getSimilarityScore(List<T> list1, List<T> list2)
	{
		if (list1 == null || list1.size() == 0 || list2 == null || list2.size() == 0)
			return 0.0;
		
		int intersect = 0;
		
		int ind1 = 0, ind2 = 0;
		while (ind1 < list1.size() && ind2 < list2.size())
		{
			int comp = list1.get(ind1).compareTo(list2.get(ind2));
			if (comp == 0)
			{
				ind1++;
				ind2++;
				intersect++;
			}
			else if (comp < 0)
				ind1++;
			else
				ind2++;
		}
		
		return (1.0)*intersect /  ( Math.sqrt(list1.size()) * Math.sqrt(list2.size())  ) ;
	}

	@Override
	public <T extends Object & Comparable<T>> double getSimilarityScoreFromFeatures(
			List<Pair<T, Double>> list1, List<Pair<T, Double>> list2) {
		if (list1 == null || list1.size() == 0 || list2 == null || list2.size() == 0)
			return 0.0;
		
		double intersect = 0.0;
		
		int ind1 = 0, ind2 = 0;
		while (ind1 < list1.size() && ind2 < list2.size())
		{
			int comp = list1.get(ind1).first.compareTo(list2.get(ind2).first);
			if (comp == 0)
			{
				intersect += Math.min(list1.get(ind1).second, list2.get(ind2).second);
				ind1++;
				ind2++;
			}
			else if (comp < 0)
				ind1++;
			else
				ind2++;
		}
		
		//calculate the vectors length
		double len1 = 0.0;
		for (int index1 = 0; index1 < list1.size(); ++index1)
		{
			len1 += Math.pow(list1.get(index1).second,2);
		}
		len1 = Math.sqrt(len1);
		
		double len2 = 0.0;
		for (int index2 = 0; index2 < list2.size(); ++index2)
		{
			len2 += Math.pow(list2.get(index2).second,2);
		}
		len2 = Math.sqrt(len2);
		
		return (1.0)*intersect /  ( len1 * len2 ) ;
		
	}
	
}

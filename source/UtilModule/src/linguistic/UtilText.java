/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package linguistic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.tartarus.snowball.SnowballStemmer;
import org.tartarus.snowball.ext.englishStemmer;

import utility.Pair;
import connectors.WordNetDictionary;
import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import functionality.UtilCollections;
import functionality.UtilFiles;
import functionality.UtilString;

/**
 * Utility methods for text preprocessing (taken initially from Marina)
 */
public class UtilText 
{
	private static Vector<String> stopWords = null;
	private static Vector<String> negationWords = null;
	private static Vector<String> minusWords = null;
	private static Vector<String> plusWords = null;
	private static Vector<String> personalPronounWords = null;
	private static Vector<String> interjectionWords = null;
	private static Vector<String> negationVerbs = null;
	
	public static Set<String> separators;
	public static Set<String> sentenceSeparators;
	
	
	private static final String regexMetacharacters = "<([{\\^-=$!|]})?*+.>";
	
	private static Pattern digitPatern = null;
	
	private static Pattern nonAlphaNumericCharactersPattern = null;
	private static Pattern nonAlphaNumericCharactersPatternWithPunctuation = null;
	
	
	private static Pattern repeatedCharactersPattern = null;
	
	private static Properties properties = null;
	
	public static StanfordCoreNLP pipeline = null;
	
	public static SnowballStemmer stemmer = (SnowballStemmer) new englishStemmer();
	
	public static final String punctuationSigns = "?!.,;:-\"()[]/{}*|~^&#$+";
	
	

	public static String escapeMetacharacters(String text)
	{
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0; i < text.length(); i++)
		{
			char c = text.charAt(i);
			
			if(regexMetacharacters.contains(c + ""))
			{
				sb.append("\\" + c);
			}
			else
			{
				sb.append(c);
			}
		}
		
		return sb.toString();
	}
	
	/**
	 * This returns the string where the alongated symbols will be replaced:
	 * 
	 * if a letter symbol is repeated twice or once - it remains the same
	 * if a letter symbol is repeated at least 3 times - we replace it by 2 letters and add a sign * in the end on the word
	 * @param text
	 * @return
	 */
	public static String replaceAlongationsInWord(String word)
	{
		if(repeatedCharactersPattern == null)
		{
			String regex = "([a-zA-Z])\\1{2,}";
			
			repeatedCharactersPattern = Pattern.compile(regex);
		}
		
		StringBuilder newWord = new StringBuilder();
		
		Matcher m = repeatedCharactersPattern.matcher(word);
		
		int offset = 0;
		
		while(m.find())
		{
			newWord.append(word.substring(offset, m.start()));
			
			newWord.append(m.group().substring(0,2));
			
			offset = m.end();
		}
		newWord.append(word.substring(offset));
		if (offset > 0)
			newWord.append("*");
		
		return newWord.toString();
	}
	
	public static String replaceRepeatedCharacters(String text)
	{
		if(repeatedCharactersPattern == null)
		{
			String regex = "(\\w)\\1+";
			
			repeatedCharactersPattern = Pattern.compile(regex);
		}
		
		StringBuilder newText = new StringBuilder();
		
		Matcher m = repeatedCharactersPattern.matcher(text);
		
		int offset = 0;
		
		while(m.find())
		{
			newText.append(text.substring(offset, m.start()));
			
			newText.append(m.group().substring(0,2));
			
			offset = m.end();
		}
		newText.append(text.substring(offset));
		
		return newText.toString();
	}
	
	public static String replaceNonAlphaNumericCharacters(String text)
	{
		return replaceNonAlphaNumericCharacters(text, true);
	}
	
	/**
	 * Replaces all NonAlphaNumeric symbols with the white space
	 * @param text
	 * @param leaveCommonPunctuations
	 * @return
	 */
	public static String replaceNonAlphaNumericCharacters(String text, boolean leaveCommonPunctuations)
	{
		return replaceNonAlphaNumericCharacters(text, leaveCommonPunctuations, " ");
	}
	
	public static String replaceNonAlphaNumericCharacters(String text, boolean leaveCommonPunctuations, String replacement)
	{
		Pattern patternToUse;
		if (leaveCommonPunctuations)
		{
			if(nonAlphaNumericCharactersPatternWithPunctuation == null)
			{
				String regex = "[[[\\W]&&[\\S]]&&[^\\.\\!\\?\\,]]+";
				
				nonAlphaNumericCharactersPatternWithPunctuation = Pattern.compile(regex);
			}
			patternToUse = nonAlphaNumericCharactersPatternWithPunctuation;
		}
		else
		{
			if(nonAlphaNumericCharactersPattern == null)
			{
				String regex = "[[\\W]&&[\\S]]+"; // \\S to avoid replacing the white spaces also
				
				nonAlphaNumericCharactersPattern = Pattern.compile(regex);
			}
			patternToUse = nonAlphaNumericCharactersPattern;
		}
		String newText = text;
			
		newText = patternToUse.matcher(newText).replaceAll(replacement);

		return newText;
	}
	
	/**
	 * separate punctiation signs from words
	 * @param text
	 * @return
	 */
	public static String separatePunctuationFromWords(String text)
	{ 
		String punctuationSigns = "?!."; //only main ones remaining
		
		String newText = text;
		
		for (int i = 0; i <  punctuationSigns.length(); ++i)
		{
			char c = punctuationSigns.charAt(i);
			
			//add white space at the word end
			newText = Pattern.compile("(?<=[\\w])\\" + c ).matcher(newText).replaceAll(" " + c);
			
			//add white space at the word beginning
			newText = Pattern.compile("\\" + c + "(?=[\\w])").matcher(newText).replaceAll(c + " ");
		}
		return newText;
	}
	
	//static String dateRegExp = "\\d\\d([/\\.\\-])\\d\\d\\1\\d\\d\\d\\d"; //not done
	static String timeRegExp = "(\\d\\d:\\d\\d)|(\\d{1,2}(am|pm))";
	static String yearRegExp = "((19)|(20))[0-9][0-9]";
	static String intNumRegExp = "[-+]?[0-9]+(,[0-9]+)?";
	static String floatNumRegExp = "[-+]?[0-9]+(,[0-9]+)?\\.[0-9]+";
	static String anyNumRegExp = "[-+]?[0-9]*\\.?[0-9]+";
	
	public static String replaceNumbers(String text)
	{
		String newText = text.replaceAll(yearRegExp, "<YEAR>");
		newText = newText.replaceAll(anyNumRegExp, "<NUM>");
		return newText;
	}
	
	public static String replaceNumbersFully(String text)
	{
		String newText = text.replaceAll(timeRegExp, "<TIME>");
		newText = newText.replaceAll(yearRegExp, "<YEAR>");
		newText = newText.replaceAll(floatNumRegExp, "<FLOAT_NUM>");
		newText = newText.replaceAll(intNumRegExp, "<INT_NUM>");
		return newText;
	}
	
	public static List<String> getAllStopWords()
	{
		if(stopWords == null)
			try {
				initializeStopWordList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return new ArrayList<String>(stopWords);
	}
	
	public static boolean containsOnlyStopWords(String term)
	{
		if(stopWords == null)
			try {
				initializeStopWordList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		for (String word : term.trim().split("\\s+"))
		{
			if (!stopWords.contains(word))
				return false;
		}
		return true;
	}
	
	public static boolean containsOnlyStopWordsAndGiven(String term, Set<String> givenTerms)
	{
		if (givenTerms == null)
			return containsOnlyStopWords(term);
			
		if(stopWords == null)
			try {
				initializeStopWordList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		for (String word : term.trim().split("\\s+"))
		{
			if (!stopWords.contains(word) && !givenTerms.contains(word))
				return false;
		}
		return true;
	}
	
	public static String removeStopWordsAndGiven(String term, Set<String> givenTerms)
	{
		if(stopWords == null)
			try {
				initializeStopWordList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		StringBuilder sb = new StringBuilder();
		for (String word : term.trim().split("\\s+"))
		{
			if (!stopWords.contains(word) && !givenTerms.contains(word))
				sb.append(word + " ");
			
		}
		return sb.toString().trim();
	}
	
	public static int countTokenNum(String text)
	{
		return text.trim().split("\\s+").length;
	}
	
	private static void initializeStopWordList() throws Exception
	{
		String stopWordsResoucePath = UtilText.class.getClassLoader().getResource("termLists/stop-words.txt").toURI().getPath();
		stopWords = new Vector<String>(UtilFiles.getContentLines(stopWordsResoucePath));
	}
	
	private static void initializeNegationWordList() throws Exception
	{
		String negationWordsResoucePath = UtilText.class.getClassLoader().getResource("termLists/negation-words.txt").toURI().getPath();
		negationWords = new Vector<String>(UtilFiles.getContentLines(negationWordsResoucePath));
	}
	
	private static void initializeIntensifierWordList() throws Exception
	{
		String wordsResoucePath = UtilText.class.getClassLoader().getResource("termLists/plus-words.txt").toURI().getPath();
		plusWords = new Vector<String>(UtilFiles.getContentLines(wordsResoucePath));
	}
	
	private static void initializeDiminisherWordList() throws Exception
	{
		String wordsResoucePath = UtilText.class.getClassLoader().getResource("termLists/minus-words.txt").toURI().getPath();
		minusWords = new Vector<String>(UtilFiles.getContentLines(wordsResoucePath));
	}
	
	private static void initializePronounWordList() throws Exception
	{
		String wordsResoucePath = UtilText.class.getClassLoader().getResource("termLists/person-pronouns.txt").toURI().getPath();
		personalPronounWords = new Vector<String>(UtilFiles.getContentLines(wordsResoucePath));
	}
	
	private static void initializeInterjectionWordList() throws Exception
	{
		String wordsResoucePath = UtilText.class.getClassLoader().getResource("termLists/interjection-words.txt").toURI().getPath();
		interjectionWords = new Vector<String>(UtilFiles.getContentLines(wordsResoucePath));
	}

	private static void initializeNegationVerbsList() throws Exception
	{
		String wordsResoucePath = UtilText.class.getClassLoader().getResource("termLists/negation-verbs.txt").toURI().getPath();
		negationVerbs = new Vector<String>(UtilFiles.getContentLines(wordsResoucePath));
	}
	
	private static void initializeSimpleSeparatorsList()
	{
		separators = new HashSet<String>();
		String[] separatorsArray = {".", ",", ";", ":", "(", ")", "{", "}", "-lrb-", "-rrb-", "!", "?", "-", "``", "''", "...",
				"`", "\n", "!*", "?*", "...*", "\\n", "!?", "?!", "?!*", "?!?", "?!?*", "?*!", "?*!*", "!*?*", "!?*", "\"", "[", "]", ".."};
		separators.addAll(Arrays.asList(separatorsArray));
	}
	
	private static void initializeSentenceSeparatorsList()
	{
		sentenceSeparators = new HashSet<String>();
		String[] separatorsArray = {".", ";", "!", "?", "...", "\n", "!*", "?*", "...*", 
				"\\n", "!?", "?!", "?!*", "?!?", "?!?*", "?*!", "?*!*", "!*?*", "!?*", ".."};
		sentenceSeparators.addAll(Arrays.asList(separatorsArray));
	}
	
	public static Vector<String> getAllNegationWords() throws Exception
	{
		if(negationWords == null)
			initializeNegationWordList();
		
		//return new ArrayList<String>(negationWords);
		return negationWords;
	}
	
	public static Vector<String> getAllPlusWords() throws Exception
	{
		if(plusWords == null)
			initializeIntensifierWordList();
		
		//return new ArrayList<String>(plusWords);
		return plusWords;
	}
	
	public static Vector<String> getAllMinusWords() throws Exception
	{
		if(minusWords == null)
			initializeDiminisherWordList();
		
		//return new ArrayList<String>(minusWords);
		return minusWords;
	}
	
	public static Vector<String> getAllInterjectionsWords() throws Exception
	{
		if(interjectionWords == null)
			initializeInterjectionWordList();
		
		return interjectionWords;
	}

	public static Vector<String> getAllNegationVerbs() throws Exception
	{
		if(negationVerbs == null)
			initializeNegationVerbsList();
		
		//return new ArrayList<String>(minusWords);
		return negationVerbs;
	}
	
	
	public static boolean isStopWord(String word) 
	{
		if(stopWords == null)
			try {
				initializeStopWordList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return stopWords.contains(word.toLowerCase());
	}
	
	public static boolean isStopWord(String word, Set<String> extraStopWords) 
	{
		if(stopWords == null)
			try {
				initializeStopWordList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		String wordL = word.toLowerCase();
		boolean isStop = stopWords.contains(wordL);
		if (!isStop && extraStopWords != null)
			isStop = extraStopWords.contains(wordL);
		return isStop;
	}
	
	public static boolean isNegationWord(String word)
	{
		if (negationWords == null)
			try {
				initializeNegationWordList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return negationWords.contains(word) ||  word.endsWith("n't") || word.endsWith("n`t");
	}
	
	public static boolean isIntensifierWord(String word)
	{
		if (plusWords == null)
			try {
				initializeIntensifierWordList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return plusWords.contains(word);
	}
	
	public static boolean isDiminisherWord(String word)
	{
		if (minusWords == null)
			try {
				initializeDiminisherWordList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return minusWords.contains(word);
	}
	
	/**
	 * This will check if the word is personal pronoun standing for the person ("it" is excluded thus)
	 * @param word
	 * @return
	 */
	public static boolean isPersonalPronoun(String word) 
	{
		if (personalPronounWords  == null)
			try {
				initializePronounWordList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return personalPronounWords.contains(word);
	}

	public static boolean isPronoun(String word) {
		String[] pronouns = {"i", "he","she","we","they"};
		boolean isPronoun = false;
		
		for (String pr:pronouns) {
			if (word.toLowerCase().equals(pr)) {
				isPronoun = true;
			}
		}
		return isPronoun;
	}
	
	public static boolean isPunctuationSign(String word) 
	{
		String[] repeatedPunctuationSigns = {"-?!-*","?*","!*","...*","...","!?", "?!", "?!*", "?!?", "?!?*", "?*!", "?*!*", "!*?*", "!?*"};
		boolean isPS = false;
		
		for (int i=0;i<punctuationSigns.length();i++)
			if (word.equals(String.valueOf(punctuationSigns.charAt(i))))
				isPS = true;
		
		for (String punctSign : repeatedPunctuationSigns)
			if (word.equals(punctSign))
				isPS = true;
		
		return isPS;
		
	}
	

	public static boolean isNgramSeparator(String word) 
	{
		if (separators == null)
			initializeSimpleSeparatorsList();
		
		return separators.contains(word) || word.startsWith("<emot") || word.startsWith("<emoji");
	}
	
	public static boolean isSentenceSeparator(String word) 
	{
		if (sentenceSeparators == null)
			initializeSentenceSeparatorsList();
		
		return sentenceSeparators.contains(word) || word.startsWith("<emot") || word.startsWith("<emoji");
	}
	
	/**
	 * Check if the first char in the word is a punctuation sign
	 * @param word
	 * @return
	 */
	public static boolean startsFromPunctuationSign(String word) 
	{
		boolean isPS = false;
	
		for (int i=0;i<punctuationSigns.length();i++)
			if (word.charAt(0) == punctuationSigns.charAt(i))
				isPS = true;
		return isPS;
	}
	
	public static boolean potentiallyEndingDiminisherWord(String token)
	{
		return (token.equals("of")); // to help detect diminishers "kind of" and "sort of"
	}
	
	public static boolean isWNDictionaryWord(String word)
	{
		return WordNetDictionary.containsTerm(word);
	}
	
	public static boolean containsDigit(String word)
	{
		if(digitPatern == null)
		{
			digitPatern = Pattern.compile("[0-9]+");
		}
		
		Matcher m = digitPatern.matcher(word);
		
		if(m.find())
		{
			return true;
		}
		
		return false;
	}
	
	public static boolean isNumber(String word)
	{
		return word.matches("[0-9]+\\.?[0-9]+");
	}
	
	public static int getFirstInteger(String word, int errorNum)
	{
		if(digitPatern == null)
		{
			digitPatern = Pattern.compile("[0-9]+");
		}
		Matcher m = digitPatern.matcher(word);
		if (m.find())
		{
			return UtilString.parseInt(m.group(), errorNum);
		}
		else
			return errorNum;
	}
	
	public static String toLowerCase(String text)
	{
		return text.toLowerCase();
	}
	
	/**
	 * Returns true if the text contains any non-alpha numeric characters.
	 * @param term
	 * @return
	 */
	public static boolean containsNonWordCharacters(String term)
	{
		String regex = "[\\W&&[^ ]]";
		Matcher m = Pattern.compile(regex).matcher(term);
		if (m.find())
		{
			return true;
		}
		return false;
	}
	
	/** Returns all tokens with word characters [a-zA-Z_0-9]*/
	public static Vector<String> getTokens(String text)
	{
		Vector<String> tokens = new Vector<String>();
		
		String regex = "[\\w]+";
		
		Matcher m = Pattern.compile(regex).matcher(text);
		
		while(m.find())
		{
			tokens.add(m.group());
		}
		
		return tokens;
	}
	
	/** Returns all tokens separated with write space, or smth like it [ \t\n\x0B\f\r]*/
	public static Vector<String> getAllTokens(String text)
	{
		Vector<String> tokens = new Vector<String>();
		
		String regex = "[\\S]+";
		
		Matcher m = Pattern.compile(regex).matcher(text);
		
		while(m.find())
		{
			tokens.add(m.group());
		}
		
		return tokens;
	}
	
	
	
	public static Vector<String> getTokensWithPunctuation(String text)
	{
		Vector<String> tokens = new Vector<String>();
		
		String regex = "[\\w\\.\\!\\?]+";
		
		Matcher m = Pattern.compile(regex).matcher(text);
		
		while(m.find())
		{
			tokens.add(m.group());
		}
		
		return tokens;
	}
	
	/** No punctuation, stop words or words with digits is returned (however, terms with digits inside tags are included, e.g. <emot1>)*/
	public static Vector<String> getUsefulTokens(String text)
	{
		Vector<String> tokens = new Vector<String>();
		
		String regex = "<?[\\w]+>?"; //should also include terms like < >
		
		Matcher m = Pattern.compile(regex).matcher(text);
		
		while(m.find())
		{
			String token = m.group();
			
			if ((!isStopWord(token) && !containsDigit(token) && token.length() >= 3) // if not stop word, or not useful
					|| (token.matches("<.+>"))) // or if one of specialized tokens
			{
				tokens.add(token);
			}
		}
		
		return tokens;
	}
	
	/** Contains digits out of the scope of reserved words*/
	public static boolean containsOutDigit(String text)
	{
		if(digitPatern == null)
		{
			digitPatern = Pattern.compile("[0-9]+");
		}
		
		Matcher m = digitPatern.matcher(text);
		
		if(m.find())
		{
			return containsDigit(replaceReservedWords(text));
		}
		
		return false;
	}
	
	private static String replaceReservedWords(String text)
	{
		String regex = "<.+>";
		Matcher m = Pattern.compile(regex).matcher(text);
		return m.replaceAll("-");
	}
	
	/** Returns tokens separated with write space, or smth like it [ \t\n\x0B\f\r]
	 * Excludes tokens which are stop words, too short, only main punctuation signs, or contains numbers (but not specialized like <emot21>)*/
	public static Vector<String> getAllRefinedTokens(String text)
	{
		Vector<String> tokens = new Vector<String>();
		
		String regex = "[\\S]+";
		
		Matcher m = Pattern.compile(regex).matcher(text);
		
		while(m.find())
		{
			String token = m.group();
			
			if ((!isStopWord(token) && !containsDigit(token) && token.length() >= 3 && !token.matches("[\\.\\!\\?\\\"]+")) // if not stop word, or not useful
					|| (token.matches("<.+>"))) // or if one of specialized tokens
			{
				tokens.add(token);
			}
		}
		
		return tokens;
	}
	
	public static String stemTerm(String term)
	{
		if (term == null)
			return null;
		
		 stemmer.setCurrent(term);
		  
		 stemmer.stem();
		    
		 return stemmer.getCurrent();
	}
	
	public static String stemText(String text)
	{
		StringBuilder newText = new StringBuilder();
		
		
		
		StringBuilder current = new StringBuilder();
		
		for(int i = 0; i < text.length(); i++)
		{
			char ch = (char) text.charAt(i);
		    
			if (Character.isWhitespace((char) ch)) 
		    {
				if (current.length() > 0) 
				{
				    stemmer.setCurrent(current.toString());
				  
				    stemmer.stem();
				    
				    newText.append(stemmer.getCurrent());
				    
				    newText.append(" ");
				    
				    current = new StringBuilder();
				}
		    } 
		    else 
		    {
		    	current.append(ch);
		    }
		}
		
		newText.append(current.toString());
		
		return newText.toString();
	}
	
	/**
	 * This function uses StanfordCoreNLP to tokenize text, find POS tags, and then only lemmatize.
	 * @param text
	 * @return
	 */
	public static String lemmatizeText(String text)
	{
		if(properties == null)
		{
			properties = new Properties();
			
			properties.put("annotators", "tokenize, ssplit, pos, lemma");
		}
		
		if(pipeline == null)
		{
			pipeline = new StanfordCoreNLP(properties, false);
		}
		
		Annotation document = pipeline.process(text);
		
		StringBuilder current = new StringBuilder();

		for(CoreMap sentence: document.get(SentencesAnnotation.class)) 
		{
		    for(CoreLabel token: sentence.get(TokensAnnotation.class)) 
		    {
		        String lemma = token.get(LemmaAnnotation.class);
		        
		        current.append(lemma + " ");
		    }
		}
		
		return current.toString().trim();
	}
	
	/**
	 * Detects if there are words in the collection with * in the end. 
	 * This is to detect whether pattern-based matching should be used, when any continuation of a given pattern would be accepted.
	 * @param allTerms
	 * @return
	 */
	public static boolean containsStemmedFormPatterns(Collection<String> allTerms)
	{
		boolean usePatterns = false;
		for (String term : allTerms)
		{
			//detect if need to use patterns (*)
			if (term.endsWith("*"))
				usePatterns = true;
			
			if (term.contains("?")) //there can be errors if we use patterns when '?' also present
				usePatterns = false;
		}
		return usePatterns;
	}
	
	/**
	 * Removes any term containing white spaces from the termSet.
	 * @param termSet
	 */
	public static void removeNgrams(Set<String> termSet)
	{		
		Set<String> termToRemove = findNgrams(termSet);
		termSet.removeAll(termToRemove);
	}
	
	/**
	 * Returns the set of terms containing white spaces.
	 * @param termSet
	 * @return
	 */
	public static Set<String> findNgrams(Set<String> termSet)
	{
		Set<String> termToRemove = new HashSet<String>();
		
		for (String term : termSet)
		{
			if (term.contains(" "))
				termToRemove.add(term);
		}
		return termToRemove;
	}
	
	/**
	 * Returns all found ngrams up to the length of n with the number of occurrences with which they are found. No negations are treated.
	 * @param tokenizedSentence
	 * @param n
	 * @return
	 */
	public static Map<String, Integer> getNGramsFromTokenizedSentence(List<String> tokenizedSentence, int n, TermDetectionParameters termDetectionParameters)
	{
		return NgramExtraction.extractAllNGramsFromSentence(tokenizedSentence, n, termDetectionParameters);
	}
	
	/**
	 * Returns all found ngrams up to the length of n with the number of occurrences with which they are found. No negations are treated.
	 * @param tokenizedSentence
	 * @param n
	 * @return
	 */
	public static Map<String, Integer> getNGramsFromTokenizedSentence(List<String> tokenizedSentence, int n)
	{
		return NgramExtraction.extractAllNGramsFromSentence(tokenizedSentence, n, TermDetectionParameters.defaultEmptyTreatmentParams);
	}

	
	/** Returns all found ngrams with the length between minN and maxN  with the number of occurrences with which they are found. . 
	 * When negation is present, it is treated according to the specified parameters
	 * If addNegations is false, no negations are treated.*/
	public static Map<String, Integer> getNGramsFromTokenizedSentence(List<String> tokenizedSentence, int minN, int maxN, TermDetectionParameters termDetectionParameters)
	{
		return NgramExtraction.extractAllNGramsFromSentence(tokenizedSentence, minN, maxN,  termDetectionParameters);
	}
	
	
	public static Map<String, Integer> getSubstringsFromTokenizedSentence(List<String> tokenizedSentence, int n)
	{
		return SubstringExtraction.extractAllSubstringsFromSentence(tokenizedSentence, n);
	}

	public static int getPatternFirstStart (String text,  String pattern, int startSearchIndex)
	{
		Pattern retwPattern = Pattern.compile(pattern);
		Matcher m = retwPattern.matcher(text);
		
		if (m.find(startSearchIndex))
		{
			return m.start();
		}
		
		return -1;
	}
	
	/**
	 * Returns -1 if nothing found, or index of the start if found
	 * @param text
	 * @param pattern
	 * @return
	 */
	public static int getPatternFirstStart (String text, String pattern)
	{
		Pattern retwPattern = Pattern.compile(pattern);
		Matcher m = retwPattern.matcher(text);
		
		if (m.find())
		{
			return m.start();
		}
		
		return -1;
	}
	
	public static Pair<Integer, Integer> getPatternFirstStartEnd (String text, String pattern, int startSearchIndex)
	{
		Pattern retwPattern = Pattern.compile(pattern);
		Matcher m = retwPattern.matcher(text);
		
		if (m.find(startSearchIndex))
		{
			return new Pair(m.start(), m.end());
		}
		
		return null;
	}
	
	public static Pair<Integer, Integer> getPatternFirstStartEnd (String text, String pattern)
	{
		Pattern retwPattern = Pattern.compile(pattern);
		Matcher m = retwPattern.matcher(text);
		
		if (m.find())
		{
			return new Pair(m.start(), m.end());
		}
		
		return null;
	}
	
	public static String getPatternInstance (String text, String pattern)
	{
		Pattern retwPattern = Pattern.compile(pattern);
		Matcher m = retwPattern.matcher(text);
		
		if (m.find())
		{
			return m.group();
		}
		
		return null;
	}

	/**
	 * Split the tokens based on white spaces.
	 * @param text
	 * @return
	 */
	public static List<String> getTokensWithoutPreprocessing(String text)
	{
		List<String> tokens = new ArrayList<String>();
		tokens.addAll(Arrays.asList(text.split("\\s+")));
		return tokens;
	}
	
	static Set<String> extraPhraseSeparators;
	
	private static boolean checkPatternPrefixInCollection(String text, Collection<String> prefixes)
	{
		for (String prefix : prefixes)
		{
			if (text.startsWith(prefix))
				return true;
		}
		return false;
	}
	
	public static int getNumberOfPhraseBreaks(String text)
	{
		if (extraPhraseSeparators == null)
		{
			extraPhraseSeparators = new HashSet<String>();
			extraPhraseSeparators.add("<emot");
			extraPhraseSeparators.add("<emoji");
		}
		
		// parse on words
		Vector<String> tokens = getAllTokens(text);
		
		boolean spaceToPrevious = false;
		int phrBreakNum = 0;
		for (String token : tokens)
		{
			if (NgramExtraction.separators.contains(token) || checkPatternPrefixInCollection(token, extraPhraseSeparators))
			{
				if (spaceToPrevious)
					phrBreakNum++;
				spaceToPrevious=false;
			}
			else
				spaceToPrevious=true;
		}
		
		if (spaceToPrevious)
			phrBreakNum++; // counting the end of the tweet
		return phrBreakNum;
	}
	
	private static Pattern notPunctuationMarkPatern;
	
	public static boolean containsOnlyPunctuations(String term)
	{
		if (notPunctuationMarkPatern == null)
		{
			notPunctuationMarkPatern = Pattern.compile("[^\\p{Punct}]"); //in Pattern: \p{Punct} means One of !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~
		}
		Matcher m = notPunctuationMarkPatern.matcher(term);
		
		if (m.find())
		{
			return false;
		}
		return true;
	}
}


/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package math;

import java.util.Random;

public class LogBinomialCoefficient {
	
	
	
	/**
	 * Computes the logarithm of basis 2 of binomial coefficient of m out of n
	 * @param n
	 * @param m
	 */
	public static double compute(int n, int m) throws Exception
	{
	    if (n >= 0)
	        if (m > n || m < 0) 
	        	throw new Exception ("Illegal m!!\n");
	        else 
	        {
	        	if (isCoefOne(n,m))
	        		return 0.0;
	        	else
		        	return LogFactorial.compute(n) - LogFactorial.compute(n - m) - LogFactorial.compute(m);
	        }
	    else 
	    	throw new Exception ("Negative n!!\n");
	}
	
	private static boolean isCoefOne(int n, int m)
	{
		return (n == m || m == 0);
	}
	
	
	
	
	
	
	
	
	
	public static void main (String[] args) throws Exception
	{
		// testing the computation
		boolean testResult = test();
		LogFactorial.compute(3);
		double coef = compute(3,1);
		double coef2 = compute(5,4);
		double coef3 = compute(6,3);
		coef++;
	}
	
	private static double compute_test(int n, int m)
	{
		double coeff = 1.0;
		for (int i = n - m + 1; i <= n; i++) {
			coeff *= i;
		}
		for (int i = 1; i <= m; i++) {
			coeff /= i;
		}
		return Math.log(coeff)/Math.log(2);
	}
	
	private static boolean test() throws Exception
	{
		int test_size = 1000;
		int maximumTestN = 100;
		double precision = 1e-10;
		boolean test_done = true;
		Random rand = new Random();
		for (int attempt = 0; attempt < test_size; ++attempt)
		{
			int n = rand.nextInt(maximumTestN) + 1;
			int m = rand.nextInt(n);
			if (Math.abs(compute(n,m)  - compute_test(n,m)) > precision)
			{
				test_done = false;
				break;
			}
				
		}
		return test_done;
	}
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package math;

import java.util.ArrayList;

/** 
 * Computes the logarithm of basis 2 of the factorial
 *
 */
public class LogFactorial {

	private static ArrayList<Double> logFactorials; //internal array of computed values

	static
	{
		logFactorials = new ArrayList<Double>();
		logFactorials.add(0.0);
		computeUpToN(100);
	}
	
	/**
	 * Computes the logarithm of basis 2 of the factorial of n
	 * @param n
	 * @return
	 */
	public static double compute (int n)
	{
		if (n > logFactorials.size() - 1)
		{
			computeUpToN(n);
		}
		return logFactorials.get(n);
	}
	
	
	private static void computeUpToN(int n)
	{
		System.out.println("Have to compute log factorial up to " + n);
		for (int i = logFactorials.size(); i <= n; ++i)
		{
			double nextLogFactorial = logFactorials.get(i - 1) + Math.log(i)/Math.log(2);
			logFactorials.add(nextLogFactorial);
		}
	}
}

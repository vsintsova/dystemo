/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package math;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class StepWiseCurve {

	List<Double[]> points; // each line contains [0] for x and [1] for y coordinates
	
	public StepWiseCurve (List<Double[]> points)
	{
		// sort the points by x coordinate
		Collections.sort (points, new Comparator<Double[]>() {
			public int compare (Double[] p1, Double[] p2) {
				return p1[0].compareTo(p2[0]);
			}
		});
		
		this.points = points;
		
		
		
		//TODO: add the start points (for ROC : 0 - 0 and 1 - 1)
	}
	
	public double subROCArea()
	{
		// add the start and end points (for ROC : 0 - 0 and 1 - 1)
		points.add(0, new Double[]{0.0, 0.0});
		points.add(new Double[]{1.0, 1.0});
		
		// TODO: check bounds (all x and y within 0 and 1)
		return subArea();
		
	}
	
	public double subPRArea()
	{
		// add the start and end points (for PR : 0 - 1 and 1 - 0)
		points.add(0, new Double[]{0.0, 1.0});
		points.add(new Double[]{1.0, 0.0});
		
		return subArea();
		
	}
	
	public double subArea()
	{
		double sum = 0.0;
		for (int i = 1; i < points.size(); ++i)
		{
			double xdiff = points.get(i)[0] - points.get(i - 1)[0];
			double yprod = (points.get(i)[1] + points.get(i - 1)[1])/2.0;
			sum += yprod * xdiff;
		}
		
		return sum;
	}
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package statistics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import functionality.UtilArrays;

public class UtilAggregation {
	
	public static double sum (Collection<Double> values)
	{
		double sumRes = 0.0;
		for (Double value : values)
			sumRes += value;
		return sumRes;
	}
	
	public static double sum (double[] values)
	{
		return  UtilArrays.getSum(values);
	}

	public static double average (Collection<Double> values)
	{
		return sum(values)/values.size();
	}
	
	public static double average (double[] values)
	{
		return UtilArrays.getSum(values)/values.length;
	}
	
	public static double variance (Collection<Double> values)
	{
		double mean = average (values);
		double sumRes = 0.0;
		for (Double value : values)
			sumRes += Math.pow(value - mean,2);
		return sumRes/(values.size() - 1);
	}
	
	public static double variance (double[] values)
	{
		double mean = average (values);
		double sumRes = 0.0;
		for (Double value : values)
			sumRes += Math.pow(value - mean,2);
		return sumRes/(values.length - 1);
	}
	
	public static double stdDev (Collection<Double> values)
	{
		return Math.sqrt(variance(values));
	}
	
	public static double stdDev (double[] values)
	{
		return Math.sqrt(variance(values));
	}
	
	public static double max (Collection<Double> values)
	{
		return Collections.max(values);
	}
	
	
	// multidimentional collections
	
	public static double[] averageArray (Collection<double[]> values)
	{
		if (values.isEmpty())
			return null;
		
		double[] res = sumArray(values);
		for (int j = 0; j < res.length; ++j)
		{
			res[j] /= values.size();
		}
		return res;
	}
	
	public static double[] maxArray (Collection<double[]> values)
	{
		if (values.isEmpty())
			return null;
		
		double[] result = new double[values.iterator().next().length];
		Arrays.fill(result, -Double.MAX_VALUE);
		for (double[] value : values)
		{
			for (int j = 0; j < result.length; ++j)
			{
				if (result[j] < value[j])
					result[j] = value[j];
			}
		}
		return result;
	}
	
	public static double[] sumArray (Collection<double[]> values)
	{
		if (values.isEmpty())
			return null;
		double[] sumRes = new double[values.iterator().next().length];
		Arrays.fill(sumRes, 0.0);
		for (double[] value : values)
		{
			for (int j = 0; j < sumRes.length; ++j)
			{
				sumRes[j] += value[j];
			}
		}
		return sumRes;
	}
	
	public static double median(Collection<Double> values)
	{
		List<Double> curSortedValues = new ArrayList<Double>(values);
		Collections.sort(curSortedValues);
		
		int medInd = curSortedValues.size() / 2;
		return curSortedValues.get(medInd);
	}
	
	public static double median(double[] values)
	{
		double[] curSortedValues  = Arrays.copyOf(values, values.length);
		Arrays.sort(curSortedValues);
		int medInd = curSortedValues.length / 2;
		return curSortedValues[medInd];
	}
	
	public static double entropy (double[] values)
	{
		double result = 0;
		double sum = UtilArrays.getSum(values);
		for (int i = 0; i < values.length; ++i)
		{
			double x = values[i];
			if (x > 0.0)
			{
				double probX = (1.0) * x / sum;
				result -= probX * Math.log(probX);
			}
		}
		return result;
	}
	
	public static double entropy (Double[] values)
	{
		double result = 0;
		double sum = UtilArrays.getSum(values);
		for (int i = 0; i < values.length; ++i)
		{
			double x = values[i];
			if (x > 0.0)
			{
				double probX = (1.0) * x / sum;
				result -= probX * Math.log(probX);
			}
		}
		return result;
	}
}

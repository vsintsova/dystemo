/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experimentsavers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import classification.evaluation.ComplexEvaluation.CategoryEvaluationResult;
import classification.evaluation.ComplexEvaluation.EvaluationResult;
import data.DatabaseConnectionsIndex;
import functionality.UtilCollections;

public class EvaluationSaving {
	
	public static String databaseConnectionForSaving = null;
	public static String evaluationResultTablename = "evaluation_results_full";
	public static String evaluationCategoryResultTablename = "evaluation_results_category";
	
	
	public static Connection GetDatabaseConnectionForEvaluationSaving() throws Exception {
		if (databaseConnectionForSaving == null)
			throw new Exception("The database connection for evaluation saving is not set. " + 
		    "Set it by assigning EvaluationSaving.databaseConnectionForSaving a name of database connection from current DatabaseConnectionsIndex");
		return DatabaseConnectionsIndex.getConnection(databaseConnectionForSaving); 
	}
	
	public static void createEmptyGeneralEvaluationResultsDatabaseTable() throws Exception 
	{
		String[] fixedFields = {"TestName", "RunComp", "RunId", "TestFileName", // general experiment description
			"trainDataName", "featureExtractionParams", "featureSelectionParams", "LearnerName", "ClassifierLearnerParams", // learning parameters
			"sslFullParams", "sslUnlabeledDataName", "sslInitialClassifier", // semi-supervised learning parameters
			"givenClassifierName", "givenClassifierApplicationParams", // parameters for running a given classifier
			"IterNum", "LexSize" 
			};
		String[] fixedFieldsTypes = new String[16];
		Arrays.fill(fixedFieldsTypes, "text");
		fixedFieldsTypes[2] = "int"; // for storing run id (RunId)
		fixedFieldsTypes[14] = "int"; // for storing iteration num (IterNum)
		fixedFieldsTypes[15] = "int"; // for storing lexicon size (LexSize)
		
		List<String> evaluationFields = EvaluationResult.getDataBaseFieldNamesWithoutCategories();
		List<String> evaluationFieldsTypes = EvaluationResult.getDataBaseFieldTypesWithoutCategories();
		
		//add `geomGenProd` and `emotionf05Score` new scores (will not be computed automatically!)
		evaluationFields.add("geomGenProd");
		evaluationFields.add("emotionf05Score");
		evaluationFieldsTypes.add("double");
		evaluationFieldsTypes.add("double");
		
		List<String> allFields = new ArrayList<String>();
		allFields.addAll(UtilCollections.transformArrayToList(fixedFields));
		allFields.addAll(evaluationFields);
		
		List<String> allTypes = new ArrayList<String>();
		allTypes.addAll(UtilCollections.transformArrayToList(fixedFieldsTypes));
		allTypes.addAll(evaluationFieldsTypes);
		
		Connection conn = GetDatabaseConnectionForEvaluationSaving(); 
		
		try
		 {			
			// "create general table " query 
			Statement stat = conn.createStatement();
			StringBuilder createTableQuery = new StringBuilder();
			createTableQuery.append("create table IF NOT EXISTS `" + evaluationResultTablename +"` (");
			for (int colInd = 0; colInd < allTypes.size(); ++colInd)
			{
				String curType = allTypes.get(colInd);
				String curField = allFields.get(colInd);
				if (curType.equals("int") || curType.equals("Integer") )
					createTableQuery.append("`" + curField + "` int(11), ");
				else if (curType.equals("double") || curType.equals("Double"))
					createTableQuery.append("`" + curField + "` double, ");
				else 
					createTableQuery.append("`" + curField + "` text, ");
			}
			createTableQuery.append("`computeDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP, ");
			createTableQuery.append("KEY `order` (`RunId`,`RunComp`(100),`TestFileName`(100))");
			
			createTableQuery.append(") ENGINE=InnoDB DEFAULT CHARSET=utf8"); 
			
			stat.execute(createTableQuery.toString());
			
		 }
		 catch (Exception e)
		 {
		    throw e;
		 }
		 finally{
		    conn.close();
		 } 
	
	}
	
	public static void createEmptyCategoryEvaluationResultsDatabaseTable() throws Exception 
	{
		String[] fixedFields = {"TestName", "RunComp", "RunId", "TestFileName", "CategoryType", // general experiment description
			"IterNum"
			};
		String[] fixedFieldsTypes = new String[6];
		Arrays.fill(fixedFieldsTypes, "text");
		fixedFieldsTypes[2] = "int"; // for storing run id (RunId)
		fixedFieldsTypes[5] = "int"; // for storing iteration num (IterNum)
		
		List<String> evaluationFields = CategoryEvaluationResult.getDataBaseFieldNames();
		List<String> evaluationFieldsTypes = CategoryEvaluationResult.getDataBaseFieldTypes();
		
		List<String> allFields = new ArrayList<String>();
		allFields.addAll(UtilCollections.transformArrayToList(fixedFields));
		allFields.addAll(evaluationFields);
		
		List<String> allTypes = new ArrayList<String>();
		allTypes.addAll(UtilCollections.transformArrayToList(fixedFieldsTypes));
		allTypes.addAll(evaluationFieldsTypes);
		
		Connection conn = GetDatabaseConnectionForEvaluationSaving(); 
		
		try
		 {			
			// "create general table " query 
			Statement stat = conn.createStatement();
			StringBuilder createTableQuery = new StringBuilder();
			createTableQuery.append("create table IF NOT EXISTS `" + evaluationCategoryResultTablename +"` (");
			for (int colInd = 0; colInd < allTypes.size(); ++colInd)
			{
				String curType = allTypes.get(colInd);
				String curField = allFields.get(colInd);
				if (curType.equals("int") || curType.equals("Integer") )
					createTableQuery.append("`" + curField + "` int(11), ");
				else if (curType.equals("double") || curType.equals("Double"))
					createTableQuery.append("`" + curField + "` double, ");
				else 
					createTableQuery.append("`" + curField + "` text, ");
			}
			createTableQuery.append("KEY `order` (`RunId`,`RunComp`(100),`TestFileName`(100), `CategoryType`(100))");
			
			createTableQuery.append(") ENGINE=InnoDB DEFAULT CHARSET=utf8"); 
			
			stat.execute(createTableQuery.toString());
			
		 }
		 catch (Exception e)
		 {
		    throw e;
		 }
		 finally{
		    conn.close();
		 } 
	
	}
	
	
	public static void saveEvaluationResultsToDatabase(String tabString, String fieldTabString, String tableType) throws Exception
	{
		// there can be only 3 types of the columns: text, double, int (but some columns may contain the number while being represented as 'text' in database, but it shouldn't be a problem)
		
		String[] fieldValues = tabString.split("\t", -1);
		String[] fieldNames = fieldTabString.split("\t");
		
		if (fieldValues.length != fieldNames.length)
			throw new Exception("The array lengths of names and values are different!");
		
		Connection conn = GetDatabaseConnectionForEvaluationSaving(); 
		
	    try
		{
	    	// "insert" query
			
	    	StringBuilder questionString = new StringBuilder("?");
	    	StringBuilder fieldString = new StringBuilder("`" + fieldNames[0] + "`");
	    	for (int colInd = 1; colInd < fieldNames.length; ++colInd)
	    	{
	    		if (fieldNames[colInd].length() == 0)
	    		{
	    			throw new Exception("Not all fieldNames have name!");
	    		}
	    		questionString.append(",?");
	    		fieldString.append(",`" + fieldNames[colInd] + "`");
	    	}
	    	String tablename = (tableType.equals("Category"))?evaluationCategoryResultTablename : 
	    		evaluationResultTablename;
	    	PreparedStatement prep = conn.prepareStatement(
	    					      "INSERT INTO " + tablename + " (" + fieldString.toString() + ") VALUES (" + questionString.toString() + ")");

	    	for (int colInd = 0; colInd < fieldValues.length; ++colInd)
	    	{
	    		// detect type of the column
	    		String value = fieldValues[colInd];
	    		
	    		try
	    		{
	    			int intValue = Integer.parseInt(value);
	    			// save integer
	    			prep.setInt(colInd + 1, intValue);
	    		}
	    		catch (NumberFormatException  e)
	    		{
	    			// not integer - try double next
	    			
	    			try
	    			{
	    				Double doubleValue = Double.parseDouble(value);
	    				// save double
	    				if (doubleValue.isNaN())
							prep.setNull(colInd + 1, java.sql.Types.DOUBLE);
						else
							prep.setDouble(colInd + 1, doubleValue);
	    			}
	    			catch (NumberFormatException  e1)
		    		{
		    			// not double - then save as string
	    				prep.setString(colInd + 1, value);
		    		}
	    		}
	    	}
	    	prep.addBatch();
	    	
				  conn.setAutoCommit(false);
				  prep.executeBatch();
				  conn.setAutoCommit(true);
		}
		catch (Exception e)
		{
			throw e;
		}
		finally{
			conn.close();
		} 
		
		
	}
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experimentsavers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import semisupervisedlearning.parameters.AllSemiSupervisedLearningParameters;
import learning.evaluation.ClassifierEvaluation;
import learning.parameters.AllLearningParameters;
import learning.parameters.AllParametersToEvaluateGiven;
import classification.definitions.WeightedClassifier;
import classification.evaluation.ComplexEvaluation.CategoryEvaluationResult;
import classification.evaluation.ComplexEvaluation.EvaluationResult;
import data.loading.DataFilesFormatSaver;
import functionality.UtilCollections;
import functionality.UtilFiles;
import functionality.UtilString;

public class ExperimentsResultsSaver
{
	public static String evaluationFullLogFileGiven = "output/evaluationResults/evaluationFullResultsGiven.txt";
	public static String evaluationFullLogFileSupervised = "output/evaluationResults/evaluationFullResultsSupervised.txt";
	public static String evaluationFullLogFileSemiSupervised = "output/evaluationResults/evaluationFullResultsSemiSupervised.txt";
	public static String evaluationCatLogFile = "output/evaluationResults/evaluationCatResults.txt";
	public static String filePatternForDetailedEvalResult = "output/evaluatedLexiconsOutput/lexdetailres-";
	public static String filePatternForDetailedClassifierOutput = "output/evaluatedLexiconsOutput/lexoutput-";
	
	
	public static int currentRunNum; 
	public static String currentTestName = "Unknown";
	public static String currentComputerName = EnvSettings.computerName;
	
	public static int currentRunIter = 0; 
	
	public static boolean toSaveDetailedClassifierResults = false;
	
	static String[] testFiles = {}; // enumerates all available test files to test on
	
	final static String logNumFilename = "output/evaluationLogNum.txt";
	
	
	public static void loadAndUpdateEvaluationNumber() throws FileNotFoundException, IOException
	{
		File logNumFile = new File(logNumFilename);
		if (!logNumFile.exists()) {
			System.err.println("The new experiment log number file is created! The experiment counter is set to 0.");
			UtilFiles.writeToFile(logNumFilename, "0"); // this will create a new file automatically
			initializeNewEvaluationFiles();
		}
		UtilFiles.CheckIfExistsOrCreate(logNumFilename); // will create such a file if not yet present
		int lastLogRunNum = Integer.parseInt(UtilFiles.getContentLines(logNumFile).get(0));
		currentRunNum = lastLogRunNum + 1;
		System.out.println();
		System.out.println();
		System.out.println("Run " + currentRunNum);  
		System.out.println();
		UtilFiles.setContents(logNumFile, Integer.toString(currentRunNum));
	}
	
	
	public static void initializeNewEvaluationFiles() throws IOException
	{
		if (UtilFiles.checkIfFileExists(evaluationFullLogFileSupervised)) {
			// move the previous file...
			UtilFiles.copyFile(evaluationFullLogFileSupervised, evaluationFullLogFileSupervised + "-prev" + (new Date()).getTime());
		}
		
		if (UtilFiles.checkIfFileExists(evaluationFullLogFileSemiSupervised)) {
			// move the previous file...
			UtilFiles.copyFile(evaluationFullLogFileSemiSupervised, evaluationFullLogFileSemiSupervised + "-prev" + (new Date()).getTime());
		}
		
		if (UtilFiles.checkIfFileExists(evaluationFullLogFileGiven)) {
			// move the previous file...
			UtilFiles.copyFile(evaluationFullLogFileGiven, evaluationFullLogFileGiven + "-prev" + (new Date()).getTime());
		}
		
		if (UtilFiles.checkIfFileExists(evaluationCatLogFile)) {
			// move the previous file...
			UtilFiles.copyFile(evaluationCatLogFile, evaluationCatLogFile + "-prev" + (new Date()).getTime());
		}
		
		
		File tempFile = new File(evaluationFullLogFileSupervised);
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		pw.println("TestName\tRunComp\tCurrentRunNum\tEvalDataName\t" +  AllLearningParameters.printNames("\t") + EvaluationResult.getTabFieldNamesWithoutCategoryResults() + "\tFeatNumSize");
		pw.close();
		
		tempFile = new File(evaluationFullLogFileSemiSupervised);
		pw = new PrintWriter(tempFile, "UTF-8");
		pw.println("TestName\tRunComp\tCurrentRunNum\tEvalDataName\t" +  AllSemiSupervisedLearningParameters.printNames("\t") + EvaluationResult.getTabFieldNamesWithoutCategoryResults() + "\tFeatNumSize");
		pw.close();
		
		tempFile = new File(evaluationFullLogFileGiven);
		pw = new PrintWriter(tempFile, "UTF-8");
		pw.println("TestName\tRunComp\tCurrentRunNum\tEvalDataName\t" +  AllParametersToEvaluateGiven.printNames("\t") + EvaluationResult.getTabFieldNamesWithoutCategoryResults() + "\tFeatNumSize");
		pw.close();
		
		tempFile = new File(evaluationCatLogFile);
		pw = new PrintWriter(tempFile, "UTF-8");
		pw.println("TestName\tRunComp\tCurrentRunNum\tEvalDataName\t" +CategoryEvaluationResult.getTabFieldNames());
		pw.close();
	}
	
	public static void getFieldNames()
	{
		List<String> fields = EvaluationResult.getDataBaseFieldNamesWithoutCategories();
		System.out.println(UtilCollections.join(fields, "\t"));
		
		List<String> fields2 = CategoryEvaluationResult.getDataBaseFieldNames();
		System.out.println(UtilCollections.join(fields2, "\t"));
	}
	
	
	public static void saveEvaluationResults(String testFileName, AllLearningParameters curParameters, EvaluationResult result) throws Exception
	{
		saveEvaluationResults(testFileName, curParameters, result, 0);
	}
	
	private static String getFilenameBasedOnParameters(AllLearningParameters curParameters)
	{
		if (curParameters instanceof AllSemiSupervisedLearningParameters)
			return evaluationFullLogFileSemiSupervised;
		else if (curParameters instanceof AllParametersToEvaluateGiven)
			return evaluationFullLogFileGiven;
		else
			return evaluationFullLogFileSupervised;
	}
	
	public static void saveEvaluationResults(String testFileName, AllLearningParameters curParameters, EvaluationResult result, int lexiconSize) throws Exception
	{
		PrintWriter pw = new PrintWriter(new FileWriter(getFilenameBasedOnParameters(curParameters), true));
		pw.println(currentTestName + "\t" + currentComputerName+"\t" +currentRunNum + "\t" + testFileName + "\t" +  (curParameters == null ? "null" : curParameters.printParametersShort()) + result.toTabString(false) + "\t" + lexiconSize + "\t");
		
		pw.close();
		
		pw = new PrintWriter(new FileWriter(evaluationCatLogFile, true));
		
		// print category, polarity, and quadrant results
		for (int i = 0; i < result.categoryResults.length; ++i)
		{
			pw.println(currentTestName + "\t" + currentComputerName+"\t" +currentRunNum + "\t" + testFileName +"\t"  + "\t" +result.categoryResults[i].toTabString());
		}
		
		for (int i = 0; i < result.polarityResults.length; ++i)
		{
			pw.println(currentTestName + "\t" + currentComputerName+"\t" +currentRunNum + "\t" + testFileName +"\t"  + "\t" +result.polarityResults[i].toTabString());
		}
			
		for (int i = 0; i < result.quadrantResults.length; ++i)
		{
			pw.println(currentTestName + "\t" + currentComputerName+"\t" +currentRunNum + "\t" + testFileName +"\t"  + "\t" +result.quadrantResults[i].toTabString());
		}
			
			
		pw.close();
		
		saveEvaluationResultsToDatabase(testFileName, curParameters, result, lexiconSize);
	}
	
	public static void saveAnnotationResults(Map<Integer, List<Integer>> labelData, String testFile) throws Exception
	{
		DataFilesFormatSaver.printLabeledDataIntoFile(filePatternForDetailedClassifierOutput + currentRunNum + "-" + extractMainPartFromFilename(testFile) + ".txt", labelData, getEmptyStringMap(labelData.keySet()));
	}
	
	private static String extractMainPartFromFilename(String filename)
	{
		int lastSeparator = filename.lastIndexOf('/'); // even if not found and -1 is return,  + 1 below will move it to 0
		int lastDot = filename.lastIndexOf('.');
		if (lastDot == -1)
			lastDot = filename.length();
		return filename.substring(lastSeparator + 1,lastDot); 
	}
	
	
	/** Saves the annotation for each tweet, its word number and feature number, as well as evaluation results for each **/
	public static void saveDetailedResults(Map<Integer,EvaluationResult> detailResData, String testFile, 
			Map<Integer, String> textData, Map<Integer, List<String>> usedFeaturesData, Map<Integer, List<Integer>> foundLabels,
			Map<Integer, List<Integer>> givenLabels) throws Exception
	{
		File tempFile = new File(filePatternForDetailedEvalResult + currentRunNum + "-" + extractMainPartFromFilename(testFile) + ".txt");
		PrintWriter pw = new PrintWriter(tempFile, "UTF-8");
		pw.println("twId\ttwLen\ttwFeatureNum\tAcc\tF1\tP\tR\tgivenLabels\tfoundLabels");
		for (Integer twId : detailResData.keySet())
		{
			StringBuilder sb = new StringBuilder();
			sb.append(twId);
			sb.append("\t");
			String twText = textData.get(twId);
			sb.append(UtilString.getWordNumber(twText));
			sb.append("\t");
			if (usedFeaturesData != null)
				sb.append(usedFeaturesData.get(twId).size());
			sb.append("\t");
			EvaluationResult twEval = detailResData.get(twId);
			sb.append(twEval.emotionAccuracy);
			sb.append("\t");
			sb.append(twEval.emotionF1scoreOnAll);
			sb.append("\t");
			sb.append(twEval.emotionPrecisionOnAllWithoutNeutral);
			sb.append("\t");
			sb.append(twEval.emotionRecallWithSet);
			sb.append("\t");
			sb.append(UtilCollections.join(givenLabels.get(twId), ","));
			sb.append("\t");
			sb.append(UtilCollections.join(foundLabels.get(twId), ","));
			sb.append("\t");	
			if (usedFeaturesData != null)
				sb.append(UtilCollections.join(usedFeaturesData.get(twId), ","));
			sb.append("\t");
			sb.append(twText);
			sb.append("\t");
			pw.println(sb.toString());
		}
		
		pw.close();
		
	}

	
	private static Map<Integer, String> getEmptyStringMap(Collection<Integer> ids)
	{
		Map<Integer, String> emptyRes = new HashMap<Integer, String>();
		for (Integer id : ids)
			emptyRes.put(id, "");
		return emptyRes;
	}
	
		
	public static void evaluateCurClassifierOverAllTestFiles(WeightedClassifier classifier,  AllLearningParameters learningParameters) throws Exception
	{
		for (String testFile : testFiles)
		{
			evaluateClassifierOnTest(testFile, classifier, learningParameters);
		}
	}
	
	static void evaluateClassifierOnTest(String testFile, WeightedClassifier resClassifier, AllLearningParameters learningParameters) throws Exception
	{
		EvaluationResult evalResult = ClassifierEvaluation.evaluateClassifierOnTestFile(resClassifier, testFile);
		System.out.println(evalResult.toString());
		
		ExperimentsResultsSaver.saveEvaluationResults(testFile, learningParameters, evalResult, resClassifier.getFeatureNumber());
	}
	
	public static void evaluateClassifierForIterationOverAllTestFiles(WeightedClassifier resClassifier, AllLearningParameters learningParameters, int iterNum) throws Exception
	{
		for (String testFile : testFiles)
		{
			evaluateClassifierForIteration(testFile, resClassifier, learningParameters, iterNum);
		}
	}
	
	static void evaluateClassifierForIteration(String testFile, WeightedClassifier resClassifier, AllLearningParameters learningParameters, int iterNum) throws Exception
	{
		EvaluationResult evalResult = ClassifierEvaluation.evaluateClassifierOnTestFile(resClassifier, testFile);
		System.out.println(evalResult.toString());
		currentRunIter = iterNum;
		ExperimentsResultsSaver.saveEvaluationResults(testFile, learningParameters, evalResult, resClassifier.getFeatureNumber());
		currentRunIter = 0;
	}
	
	public static void saveEvaluationResultsToDatabase(String testFile, AllLearningParameters learningParameters, EvaluationResult result, int lexiconSize) throws Exception
	{
		// save general results
		
		// get fieldNames for categories
		String generalFieldTabString = "TestName\tRunComp\tRunId\tTestFileName\t" + 
					((learningParameters instanceof AllSemiSupervisedLearningParameters)?
							AllSemiSupervisedLearningParameters.printNames("\t"):
								((learningParameters instanceof AllParametersToEvaluateGiven)?AllParametersToEvaluateGiven.printNames("\t"):
								AllLearningParameters.printNames("\t"))) + 
						UtilCollections.join(EvaluationResult.getDataBaseFieldNamesWithoutCategories(),"\t") + "\tIterNum\tlexSize";//;
		
		String curRunStr = Integer.toString(currentRunNum);// + ((currentRunIter > 0) ? currentRunIter : "");
		//String curIterAdd = ";i=" +((currentRunIter > 0) ? Integer.toString(currentRunIter) : "");
		
		StringBuilder sb = new StringBuilder();
		sb.append(currentTestName);
		sb.append("\t");
		sb.append(currentComputerName);
		sb.append("\t");
		sb.append(curRunStr);
		sb.append("\t");
		sb.append(testFile);
		sb.append("\t");
		sb.append(learningParameters.printValues("\t", false));
		sb.append(result.toTabString(false));
		sb.append(currentRunIter);
		sb.append("\t");
		sb.append(lexiconSize);		
		
		EvaluationSaving.saveEvaluationResultsToDatabase(
				sb.toString(), generalFieldTabString,
				"General");
		
		
		// save categories results
		
		// get fieldNames for categories
		String categoryFieldTabString = "TestName\tRunComp\tCategoryType\tRunId\tTestFileName\t" + "IterNum\t" +
				  UtilCollections.join(CategoryEvaluationResult.getDataBaseFieldNames(),"\t");
		
		sb = new StringBuilder();
		sb.append(currentTestName);
		sb.append("\t");
		sb.append(currentComputerName);
		sb.append("\t");
		sb.append("Category");
		sb.append("\t");
		sb.append(curRunStr);
		sb.append("\t");
		sb.append(testFile);
		sb.append("\t");
		sb.append(currentRunIter);
		sb.append("\t");
		String preDefiniteTabString = sb.toString();
		for (int i = 0; i < result.categoryResults.length; ++i)
		{
			String categoryTabString = preDefiniteTabString + result.categoryResults[i].toTabString();
			EvaluationSaving.saveEvaluationResultsToDatabase(categoryTabString, categoryFieldTabString, "Category");
		}
		
		categoryFieldTabString = "TestName\tRunComp\tCategoryType\tRunId\tTestFileName\t" + 
				"IterNum\t" + UtilCollections.join(CategoryEvaluationResult.getDataBaseFieldNames(),"\t");
		
		sb = new StringBuilder();
		sb.append(currentTestName);
		sb.append("\t");
		sb.append(currentComputerName);
		sb.append("\t");
		sb.append("Polarity");
		sb.append("\t");
		sb.append(curRunStr);
		sb.append("\t");
		sb.append(testFile);
		sb.append("\t");
		sb.append(currentRunIter);
		sb.append("\t");
		preDefiniteTabString = sb.toString();
		for (int i = 0; i < result.polarityResults.length; ++i)
		{
			String categoryTabString = preDefiniteTabString + result.polarityResults[i].toTabString();
			EvaluationSaving.saveEvaluationResultsToDatabase(categoryTabString, categoryFieldTabString, "Category");
		}
		
		sb = new StringBuilder();
		sb.append(currentTestName);
		sb.append("\t");
		sb.append(currentComputerName);
		sb.append("\t");
		sb.append("Quadrant");
		sb.append("\t");
		sb.append(curRunStr);
		sb.append("\t");
		sb.append(testFile);
		sb.append("\t");
		sb.append(currentRunIter);
		sb.append("\t");
		preDefiniteTabString = sb.toString();
		for (int i = 0; i < result.quadrantResults.length; ++i)
		{
			String categoryTabString = preDefiniteTabString + result.quadrantResults[i].toTabString();
			EvaluationSaving.saveEvaluationResultsToDatabase(categoryTabString, categoryFieldTabString, "Category");
		}
		
		

	}
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package classifiers.initialization;

import java.util.HashMap;
import java.util.List;

import functionality.UtilFiles;

public class GivenFeaturesIndex {

	static HashMap<String, String> givenFeaturesInFilesMap; // based on the name - stores in which file to find the features
	
	static {
		givenFeaturesInFilesMap = new HashMap<String, String>();
	}
	
	/**
	 * The file should contain the list of the features, one per line. 
	 * If the lines are tabulated, the first text until the first tab will be extracted as a feature in each line.
	 * @param featureListName
	 * @param fileWithFeatureEnumeration
	 */
	public static void addNewFeatureList(String featureListName, String fileWithFeatureEnumeration) {
		givenFeaturesInFilesMap.put(featureListName, fileWithFeatureEnumeration);
	}
	
	public static List<String> getGivenFeatureList(String featureListName) throws Exception {
		if (!givenFeaturesInFilesMap.containsKey(featureListName))
			throw new Exception("The requested feature list " + featureListName + " is not provided.");
		String filename = givenFeaturesInFilesMap.get(featureListName);
		if (filename == null)
			throw new Exception("The file for the requested feature list " + featureListName + " is not set.");
		return UtilFiles.getFirstParameterLines(filename, 0);
	}
}

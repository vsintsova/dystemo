/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experiments.helpers;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import utility.Pair;
import experimentsavers.EvaluationSaving;
import functionality.UtilCollections;
import functionality.UtilString;

public class BestResultsParamsLoader {
	
	public enum SearchSettings {F1Macro, P, C, A, CHalf, F1Micro, Geom_2F1_A};
	
	static class LogCategoryResult
	{
		int runId;
		String params;
		double precision;
		double recall;
		double f1Score;
	}
	
	public static class LogResult
	{
		public int runId;
		public Map<String,String> params; // description of parameters by name
 		double macroPrecision;
		double macroRecall;
		double macroF1Score;
		double accuracy;
		double microPrecision;
		double microRecall;
		double microF1Score;
		
		public String printValuesInTabString()
		{
			return macroPrecision + "\t" + macroRecall + "\t" + macroF1Score + "\t" + accuracy + "\t" + microPrecision + "\t" + microRecall + "\t" + microF1Score + "\t";
		}
		
		public static String printNamesInTabString()
		{
			return "macroPrecision\tmacroRecall\tmacroF1Score\taccuracy\tmicroPrecision\tmicroRecall\tmicroF1Score\t";
		}
	}
	
	/**
	 * Get the best results for learned classifier.
	 * @param testNames Names of experiments in the database
	 * @param learnerName
	 * @param searchSettings
	 * @param extraCondition
	 * @return
	 * @throws Exception
	 */
	public static LogResult getBestParamsForAlg(String[] testNames, String learnerName, SearchSettings searchSettings, String extraCondition) throws Exception
	{
		return getBestParamsForAlgInGeneral(testNames, learnerName, null, searchSettings, extraCondition);
	}
	
	/**
	 * Get the best results for learned classifier in semi-supervised process.
	 * @param testNames Names of experiments in the database
	 * @param learnerName
	 * @param searchSettings
	 * @param extraCondition
	 * @return
	 * @throws Exception
	 */
	public static LogResult getBestParamsForSSLAlg(String[] testNames, String learnerName, String initialClassifier, SearchSettings searchSettings, String extraCondition) throws Exception
	{
		return getBestParamsForAlgInGeneral(testNames, learnerName, initialClassifier, searchSettings, extraCondition);
	}
	
	
	public static LogResult getBestParamsForAlgInGeneral(String[] testNames, String learnerName, String initialClassifier, SearchSettings searchSettings, String extraCondition) throws Exception
	{
		String queryBeg = "select * from " +
				" (select RunId, emotionMacroF1Score as macroF1Score, "
		+ "emotionMacroPrecision as `macroPrecision`, emotionMacroRecall as macroRecall, emotionAccuracy as Accuracy " +
				", emotionMicrof1Score as `microF1Score`, emotionMicroPrecision as `microPrecision`, emotionMicroRecall as `microRecall` " + 
				", geomGenProd " +
				
				// parameters of run
				", trainDataName, featureExtractionParams, featureSelectionParams, LearnerName, ClassifierLearnerParams, sslInitialClassifier, sslFullParams, sslUnlabeledDataName, TestFileName  " +
				" from " + EvaluationSaving.evaluationResultTablename + 
				" where TestName in ('" +UtilCollections.join(testNames, "','") +"') and LearnerName = ? " +
				 ((initialClassifier != null) ? " and sslInitialClassifier = ?": "") +
						") q ";
		
		String limitQ = " limit 1";
		
		String middleLine = "";
		switch (searchSettings)
		{
			case F1Macro : middleLine = "order by  macroF1Score desc "; break;
			case P : middleLine =  "order by `macroPrecision` desc "; break;
			case C: middleLine = "where `macroPrecision` >= `macroRecall` order by  macroF1Score desc "; break;
			case A: middleLine = "order by  Accuracy desc "; break;
			case CHalf: middleLine = "where `macroPrecision` >= 0.5 * `macroRecall` order by  macroF1Score desc "; break;
			case F1Micro:  middleLine = "order by  microF1Score desc "; break;
			case Geom_2F1_A: middleLine = "order by geomGenProd desc "; break;
		}
		if ( extraCondition != null)
		{
			
			if (searchSettings == SearchSettings.C || searchSettings == SearchSettings.CHalf)
			{
				Pair<String,String> middlePair = UtilString.parseInPair(middleLine, "order");
				middleLine =  middlePair.first + " and " + extraCondition + " order " + middlePair.second;
			}
			else 
				middleLine = " where " + extraCondition + middleLine;
		}
		
		String finalQuery = queryBeg + middleLine + limitQ;
		
		
		Connection conn = EvaluationSaving.GetDatabaseConnectionForEvaluationSaving(); 
		
	    PreparedStatement prep = conn.prepareStatement(finalQuery);
	   
	   
	    prep.setString(1, learnerName);
	   // System.out.println(UtilString.parseInPair(prep.toString(),":").second + ";");
		if (initialClassifier != null)
			prep.setString(2, initialClassifier);
	    
	   
	    ResultSet rs = prep.executeQuery();
	    
	    LogResult res = null;
	    
	    while (rs.next()) 
	    {
	    	res = new LogResult();
	    	res.macroF1Score = rs.getDouble("macroF1Score");
	    	res.macroPrecision = rs.getDouble("macroPrecision");
	    	res.macroRecall = rs.getDouble("macroRecall");
	    	res.accuracy = rs.getDouble("Accuracy");
	    	res.microF1Score = rs.getDouble("microF1Score");
	    	res.microPrecision = rs.getDouble("microPrecision");
	    	res.microRecall = rs.getDouble("microRecall");
	    
	    	
	    	res.params = new LinkedHashMap<String,String>();
	    	String[] paramsNames = new String[]{"trainDataName", "featureExtractionParams", "featureSelectionParams", "LearnerName", "ClassifierLearnerParams", "sslInitialClassifier", "sslFullParams", "sslUnlabeledDataName" };
	    	for (String param : paramsNames)
	    		res.params.put(param, rs.getString(param));
	    	res.runId = rs.getInt("RunId");
	    	
	    }
	    rs.close();
	    conn.close();
	    return res;
	}
	
	public static LogResult getBestParamsForAlg(String[] testNames, String learnerName, SearchSettings searchSettings) throws Exception
	{
		return getBestParamsForAlg(testNames, learnerName, searchSettings, null);
	}
	
	
	/**
	 * Current sql-query condition for selecting the evaluation results from the database.
	 */
	public static String curCondition = null;
	public static SearchSettings[] settingsToReturn = new SearchSettings[]{SearchSettings.F1Macro, SearchSettings.A, SearchSettings.F1Micro};
	
	public static List<LogResult> findBestLearningResultsInOrder (String outputParamsFile, String[] testNames, String learnerName) throws Exception
	{
		return findSSLInOrder(outputParamsFile, testNames, learnerName, null);
	}
	
	public static LogResult findBestAlgorithmResult(String[] testNames, String learnerName, String initialClassifier, SearchSettings searchSettings) throws Exception
	{
		return getBestParamsForSSLAlg(testNames, learnerName, initialClassifier, searchSettings, curCondition);
	}
	
	/**
	 * This function will find the best parameters by searching the results in the database and optimizing according to the preset criteria.
	 * The found parameters will be also appended to the provided file.
	 * This function uses curCondition to set up the additional restrictions on the searched evaluation results, such as test data.
	 * @param fileBestRecord
	 * @param testNames The names of the saved experiments.
	 * @param learnerName
	 * @param initialClassifier
	 * @return
	 * @throws Exception
	 */
	public static List<LogResult> findSSLInOrder (String fileBestRecord, String[] testNames, String learnerName, String initialClassifier) throws Exception
	{
		List<LogResult> foundResults = new ArrayList<LogResult>();
		PrintWriter pw = new PrintWriter(new FileWriter(fileBestRecord, true));
		for (SearchSettings sSettings : settingsToReturn)
		{
			LogResult algRes = getBestParamsForSSLAlg(testNames, learnerName, initialClassifier, sSettings, curCondition);
			
			if (algRes != null)
				pw.println( learnerName + "\t" + sSettings + "\t" + algRes.runId + "\t" + UtilCollections.joinMap(algRes.params, "\t", "!") +
					"\t" + algRes.macroPrecision + "\t" + algRes.macroRecall + "\t" + algRes.macroF1Score + "\t" + algRes.accuracy + "\t" + algRes.microPrecision + "\t" + algRes.microRecall + "\t" + algRes.microF1Score + "\t" + curCondition);
			foundResults.add(algRes);
		}
			
		pw.close();
		return foundResults;
	}
	
	public static List<LogResult> findSSLInOrderWithoutSaving (String[] testNames, String learnerName, String initialClassifier) throws Exception
	{
		return findSSLInOrderWithoutSaving(testNames, learnerName, initialClassifier, settingsToReturn);
	}
	
	public static List<LogResult> findSSLInOrderWithoutSaving (String[] testNames, String learnerName, String initialClassifier, SearchSettings[] settingsToRun) throws Exception
	{
		List<LogResult> foundResults = new ArrayList<LogResult>();
		for (SearchSettings sSettings : settingsToRun)
		{
			LogResult algRes = getBestParamsForSSLAlg(testNames, learnerName, initialClassifier, sSettings, curCondition);
			
			foundResults.add(algRes);
		}
		return foundResults;
	}

	
}

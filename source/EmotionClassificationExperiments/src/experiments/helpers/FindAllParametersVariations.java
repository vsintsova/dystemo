/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package experiments.helpers;

import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


import learners.given.ClassifierLearnerFactory.LearnerName;
import learning.parameters.FeatureSelectionParameters.FeatureSelectionAlgOption;
import learning.parameters.IndependentLearnerParameters;
import learning.parameters.LearningParametersParser;

import semisupervisedlearning.parameters.AllSemiSupervisedLearningParameters;
import experiments.helpers.BestResultsParamsLoader.LogResult;
import experimentsavers.EvaluationSaving;
import functionality.UtilCollections;
import functionality.UtilString;

public class FindAllParametersVariations {

	static String curCondition = null; // used testFileName is not needed here as it is specified in the query
	
	static class ParameterSelectionOption
	{
		public double polaritySelectionThreshold = 0.0;
		public double emotionalitySelectionThreshold = -1.0;
		public int minOccNum = -1;
		public int ngram = 0;
		public double outputAlphaCut = 0.0;
		public double inputAlphaCut = 0.0;
		public double binaryProbThreshold = -1.0;
		public double binaryFeatureSelectionThreshold = -1.0;
		public int weightType = -1;
		
		public String printInTabString() throws IllegalArgumentException, IllegalAccessException
		{
			StringBuilder sb = new StringBuilder();
			
			Field[] fields = ParameterSelectionOption.class.getFields();
			for (Field field : fields)
			{
					Object thisValue = field.get(this);
					sb.append(thisValue.toString() + "\t");
			}
			return sb.toString();
		}
		
		public static String getTabFieldNames()
		{
			StringBuilder sb = new StringBuilder();
			
			Field[] fields = ParameterSelectionOption.class.getFields(); 
			for (Field field : fields)
			{
				sb.append(field.getName() + "\t");
			}
			return sb.toString();
		}
	}
	
	public static void printAllResultsWithParametersVariations(String[] usedTestNames, String usedTestFileName, String outputFilename) throws Exception
	{
		List<LogResult> allResults  = getAllRunsResults(usedTestNames, usedTestFileName, curCondition);
		PrintWriter pw = new PrintWriter(outputFilename);
		AllSemiSupervisedLearningParameters fullLearningParams;
		
		ParameterSelectionOption varOption;
		IndependentLearnerParameters indParams ;
		
		
		pw.println("id\trunComp\tLearnerName\tInitClassifier\t" + LogResult.printNamesInTabString() + ParameterSelectionOption.getTabFieldNames());
		
		for (LogResult res : allResults)
		{
			fullLearningParams = (AllSemiSupervisedLearningParameters)LearningParametersParser.parseAndLoadAllLearningParameters(res.params);
			varOption = new ParameterSelectionOption();
			varOption.polaritySelectionThreshold = (fullLearningParams.featureSelectionParams.polaritySelectionParams == null || fullLearningParams.featureSelectionParams.polaritySelectionParams.selectionAlg == FeatureSelectionAlgOption.No)?0.0:fullLearningParams.featureSelectionParams.polaritySelectionParams.scoreThreshold;
			varOption.emotionalitySelectionThreshold = (fullLearningParams.featureSelectionParams.emotionalitySelectionParams== null  || fullLearningParams.featureSelectionParams.emotionalitySelectionParams.selectionAlg == FeatureSelectionAlgOption.No)?-1.0:fullLearningParams.featureSelectionParams.emotionalitySelectionParams.scoreThreshold;
			varOption.minOccNum = fullLearningParams.featureExtractionParams.dataNgramExtractionParams.minTermOccurThreshold;
			varOption.ngram = fullLearningParams.featureExtractionParams.dataNgramExtractionParams.maxN;
			varOption.outputAlphaCut = (fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams == null)?0.0:fullLearningParams.learnerParams.outputClassifierApplicationParameters.multiLabelOutputRefinementParams.alphaCut;
			varOption.inputAlphaCut = (fullLearningParams.learnerParams.multiLabelInitialRefinementParams == null)?0.0:fullLearningParams.learnerParams.multiLabelInitialRefinementParams .alphaCut;
			varOption.binaryProbThreshold = (fullLearningParams.learnerParams.outputClassifierApplicationParameters.probabilisticBinaryOutputTreatmentParams == null)?0.0:fullLearningParams.learnerParams.outputClassifierApplicationParameters.probabilisticBinaryOutputTreatmentParams.probabilityThreshold;
			if (fullLearningParams.learnerParams instanceof IndependentLearnerParameters)
			{
				indParams = (IndependentLearnerParameters)fullLearningParams.learnerParams;
				varOption.binaryFeatureSelectionThreshold = 
						(indParams.binaryLearnerParameters != null && 
						indParams.binaryLearnerParameters.binaryFeatureSelectionParams != null && 
						indParams.binaryLearnerParameters.binaryFeatureSelectionParams.selectionAlg != FeatureSelectionAlgOption.No)? indParams.binaryLearnerParameters.binaryFeatureSelectionParams.scoreThreshold: 0.0;
				if (fullLearningParams.learnerName == LearnerName.IndependentPMI)
				{
					if (indParams.binaryLearnerParameters.binaryLexiconParams.contains("-T"))
					{
						double binThreshold = Double.parseDouble(UtilString.getPatternEntry("-T(\\d.\\d)(?=-|$|,)", indParams.binaryLearnerParameters.binaryLexiconParams));
						varOption.binaryFeatureSelectionThreshold = binThreshold;
					}
				}
			}
			if (fullLearningParams.learnerParams.doRebalance())
				varOption.weightType = fullLearningParams.learnerParams.rebalancingParameters.weightType.ordinal();
			
			pw.print(res.runId + "\t");
			pw.print(res.params.get("RunComp") + "\t");
			pw.print(res.params.get("LearnerName") + "\t");
			pw.print(res.params.get("sslInitialClassifier") + "\t");
			pw.print(res.printValuesInTabString());
			pw.print(varOption.printInTabString());
			pw.println();
		}
		
		pw.close();
	}
	
	
	public static List<LogResult> getAllRunsResults(String[] usedTestNames, String usedTestFileName, String extraCondition) throws Exception
	{
		String query = "select RunComp, RunId, emotionMacroF1Score as macroF1Score, "
		+ "emotionMacroPrecision as `macroPrecision`, emotionMacroRecall as macroRecall, emotionAccuracy as Accuracy " +
				", emotionMicroF1Score as `microF1Score`, emotionMicroPrecision as `microPrecision`, emotionMicroRecall as `microRecall` " + 
				
				// parameters of run
				", trainDataName, featureExtractionParams, featureSelectionParams, LearnerName, ClassifierLearnerParams, sslInitialClassifier, sslFullParams, sslUnlabeledDataName, TestFileName  " +
				" from " + EvaluationSaving.evaluationResultTablename +" " + 
				" where TestName in ('" +UtilCollections.join(usedTestNames, "','") +"') and TestFileName = ? ";
		
		if ( extraCondition != null)
		{
			query += " and (" + extraCondition + ")";
		}
			
		Connection conn = EvaluationSaving.GetDatabaseConnectionForEvaluationSaving(); 
		
	    PreparedStatement prep = conn.prepareStatement(query);
	   
	   
	    prep.setString(1, usedTestFileName);
	   
	    ResultSet rs = prep.executeQuery();
	    
	    
	    List<LogResult> resultList = new ArrayList<LogResult>();
	    
	    LogResult res = null;
	    
	    while (rs.next()) 
	    {
	    	res = new LogResult();
	    	res.macroF1Score = rs.getDouble("macroF1Score");
	    	res.macroPrecision = rs.getDouble("macroPrecision");
	    	res.macroRecall = rs.getDouble("macroRecall");
	    	res.accuracy = rs.getDouble("Accuracy");
	    	res.microF1Score = rs.getDouble("microF1Score");
	    	res.microPrecision = rs.getDouble("microPrecision");
	    	res.microRecall = rs.getDouble("microRecall");
	    
	    	
	    	res.params = new LinkedHashMap<String,String>();
	    	String[] paramsNames = new String[]{"trainDataName", "featureExtractionParams", "featureSelectionParams", "LearnerName", "ClassifierLearnerParams", "sslInitialClassifier", "sslFullParams", "sslUnlabeledDataName", "RunComp"};
	    	for (String param : paramsNames)
	    		res.params.put(param, rs.getString(param));
	    	res.runId = rs.getInt("RunId");
	    	resultList.add(res);
	    	
	    }
	    rs.close();
	    conn.close();
	    return resultList;
	}
}

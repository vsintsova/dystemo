/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.evaluation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import utils.UtilCategoryLabelFormatter;

import learners.definitions.ClassifierLearner;
import classification.definitions.WeightedClassifier;
import classification.evaluation.ComplexEvaluation;
import classification.evaluation.ComplexEvaluation.EvaluationResult;
import classification.evaluation.EvaluationUtil;
import data.categories.CategoryProcessor;
import data.documents.Tweet;

/** The class for cross-validation the classifier over the given data. */
public class CrossValidatingClassifierLearner {
	
	/** Before launching this evaluation the semi-supervised learner should be already initialized. 
	 */
	public static EvaluationResult crossValidateClassifier (List<Tweet> fullTweetData,
						Map<Integer, List<Integer>> categoryAssignment, int nFolds, 
						ClassifierLearner classifierLearner) throws Exception
	{
		// Get the folds
		Vector<List<Integer>> foldIdsVector = EvaluationUtil.separateIndexesRandomlyForCrossValidation(fullTweetData.size(), nFolds);
				
		EvaluationResult sumResult = new EvaluationResult(classifierLearner.getEmotionCategoriesType());
		classifierLearner.initialize(fullTweetData);
		
		
		// get labels data in weighted format
		Map<Integer, double[]> annotatedTweetEmotions = UtilCategoryLabelFormatter.transformStrictAnnotationsIntoWeighted(
				CategoryProcessor.getCategoriesData(classifierLearner.getEmotionCategoriesType()), categoryAssignment);
		
		// For each fold
		for (int i = 0; i < nFolds; i++)
		{
			List<Tweet> trainData = EvaluationUtil.getTrainDataForFold(fullTweetData, foldIdsVector, i);
			List<Tweet> testData = EvaluationUtil.getTestDataForFold(fullTweetData, foldIdsVector, i);
				
			
			Map<Integer, double[]> trainEmotionData = new HashMap<Integer, double[]>();
			for (Tweet tweet : trainData)
			{
				trainEmotionData.put(tweet.tweetId, annotatedTweetEmotions.get(tweet.tweetId));
			}
			
			WeightedClassifier foldClassifier = classifierLearner.learnClassifier(trainEmotionData);
			
			foldClassifier.defaultPreprocessing = false;
			EvaluationResult foldResult = ComplexEvaluation.getEvaluationSumsOnTweets(categoryAssignment, testData, foldClassifier); 
			sumResult.plus(foldResult);
		}
				
		sumResult.normalize();
				
		return sumResult;
	}
}

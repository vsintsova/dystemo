/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.evaluation;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import linguistic.TermDetector;
import linguistic.NegationDetector.NegationTreatmentParams;
import linguistic.NegationDetector.NegationTreatmentType;
import utility.Pair;
import classification.definitions.WeightedClassifier;
import classification.detectors.TermDetectorWithNegations;
import classification.detectors.TermDetectorWithProcessing;
import classification.evaluation.ComplexEvaluation;
import classification.evaluation.ComplexEvaluation.EvaluationResult;
import data.DataRepositoryIndex;
import data.LabeledData;
import data.documents.Tweet;
import data.loading.DataFilesFormatReader;
import experimentsavers.ExperimentsResultsSaver;
import functionality.UtilArrays;
import functionality.UtilCollections;
import functionality.UtilFiles;

public class ClassifierEvaluation {
	
	public static EvaluationResult evaluateClassifier (WeightedClassifier classifier, List<Tweet> textData, Map<Integer, List<Integer>> emotionLabels) throws Exception
	{
		EvaluationResult evalResult = ComplexEvaluation.getEvaluationSumsOnTweets(emotionLabels, textData, classifier); 
		evalResult.normalize();
		return evalResult;
	}
	
	public static EvaluationResult evaluateClassifier (WeightedClassifier classifier, Map<Integer, String> textData, Map<Integer, List<Integer>> emotionLabels) throws Exception
	{
		EvaluationResult evalResult = ComplexEvaluation.getEvaluationSums(emotionLabels, textData, classifier); 
		evalResult.normalize();
		return evalResult;
	}
	
	public static EvaluationResult evaluateClassifierOnTestData (WeightedClassifier classifier, String dataNameInIndex) throws Exception
	{
		LabeledData labelData = DataRepositoryIndex.getLabeledDataByName(dataNameInIndex);
		Map<Integer, String> textData = labelData.textData;
		Map<Integer, List<Integer>> emotionLabels = labelData.emotionLabels;
	
		EvaluationResult evalResult = ComplexEvaluation.getEvaluationSums(emotionLabels, textData, classifier); 
		evalResult.normalize();
		return evalResult;
	}
	
	
	public static EvaluationResult evaluateClassifierOnTestFile (WeightedClassifier classifier, String testFile) throws Exception
	{
		if (checkForFeatureLoad(testFile))
			return evaluateClassifierWithFeatures(classifier, testFile);
		
		// read test data:
		Map<Integer, List<Integer>> emotionLabels = new HashMap<Integer, List<Integer>>();
		Map<Integer, String> textData = new HashMap<Integer, String> ();
		DataFilesFormatReader.readLabeledDataFromFile(testFile, emotionLabels, textData);
			
		boolean doBinaryTesting = checkForBinaryTesting(emotionLabels.values());
		
		
		EvaluationResult evalResult = null;
		if (!doBinaryTesting)
			evalResult = ComplexEvaluation.getEvaluationSums(emotionLabels, textData, classifier); 
		else
		{
			List<Pair<Integer, Integer>> emotionBinaryLabels = new ArrayList<Pair<Integer,Integer>>();
			DataFilesFormatReader.readBinaryDataFromFile(testFile, emotionBinaryLabels, textData);
			evalResult = ComplexEvaluation.getEvaluationSumsInBinaryMode(emotionBinaryLabels, textData, classifier); 
		}
		evalResult.normalize();
		return evalResult;
	}
	
	public static EvaluationResult evaluateClassifierWithFeatures (WeightedClassifier classifier, String testFile) throws Exception
	{
		// read test data:
		Map<Integer, List<Integer>> emotionLabels = new HashMap<Integer, List<Integer>>();
		Map<Integer, Tweet> tweetData = new HashMap<Integer, Tweet> ();
		DataFilesFormatReader.readDataFromFileWithFeatures(testFile, emotionLabels, tweetData);
			
		boolean doBinaryTesting = checkForBinaryTesting(emotionLabels.values());
		
		
		EvaluationResult evalResult = null;
		if (!doBinaryTesting)
			evalResult = ComplexEvaluation.getEvaluationSumsOnTweets(emotionLabels, tweetData, classifier); 
		else
		{
			List<Pair<Integer, Integer>> emotionBinaryLabels = new ArrayList<Pair<Integer,Integer>>();
			DataFilesFormatReader.readBinaryDataFromFileWithFeatures(testFile, emotionBinaryLabels, tweetData);
			evalResult = ComplexEvaluation.getEvaluationSumsInBinaryModeOnTweets(emotionBinaryLabels, tweetData, classifier); 
		}
		evalResult.normalize();
		return evalResult;
	}
	
	
	/** 
	 * Check if binary mode testing is required (detected by presence of negative category labels)
	 * @param emotionLabels
	 * @return
	 */
	private static boolean checkForBinaryTesting(Collection<List<Integer>> emotionLabels)
	{
		for (List<Integer> labels : emotionLabels)
		{
			for (Integer label : labels)
				if (label < 0)
					return true;
		}
		return false;
	}
	
	private static boolean checkForFeatureLoad(String fileName)
	{
		List<String> dataLines = UtilFiles.getContentLines(fileName);
		String[] parsedLine = dataLines.get(0).split("\t");
		return (parsedLine.length > 3);
	}
	
	public static Map<Integer, List<Integer>> constructClassifierOutput (
			WeightedClassifier classifier, Map<Integer, String> textData) throws Exception
	{
		Map<Integer, List<Integer>> emotionLabels = new HashMap<Integer, List<Integer>>();
		
		for (Map.Entry<Integer, String> oneText : textData.entrySet())
		{
			double[] weights = classifier.findCategoryWeights(oneText.getValue());
			List<Integer> dominantCatsIds = classifier.getDominantCategories(weights);		
			emotionLabels.put(oneText.getKey(), dominantCatsIds);
		}
		return emotionLabels;
	}
	
	
	public static Map<Integer, List<Integer>> constructClassifierOutputFromTestData (WeightedClassifier classifier, String testDataName) throws Exception
	{
		// read test data:
		LabeledData testData = DataRepositoryIndex.getLabeledDataByName(testDataName);
		return constructClassifierOutput(classifier, testData.textData); 
	}
	
	public static Map<Integer, List<Integer>> constructClassifierOutputFromTestFile (WeightedClassifier classifier, String testFile) throws Exception
	{
		// read test data:
		Map<Integer, List<Integer>> emotionLabels = new HashMap<Integer, List<Integer>>();
		Map<Integer, String> textData = new HashMap<Integer, String> ();
		DataFilesFormatReader.readLabeledDataFromFile(testFile, emotionLabels, textData);
		
		return constructClassifierOutput(classifier, textData);
	}
	
	/**
	 * Note: The emotionLabels and textData should be alread set!
	 * @param classifier
	 * @param textData
	 * @param emotionLabels
	 * @return
	 * @throws Exception
	 */
	public static Map<Integer,EvaluationResult> constructPerTweetResultsFromTestData(WeightedClassifier classifier, Map<Integer, String> textData, Map<Integer, List<Integer>> emotionLabels) throws Exception
	{		
		Map<Integer,EvaluationResult> evalRes = new HashMap<Integer,EvaluationResult>();
		
		for (Integer tweetId : textData.keySet())
		{
			EvaluationResult twdResult = ComplexEvaluation.getEvaluation(textData.get(tweetId), classifier, emotionLabels.get(tweetId), null, classifier.getCategoriesData().getCategoriesType());
			twdResult.normalize();
			evalRes.put(tweetId, twdResult);
		}
		return evalRes;
	}
	
	public static Map<Integer,EvaluationResult> constructPerTweetResultsFromTestFile(WeightedClassifier classifier, String testFile, Map<Integer, String> textData, Map<Integer, List<Integer>> emotionLabels) throws Exception
	{
		// read test data:
		DataFilesFormatReader.readLabeledDataFromFile(testFile, emotionLabels, textData);
			
		Map<Integer,EvaluationResult> evalRes = new HashMap<Integer,EvaluationResult>();
		
		for (Integer tweetId : textData.keySet())
		{
			EvaluationResult twdResult = ComplexEvaluation.getEvaluation(textData.get(tweetId), classifier, emotionLabels.get(tweetId), null, classifier.getCategoriesData().getCategoriesType());
			twdResult.normalize();
			evalRes.put(tweetId, twdResult);
		}
		return evalRes;
	}
	
	public static Map<Integer, List<String>> constructClassifierUsedFeatures (WeightedClassifier classifier, Map<Integer, String> textData) throws Exception
	{
		List<String> features = classifier.getFeatureNames();
		TermDetector td = new TermDetector(features); 
		
		// next lines were added later
		td = new TermDetectorWithProcessing(td);
		td = new TermDetectorWithNegations((TermDetectorWithProcessing)td, new NegationTreatmentParams(NegationTreatmentType.ToReplace, 2, true));
		td.ignoreHahstagSymbols = true;
		
		Map<Integer, List<String>> tweetUsedFeatures = new HashMap<Integer, List<String>>();
		
		for (Map.Entry<Integer, String> oneText : textData.entrySet())
		{
			List<String> oneFeatList = new ArrayList<String>(((TermDetectorWithProcessing)td).findEmotionalTermsInText(oneText.getValue()).keySet());
			tweetUsedFeatures.put(oneText.getKey(), oneFeatList);
		}
		return tweetUsedFeatures;
	}
	
	public static void printOutDetailedPerTweetResults(WeightedClassifier classifier, String testFile, String filename) throws Exception
	{
		Map<Integer, List<Integer>> emotionLabels = new HashMap<Integer, List<Integer>>();
		Map<Integer, String> textData = new HashMap<Integer, String> ();
		DataFilesFormatReader.readLabeledDataFromFile(testFile, emotionLabels, textData);
		
		printOutDetailedPerTweetResults(classifier, textData, emotionLabels, filename);
	}
	
	public static void printOutDetailedPerTweetResults(WeightedClassifier classifier, Map<Integer, String> textData, Map<Integer, List<Integer>> emotionLabels, String filename) throws Exception
	{
		PrintWriter pw = new PrintWriter(filename);
		
		for (Map.Entry<Integer, String> textEntry : textData.entrySet())
		{
			double[] weights = classifier.findCategoryWeights(textEntry.getValue());
			pw.println(textEntry.getKey() + "\t" +  textEntry.getValue() + "\t" + UtilCollections.join(emotionLabels.get(textEntry.getKey()), ",") + "\t" + UtilArrays.join(weights, "\t"));
		}
		
		pw.close();
	}
	
	/**
	 * Prints out 2 files with evaluation of the current classifier at per object level (the patterns of the file names are given in ExperimentsResultsSavers: 
	 * one file enumerated the exact labels outputted by the classifier, 
	 * another file (saved if toSaveEvaluationResults is true) specifies some information from the classifier for each object (e.g. detected features) and whether the each answer is correct or not (or partially)
	 * The second file will also save the information on the detected features, if toSaveDetectedFeatures is true.
	 * @param resClassifier
	 * @param testFile
	 * @throws Exception
	 */
	public static void computeAndSaveDetailedEvaluationOnTestFile(WeightedClassifier resClassifier, String testFile, boolean toSaveEvaluationResults, boolean toSaveDetectedFeatures) throws Exception
	{
			Map<Integer, List<Integer>> foundLabels = constructClassifierOutputFromTestFile(resClassifier, testFile);
			ExperimentsResultsSaver.saveAnnotationResults(foundLabels, testFile);
			
			if (toSaveEvaluationResults) {
				Map<Integer, String> textData = new HashMap<Integer, String> ();
				Map<Integer, List<Integer>> givenLabels = new HashMap<Integer, List<Integer>> ();
				Map<Integer,EvaluationResult> perTwResults = constructPerTweetResultsFromTestFile(resClassifier, testFile, textData, givenLabels);
				Map<Integer, List<String>> featData = null;
				if (toSaveDetectedFeatures)
					featData = constructClassifierUsedFeatures(resClassifier, textData);
				ExperimentsResultsSaver.saveDetailedResults(perTwResults, testFile, textData, featData, foundLabels, givenLabels);
			}
	}
	
	public static void computeAndSaveDetailedEvaluationOnTestData(WeightedClassifier resClassifier, String testDataName, boolean toSaveEvaluationResults, boolean toSaveDetectedFeatures) throws Exception
	{
			Map<Integer, List<Integer>> foundLabels = constructClassifierOutputFromTestData(resClassifier, testDataName);
			ExperimentsResultsSaver.saveAnnotationResults(foundLabels, testDataName);
			
			if (toSaveEvaluationResults) {
				// read test data:
				LabeledData testData = DataRepositoryIndex.getLabeledDataByName(testDataName);
				Map<Integer, List<Integer>> givenLabels = testData.emotionLabels;
				Map<Integer, String> textData = testData.textData;
				Map<Integer,EvaluationResult> perTwResults = constructPerTweetResultsFromTestData(resClassifier, textData, givenLabels);
				
				Map<Integer, List<String>> featData = null;
				if (toSaveDetectedFeatures)
					featData = constructClassifierUsedFeatures(resClassifier, textData);
				ExperimentsResultsSaver.saveDetailedResults(perTwResults, testDataName, textData, featData, foundLabels, givenLabels);
			}
	}
}

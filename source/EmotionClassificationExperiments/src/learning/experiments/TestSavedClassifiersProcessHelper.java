/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.experiments;

import parameters.EmotionRecognitionParameters;
import learning.evaluation.ClassifierEvaluation;
import learning.parameters.AllLearningParameters;
import learning.parameters.AllParametersToEvaluateGiven;
import learning.parameters.ClassifierApplicationParameters;
import classification.definitions.WeightedClassifier;
import classification.evaluation.ComplexEvaluation.EvaluationResult;
import classifiers.initialization.UsedClassifierFactory;
import experimentsavers.ExperimentsResultsSaver;

/**
 * This class allows to reload the saved classifiers and test their performance.
 * Particularly, it allows to get the detailed output results (the classification of each tweet), 
 * when ExperimentsResultsSaver.toSaveDetailedClassifierResults is set to true.
 * 
 */
public class TestSavedClassifiersProcessHelper {
	
	
	/**
	 * The filenamePattern for the saved classifiers is supposed to contain <int> pattern as a replacement for the run id of the classifier to load.
	 * @param testDataName
	 * @param classifierType Can be 'Weka' or 'Lexicon'
	 * @throws Exception
	 */
	public static void runAllSavedClassifiersOverTest(
			String testDataName, int[] classifierRunsIds, String classifierFilenamePattern, 
			String classifierType, boolean toPreserveRunNumber) throws Exception 
	{		
		for (int runId : classifierRunsIds)
		{
			String classifierName = Integer.toString(runId);
			String classifierFile = classifierFilenamePattern.replace("<int>", classifierName);
			
			runSavedClassifierOverTest(new String[]{testDataName}, classifierName, classifierFile, classifierType, toPreserveRunNumber);
		}
	}
	
	/**
	 * The filenamePattern for the saved classifiers is supposed to contain <int> pattern as a replacement for the run id of the classifier to load.
	 * @param testDataName
	 * @throws Exception
	 */
	public static void runAllSavedClassifiersOverTest(
			String[] testDataNames, int[] classifierRunsIds, String classifierFilenamePattern, 
			String classifierType, boolean toPreserveRunNumber) throws Exception 
	{		
		for (int runId : classifierRunsIds)
		{
			String classifierName = Integer.toString(runId);
			String classifierFile = classifierFilenamePattern.replace("<int>", classifierName);
			
			runSavedClassifierOverTest(testDataNames, classifierName, classifierFile, classifierType, toPreserveRunNumber);
		}
	}
	
	
	public static void runSavedClassifierOverTest(String[] testDataNames, 
			String classifierName, String classifierFile, String classifierType,
			boolean toPreserveRunNumber) throws Exception
	{
		runSavedClassifierOverTest(testDataNames, 
				classifierName, classifierFile, classifierType, 
				ClassifierApplicationParameters.getDefaultClassifierApplicationParams(), toPreserveRunNumber);
	}
	
	/**
	 * This reloads the saved classifier and tests its performance.
	 * @param testDataNames The list of test data for evaluation
	 * @param classifierName The name of classifier (if toPreserveRunNumber is true, should be simply the id of the previous run)
	 * @param classifierFile The file with saved classifier. 
	 * Note that the application parameters are usually not saved along the classifier in the file and should be set separately!
	 * @param classifierType Can be 'Weka' or 'Lexicon', depending on the type of classifier. This defines how the given file is to be read.
	 * @param classifierApplicationParams Parameters for application of the loaded classifier.
	 * @param toPreserveRunNumber Whether to save the evaluation results with the same experiment run number as was the loaded classifier. 
	 * If true - the classifier name should be the run id to save.
	 * @throws Exception
	 */
	public static void runSavedClassifierOverTest(String[] testDataNames, 
			String classifierName, String classifierFile, String classifierType,  
			ClassifierApplicationParameters classifierApplicationParams, boolean toPreserveRunNumber) throws Exception
	{
		if (toPreserveRunNumber)
			ExperimentsResultsSaver.currentRunNum = Integer.parseInt(classifierName);
		else 
			ExperimentsResultsSaver.loadAndUpdateEvaluationNumber();
		
		AllParametersToEvaluateGiven fullLearningParams = new AllParametersToEvaluateGiven();
		
		fullLearningParams.evaluatedClassifierName = classifierName;
		fullLearningParams.classifierApplicationParams = classifierApplicationParams;
		
		System.out.println("Parameters set:\n " + fullLearningParams.printParametersShort());
		
		// learning part
		AllLearningParameters.currentSetup = fullLearningParams;
	
		
		WeightedClassifier resClassifier = null;
		// loading classifier from the file depending on the classifiers' type
		if (classifierType.equals("Weka")) {
			resClassifier = UsedClassifierFactory.getBuiltWekaBasedClassifierFromFile(
					classifierName, classifierFile,
					fullLearningParams.classifierApplicationParams);
		} else if (classifierType.equals("Lexicon")) {
			resClassifier = UsedClassifierFactory.getBuiltLexiconClassifierFromFile(
					classifierName, classifierFile, EmotionRecognitionParameters.defaultEmotionCategoriesType, 
					fullLearningParams.classifierApplicationParams );
		} else 
			System.out.println("classifierType is not set correctly!");
		
		if (!toPreserveRunNumber)
			System.out.println("Link from previous classifier run " + classifierName + " to the current run " + ExperimentsResultsSaver.currentRunNum);
		
		for (String testDataName : testDataNames) 
		{
			System.out.println("Test data:"  + testDataName);
			EvaluationResult evalResult = ClassifierEvaluation.evaluateClassifierOnTestData(resClassifier, testDataName);
			System.out.println(evalResult.toString());
			
			ExperimentsResultsSaver.saveEvaluationResults(testDataName, fullLearningParams, evalResult, resClassifier.getFeatureNumber());
		
			// if to save the detailed evaluation
			if (ExperimentsResultsSaver.toSaveDetailedClassifierResults)
				ClassifierEvaluation.computeAndSaveDetailedEvaluationOnTestData(resClassifier, testDataName, true, true);
		}
	}
	
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.experiments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import semisupervisedlearning.parameters.AllSemiSupervisedLearningParameters;
import utility.Pair;
import experiments.helpers.BestResultsParamsLoader;
import experiments.helpers.BestResultsParamsLoader.LogResult;
import experiments.helpers.BestResultsParamsLoader.SearchSettings;
import experiments.helpers.FindAllParametersVariations;
import experimentsavers.ExperimentsResultsSaver;
import functionality.UtilCollections;
import functionality.UtilFiles;
import functionality.UtilString;
import learners.given.ClassifierLearnerFactory.LearnerName;
import learning.parameters.LearningParametersParser;

public class TestClassifiersWithBestParametersHelper {
	
	/**
	 * 
	 * @param evaluatedLearners The list of names of classifier learners for which evaluation was performed.
	 * @param evaluatedInitialClassifiers The list of names of initial classifiers for which evaluation was performed.
	 * @param usedTestNames The names of the saved experiments, as set by ExperimentsResultsSaver.currentTestName
	 * @param fileOutputForParams The name of file for outputing found best parameters
	 * @param criteriaForBestParamsSearch The criteria for finding best parameters
	 * @param conditionOnDatabaseParams The SQL query condition for selecting the performance results from the database
	 * @throws Exception
	 */
	public static void saveBestParametersIntoFile(
			LearnerName[] evaluatedLearners, String[] evaluatedInitialClassifiers,
			String[] usedTestNames, String fileOutputForParams, 
			SearchSettings[] criteriaForBestParamsSearch,
			String conditionOnDatabaseParams) throws Exception
	{
		BestResultsParamsLoader.settingsToReturn =  criteriaForBestParamsSearch;
		BestResultsParamsLoader.curCondition = conditionOnDatabaseParams;
		
		for (String initialClassifier : evaluatedInitialClassifiers)
			for (LearnerName learnerName : evaluatedLearners)
				BestResultsParamsLoader.findSSLInOrder(fileOutputForParams, usedTestNames, learnerName.toString(), initialClassifier);
	}
	
	/**
	 * 
	 * @param parameterFilename It has the following format:  IndependentWekaMNB	F1Micro	3715	trainDataName!null	featureExtractionParams!Ngrams:-maxN-2-minO-5-noSt-1-iHash-1-tNeg-1--negLens-2-negType-3-	featureSelectionParams!PolSel-0,EmtSel-0,OccSel-1-minOcc-5	LearnerName!IndependentWekaMNB	ClassifierLearnerParams!:InRef:-No-:ApplParams:termDet=-iHash-1-tNeg-1--negLens-2-negType-3-;OutRef=-NoMLRef;-PrT-0.7;,-lT-WekaMNB-lexP--FS:-alg-no:-simUse-IncludeOut	sslInitialClassifier!OlympLex	sslFullParams!DT=SSL-Olympic-100000-Neutral;IC=OlympLex;FramWSet=mxI1;;corNeut=11;treatNeg=-negLens-2-negType-3;initClassP=termDet=-iHash-1-tNeg-1--negLens-2-negType-3-;OutRef=-NoMLRef;;;	sslUnlabeledDataName!SSL-Olympic-100000-Neutral
	 * (// the first 4 lines are parameters for the three BWV methods for 3 initial classifiers)
	 * @param differentNewUnlabeledData Specifies which new data to take as unlabeled in the semi-supervised process
	 * @param diffFeatureOccMins Sets a new minimum occurrences of the features
	 */
	public static void testClassifiersWithBestParametersOnBiggerSizeFromFile(String parameterFilename, 
			String curExperimentName, String[] testDataNames, String[] differentNewUnlabeledData, 
			int[] diffFeatureOccMins, int startInd, boolean toSaveClassifiers) throws Exception
	{
		
		ExperimentsResultsSaver.currentTestName = curExperimentName;
		
		
		List<String> lines = UtilFiles.getContentLines(parameterFilename);
		
		List<AllSemiSupervisedLearningParameters> allLearningParamsList = new ArrayList<AllSemiSupervisedLearningParameters>();
		
		for (String line : lines)
		{
			Map<String,String> params = ExtractSSLParamsEnumerationFromLine(line);
			
			AllSemiSupervisedLearningParameters fullLearningParams = (AllSemiSupervisedLearningParameters)LearningParametersParser.parseAndLoadAllLearningParameters(params);
			
			List<AllSemiSupervisedLearningParameters> learningParamsList = 
					getLearningParametersForDifferentSizesAndFeatureOccurrences(
							fullLearningParams, differentNewUnlabeledData, diffFeatureOccMins);
			allLearningParamsList.addAll(learningParamsList);
		}
		
		if (toSaveClassifiers)
			MultipleSemiSupervisedExperimentsHelper.runOverMultipleParametersOnMultipleTestsWithSaving(allLearningParamsList, testDataNames, startInd);
		else
			MultipleSemiSupervisedExperimentsHelper.runOverMultipleParametersOnMultipleTests(allLearningParamsList, testDataNames, startInd);
	}
	
	
	private static Map<String,String> ExtractSSLParamsEnumerationFromLine(String line) throws Exception {
		String[] paramsSplit = line.split("\t");
		Map<String,String> params = new HashMap<String,String>();
		for (int i = 3 ; i <= 10; ++i) // here i is the index of field in the file
		{
			Pair<String,String> pair = UtilString.parseInPair(paramsSplit[i], "!");
			params.put(pair.first, pair.second);
		}
		return params;
	}
	
	private static List<AllSemiSupervisedLearningParameters> getLearningParametersForDifferentSizesAndFeatureOccurrences(
			AllSemiSupervisedLearningParameters initialLearningParameters,
			String[] differentUnlabeledData,  
			int[] diffFeatureOccMins)
	{
		List<AllSemiSupervisedLearningParameters> resParamsList = new ArrayList<AllSemiSupervisedLearningParameters>();
		
		for (String unlabeledData : differentUnlabeledData)
			for (int occMin : diffFeatureOccMins)
			{
				AllSemiSupervisedLearningParameters curLearningParameters = (AllSemiSupervisedLearningParameters) initialLearningParameters.clone();
				
				curLearningParameters.featureExtractionParams.dataNgramExtractionParams.minTermOccurThreshold = occMin;
				curLearningParameters.featureSelectionParams.minOccurrenceThreshold = occMin;
				curLearningParameters.sslParameters.unlabeledDataName = unlabeledData;
				
				curLearningParameters.featureExtractionParams.useCache = false;
				
				resParamsList .add(curLearningParameters);
			}
		return  resParamsList ;
	}
	
	
}

/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.experiments;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

import parameters.EmotionRecognitionParameters;
import utility.Triple;
import classification.evaluation.ComplexEvaluation;
import classification.evaluation.ComplexEvaluation.EvaluationResult;
import data.DataRepositoryIndex;
import data.LabeledData;
import data.categories.CategoryProcessor.CategoryDataType;
import data.loading.DataFilesFormatReader;

/**
 * The module for running randomization significance tests on the main performance metrics.
 *
 */
public class SignificanceTestsHelper {
	
	static Random rand = new Random(new Date().getTime());
	
	public static int runsNum = 50000;
	
	
	/**
	 * Run randomization-based statistical significance tests for the main evaluation metrics.
	 * The output format: for each metric we first output the difference (first - second) and its p-value (paired, computed via randomization techniques).
	 * @param testData
	 * @param filenamePattern The pattern of the files with the detailed output of the classifier runs. 
	 * The pattern should contain the "<int>" entry, which will be replaced with the classifier run ids. 
	 * Example: "output/evaluatedLexiconsOutput/lexoutput-<int>-testdataname.txt"
	 * @param runComparisons The run ids to compare. Each entry is a pair of run ids.
	 * @throws Exception 
	 */
	public static void doSignificanceTests(String testDataName, String filenamePattern, int[][] runComparisons, CategoryDataType catDataType) throws Exception 
	{
		LabeledData labelData = DataRepositoryIndex.getLabeledDataByName(testDataName);
		Map<Integer, List<Integer>> testEmotionLabels = labelData.emotionLabels;
		
		System.out.println("Statistical comparisons with " + runsNum + " runs");
		
		int ind = -1;
		int startInd = 0;
		for (int[] runComp : runComparisons)
		{
			ind++;
			if (ind < startInd)
				continue;
			Map<Integer, List<Integer>> labels1 = DataFilesFormatReader.getLabelDataFromFileOutput(filenamePattern.replace("<int>", Integer.toString(runComp[0])));
			Map<Integer, List<Integer>> labels2 = DataFilesFormatReader.getLabelDataFromFileOutput(filenamePattern.replace("<int>", Integer.toString(runComp[1])));
			
			System.out.println("Comparison:\t" + runComp[0] + "\t" + runComp[1]);
			runStatTestOverTwoAlgorithmsResults(testEmotionLabels, labels1, labels2, catDataType);
			
		}
		
	}
	
	/**
	 * The output format: for each metric we first output the difference (first - second) and its p-value (paired, computed via randomization techniques).
	 * 
	 * @param testDataName
	 * @param filenamePattern
	 * @param mainRunIdToCompare
	 * @param otherRunIdsToCompare
	 * @param catDataType
	 * @throws Exception
	 */
	public static void doSignificanceTests(String testDataName, String filenamePattern, int mainRunIdToCompare, int[] otherRunIdsToCompare, CategoryDataType catDataType) throws Exception
	{
		int[][] runComparisons = new int[otherRunIdsToCompare.length][];
		
		for (int ind = 0; ind < otherRunIdsToCompare.length; ++ind) {
			runComparisons[ind] = new int[]{mainRunIdToCompare, otherRunIdsToCompare[ind]};
		}
		
		doSignificanceTests(testDataName, filenamePattern, runComparisons, catDataType);
	}
	
	/**
	 * Perform randomized statistical significance tests between two algorithms. 
	 * There are given 3 maps, each associating tweet ids with the emotion labels (given as category ids).
	 * The results are printed in the system output.
	 * @param correctEmotionLabels The ground-truth, correct labels for the tweets.
	 * @param labels1 The tweet labels returned by the first classifier to compare.
	 * @param labels2 The tweet labels returned by the second classifier to compare..
	 * @throws Exception
	 */
	public static void runStatTestOverTwoAlgorithmsResults(Map<Integer, List<Integer>> correctEmotionLabels, 
			Map<Integer, List<Integer>> labels1, 
			Map<Integer, List<Integer>> labels2,
			CategoryDataType catDataType) throws Exception
	{
		
		// 1) compute the observed differences
		ConsideredMetrics observedDiff = computeScoreDifference(correctEmotionLabels, labels1, labels2, catDataType);
		
		// build the faster dataset
		List<Integer> tweetIdsList = new ArrayList<Integer>();
		List<Triple<List<Integer>, List<Integer>, List<Integer>>> givenlabelLists = new ArrayList<Triple<List<Integer>, List<Integer>, List<Integer>>>();
		
		for (Integer twId : correctEmotionLabels.keySet())
		{
			tweetIdsList.add(twId);
			givenlabelLists.add(new Triple(
					new ArrayList<Integer>(new HashSet<Integer>(correctEmotionLabels.get(twId))), // to ensure there are no repeated labels
					labels1.get(twId), 
					labels2.get(twId)));
		}
		
		
		// define number of randomized runs
		List<ConsideredMetrics> recordedDifferences = new ArrayList<ConsideredMetrics>();
		Date startTime = new Date();
		for (int runInd = 0; runInd < runsNum; ++runInd)
		{	
			// 3) compute the difference of permutation
			ConsideredMetrics permDiff = evaluateTwoRunsWithRandomization(givenlabelLists);
			recordedDifferences.add(permDiff);
		}
		Date endTime = new Date();
		System.out.println("Randomization done in " + (endTime.getTime() - startTime.getTime()) / 1000.0 + " seconds");
		
		// 4) compute p-value of observed difference (2-tailed)
		ConsideredMetrics pValueOfMetrics = new ConsideredMetrics();
		
		List<Double> curMetricList = getValuesListFromClassList( recordedDifferences, "macroPrecision");
		pValueOfMetrics.macroPrecision = computePValue2Tails(curMetricList,  observedDiff.macroPrecision);
		
		curMetricList = getValuesListFromClassList( recordedDifferences, "macroRecall");
		pValueOfMetrics.macroRecall = computePValue2Tails(curMetricList,  observedDiff.macroRecall);
		
		curMetricList = getValuesListFromClassList( recordedDifferences, "macroF1");
		pValueOfMetrics.macroF1 = computePValue2Tails(curMetricList,  observedDiff.macroF1);
		
		curMetricList = getValuesListFromClassList( recordedDifferences, "accuracy");
		pValueOfMetrics.accuracy = computePValue2Tails(curMetricList,  observedDiff.accuracy);
		
		curMetricList = getValuesListFromClassList( recordedDifferences, "microPrecision");
		pValueOfMetrics.microPrecision = computePValue2Tails(curMetricList,  observedDiff.microPrecision);
		
		curMetricList = getValuesListFromClassList( recordedDifferences, "microRecall");
		pValueOfMetrics.microRecall = computePValue2Tails(curMetricList,  observedDiff.microRecall);
		
		curMetricList = getValuesListFromClassList( recordedDifferences, "microF1");
		pValueOfMetrics.microF1 = computePValue2Tails(curMetricList,  observedDiff.microF1);
		
		// 5) print out the p-values
		System.out.print("macroP:\t" + observedDiff.macroPrecision + "\t" + pValueOfMetrics.macroPrecision);
		System.out.print("\t");
		System.out.print("macroR:\t" + observedDiff.macroRecall + "\t" + pValueOfMetrics.macroRecall);
		System.out.print("\t");
		System.out.print("macroF1:\t" + observedDiff.macroF1 + "\t" +  pValueOfMetrics.macroF1);
		System.out.print("\t");
		System.out.print("A:\t" + observedDiff.accuracy + "\t" + pValueOfMetrics.accuracy);
		System.out.print("\t");
		System.out.print("microP:\t" + observedDiff.microPrecision + "\t" + pValueOfMetrics.microPrecision);
		System.out.print("\t");
		System.out.print("microR:\t" + observedDiff.microRecall + "\t" + pValueOfMetrics.microRecall);
		System.out.print("\t");
		System.out.print("microF1:\t" + observedDiff.microF1 + "\t" + pValueOfMetrics.microF1);
		System.out.print("\t");	
		System.out.println();
	}
	
	static class ConsideredMetrics
	{
		public double macroPrecision;
		public double macroRecall;
		public double macroF1;
		public double accuracy;
		public double microPrecision;
		public double microRecall;
		public double microF1;
	}
	
	private static ConsideredMetrics computeScoreDifference(
			Map<Integer, List<Integer>> correctEmotionLabels, 
			Map<Integer, List<Integer>> labels1, 
			Map<Integer, List<Integer>> labels2,
			CategoryDataType catDataType) throws Exception
	{
		EvaluationResult evalRes1 = ComplexEvaluation.getEvaluationTotalOnFoundLabelsSimplified(correctEmotionLabels, labels1, catDataType);
		EvaluationResult evalRes2 = ComplexEvaluation.getEvaluationTotalOnFoundLabelsSimplified(correctEmotionLabels, labels2, catDataType);
	
		ConsideredMetrics foundDifference = new ConsideredMetrics();
		foundDifference.accuracy = evalRes1.emotionAccuracy - evalRes2.emotionAccuracy;
		foundDifference.macroF1 = evalRes1.emotionMacroF1Score - evalRes2.emotionMacroF1Score;
		foundDifference.macroPrecision = evalRes1.emotionMacroPrecision - evalRes2.emotionMacroPrecision;
		foundDifference.macroRecall = evalRes1.emotionMacroRecall - evalRes2.emotionMacroRecall;
		foundDifference.microF1 = evalRes1.emotionMicroF1Score - evalRes2.emotionMicroF1Score;
		foundDifference.microPrecision = evalRes1.emotionMicroPrecision - evalRes2.emotionMicroPrecision;
		foundDifference.microRecall = evalRes1.emotionMicroRecall - evalRes2.emotionMicroRecall;
		
		return foundDifference;
	}
	
	static CategoryDataType usedCatDataType = EmotionRecognitionParameters.defaultEmotionCategoriesType;
	
	static EvaluationResult result1 = new EvaluationResult(usedCatDataType);
	static EvaluationResult result2 = new EvaluationResult(usedCatDataType);
	
	private static  ConsideredMetrics evaluateTwoRunsWithRandomization(List<Triple<List<Integer>, List<Integer>, List<Integer>>> givenlabelLists) throws Exception
	{
		result1.clearSimplified();
		result2.clearSimplified();
		
		for (Triple<List<Integer>, List<Integer>, List<Integer>> oneEntry : givenlabelLists) 
		{
			if (oneEntry.first != null)// we evaluate the results only for those tweets were we know the ground truth
			{
				if (rand.nextDouble() <= 0.5)
				{
					ComplexEvaluation.getEvaluationOverSimplified(oneEntry.second, oneEntry.first, usedCatDataType, result1);
					ComplexEvaluation.getEvaluationOverSimplified(oneEntry.third, oneEntry.first, usedCatDataType, result2);
				}
				else
				{
					ComplexEvaluation.getEvaluationOverSimplified(oneEntry.second, oneEntry.first, usedCatDataType, result2);
					ComplexEvaluation.getEvaluationOverSimplified(oneEntry.third, oneEntry.first, usedCatDataType, result1);
				}
				
			}
		}
		
		result1.normalizeSimplified();
		result2.normalizeSimplified();
		
		return getMetricComparison (result1, result2);
	}
	
	private static  ConsideredMetrics getMetricComparison(EvaluationResult evalRes1, EvaluationResult evalRes2)
	{
		ConsideredMetrics foundDifference = new ConsideredMetrics();
		foundDifference.accuracy = evalRes1.emotionAccuracy - evalRes2.emotionAccuracy;
		foundDifference.macroF1 = evalRes1.emotionMacroF1Score - evalRes2.emotionMacroF1Score;
		foundDifference.macroPrecision = evalRes1.emotionMacroPrecision - evalRes2.emotionMacroPrecision;
		foundDifference.macroRecall = evalRes1.emotionMacroRecall - evalRes2.emotionMacroRecall;
		foundDifference.microF1 = evalRes1.emotionMicroF1Score - evalRes2.emotionMicroF1Score;
		foundDifference.microPrecision = evalRes1.emotionMicroPrecision - evalRes2.emotionMicroPrecision;
		foundDifference.microRecall = evalRes1.emotionMicroRecall - evalRes2.emotionMicroRecall;
		
		return foundDifference;
	}
	
	private static double computePValue2Tails(List<Double> statisticsValues, double observedValue)
	{
		double absObservedValue = Math.abs(observedValue);
		
		int highStatValuesNum = 0; 
		for (Double statV : statisticsValues)
		{
			if (Math.abs(statV) >= absObservedValue)
				highStatValuesNum++;
		}
		return highStatValuesNum / (1.0 *statisticsValues.size());
	}
	
	private static List<Double> getValuesListFromClassList(List<ConsideredMetrics> valuesList, String metricName) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
	{
		List<Double> metricValues = new ArrayList<Double>();
		double score;
		
		for (ConsideredMetrics valueRes : valuesList)
		{
			score = valueRes.getClass().getField(metricName).getDouble( valueRes);
			metricValues.add(score);
		}
		
		return metricValues;
	}
}

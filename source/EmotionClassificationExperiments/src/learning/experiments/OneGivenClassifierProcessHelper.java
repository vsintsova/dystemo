/*
    Copyright 2016 Valentina Sintsova
    
    This file is part of Dystemo.

    Dystemo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or 
    (at your option) any later version.

    Dystemo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Dystemo.  If not, see <http://www.gnu.org/licenses/>.
*/

package learning.experiments;

import learning.evaluation.ClassifierEvaluation;
import learning.parameters.AllLearningParameters;
import learning.parameters.AllParametersToEvaluateGiven;
import learning.parameters.ClassifierApplicationParameters;
import classification.definitions.WeightedClassifier;
import classification.evaluation.ComplexEvaluation.EvaluationResult;
import classifiers.initialization.UsedClassifierFactory;
import experimentsavers.ExperimentsResultsSaver;

/**
 * Evaluate the given classifiers (one of specified in UsedClassifierFactory) over different test data.
 *
 */
public class OneGivenClassifierProcessHelper {
	
	/**
	 * The classifier will be tested with the default classification application parameters, 
	  as specified by ClassifierApplicationParameters getDefaultClassifierApplicationParams()
	 * @param testDataNameInIndex
	 * @param givenClassifierName
	 * @throws Exception
	 */
	public static void runOverTest(String testDataNameInIndex, String givenClassifierName) throws Exception
	{
		runOverTest(testDataNameInIndex, givenClassifierName, 
				ClassifierApplicationParameters.getDefaultClassifierApplicationParams());
	}
	
	/**
	 * The classifier name should be the one available in UsedClassifierFactory.
	 * The specified application parameters will be applied.
	 * @param testDataNameInIndex
	 * @param givenClassifierName
	 * @param classifierApplicationParams
	 * @throws Exception
	 */
	public static void runOverTest(String testDataNameInIndex, String givenClassifierName, 
			ClassifierApplicationParameters classifierApplicationParams) throws Exception
	{
		ExperimentsResultsSaver.loadAndUpdateEvaluationNumber();
		
		AllParametersToEvaluateGiven fullLearningParams = new AllParametersToEvaluateGiven();
		
		if (givenClassifierName != null) {
			fullLearningParams.evaluatedClassifierName = givenClassifierName;
		} else {
			throw new Exception("The name of the given classifier is not provided!");
		}
				
		
		fullLearningParams.classifierApplicationParams = classifierApplicationParams;
		
		System.out.println("Parameters set:\n " + fullLearningParams.printParametersShort());
		System.out.println("Test data:"  + testDataNameInIndex);
		
		// learning part
		AllLearningParameters.currentSetup = fullLearningParams;
	
		
		WeightedClassifier resClassifier = UsedClassifierFactory.getClassifierByName(
				fullLearningParams.evaluatedClassifierName,
				fullLearningParams.classifierApplicationParams);
		
		EvaluationResult evalResult = ClassifierEvaluation.evaluateClassifierOnTestData(resClassifier, testDataNameInIndex);
		System.out.println(evalResult.toString());
		
		ExperimentsResultsSaver.saveEvaluationResults(testDataNameInIndex, fullLearningParams, evalResult, resClassifier.getFeatureNumber());
		
		// if to save the detailed evaluation
		if (ExperimentsResultsSaver.toSaveDetailedClassifierResults)
			ClassifierEvaluation.computeAndSaveDetailedEvaluationOnTestData(resClassifier, testDataNameInIndex, true, true);
	}
}
